# Planet React

Source code repository for planet-react.org


## Prerequisites

Before setting up the project, you need to verify that the following software are installed:

* GIT
* MySQL
* PHP 7
* Node 6
* Yarn or NPM
* PHP Composer
* PHPUnit
* An Auth0 account (for authentication)

Replace `{PLANET_INSTALL}` with your installation location (no brackets)


## Installing

1) Clone the repo and cd:
> git clone git@gitlab.com:planet-react/planet-react.org.git && cd planet-react.org

2) Install the PHP dependencies:
> composer install

3) Install the Node dependencies:
> yarn

4) Create the database schema
> create database `planet_react`

5) Create the env file
> cp .env.example .env

6) Open the `.env` file and update the database (`DB_*`) environment variables.

7) Generate the Laravel application key:
> php artisan key:generate

8) Run the database migration:
> php artisan migrate

Additionally create a `test_planet_react` database schema for unit testing

> create database test_planet_react

You should now be able to start the dev server by typing `yarn planet:start [-l]`

Open the browser and navigate to [http://localhost:3000](http://localhost:3000)


## Tests

In order to run the tests, PHPUnit and Jest must be installed on your system.
Please referrer the documentation for how to install.

The php tests can be found in the `{PLANET_INSTALL}/tests` directory.

React tests can be found side by side to the file that is tested. Uses standard Jest test file name conventions. Eg.: `{FILE}.test.js`

Run the app tests:

> phpunit

Run the react-app test:

> yarn test


## Deployment

TBD

[Deployer.php](https://deployer.org/) - Deployment tool

## Stack

* [Laravel](http://laravel.com/) - Open source PHP Web framework
* [React](https://facebook.github.io/react/) - Open source JavaScript library for building user interfaces

See `composer.json` and `package.json` for details.


## Configuration

TBD


## Thanks

Thanks to [JetBrains](https://www.jetbrains.com/) for providing the project open source license of their IDEs
