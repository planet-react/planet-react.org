<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\PlanetReact\Cli\AggregateCommand::class,
        \App\PlanetReact\Cli\DatabaseBackupCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $aggregateCron = config( 'planet.aggregator.cron' );
        $dbBackupCron  = config( 'planet.db_backup.cron' );

        $schedule->command( 'planet:aggregate' )->cron( $aggregateCron );
        $schedule->command( 'planet:db-backup' )->cron( $dbBackupCron );

        // @todo: Commented for now as security-check is not compatible with Laravel 5.5
        //$schedule->command( \Jorijn\LaravelSecurityChecker\Console\SecurityMailCommand::class )
        //        ->weekly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
