<?php
namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SetLocaleMiddleware {

    public function handle( $request, Closure $next ) {
        $language = $request->input( 'language' ) ? $request->input( 'language' ) : 'en';
        if ( $language && $language != $request->cookie( 'language' ) ) {
            $response = $next( $request );
            Carbon::setLocale( $language );
            return $response->withCookie( cookie()->forever( 'language', $language ) );
        }

        Carbon::setLocale( $language );

        return $next( $request );
    }
}