<?php

namespace App\PlanetReact\Aggregator;

use App\PlanetReact\Aggregator\Filter\PostFilter;
use App\PlanetReact\Domain\Feed\Fetcher\Fetcher;
use App\PlanetReact\Domain\Feed\Fetcher\Rss\RssFetcher;
use App\PlanetReact\Domain\Feed\Fetcher\YouTube\YouTubeChannelFetcher;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Services\YouTube\Api\YouTubeApi;
use File;
use Illuminate\Support\Collection;
use PicoFeed\Parser\Feed as PicoFeed;
use PicoFeed\Reader\Reader;

class Aggregator {

    /**
     * @var Collection
     */
    protected $feeds = [];

    /**
     * Only grab posts that has the categories.
     * @var array
     */
    protected $allowedCategories;

    /**
     * @var string
     */
    protected $lockFile;

    /**
     * @var Fetcher
     */
    protected $fetcher;

    /**
     * Maximum posts to save per Feed
     * @var int
     */
    protected $maxPostsPerFeed;

    /**
     * Posts that was saved.
     * @var Collection
     */
    protected $savedPosts;

    /**
     * Aggregator constructor.
     * @param Collection $feeds
     * @param string $lockFile
     */
    public function __construct( $feeds, $lockFile ) {
        $this->feeds             = $feeds;
        $this->maxPostsPerFeed   = config( 'planet.aggregator.max_posts_per_feed' );
        $this->allowedCategories = $this->parseAllowedCategories();
        $this->savedPosts        = new Collection();
        $this->lockFile          = $lockFile;

        $this->fetcher = new Fetcher();
        $this->fetcher->register( new RssFetcher( new Reader ) );
        $this->fetcher->register( new YouTubeChannelFetcher( new YouTubeApi() ) );
    }

    /**
     * @return AggregatorResult
     */
    public function aggregate() {
        if ( $this->isLocked() ) {
            $this->log( 'Lock file `' . $this->lockFile . '` exists' );
            return null;
        }

        $this->lock();

        $progressCount      = 0;
        $failedFeedIds      = collect();
        $feedsToFetchCount  = $this->feeds->count();
        $totalExecutionTime = 0;

        foreach ( $this->feeds as $feed ) {
            $progressCount++;

            $count     = 0;
            $failed    = false;
            $timeStart = microtime( true );

            $this->log( "Fetching feed {$progressCount}/{$feedsToFetchCount}: #{$feed->id}, {$feed->name}, {$feed->feed_url}" );

            try {
                $picoFeed = $this->fetcher->fetch( $feed );
                $posts    = $this->processFeed( $picoFeed, $feed->id );

                $count = $this->savePosts( $posts );

            } catch ( \Exception $exception ) {
                $failed = true;
                $this->log( "Failed storing posts from {$feed->name} - {$exception->getTraceAsString()}" );

                $failedFeedIds->push( $feed->id );
            }

            $timeEnd       = microtime( true );
            $executionTime = ( $timeEnd - $timeStart );

            $feed->last_fetched_at = date( 'Y-m-d H:i:s' );
            $feed->execution_time  = $executionTime;
            $feed->failed          = $failed ? 1 : 0;
            $feed->save();

            $this->log( "Posts saved: {$count}, Time: {$executionTime} sec" );
            if ( $progressCount < $feedsToFetchCount ) {
                $this->log( "----------------------------------------------------------------------------" );
            }

            $totalExecutionTime += $executionTime;

            // Make it less aggressive.
            sleep( 2 );
        }

        $result = new AggregatorResult();
        $result->setSavedPosts( $this->savedPosts );
        $result->setFailedIds( $failedFeedIds );
        $result->setTotalExecutionTime( $totalExecutionTime );

        $this->unLock();

        return $result;
    }


    /**
     * @param PicoFeed $picoFeed
     * @param int $feedId
     * @return Collection
     */
    protected function processFeed( $picoFeed, $feedId ) {

        $items = $picoFeed->getItems();
        $this->sortFeedItemsByPubdateDesc( $items );

        $posts = [];

        foreach ( $items as $item ) {
            $post = new Post();

            $post->feed_id    = $feedId;
            $post->fetched_at = date( 'Y-m-d H:i:s' );
            $post->pubdate    = $item->getPublishedDate()->format( 'Y-m-d H:i:s' );;
            $post->title     = $item->getTitle();
            $post->content   = $item->getContent() ? $item->getContent() : '';
            $post->link      = $item->getUrl();
            $post->enclosure = $item->getEnclosureUrl();
            $post->guid      = $item->getTag( 'guid' )
                    ? $item->getTag( 'guid' )[0]
                    : $item->getId();

            $categories = $item->getTag( 'category' )
                    ? $categories = $item->getTag( 'category' )
                    : $item->getCategories();

            if ( $categories && $this->hasReactCategory( $categories ) ) {
                foreach ( $categories as $category ) {
                    $post->categories[] = $category;
                }
            }
            else {
                // If there are no categories, try scan the post's title
                // and content for relevant topics.
                $title   = strtolower( $item->getTitle() );
                $content = strtolower( $item->getContent() );
                foreach ( $this->allowedCategories as $category ) {

                    $cat = " {$category} ";
                    if ( str_contains( $title, $cat ) || str_contains( $content, $cat ) ) {
                        $post->categories[] = $category;
                    }
                }
            }

            $post->authors = ! empty( $item->getAuthor() ) ? $item->getAuthor() : '';

            $posts[] = $post;
        }

        $filter = new PostFilter( $posts, $this->maxPostsPerFeed );

        return $filter->filter();

    }

    protected function savePosts( $posts ) {
        $savedCount = 0;
        foreach ( $posts as $post ) {
            if ( $post->save() ) {
                $this->createTagsForPost( $post );
                $this->savedPosts->push( $post );
                $savedCount++;
            }
        }
        return $savedCount;
    }

    protected function createTagsForPost( Post $post ) {
        // @todo: refactor: merge categories and title words
        // @todo: refactor: create helper function: 'should discard' and 'has category'

        $categories    = explode( ',', config( 'planet.aggregator.categories' ) );
        $tagsToDiscard = explode( ',', config( 'planet.aggregator.tags_to_discard' ) );

        if ( property_exists( $post, 'categories' ) ) {
            foreach ( $post->categories as $category ) {
                if ( ! in_array( strtolower( $category ), $tagsToDiscard ) ) {
                    $post->tag( $category );
                }
            }
        }

        $titleWords = explode( ' ', $post->title );
        foreach ( $titleWords as $word ) {
            if ( in_array( strtolower( $word ), $categories ) ) {
                if ( ! in_array( strtolower( $word ), $tagsToDiscard ) ) {
                    $post->tag( $word );
                }
            }
        }
    }

    /**
     * @param array $categories
     * @return boolean
     */
    private function hasReactCategory( $categories ) {
        if ( ! $categories ) {
            return false;
        }
        foreach ( $categories as $category ) {
            if ( in_array( strtolower( $category ), $this->allowedCategories ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $items \PicoFeed\Parser\Item[]
     * @todo: test
     */
    private function sortFeedItemsByPubdateDesc( $items ) {
        uasort( $items, function ( $a, $b ) {
            return strcmp( $b->getPublishedDate()->format( 'Y-m-d H:i:s' ),
                    $a->getPublishedDate()->format( 'Y-m-d H:i:s' ) );
        } );
    }

    /**
     * @return array
     */
    public function parseAllowedCategories() {
        $categories = config( 'planet.aggregator.categories' );
        return array_map( 'trim', explode( ',', $categories ) );
    }

    /**
     * @param string $msg
     */
    private function log( $msg ) {
        if ( php_sapi_name() == 'cli' ) {
            echo "{$msg}\n";
        }
    }

    private function lock() {
        File::put( $this->lockFile, '1' );
    }

    private function isLocked() {
        return File::exists( $this->lockFile );
    }

    private function unLock() {
        if ( File::exists( $this->lockFile ) ) {
            File::delete( $this->lockFile );
        }
    }
}
