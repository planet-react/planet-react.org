<?php

namespace App\PlanetReact\Aggregator;

class AggregatorResult {

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $postSaved;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $failedIds;

    /**
     * @var int
     */
    protected $totalExecutionTime;

    public function getSavedPosts() {
        return $this->postSaved;
    }

    public function setSavedPosts( $posts ) {
        $this->postSaved = $posts;
    }

    public function getFailedIds() {
        return $this->failedIds;
    }

    public function setFailedIds( $ids ) {
        $this->failedIds = $ids;
    }

    public function getTotalExecutionTime() {
        return $this->totalExecutionTime;
    }

    public function setTotalExecutionTime( $totalExecutionTime ) {
        $this->totalExecutionTime = $totalExecutionTime;
    }

}