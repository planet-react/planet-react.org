<?php

namespace App\PlanetReact\Aggregator\Filter;

use App\PlanetReact\Aggregator\Util\MediumUrl;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Report\ReportService;
use Cache;

class PostFilter {

    /**
     * @var ReportService
     */
    protected $reportService;
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $posts;

    /**
     * Take only the fist `$maxCount` posts.
     * @var int
     */
    protected $maxCount;

    /**
     * PostFilter constructor.
     * @param array $posts
     * @param int $maxCount
     */
    public function __construct( $posts, $maxCount = 10 ) {
        $this->reportService = new ReportService;
        $this->posts         = collect( $posts );
        $this->maxCount      = $maxCount;
    }

    /**
     * Returns posts filtered.
     */
    public function filter() {

        return $this->posts
                // Only take the last `count` posts.
                ->take( $this->maxCount )
                // Reject the post if it has been stored.
                ->reject( function ( Post $post ) {
                    return $post->alreadyExists();
                } )
                // Reject post if it has been reported and dealt with.
                ->reject( function ( Post $post ) {
                    return $this->reportService->postHasBeenReportedAndDealtWith( $post->link );
                } )
                // Reject post if the pubdate is in the future.
                ->reject( function ( Post $post ) {
                    $pubDate = new \DateTime( $post->pubdate );
                    $now     = new \DateTime();
                    return $pubDate > $now;
                } )
                // Reject post if there are no categories.
                ->reject( function ( Post $post ) {
                    return empty( $post->categories );
                } )
                // Reject post if it is a response / comment.
                // Eg. medium feeds often contains comments.
                ->reject( function ( Post $post ) {
                    $host = parse_url( $post->link )['host'];

                    $isMedium = str_contains( $host, [ 'medium.com', 'hackernoon.com' ] );
                    if ( ! $isMedium ) {
                        return false;
                    }

                    // Reject if the post is a response / comment.
                    try {
                        return $this->isMediumComment( $post->link );
                    } catch ( \Exception $exception ) {
                        return false;
                    }
                } );
    }

    public function isMediumComment( $url ) {
        $mediumUrl = new MediumUrl( $url );

        $parsed    = $mediumUrl->parse();
        $authorUrl = $parsed['author_url'];
        $postId    = $parsed['post_id'];

        $cacheKey = 'aggregator_medium_json_' . $authorUrl;

        $payload = Cache::remember( $cacheKey, 5, function () use ( $authorUrl ) {
            return $this->getJsonPayloadFromMedium( $authorUrl . '/responses' );
        } );

        return $payload ? property_exists( $payload->references->Post, $postId ) : false;
    }

    public function getJsonPayloadFromMedium( $url ) {
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request( 'GET', $url . '?format=json' );
        } catch ( \Exception $exception ) {
            return false;
        }

        if ( $response->getStatusCode() != 200 ) {
            return false;
        }

        $body = json_decode( str_replace( '])}while(1);</x>', '', $response->getBody() ) );

        if ( ! isset( $body->success, $body->payload ) ) {
            return false;
        }

        return $body->payload;
    }
}
