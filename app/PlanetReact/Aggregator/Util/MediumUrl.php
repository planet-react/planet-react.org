<?php

namespace App\PlanetReact\Aggregator\Util;

class MediumUrl {

    protected $url;

    public function __construct( $url ) {
        $this->url = $url;
    }

    public function parse() {
        return [
                'url'        => $this->url,
                'author_url' => $this->getAuthorUrlFromPostUrl(),
                'user_id'    => $this->getUserId(),
                'post_id'    => $this->getIdFromPostUrl(),
        ];
    }

    private function getAuthorUrlFromPostUrl() {
        $pattern = "/(^https?:\/\/(medium|hackernoon)\.com\/@.+)\//";
        preg_match( $pattern, $this->url, $matches );
        if ( ! ( $matches || $matches[1] ) ) {
            return false;
        }
        return $matches[1];
    }

    private function getIdFromPostUrl() {
        $path  = parse_url( $this->url )['path'];
        $parts = explode( '-', $path );
        return end( $parts );
    }

    private function getUserId() {
        $parts = parse_url( $this->url );

        parse_str( $parts['query'], $query );

        preg_match( "/rss-(.\w+)/", $query['source'], $matches );

        if ( ! ( $matches || $matches[1] ) ) {
            return false;
        }
        return $matches[1];
    }

}