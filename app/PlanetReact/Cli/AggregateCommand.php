<?php

namespace App\PlanetReact\Cli;

use App\PlanetReact\Aggregator\Aggregator;
use App\PlanetReact\Aggregator\AggregatorResult;
use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Events\AggregateCommandCompleteEvent;
use Illuminate\Console\Command;

class AggregateCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planet:aggregate {feedIds?} {--skipEvent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggregates posts from feeds';

    /**
     * Create a new command instance.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $feedIds      = $this->parseFeedIdsArg( $this->argument( 'feedIds' ) );
        $feedsToFetch = $feedIds->count() > 0
                ? Feed::find( $feedIds->all() )
                : Feed::all();

        $aggregator = new Aggregator( $feedsToFetch, config( 'planet.aggregator.lock_file' ) );

        $result = $aggregator->aggregate();

        if ( ! $result ) {
            return;
        }

        $this->printTotals( $result );

        if ( ! $this->option( 'skipEvent' ) ) {
            event( new AggregateCommandCompleteEvent( $result->getSavedPosts() ) );
        }

        return;
    }

    /**
     * @param AggregatorResult $result
     */
    private function printTotals( $result ) {
        $failedCount = $result->getFailedIds()->count();
        $totalText   = "Saved: {$result->getSavedPosts()->count()}, Failed: {$failedCount}, Total Time: {$result->getTotalExecutionTime()}";
        $divider     = str_repeat( "=", strlen( $totalText ) );

        $this->log( $divider );
        $this->log( $totalText );
        $this->log( $divider );
    }

    /**
     * @param string $feedIds
     * @return \Illuminate\Support\Collection
     */
    public function parseFeedIdsArg( $feedIds ) {
        $ids = explode( ',', trim( $feedIds ) );
        return collect( $ids )->reject( function ( $id ) {
            return empty( $id );
        } );
    }

    /**
     * @return array
     */
    public function parseAllowedCategories() {
        $categoryFilter = explode( ',', config( 'planet.aggregator.categories' ) );
        return array_filter( $categoryFilter, function ( $category ) {
            return trim( $category );
        } );
    }

    /**
     * @param string $msg
     */
    protected function log( $msg ) {
        if ( php_sapi_name() == 'cli' ) {
            echo "${msg}\n";
        }
    }

}