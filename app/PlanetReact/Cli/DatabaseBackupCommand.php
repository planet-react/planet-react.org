<?php

namespace App\PlanetReact\Cli;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Storage;

class DatabaseBackupCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planet:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backs up the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if ( ! config( 'planet.db_backup.active' ) ) {
            $this->info( "PLANET_DB_BACKUP is not set or `false`. Exit." );
            return;
        }

        $backupDir = $this->createLocalBackupDir();
        $fileName  = $this->createFileName( time() );
        $localPath = $backupDir . '/' . $fileName;

        $this->info( "Creating backup {$localPath}" );
        $this->dumpDatabase( $localPath );

        $this->info( "Saving to Google Drive" );
        Storage::disk( 'google' )->put( $fileName, file_get_contents( $localPath ) );

        $this->info( 'Delete remote files' );
        $remoteFiles = Storage::disk( 'google' )->listContents();
        $this->deleteOldFiles( Storage::disk( 'google' ), $remoteFiles );

        $this->info( 'Delete local files' );
        $localFiles = Storage::disk( 'local' )->listContents( 'db-backup' );
        $this->deleteOldFiles( Storage::disk( 'local' ), $localFiles );

        $this->info( "Done" );
    }

    private function dumpDatabase( $localPath ) {
        $username = escapeshellarg( config( 'database.connections.mysql.username' ) );
        $password = escapeshellarg( config( 'database.connections.mysql.password' ) );
        exec( "mysqldump -u {$username} -p{$password} planet_react > {$localPath}" );
    }

    private function deleteOldFiles( $storage, $files ) {
        if ( ! config( 'planet.db_backup.delete_older_than' ) ) {
            return;
        }

        // Make sure the time is always negative.
        $time = trim( config( 'planet.db_backup.delete_older_than' ), ' -+' );
        $time = strtotime( "-{$time}" );

        $hostName = $this->getHostName();

        foreach ( $files as $file ) {
            // Skip files not created by this command.
            if ( ! str_contains( $file['filename'], "{$hostName}-dump" ) ) {
                continue;
            }

            if ( $file['timestamp'] <= $time ) {
                $storage->delete( $file['path'] );
            }
        }
    }

    private function createFileName( $timeStamp ) {
        $host = $this->getHostName();
        return "{$host}-dump-{$timeStamp}.sql";
    }

    private function getHostName() {
        return parse_url( config( 'app.url' ) )['host'];
    }

    private function createLocalBackupDir() {
        $path = storage_path( 'app' ) . '/db-backup';
        if ( ! File::isDirectory( $path ) ) {
            File::makeDirectory( $path );
        }
        return $path;
    }

}
