<?php

namespace App\PlanetReact\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class BaseModel extends Model {
    use SoftDeletes;

    protected $rules = [];

    protected $errors;

    public function validate() {
        $validator = Validator::make( $this->toArray(), $this->rules );
        if ( $validator->fails() ) {
            $this->errors = $validator->errors();
            return false;
        }
        return true;
    }

    public function errors() {
        return $this->errors;
    }

}
