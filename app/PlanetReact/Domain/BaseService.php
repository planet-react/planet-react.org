<?php

namespace App\PlanetReact\Domain;

class BaseService {

    /**
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return bool
     * @throws \Exception
     */
    protected function handleLogin( $auth0User ) {
        if ( ! $auth0User ) {
            throw new \Exception( 'User is not logged in' );
        }
        return true;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return bool
     * @throws \Exception
     */
    protected function handleNotModelOwner( $model, $auth0User ) {
        $this->handleLogin( $auth0User );

        if ( ! $model ) {
            throw new \Exception( 'Model does not exists' );
        }

        if ( $model->user_id != $auth0User->getUserId() ) {
            throw new \Exception( 'User is not the owner' );
        }

        return true;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     * @throws \Exception
     */
    public function saveModel( $model ) {
        if ( ! $model->save() ) {
            throw new \Exception( 'Model was not saved' );
        }

        return true;
    }

}