<?php

namespace App\PlanetReact\Domain\Bookmark;

use App\PlanetReact\Domain\BaseModel;
use Conner\Tagging\Taggable;

class Bookmark extends BaseModel {
    use Taggable;
}
