<?php

namespace App\PlanetReact\Domain\Bookmark;

use App\PlanetReact\Domain\BaseService;

class BookmarkService extends BaseService {

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Bookmark|BookmarkService|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function toggleSave( $args, $auth0User ) {
        $this->handleLogin( $auth0User );

        $delete = key_exists( 'id', $args ) && $args['id'] != null;

        if ( $delete ) {
            $bookmark = Bookmark::find( $args['id'] );
            // Throw an in case the user does not own the bookmark.
            $this->handleNotModelOwner( $bookmark, $auth0User );

            if ( $bookmark ) {
                $bookmark->forceDelete();
            }

            return null;
        }
        else {
            return $this->save( $args, $auth0User );
        }
    }

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Bookmark|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \Exception
     */
    public function save( $args, $auth0User ) {
        $this->handleLogin( $auth0User );

        $isUpdate = key_exists( 'id', $args ) && $args['id'] != null;

        // Load existing Model or create a new model.
        $bookmark = $isUpdate ?
                Bookmark::find( $args['id'] ) : new Bookmark;

        // Throw an error if the user does not own the bookmark.
        if ( $isUpdate ) {
            $this->handleNotModelOwner( $bookmark, $auth0User );
        }

        // Update model
        $bookmark->user_id = $auth0User->getUserId();
        $bookmark->url     = trim( $args['url'] );
        $bookmark->title   = trim( $args['title'] );
        if ( key_exists( 'notes', $args ) ) {
            $bookmark->notes = trim( $args['notes'] );
        }

        // Save
        $saved = $bookmark->save();
        if ( ! $saved ) {
            throw new \Exception( 'Model not updated' );
        }

        // Add tags
        if ( key_exists( 'tags', $args ) ) {
            $this->tag( $bookmark, $args['tags'] );
        }

        return $bookmark;
    }

    /**
     * @param $auth0User
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @param int $bookmarkId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getBookmarkForUser( $auth0User, $bookmarkId ) {
        $bookmark = Bookmark::find( $bookmarkId );

        $this->handleNotModelOwner( $bookmark, $auth0User );

        return $bookmark;
    }

    /**
     * @param int $user_id
     * @param string|null $search
     * @return $this
     */
    public function getBookmarksForUser( $user_id, $search = null ) {
        $isTagSearch = $search != null && strlen( $search ) >= 2 && str_contains( $search, '_tag:' );

        $bookmarks = Bookmark::where( 'user_id', '=', $user_id );

        if ( $isTagSearch ) {
            $searchTerm = explode( ':', $search );
            $bookmarks->withAnyTag( [ trim( $searchTerm[1] ) ] );
        }
        else {
            if ( $search != null ) {
                $bookmarks->where( function ( $query ) use ( $search ) {
                    $query->where( 'title', 'like', '%' . $search . '%' )->orWhere( 'notes', 'like', '%' . $search . '%' );
                } );
            }
        }

        $bookmarks->orderBy( 'created_at', 'desc' );

        return $bookmarks;
    }

    /**
     * @param $bookmarkId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return bool|null
     */
    public function forceDestroy( $bookmarkId, $auth0User ) {
        return $this->destroy( $bookmarkId, $auth0User, true );
    }

    /**
     * @param $bookmarkId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @param bool $force
     * @return bool|null
     */
    public function destroy( $bookmarkId, $auth0User, $force = false ) {
        $bookmark = Bookmark::find( $bookmarkId );
        if ( ! $bookmark ) {
            return false;
        }

        $this->handleNotModelOwner( $bookmark, $auth0User );

        return $force ? $bookmark->forceDelete() : $bookmark->delete();
    }

    // @todo: move
    public function tag( $bookmark, $tagsString ) {

        $tags = array_map( 'trim', explode( ',', $tagsString ) );
        $tags = array_filter( $tags );

        if ( count( $tags ) > 0 ) {
            $bookmark->retag( $tags );
        }
        else {
            $bookmark->untag();
        }
    }

}