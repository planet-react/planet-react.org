<?php

namespace App\PlanetReact\Domain\Countries;

use App\PlanetReact\Domain\BaseService;
use PragmaRX\Countries\Facade as Countries;

class CountriesService extends BaseService {

    //@todo: cache
    public function createCountriesList() {
        $result = collect( [
                [
                        'commonName' => 'All',
                        'cca2'       => ''
                ]
        ] );

        $countries = Countries::all()->map( function ( $country, $key ) {
            return [
                    'commonName' => $country['name']['common'],
                    'cca2'       => $country['cca2']
            ];
        } );

        return $result->merge( $countries );
    }

    public function createTimezoneList() {
        static $regions = array(
                \DateTimeZone::AFRICA,
                \DateTimeZone::AMERICA,
                \DateTimeZone::ANTARCTICA,
                \DateTimeZone::ASIA,
                \DateTimeZone::ATLANTIC,
                \DateTimeZone::AUSTRALIA,
                \DateTimeZone::EUROPE,
                \DateTimeZone::INDIAN,
                \DateTimeZone::PACIFIC,
        );

        $timezones = array();
        foreach ( $regions as $region ) {
            $timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );
        }

        $timezoneOffsets = array();
        foreach ( $timezones as $timezone ) {
            $tz                         = new \DateTimeZone( $timezone );
            $timezoneOffsets[$timezone] = $tz->getOffset( new \DateTime );
        }

        // sort timezone by offset
        asort( $timezoneOffsets );

        $timezoneList = array();
        foreach ( $timezoneOffsets as $timezone => $offset ) {
            $offsetPrefix     = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs( $offset ) );

            $prettyOffset = "UTC${offsetPrefix}${offset_formatted}";

            $timezoneList[$timezone] = "(${prettyOffset}) $timezone";
        }

        return $timezoneList;
    }
}