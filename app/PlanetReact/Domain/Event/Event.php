<?php

namespace App\PlanetReact\Domain\Event;

use App\PlanetReact\Domain\BaseModel;
use Conner\Tagging\Taggable;

class Event extends BaseModel {
    use Taggable;

    protected $rules = [
            'name'       => 'required',
            'start_date' => 'required|date_format:Y-m-d\TH:i:s',
            'end_date'   => 'required|date_format:Y-m-d\TH:i:s|after_or_equal:start_date',
    ];

    protected static function boot() {
        parent::boot();

        // Only query active events by default.
        static::addGlobalScope( new EventActiveScope );
    }

}
