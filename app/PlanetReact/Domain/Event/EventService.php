<?php

namespace App\PlanetReact\Domain\Event;

use App\PlanetReact\Domain\BaseService;
use Carbon\Carbon;

class EventService extends BaseService {

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Event|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws \Exception
     */
    public function save( $args, $auth0User ) {

        $isUpdate = key_exists( 'id', $args ) && $args['id'] != null;

        // Load existing Model or create a new model.
        $event = $isUpdate ?
                Event::withoutGlobalScope( EventActiveScope::class )
                        ->find( $args['id'] )
                :
                new Event;

        if ( $isUpdate ) {
            $this->handleNotModelOwner( $event, $auth0User );
        }

        // Save
        $event->user_id      = $auth0User->getUserId();
        $event->active       = trim( $args['active'] );
        $event->type         = trim( $args['type'] );
        $event->name         = trim( $args['name'] );
        $event->description  = trim( strip_tags( $args['description'] ) );
        $event->start_date   = trim( $args['start_date'] );
        $event->end_date     = trim( $args['end_date'] );
        $event->timezone     = trim( $args['timezone'] );
        $event->location     = trim( strip_tags( $args['location'] ) );
        $event->country      = trim( $args['country'] );
        $event->website_link = trim( $args['website_link'] );
        $event->twitter_link = trim( $args['twitter_link'] );

        $saved = $event->save();
        if ( ! $saved ) {
            throw new \Exception( 'Model was not updated' );
        }

        return $event;
    }

    /**
     * @param $args
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getEvents( $args ) {
        $events = Event::query();

        if ( array_key_exists( 'type', $args ) && is_numeric( $args['type'] ) ) {
            $events->where( 'type', $args['type'] );
        }

        $today = Carbon::today()->toDateString();
        if ( array_key_exists( 'past', $args ) && $args['past'] ) {
            $events->whereDate( 'end_date', '<=', $today );
        }
        else {
            $events->whereDate( 'start_date', '>=', $today );
        }

        $orderBy          = array_key_exists( 'orderBy', $args ) && is_string( $args['orderBy'] ) ? $args['orderBy'] : 'start_date';
        $orderByDirection = array_key_exists( 'orderByDirection', $args ) && is_string( $args['orderByDirection'] ) ? $args['orderByDirection'] : 'asc';

        $events->orderBy( $orderBy, $orderByDirection );

        return $events;
    }

    public function getEvent( $id, $includeInActive = false ) {
        if ( $includeInActive ) {
            return Event::withoutGlobalScope( EventActiveScope::class )->find( $id );
        }
        return Event::find( $id );
    }

    public function getUpcomingEvent() {
        $today = Carbon::today()->toDateString();
        return Event::whereDate( 'start_date', '>=', $today )
                ->orderBy( 'start_date', 'asc' )->first();
    }

    public function getEventsForUser( $user_id ) {
        $events = Event::query()
                ->withoutGlobalScope( EventActiveScope::class );

        $events->where( 'user_id', $user_id );
        $events->orderBy( 'start_date', 'asc' );

        return $events;
    }

    public function activate( $id, $active, $auth0User ) {
        $event = Event::withoutGlobalScope( EventActiveScope::class )->find( $id );
        if ( ! $event ) {
            return false;
        }

        $this->handleNotModelOwner( $event, $auth0User );

        $event->active = $active;
        $event->save();

        return $event;
    }

    /**
     * @param $id
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return bool|null
     */
    public function remove( $id, $auth0User ) {
        $event = Event::withoutGlobalScope( EventActiveScope::class )->find( $id );
        if ( ! $event ) {
            return false;
        }

        $this->handleNotModelOwner( $event, $auth0User );

        return $event->delete();
    }

}