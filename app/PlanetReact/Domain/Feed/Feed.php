<?php

namespace App\PlanetReact\Domain\Feed;

use App\PlanetReact\Domain\BaseModel;

class Feed extends BaseModel {

    protected $appends = [
            'app_feed_url'  // The url to the feed in the app.
    ];

    protected $rules = [
            'name'        => 'required|min:2',
            'lang'        => 'required|min:2',
            'feed_url'    => 'required|url',
            'website_url' => 'url',
            'twitter_url' => 'url',
            'repo_url'    => 'url',
            'source_type' => 'integer',
    ];

    protected static function boot() {
        parent::boot();

        // Only query approved feeds by default.
        static::addGlobalScope( new FeedApprovedScope );
    }

    /**
     * Define a one-to-many relationship to Post.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post() {
        return $this->hasMany( 'App\PlanetReact\Domain\Post\Post' );
    }

    /**
     * @todo: https://gitlab.com/planet-react/planet-react.org/issues/55
     * @param string $value
     * @return string
     */
    public function getLangAttribute( $value ) {
        return strtoupper( $value );
    }

    public function getAppFeedUrlAttribute( $value ) {
        return config( 'app.url' ) . '/feeds/' . $this->slug;
    }

    public function getTotalPostCountAttribute() {
        return $this->hasMany( 'App\PlanetReact\Domain\Post\Post' )->where( 'feed_id', '=', $this->id )->count();
    }

    public function getMonthlyPostCountAttribute() {
        return $this->hasMany( 'App\PlanetReact\Domain\Post\Post' )
                ->where( 'feed_id', '=', $this->id )
                ->whereYear( 'pubdate', '=', date( 'Y' ) )
                ->whereMonth( 'pubdate', '=', date( 'm' ) )
                ->count();
    }

    public function alreadyExists() {
        $url = rtrim( $this->feed_url, '/' );
        return Feed::where( 'feed_url', 'like', '%' . $url . '%' )->withoutGlobalScopes()->count() > 0;
    }

    public function getImage() {
        return $this->image ? $this->image : '_default.png';
    }

}
