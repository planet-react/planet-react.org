<?php

namespace App\PlanetReact\Domain\Feed;

use File;
use Image;
use PicoFeed\Parser\Feed as PicoFeed;

class FeedImage {

    /*** @var Feed */
    protected $feed;

    public function __construct( Feed $feed ) {
        $this->feed = $feed;

        $imageDirectory = $this->getImagesDirectory();
        if ( ! File::isDirectory( $imageDirectory ) ) {
            File::makeDirectory( $imageDirectory );
        }
    }

    /**
     * Creates and saves a feed image based on the info in Pico/Feed object.
     * Returns the slug name if the image was saved.
     *
     * @param PicoFeed $picoFeed
     * @return string|null
     */
    public function saveFromPicoFeed( $picoFeed ) {
        $imageUrl = null;

        $icon = $picoFeed->getIcon();
        if ( $icon ) {
            $imageUrl = $icon;
        }

        $logo = $picoFeed->getLogo();
        if ( $logo ) {
            $imageUrl = $logo;
        }

        if ( ! $imageUrl ) {
            return null;
        }

        if ( ! starts_with( $imageUrl, 'http' ) ) {
            $imageUrl = rtrim( $picoFeed->getSiteUrl(), '/' ) . '/' . $imageUrl;
        }

        switch ( exif_imagetype( $imageUrl ) ) {
            case 1:
                $ext = 'git';
                break;
            case 2:
                $ext = 'jpg';
                break;
            case 3:
                $ext = 'png';
                break;
            default:
                $ext = 'jpg';
        }

        $fileName    = $this->createImageSlug() . ".{$ext}";
        $destination = $this->getImagesDirectory() . "/{$fileName}";
        $result      = file_put_contents( $destination, file_get_contents( $imageUrl ) );

        if ( ! $result ) {
            return null;
        }

        return $fileName;
    }

    /**
     * Creates and saves a feed image from a base64 encoded string.
     * Returns the slug name if the image was saved.
     *
     * @param string $imageStr
     * @return string|null
     *
     * @todo: merge code with saveFromXml()
     * @todo: resize image
     */
    public function saveFromBase64( $imageStr ) {
        $imageData   = base64_decode( explode( ',', $imageStr )[1] );
        $extension   = $this->getFileExtension( $imageData );
        $slug        = $this->createImageSlug() . ".{$extension}";
        $destination = $this->getImagesDirectory() . "/{$slug}";

        $img = Image::make( $imageData );
        $img->fit( 120, 120 );
        $img->save( $destination );

        return $slug;
    }

    /**
     * Return path to feed images directory.
     *
     * @return string
     */
    public function getImagesDirectory() {
        return public_path() . '/' . config( 'planet.directories.feed_images' );
    }

    /**
     * Create image slug.
     *
     * @return string
     */
    public function createImageSlug() {
        return str_slug( $this->feed->name . '-' . $this->feed->id );
    }

    /**
     * Returns image extension.
     *
     * @param $image
     * @return string|null
     */
    public function getFileExtension( $image ) {
        $fileInfo = finfo_open();

        $mimeType = finfo_buffer( $fileInfo, $image, FILEINFO_MIME_TYPE );
        try {
            return explode( '/', $mimeType )[1];
        } catch ( \Exception $exception ) {
            return null;
        }
    }

}