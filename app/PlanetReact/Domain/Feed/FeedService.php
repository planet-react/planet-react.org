<?php

namespace App\PlanetReact\Domain\Feed;

use App\PlanetReact\Aggregator\Aggregator;
use App\PlanetReact\Domain\BaseService;
use App\PlanetReact\Domain\Feed\Fetcher\Fetcher;
use App\PlanetReact\Domain\Feed\Fetcher\Rss\RssFetcher;
use App\PlanetReact\Domain\Feed\Fetcher\YouTube\YouTubeChannelFetcher;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Events\FeedReviewedEvent;
use App\PlanetReact\Services\YouTube\Api\YouTubeApi;
use DB;
use Illuminate\Support\Collection;
use PicoFeed\Reader\Reader;

class FeedService extends BaseService {

    /**
     * @var Reader
     */
    protected $reader;

    public function __construct( Reader $reader = null ) {
        $this->reader = $reader;
    }

    //@todo: test

    /**
     * Saves/updates a feed.
     *
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Feed
     * @throws \Exception
     */
    public function save( $args, $auth0User ) {
        $this->handleLogin( $auth0User );

        $isUpdate = key_exists( 'id', $args ) && $args['id'] != null;

        $feed = ! $isUpdate ? new Feed() : Feed::find( $args['id'] );

        // Throw an error if the user does not own the feed.
        if ( $isUpdate ) {
            $this->handleNotModelOwner( $feed, $auth0User );
        }

        $feed->user_id     = $auth0User->getUserId();
        $feed->lang        = $args['lang'];
        $feed->name        = trim( $args['name'] );
        $feed->feed_url    = trim( $args['feed_url'] );
        $feed->twitter_url = trim( $args['twitter_url'] );
        $feed->repo_url    = trim( $args['repo_url'] );
        $feed->source_type = FeedSourceType::getSourceTypeByFeedUrl( $feed->feed_url );

        // @todo: refactor
        $fetcher = new Fetcher();
        $fetcher->register( new RssFetcher( new Reader ) );
        $fetcher->register( new YouTubeChannelFetcher( new YouTubeApi() ) );

        $picoFeed = $fetcher->fetch( $feed );

        // Save site url from feed.
        $websiteUrl = trim( $args['website_url'] );
        if ( empty( $websiteUrl ) ) {
            $feed->website_url = $picoFeed->getSiteUrl();
        }
        else {
            $feed->website_url = $websiteUrl;
        }

        // Save description from feed.
        $description = trim( $args['description'] );
        if ( empty( $description ) ) {
            $description = $picoFeed->getDescription();
        }
        if ( $description ) {
            $feed->description = strip_tags( $description );
        }

        if ( ! $feed->validate() ) {
            throw new \Exception( 'Validation error' . $feed->errors() );
        }

        if ( ! $isUpdate && $feed->alreadyExists() ) {
            throw new \Exception( "Feed exists" );
        }

        $saved = $feed->save();

        if ( $saved ) {
            // Create feed image
            $feedImage = new FeedImage( $feed );
            if ( ! $isUpdate ) {
                $feed->image = $feedImage->saveFromPicoFeed( $picoFeed );
            }
            else {
                if ( isset( $args['image'] ) && starts_with( $args['image'], 'data:image' ) ) {
                    $feed->image = $feedImage->saveFromBase64( $args['image'] );
                }
            }

            // Create slug
            if ( ! $isUpdate ) {
                $slug       = $this->createSlug( $feed );
                $feed->slug = $slug;
            }

            $feed->save();
        }

        return $feed;
    }

    /**
     * @param string $slug
     * @param bool $includeUnapproved
     * @return Feed
     */
    public function getFeedBySlug( $slug, $includeUnapproved ) {

        if ( $includeUnapproved ) {
            $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )
                    ->where( 'slug', $slug );
        }
        else {
            $feed = Feed::where( 'slug', $slug );
        }

        return $feed->first();
    }

    /**
     * @param int $id
     * @param $includeUnapproved
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getFeedById( $id, $includeUnapproved ) {

        if ( $includeUnapproved ) {
            $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )
                    ->where( 'id', $id );
        }
        else {
            $feed = Feed::where( 'id', $id );
        }

        return $feed->first();
    }

    /**
     * Returns true if the feed is a valid feed and does not exist in the database.
     *
     * @param string $feedUrl
     * @param boolean $checkExists
     * @return array
     */
    public function validateFeedUrl( $feedUrl, $checkExists = true ) {
        $feedValidator = new FeedValidator( $this->reader );
        return $feedValidator->validate( $feedUrl, $checkExists );
    }

    /**
     * Creates slug for the feed.
     *
     * @param Feed $feed
     * @return string
     */
    public function createSlug( $feed ) {
        $slug  = str_slug( $feed->name );
        $found = Feed::where( 'slug', $slug )->first();
        if ( $found ) {
            $slug = str_slug( $feed->name . '-' . $feed->id );
        }

        return $slug;
    }

    /**
     * Approves a feed.
     *
     * @param int $feedId
     * @param int $approve
     * @return Feed
     */
    public function approve( int $feedId, int $approve ) {

        $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )->find( $feedId );

        $feed->approved = $approve;

        $feed->save();

        return $feed;
    }

    /**
     * Rejects a feed.
     * Triggers {FeedReviewedEvent}
     *
     * @param $feedId
     * @return bool
     */
    public function reject( int $feedId ) {
        $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )->find( $feedId );
        if ( ! $feed ) {
            return false;
        }

        $user = -1; // @todo: get user.

        $feed->approved = 0;

        new FeedReviewedEvent( $feed, $user );

        return true;
    }

    /**
     * @param $feedId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @param $isAdmin
     * @return bool|null
     */
    public function removeFeed( $feedId, $auth0User, $isAdmin ) {
        $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )->find( $feedId );
        if ( ! $feed ) {
            return false;
        }

        if ( ! $isAdmin ) {
            $this->handleNotModelOwner( $feed, $auth0User );
        }

        return $feed->delete();
    }

    /**
     * @param $feedId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @param $isAdmin
     * @return bool
     */
    public function restoreFeed( $feedId, $auth0User, $isAdmin ) {
        $feed = Feed::withoutGlobalScope( FeedApprovedScope::class )->withTrashed()->find( $feedId );
        if ( ! $feed ) {
            return false;
        }

        if ( ! $isAdmin ) {
            $this->handleNotModelOwner( $feed, $auth0User );
        }

        return $feed->restore();
    }

    /**
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getFeedsForUser( $auth0User ) {
        return Feed::where( 'user_id', $auth0User->getUserId() )->get();
    }

    /**
     * @param $feedId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return \App\PlanetReact\Aggregator\AggregatorResult
     * @throws \Exception
     */
    public function fetchPosts( $feedId, $auth0User ) {
        $feed = $this->getFeedById( $feedId, false );

        $this->handleNotModelOwner( $feed, $auth0User );

        try {
            $feeds = new Collection();
            $feeds->push( $feed );
            $lockFile   = config( 'planet.aggregator.lock_file' ) . ".{$feed->id}";
            $aggregator = new Aggregator( $feeds, $lockFile );
            return $aggregator->aggregate();
        } catch ( \Exception $exception ) {
            throw new \Exception( 'Something went wrong' );
        }
    }

    public function changeOwner( $feedId, $newUserId, $hasOwner ) {
        $feed = $this->getFeedById( $feedId, true );
        if ( ! $feed ) {
            return null;
        }

        $feed->user_id   = trim( $newUserId );
        $feed->has_owner = trim( $hasOwner );

        if ( ! $feed->save() ) {
            return null;
        }
        return $feed;

    }

    public function getTags( $feedId ) {

        $postIds = Post::where( 'feed_id', $feedId )->pluck( 'id' )->toArray();

        if ( ! $postIds ) {
            return null;
        }

        $postIdsStr = implode( ',', $postIds );

        $tags = DB::select( DB::raw( "
        SELECT tag_slug, tag_name, COUNT(*) as tag_count FROM tagging_tagged 
        WHERE taggable_id IN ({$postIdsStr}) 
        GROUP BY tag_slug, tag_name
        " ) );

        return $tags;
    }

    /**
     * @param int $sourceType
     * @return boolean
     */
    public function validateSourceType( $sourceType ) {
        if ( ! $sourceType ) {
            return false;
        }

        $clazz     = new \ReflectionClass( 'App\PlanetReact\Domain\Feed\FeedSourceType' );
        $constants = $clazz->getConstants();
        foreach ( $constants as $constant ) {
            if ( $constant == $sourceType ) {
                return true;
            }
        }

        return false;
    }

}