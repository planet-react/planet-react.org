<?php

namespace App\PlanetReact\Domain\Feed;

abstract class FeedSourceType {
    const YOU_TUBE_CHANNEL = 2;

    const TYPES = [
        // Default
            1 => [
                    'id'          => 'RSS',
                    'name'        => 'RSS',
                    'url_pattern' => null,
            ],

            2 => [
                    'id'          => 'YOUTUBE_CHANNEL',
                    'name'        => 'You Tube Channel',
                    'url_pattern' => 'youtube.com/channel'
            ]
    ];

    static function getSourceTypeById( string $id ) {
        $types = self::TYPES;

        foreach ( $types as $type => $value ) {
            if ( trim( $id ) === $value['id'] ) {
                return $type;
            }
        }

        return 1;
    }

    /**
     * @param string $feedUrl
     * @return int
     */
    static function getSourceTypeByFeedUrl( string $feedUrl ) {
        $types = self::TYPES;

        foreach ( $types as $type => $value ) {
            if ( strpos( $feedUrl, $value['url_pattern'] ) !== false ) {
                return $type;
            }
        }

        return 1;
    }

}