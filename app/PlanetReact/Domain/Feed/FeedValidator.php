<?php

namespace App\PlanetReact\Domain\Feed;

use App\PlanetReact\Services\YouTube\Api\YouTubeApi;
use PicoFeed\Reader\Reader;
use PicoFeed\Reader\UnsupportedFeedFormatException;
use Validator;

/**
 * Class FeedValidator
 * @todo: refactor
 */
class FeedValidator {

    /**
     * @var Reader
     */
    protected $reader;

    /**
     * @var YouTubeApi
     */
    protected $youTubeApi;

    public function __construct( Reader $reader ) {
        $this->reader = $reader;

        $this->youTubeApi = new YouTubeApi();
    }

    /**
     * @param {string} $url
     * @param {boolean} $checkExists
     * @return array
     */
    public function validate( $feedUrl, $checkExists ) {

        $realFeedUrl = rtrim( trim( $feedUrl ), '/' );

        // Validate url.
        // @todo: is this correct?
        $validator = Validator::make(
                [ 'url' => $realFeedUrl ],
                [ 'url' => 'url' ]
        );
        if ( $validator->fails() ) {
            return $this->createResult( false, 'Not a valid url' );
        }

        // Check if the feed already exists.
        // @todo: if feed is not approved display proper message.
        if ( $checkExists ) {
            $feed = Feed::where( 'feed_url', 'like', '%' . $realFeedUrl . '%' )->withoutGlobalScopes()->first();
            if ( $feed ) {
                return $this->createResult( false, 'The already feed exists. Please see: ' . $feed->app_feed_url );
            }
        }

        //https://www.youtube.com/channel/UCZkjWyyLvzWeoVWEpRemrDQ
        $youTubeChannelId = $this->youTubeApi->getChannelIdFromUrl( $realFeedUrl );
        if ( $youTubeChannelId ) {
            return $this->validateYouTubeChannel( $youTubeChannelId );
        }

        return $this->validateRss( $realFeedUrl );
    }

    public function validateRss( $feedUrl ) {
        try {
            $resource = $this->reader->download( $feedUrl );
            $parser   = $this->reader->getParser(
                    $resource->getUrl(),
                    $resource->getContent(),
                    $resource->getEncoding()
            );

            $picoFeed = $parser->execute();

            return $this->createResult(
                    true,
                    'Valid',
                    $picoFeed->getTitle(),
                    $picoFeed->getDescription(),
                    $picoFeed->getSiteUrl(),
                    FeedSourceType::getSourceTypeByFeedUrl( $feedUrl )
            );

        } catch ( UnsupportedFeedFormatException $exception ) {
            return $this->createResult(
                    false,
                    $exception->getMessage() . '. Please check if the url is correct and try again'
            );
        }
    }

    public function validateYouTubeChannel( $channelId ) {
        try {
            $result = $this->youTubeApi->getChannel( $channelId );
            return $this->createResult(
                    true,
                    'Valid',
                    $result['channel']['title'],
                    $result['channel']['description'],
                    $result['channel']['url'],
                    FeedSourceType::getSourceTypeByFeedUrl( $result['channel']['url'] )
            );
        } catch ( \Exception $exception ) {
            return $this->createResult(
                    false,
                    $exception->getMessage() . '. Please check if the url is correct and try again'
            );
        }
    }

    /**
     * @param {bool} $isValid
     * @param {string|null} $message
     * @param {string|null} $name
     * @param {string|null} $description
     * @param {string|null} $url
     * @return array
     */
    private function createResult( $isValid, $message = null, $name = null, $description = null, $url = null, $sourceType = null ) {

        $realName = $name;
        $realUrl  = $url;

        // Remove medium cruft from the RSS name/title and link.
        if ( str_contains( $url, 'medium.com' ) ) {
            $realName = str_replace( 'Stories by', '', $name );
            $realName = str_replace( 'on Medium', '', $realName );
            $realName = trim( $realName );

            $realUrl = preg_replace( '/(\?|&)source=[^&]*/i', '', $url );
        }

        return [
                'is_valid'    => $isValid,
                'message'     => $message,
                'name'        => $realName,
                'description' => $description,
                'website_url' => $realUrl,
                'source_type' => $sourceType
        ];
    }

}