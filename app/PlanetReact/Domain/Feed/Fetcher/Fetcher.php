<?php

namespace App\PlanetReact\Domain\Feed\Fetcher;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedSourceType;
use Illuminate\Support\Collection;
use PicoFeed\Parser\Feed as PicoFeed;

class Fetcher {

    /**
     * List of available fetchers.
     * @var Collection
     */
    private $fetchers;

    /**
     * Fetcher constructor.
     * @param array $fetchers
     */
    public function __construct( array $fetchers = [] ) {
        $this->fetchers = new Collection( $fetchers );
    }

    /**
     * @param Feed $feed
     * @return PicoFeed
     * @throws \Exception
     */
    public function fetch( Feed $feed ) {
        $fetcher = $this->getFetcherForFeedSourceType( $feed->source_type );

        if ( ! $fetcher ) {
            throw new \Exception( 'Fetcher not registered' );
        }
        return $fetcher->fetch( $feed );
    }

    /**
     * Register a new fetcher.
     * @param FetcherInterface $fetcher
     */
    public function register( FetcherInterface $fetcher ) {
        if ( ! $this->hasFetcher( $fetcher ) ) {
            $this->fetchers->push( $fetcher );
        }
    }

    public function hasFetcher( FetcherInterface $fetcher ) {
        if ( $this->getFetcherForFeedSourceType( $fetcher->getFeedSourceType() ) ) {
            return true;
        }
        return false;
    }

    /**
     * @param int $sourceType
     * @return mixed
     */
    public function getFetcherForFeedSourceType( int $sourceType ) {
        return $this->fetchers->first( function ( FetcherInterface $fetcher ) use ( $sourceType ) {
            return $fetcher->getFeedSourceType() == $sourceType;
        } );
    }

}
