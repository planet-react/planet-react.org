<?php
namespace App\PlanetReact\Domain\Feed\Fetcher;

use App\PlanetReact\Domain\Feed\Feed;
use PicoFeed\Parser\Feed as PicoFeed;

interface FetcherInterface {

    /**
     * @return int
     */
    public function getFeedSourceType(): int;

    /**
     * @param Feed $feed
     * @return PicoFeed
     * @throws \Exception
     */
    public function fetch( Feed $feed );

}