<?php

namespace App\PlanetReact\Domain\Feed\Fetcher\Rss;

use App\PlanetReact\Domain\Feed\Fetcher\FetcherInterface;
use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedSourceType;
use PicoFeed\Parser\Feed as PicoFeed;
use PicoFeed\Reader\Reader;

class RssFetcher implements FetcherInterface {

    private $reader;

    public function __construct( Reader $reader ) {
        $this->reader = $reader;
    }

    public function getFeedSourceType(): int {
        return 1;
    }

    /**
     * @param Feed $feed
     * @return PicoFeed
     * @throws \Exception
     */
    public function fetch( Feed $feed ) {
        try {
            $resource = $this->reader->download( $feed->feed_url );
            $resource->setTimeout( config( 'planet.aggregator.timeout' ) );
            $resource->setUserAgent( config( 'planet.aggregator.user_agent' ) );
            $parser = $this->reader->getParser(
                    $resource->getUrl(),
                    $resource->getContent(),
                    $resource->getEncoding()
            );

            $picoFeed = $parser->execute();

            return $picoFeed;

        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }

    }
}