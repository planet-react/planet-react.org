<?php

namespace App\PlanetReact\Domain\Feed\Fetcher\YouTube;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\Fetcher\FetcherInterface;
use App\PlanetReact\Services\YouTube\Api\YouTubeApi;
use PicoFeed\Parser\Feed as PicoFeed;
use PicoFeed\Parser\Item as PicoFeedItem;

class YouTubeChannelFetcher implements FetcherInterface {

    private $api;

    public function __construct( YouTubeApi $api ) {
        $this->api = $api;
    }

    public function getFeedSourceType(): int {
        return 2;
    }

    /**
     * @param Feed $feed
     * @return PicoFeed
     * @throws \Exception
     */
    public function fetch( Feed $feed ) {

        try {
            $channelId = $this->api->getChannelIdFromUrl( $feed->feed_url );
            $channel   = $this->api->getChannel( $channelId );
            $videos    = $channel['videos'];
            $picoFeed  = new PicoFeed();

            $picoFeed->setTitle( $feed->name );
            $picoFeed->setIcon( $channel['channel']['thumbnails']->default->url );
            $picoFeed->setSiteUrl( $feed->feed_url );
            $picoFeed->setDate( new \DateTime() );

            $items = [];
            foreach ( $videos as $video ) {
                $picoItem = new PicoFeedItem();
                $picoItem->setXml( new \SimpleXMLElement( '<xml/>' ) );
                $picoItem->setId( $video['videoId'] );
                $picoItem->setContent( $video['description'] );
                $picoItem->setTitle( $video['title'] );
                $picoItem->setPublishedDate( new \DateTime( $video['publishedAt'] ) );
                $picoItem->setUrl( 'https://www.youtube.com/watch?v=' . $video['videoId'] );
                $picoItem->setCategories( [ 'video' ] );
                $items[] = $picoItem;
            }

            $picoFeed->setItems( $items );

            return $picoFeed;

        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }

}