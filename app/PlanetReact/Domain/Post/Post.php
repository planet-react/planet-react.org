<?php

namespace App\PlanetReact\Domain\Post;

use App\PlanetReact\Domain\BaseModel;
use Conner\Tagging\Taggable;

class Post extends BaseModel {
    use Taggable;

    public $categories;

    public function feed() {
        return $this->belongsTo( 'App\PlanetReact\Domain\Feed\Feed' );
    }

    public function alreadyExists() {

        if ( $this->guid ) {
            return Post::where( 'guid', '=', $this->guid )->count() > 0;
        }
        return Post::where( 'link', '=', $this->link )->count() > 0;
    }
}
