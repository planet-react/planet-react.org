<?php

namespace App\PlanetReact\Domain\Post;

use App\PlanetReact\Domain\BaseService;
use App\PlanetReact\Domain\Report\Report;
use App\PlanetReact\Domain\Report\ReportService;

class PostService extends BaseService {

    //@todo: refactor / use getPosts.
    //@todo: re-arrange params. `search` first.
    public function findPosts( $feedId = null, $language = null, $year = null, $month = null, $search = null ) {
        $posts = Post::whereHas( 'feed' );

        if ( $search ) {
            $posts->where( 'title', 'regexp', "[[:<:]]{$search}[[:>:]]" );
        }

        if ( $year ) {
            $posts->whereYear( 'pubdate', '=', $year );
        }

        if ( $month ) {
            $posts->whereMonth( 'pubdate', '=', $month );
        }

        if ( $feedId ) {
            $posts->where( 'feed_id', $feedId );
        }

        if ( $language ) {
            $posts->whereHas( 'feed', function ( $query ) use ( $language ) {
                $query->where( 'lang', $language );
            } );
        }

        $posts->orderBy( 'pubdate', 'desc' );

        return $posts;
    }

    public function getPost( $id, $auth0User = null ) {
        $post = Post::find( $id );
        if ( $auth0User ) {
            // Is this efficient?
            $post->leftJoin( 'bookmarks', function ( $join ) use ( $auth0User ) {
                $join->on( 'posts.link', '=', 'bookmarks.url' )
                        ->where( 'bookmarks.user_id', '=', $auth0User->getUserId() );
            } );
            $post->select( 'posts.*', 'bookmarks.id AS bookmarkId', 'feeds.user_id AS feed_user_id' );
        }

        return $post;
    }

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getPosts( $args, $auth0User = null ) {
        $defaultOrderBy = 'desc';

        $search = isset( $args['search'] ) ? $args['search'] : null;

        $isTagSearch = $search && strlen( $search ) >= 2 && str_contains( $search, '_tag:' );

        $posts = Post::with( [ 'tagged', 'feed' ] );

        $posts->leftJoin( 'feeds', function ( $join ) use ( $args ) {
            $join->on( 'feeds.id', '=', 'posts.feed_id' );
//            if ( isset( $args['lang'] ) ) {
//                $join->where( 'feeds.lang', '=', $args['lang'] );
//            }
        } )->whereNull( 'feeds.deleted_at' );
        $posts->select( 'posts.*', 'feeds.slug', 'feeds.lang' );

        if ( $isTagSearch ) {
            $searchTerm = explode( ':', $search );
            $posts->withAnyTag( [ trim( $searchTerm[1] ) ] );
        }

        if ( isset( $args['id'] ) ) {
            $posts->where( 'posts.id', $args['id'] );
        }

        if ( $search && ! $isTagSearch && strlen( $search ) >= 2 ) {
            $posts->where( 'posts.title', 'regexp', "[[:<:]]{$search}[[:>:]]" );
        }

        if ( isset( $args['feedId'] ) ) {
            $posts->where( 'posts.feed_id', $args['feedId'] );
        }

        // @todo: is this necessary?
        // @todo: optimize
        if ( isset( $args['slug'] ) ) {
            $slug = $args['slug'];
            $posts->where( 'feeds.slug', $slug );
        }

        if ( isset( $args['lang'] ) ) {
            $lang = $args['lang'];
            $posts->where( 'feeds.lang', $lang);
        }




        // @todo: test
        if ( isset( $args['pubdateYear'] ) ) {
            $posts->whereYear( 'posts.pubdate', '=', $args['pubdateYear'] );
        }

        // @todo: test
        if ( isset( $args['pubdateMonth'] ) ) {
            $posts->whereMonth( 'posts.pubdate', '=', $args['pubdateMonth'] );
        }

        // @todo: test
        if ( isset( $args['orderBy'] ) ) {
            $direction = isset( $args['orderByDirection'] ) ? $args['orderByDirection'] : $defaultOrderBy;
            $posts->orderBy( $args['orderBy'], $direction );
        }

        // @todo: test
        if ( $auth0User ) {
            // Is this efficient?
            $posts->leftJoin( 'bookmarks', function ( $join ) use ( $auth0User ) {
                $join->on( 'posts.link', '=', 'bookmarks.url' )
                        ->where( 'bookmarks.user_id', '=', $auth0User->getUserId() );
            } );
            $posts->select( 'posts.*', 'bookmarks.id AS bookmarkId', 'feeds.user_id AS feed_user_id' );
        }

        return $posts;
    }

    /**
     * @param $postId
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @param bool $isAdmin
     * @param bool $report
     * @return bool|null
     */
    public function removePost( $postId, $auth0User, $isAdmin = false, $report = false ) {
        $post = Post::find( $postId );

        if ( ! $post ) {
            return false;
        }

        if ( ! $isAdmin ) {
            $this->handleNotModelOwner( $post, $auth0User );
        }

        $deleted = $post->delete();

        if ( $report ) {
            $reportService = new ReportService();
            $reportService->save( [
                    'content_id'   => $post->id,
                    'title'        => $post->title,
                    'url'          => $post->link,
                    'reason'       => Report::REASONS['OTHER'],
                    'other_reason' => 'Deleted by owner: ' . $auth0User->getUserId(),
            ] );
        }


        return $deleted;
    }


}