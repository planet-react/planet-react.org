<?php

namespace App\PlanetReact\Domain\Report;

use App\PlanetReact\Domain\BaseModel;

class Report extends BaseModel {

    const REASONS = [
            'SPAM'          => 'This is spam',
            'PERSONAL_INFO' => 'This is personal and confidential information',
            'THREATS'       => 'This is threatening, harassing, or inciting violence',
            'OTHER'         => 'Other (max 100 characters)',
    ];

}
