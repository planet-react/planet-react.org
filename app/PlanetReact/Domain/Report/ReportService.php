<?php

namespace App\PlanetReact\Domain\Report;

use App\PlanetReact\Domain\BaseService;
use App\PlanetReact\Domain\Post\Post;

class ReportService extends BaseService {

    public function save( $args ) {

        $report = new Report;

        $report->content_id   = trim( $args['content_id'] );
        $report->title        = trim( $args['title'] );
        $report->url          = trim( $args['url'] );
        $report->reason       = trim( $args['reason'] );
        $report->other_reason = trim( $args['other_reason'] );

        $saved = $report->save();

        if ( ! $saved ) {
            throw new \Exception( 'Model not updated' );
        }

        return $report;
    }

    public function getReports() {
        $reports = Report::orderBy( 'created_at', 'desc' );

        return $reports->get();
    }

    // @todo: rename
    public function deleteReport( $reportId ) {
        $report = Report::find( $reportId );
        if ( ! $report ) {
            return false;
        }

        // Destroy the post
        $post = Post::find( $report->content_id );
        if ( $post ) {
            $postDeleted = $post->delete();
            if ( ! $postDeleted ) {
                return false;
            }
        }

        // Soft delete all active reports referring to the post.
        $reports = Report::query()->where( 'content_id', $report->content_id )->get();
        foreach ( $reports as $model ) {
            $model->delete();
        }

        return true;
    }

    public function postHasBeenReportedAndDealtWith( $url ) {
        $reported = Report::onlyTrashed()->where( 'url', '=', $url )->first();
        if ( $reported && explode( '?', $reported->url )[0] == explode( '?', $url )[0] ) {
            return true;
        }

        return false;
    }

}