<?php

namespace App\PlanetReact\Domain\Subscription;

use App\PlanetReact\Domain\BaseService;
use Illuminate\Support\Collection;

class SubscriptionService extends BaseService {

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Subscription|null
     */
    public function toggle( $args, $auth0User ) {
        if ( ! $auth0User ) {
            return null;
        }

        $id = key_exists( 'id', $args ) && $args['id'] != null ? $args['id'] : null;

        $exists = $id && $this->getSubscriptionForUser( $id, $auth0User );

        if ( $exists ) {
            return $this->unsubscribe( $args['id'], $auth0User );
        }
        else {
            return $this->subscribe( $args, $auth0User );
        }
    }

    /**
     * @param $args
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return Subscription
     */
    public function subscribe( $args, $auth0User ) {

        $this->handleLogin( $auth0User );

        $subscription = new Subscription;

        $subscription->user_id    = $auth0User->getUserId();
        $subscription->user_email = $auth0User->getEmail();
        $subscription->type       = $args['type'];
        $subscription->value      = $args['value'];

        $this->saveModel( $subscription );

        return $subscription;
    }

    /**
     * @param $id
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return null
     */
    public function unsubscribe( $id, $auth0User ) {

        $this->handleLogin( $auth0User );

        $subscription = $this->getSubscriptionForUser( $id, $auth0User );

        $this->handleNotModelOwner( $subscription, $auth0User );

        $subscription->forceDelete();

        return null;
    }

    /**
     * @param string $type
     * @param string $value
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return mixed
     */
    public function getSubscriptionInfo( $type, $value, $auth0User ) {
        $subscription = Subscription::where( [
                [ 'type', '=', $type ],
                [ 'value', '=', $value ],
                [ 'user_id', '=', $auth0User->getUserId() ]
        ] )->first();

        if ( $subscription ) {
            $this->handleNotModelOwner( $subscription, $auth0User );
        }

        return $subscription;
    }

    /**
     * @param $id
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return mixed
     */
    public function getSubscriptionForUser( $id, $auth0User ) {

        $subscription = Subscription::where( 'id', $id )
                ->where( 'user_id', $auth0User->getUserId() )
                ->first();

        if ( $subscription ) {
            $this->handleNotModelOwner( $subscription, $auth0User );
        }

        return $subscription;
    }

    /**
     * @param string $orderBy
     * @param string $orderByDirection
     * @param \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile $auth0User
     * @return \Illuminate\Database\Eloquent\Collection|Collection|static[]
     */
    public function getAllSubscriptionsForUser( $orderBy, $orderByDirection, $auth0User ) {
        $this->handleLogin( $auth0User );

        $subscriptions = Subscription::where( 'subscriptions.user_id', '=', $auth0User->getUserId() )
                ->whereNotIn( 'type', [
                        SubscriptionType::DAILY,
                        SubscriptionType::WEEKLY
                ] );

        // @todo: test
        // @todo: is there a better way to do this? eg. return an Eloquent model.
        // See graphql/type/SubscriptionType - resolveFeedImageField()
        $subscriptions->leftJoin( 'feeds', function ( $join ) {
            $join->on( 'subscriptions.value', '=', 'feeds.id' );
        } );

        $subscriptions->select(
                'subscriptions.*',
                'feeds.name AS feed_name',
                'feeds.description AS feed_description',
                'feeds.image AS feed_image'
        );


        $subscriptions->orderBy( "subscriptions.{$orderBy}", $orderByDirection );

        return $subscriptions->get();
    }

    public function getSubscriptionsForPosts( $posts ) {
        $feedIds = $this->collectAllFeedIdsFromPosts( $posts );
        $tags    = $this->collectAllTagsFromPosts( $posts );
        $values  = array_merge( $feedIds, $tags );

        return Subscription::whereIn( 'value', $values )->get();
    }

    /**
     * @param Collection $posts
     * @return mixed
     */
    private function collectAllFeedIdsFromPosts( $posts ) {
        return $posts->pluck( 'feed_id' )->unique()->all();
    }

    /**
     * @param Collection $posts
     * @return array
     */
    private function collectAllTagsFromPosts( $posts ) {
        $result = collect();

        // Pluck tags instances from all the posts.
        $tags = $posts->pluck( 'tags' )->all();
        foreach ( $tags as $tag ) {
            $result = $result->merge( $tag->pluck( 'slug' ) );
        }

        return $result->unique()->all();
    }


}