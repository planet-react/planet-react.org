<?php

namespace App\PlanetReact\Domain\Subscription;

abstract class SubscriptionType {
    const TAG = 'tag';
    const FEED = 'feed';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
}