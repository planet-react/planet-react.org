<?php

namespace App\PlanetReact\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class AggregateCommandCompleteEvent {
    use SerializesModels;

    public $posts;

    /**
     * AggregateCommandCompleteEvent constructor.
     * @param Collection $posts
     */
    public function __construct( Collection $posts ) {
        $this->posts = $posts;
    }
}