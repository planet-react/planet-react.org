<?php

namespace App\PlanetReact\Events;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use Illuminate\Queue\SerializesModels;

class FeedSubmittedEvent {
    use SerializesModels;

    public $feed;
    public $user;

    /**
     * @param Feed $feed
     * @param Auth0UserProfile $user
     */
    public function __construct( Feed $feed, Auth0UserProfile $user ) {
        $this->feed = $feed;
        $this->user = $user;
    }
}