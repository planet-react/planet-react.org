<?php

namespace App\PlanetReact\Http\Auth0;

use App\PlanetReact\Services\Auth0\Api\Auth0ApiClient;
use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use Auth0\SDK\Exception\CoreException;
use Auth0\SDK\JWTVerifier;
use Cache;
use Illuminate\Auth\AuthenticationException;

class Auth0Authentication {

    const CACHE_USER_MINUTES = 60;

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    public function authorize( $rolesToCheck = null ) {
        if ( ! $this->isAuthenticated() ) {
            throw new AuthenticationException( 'Not authenticated' );
        }

        if ( $rolesToCheck ) {
            if ( ! $this->hasRole( $rolesToCheck ) ) {
                throw new AuthenticationException( 'Not authorized' );
            }
        }
    }

    //@todo: support $rolesToCheck string
    public function hasRole( $rolesToCheck = null ) {
        $result = false;
        if ( $rolesToCheck ) {
            $user = $this->getLoggedInUserProfile();
            if ( ! $user ) {
                return $result;
            }
            foreach ( $user->getRoles() as $role ) {
                if ( in_array( $role, $rolesToCheck ) ) {
                    $result = true;
                    break;
                }
            }

        }
        return $result;
    }

    function isAuthenticated() {
        try {
            $token = $this->getTokenFromRequest();
            if ( ! $token ) {
                return false;
            }

            try {

                $verifier = new JWTVerifier( [
                        'supported_algs'  => [ 'RS256' ],
                        'valid_audiences' => [ config( 'planet.auth0.client_id' ) ],
                        'authorized_iss'  => [ $this->api->getDomainUrl() ],
                        'client_secret'   => config( 'planet.auth0.secret' ),
                ] );
            } catch ( CoreException $exception ) {
            }

            $result = $verifier->verifyAndDecode( $token );

        } catch ( \Exception $exception ) {
            $result = false;
        }

        return $result;
    }

    function isAdmin() {
        return $this->isAuthenticated() && $this->hasRole( [ 'admin' ] );
    }

    /**
     * Fetch user info from Auth0
     *
     * https://auth0.com/docs/user-profile/user-profile-structure
     *
     * @return Auth0UserProfile|bool
     */
    function getLoggedInUserProfile() {
        $token = $this->getTokenFromRequest();
        if ( ! $token ) {
            return false;
        }

        $cacheKey = "user_{$token}";
        if ( Cache::has( $cacheKey ) ) {
            return Cache::get( $cacheKey );
        }

        $response = false;
        try {
            $response = $this->api->getClient()->request( 'POST', $this->api->getDomainUrl( 'tokeninfo' ), [
                    'form_params' => [
                            'id_token' => $token
                    ]
            ] );
        } catch ( \Exception $exception ) {

        }

        if ( ! $response || ! $response->getStatusCode() == 200 ) {
            return false;
        }

        $userInfo = json_decode( $response->getBody()->getContents() );

        $profile = new Auth0UserProfile();
        $profile->setUserId( $userInfo->user_id );
        $profile->setName( $userInfo->name );
        $profile->setEmail( $userInfo->email );
        $profile->setEmailVerified( $userInfo->email_verified );
        $profile->setRoles( $userInfo->roles );

        Cache::put( $cacheKey, $profile, self::CACHE_USER_MINUTES );

        return $profile;
    }

    function getTokenFromRequest() {
        $token = request()->header( 'authorization' );
        if ( ! $token ) {
            return false;
        }

        return str_replace( 'Bearer ', '', $token );
    }

}
