<?php

namespace App\PlanetReact\Http\Controllers;

use App;
use App\PlanetReact\Domain\Post\PostService;
use PicoFeed\Syndication\AtomFeedBuilder;
use PicoFeed\Syndication\AtomItemBuilder;

class AtomFeedController extends BaseController {

    private $postService;

    public function __construct( PostService $service ) {
        $this->postService = $service;
    }

    public function index() {
        $feedId   = request()->input( 'feedId' );
        $language = request()->input( 'language' );

        $title       = config( 'planet.mame' );
        $authorName  = config( 'planet.mame' );
        $authorEmail = config( 'planet.email' );
        $authorUrl   = config( 'planet.repo' );
        $feedUrl     = url( 'api/feed' );

        $feedBuilder = AtomFeedBuilder::create()
                ->withTitle( $title )
                ->withAuthor( $authorName, $authorEmail, $authorUrl )
                ->withFeedUrl( $feedUrl )
                ->withDate( new \DateTime() );

        $posts = $this->postService->findPosts( $feedId, $language )->limit( 30 )->get();

        foreach ( $posts as $post ) {
            $pubDate = new \DateTime( $post->pubdate );
            $content = preg_replace( '#<script(.*?)>(.*?)</script>#is', '', $post->content );

            $feedBuilder
                    ->withItem( AtomItemBuilder::create( $feedBuilder )
                            ->withTitle( $post->title )
                            ->withUrl( $post->link )
                            ->withAuthor( $post->authors )
                            ->withPublishedDate( $pubDate )
                            ->withUpdatedDate( $pubDate )
                            ->withContent( $content )
                    );
        }

        return response( $feedBuilder->build(), 200 )
                ->header( 'Content-Type', 'text/xml' );
    }
}
