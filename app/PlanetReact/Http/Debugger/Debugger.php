<?php

namespace App\PlanetReact\Http\Debugger;

use File;

class Debugger {

    /**
     * @var boolean
     */
    protected $enabled;

    /**
     * @var string
     */
    protected $cacheFile;

    /**
     * @var array
     */
    protected $timings = [];

    public function __construct( $clearCacheFile = false, $cacheFile = null ) {
        $this->enabled   = config( 'app.debug' );
        $this->cacheFile = $cacheFile ? $cacheFile : config( 'planet.debugger.cache_file' );

        if ( $clearCacheFile ) {
            $this->clearCacheFile();
        }
    }

    public function log() {
        if ( ! $this->enabled ) {
            return null;
        }
        $this->addLogArgsToCache( func_get_args() );
    }

    public function time( $name ) {
        if ( ! $this->enabled ) {
            return null;
        }

        $this->timings[$name]['time_start'] = microtime( true );
    }

    public function timeEnd( $name ) {
        if ( ! $this->enabled ) {
            return null;
        }

        $result = round( microtime( true ) - $this->timings[$name]['time_start'], 2 );

        $this->addLogArgsToCache( [ [ "Time: {$result}ms" ] ] );
    }

    public function fetchData() {
        return json_decode( $this->fetchJson() );
    }

    public function fetchJson() {
        if ( ! $this->enabled ) {
            return null;
        }
        if ( ! File::exists( $this->cacheFile ) ) {
            return null;
        }
        return File::get( $this->cacheFile );
    }

    public function clearCacheFile() {
        $json = $this->createInitialJson();
        File::put( $this->cacheFile, $json );
    }

    private function addLogArgsToCache( $args ) {
        $data = $this->fetchData();

        $data->entries[] = $args;

        File::put( $this->cacheFile, json_encode( $data ) );
    }

    private function createInitialJson() {
        $data          = new \stdClass;
        $data->entries = [];

        return json_encode( $data );
    }
}
