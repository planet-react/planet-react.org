<?php

namespace App\PlanetReact\Http\Debugger\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;

class DebuggerMiddleware {

    public function handle( Request $request, Closure $next ) {
        if ( config( 'app.debug' ) ) {

            dbg_clear();

            if ( config( 'planet.debugger.log_queries' ) ) {
                DB::listen( function ( $query ) use ( $request ) {
                    $sql = $query->sql;

                    if ( count( $query->bindings ) > 0 ) {
                        foreach ( $query->bindings as $binding ) {
                            $sql = preg_replace( '/\?/', $binding, $sql, 1 );
                        }
                    }

                    dbg_log( "[SQL] {$sql}", "Time: {$query->time}ms" );
                } );
            }
        }

        return $next( $request );
    }
}
