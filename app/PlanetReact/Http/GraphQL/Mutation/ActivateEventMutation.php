<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Event\EventService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class ActivateEventMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'activateEvent'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );

        $this->eventService = $eventService;
    }

    public function type() {
        return GraphQL::type( 'Event' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],

                'active' => [
                        'name' => 'active',
                        'type' => Type::int(),
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        return $this->eventService->activate( $args['id'], $args['active'], $user );
    }

}
