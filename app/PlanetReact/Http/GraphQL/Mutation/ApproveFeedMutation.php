<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Services\Auth0\Api\Auth0Users;
use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Events\FeedReviewedEvent;
use GraphQL;
use GraphQL\Type\Definition\Type;

class ApproveFeedMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'approveFeed'
    ];

    protected $feedService;
    protected $auth0Users;

    public function __construct( array $attributes = [], FeedService $feedService, Auth0Users $auth0Users ) {
        parent::__construct( $this->attributes );

        $this->feedService = $feedService;
        $this->auth0Users  = $auth0Users;
    }

    public function type() {
        return GraphQL::type( 'Feed' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],

                'approved' => [
                        'name' => 'approved',
                        'type' => Type::int(),
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        $feed = $this->feedService->approve( $args['id'], $args['approved'] );
        if ( ! $feed ) {
            throw new \Exception( 'Feed not found' );
        }

        $user = $this->auth0Users->getUserById( $feed->user_id );

        if ( ! $user ) {
            throw new \Exception( 'User not found' );
        }

        event( new FeedReviewedEvent( $feed, $user ) );

        return $feed;
    }

}
