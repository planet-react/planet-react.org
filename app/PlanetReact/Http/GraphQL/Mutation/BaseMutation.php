<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Http\Auth0\Auth0Authentication;
use Folklore\GraphQL\Support\Mutation;

class BaseMutation extends Mutation {

    protected $auth;

    public function __construct( $attributes = [] ) {
        parent::__construct( $this->attributes );

        $this->auth = new Auth0Authentication();
    }
}
