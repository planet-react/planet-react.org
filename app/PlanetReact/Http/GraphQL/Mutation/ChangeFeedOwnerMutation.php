<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Feed\FeedService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class ChangeFeedOwnerMutation extends BaseMutation {

    protected $feedService;

    protected $attributes = [
            'name' => 'changeFeedOwner'
    ];

    public function __construct( array $attributes = [], FeedService $feedService ) {
        $this->feedService = $feedService;

        parent::__construct( $this->attributes );
    }

    public function type() {
        return GraphQL::type( 'Feed' );
    }

    public function args() {
        return [
                'id'        => [
                        'name'  => 'id',
                        'type'  => Type::int(),
                        'rules' => [ 'required' ]
                ],
                'user_id'   => [
                        'name'  => 'user_id',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'has_owner' => [
                        'name'  => 'has_owner',
                        'type'  => Type::int(),
                        'rules' => [ 'required' ]
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        return $this->feedService->changeOwner(
                $args['id'], $args['user_id'], $args['has_owner']
        );
    }

}