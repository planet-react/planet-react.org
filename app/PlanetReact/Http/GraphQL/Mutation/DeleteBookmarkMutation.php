<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Bookmark\BookmarkService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class DeleteBookmarkMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'deleteBookmark'
    ];

    protected $bookmarkService;

    public function __construct( array $attributes = [], BookmarkService $bookmarkService ) {
        parent::__construct( $this->attributes );

        $this->bookmarkService = $bookmarkService;
    }

    public function type() {
        return GraphQL::type( 'Bookmark' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        $this->bookmarkService->forceDestroy( $args['id'], $user );

        return null;
    }

}
