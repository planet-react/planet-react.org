<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Report\ReportService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class DeleteReportMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'deleteReport'
    ];

    protected $reportService;

    public function __construct( array $attributes = [], ReportService $reportService ) {
        parent::__construct( $this->attributes );

        $this->reportService = $reportService;
    }

    public function type() {
        return GraphQL::type( 'Report' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        $this->reportService->deleteReport( $args['id'] );

        return null;
    }

}
