<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Bookmark\BookmarkService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class EditBookmarkMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'editBookmark'
    ];

    protected $bookmarkService;

    public function __construct( array $attributes = [], BookmarkService $bookmarkService ) {
        parent::__construct( $this->attributes );

        $this->bookmarkService = $bookmarkService;
    }

    public function type() {
        return GraphQL::type( 'Bookmark' );
    }

    public function args() {
        return [

                'id'    => [
                        'name' => 'id',
                        'type' => Type::int(),

                ],
                'title' => [
                        'name'  => 'title',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'url'   => [
                        'name'  => 'url',
                        'type'  => Type::string(),
                        'rules' => [ 'required', 'url' ]
                ],
                'notes' => [
                        'name' => 'notes',
                        'type' => Type::string(),
                ],
                'tags'  => [
                        'name' => 'tags',
                        'type' => Type::string(),
                ],

        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        $bookmark = $this->bookmarkService->save( $args, $user );
        return $bookmark;
    }

}
