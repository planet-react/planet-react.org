<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Event\EventService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class EditEventMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'editEvent'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );

        $this->eventService = $eventService;
    }

    public function type() {
        return GraphQL::type( 'Event' );
    }

    public function args() {
        return [

                'id'           => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],
                'active'       => [
                        'name' => 'active',
                        'type' => Type::int(),
                ],
                'name'         => [
                        'name'  => 'name',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'description'  => [
                        'name' => 'description',
                        'type' => Type::string()
                ],
                'type'         => [
                        'name' => 'type',
                        'type' => Type::int(),
                ],
                'start_date'   => [
                        'name'  => 'start_date',
                        'type'  => Type::string(),
                        'rules' => [
                                'required',
                                'date_format:Y-m-d\TH:i:s'
                        ]
                ],
                'end_date'     => [
                        'name'  => 'end_date',
                        'type'  => Type::string(),
                        'rules' => [
                                'required',
                                'date_format:Y-m-d\TH:i:s',
                                'after_or_equal:start_date'
                        ]
                ],
                'timezone'     => [
                        'name' => 'timezone',
                        'type' => Type::string(),
                ],
                'location'     => [
                        'name' => 'location',
                        'type' => Type::string(),
                ],
                'country'      => [
                        'name' => 'country',
                        'type' => Type::string(),
                ],
                'website_link' => [
                        'name' => 'website_link',
                        'type' => Type::string(),
                ],
                'twitter_link' => [
                        'name' => 'twitter_link',
                        'type' => Type::string(),
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        $event = $this->eventService->save( $args, $user );
        return $event;
    }

}
