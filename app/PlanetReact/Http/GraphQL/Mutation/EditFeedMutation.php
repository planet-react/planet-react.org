<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Events\FeedSubmittedEvent;
use GraphQL;
use GraphQL\Type\Definition\Type;
use PicoFeed\Reader\Reader;

class EditFeedMutation extends BaseMutation {

    protected $feedService;

    protected $attributes = [
            'name' => 'editFeed'
    ];

    public function __construct( array $attributes = [] ) {
        parent::__construct( $this->attributes );

        $this->feedService = new FeedService( new Reader );
    }

    public function type() {
        return GraphQL::type( 'Feed' );
    }

    public function args() {
        return [
                'id'          => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],
                'source_type' => [
                        'name' => 'source_type',
                        'type' => Type::int(),
                ],
                'name'        => [
                        'name'  => 'name',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'image'       => [
                        'name' => 'image',
                        'type' => Type::string(),
                ],
                'lang'        => [
                        'name'  => 'lang',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'feed_url'    => [
                        'name'  => 'feed_url',
                        'type'  => Type::string(),
                        'rules' => [ 'required', 'url' ]
                ],
                'website_url'    => [
                        'name'  => 'website_url',
                        'type'  => Type::string(),
                        'rules' => [ 'url' ]
                ],
                'twitter_url' => [
                        'name'  => 'twitter_url',
                        'type'  => Type::string(),
                        'rules' => [ 'nullable', 'url' ]
                ],
                'repo_url'    => [
                        'name'  => 'repo_url',
                        'type'  => Type::string(),
                        'rules' => [ 'nullable', 'url' ]
                ],
                'description' => [
                        'name' => 'description',
                        'type' => Type::string(),
                ],
        ];
    }

    public function resolve( $root, $args ) {

        $user = $this->auth->getLoggedInUserProfile();

        $isNew = ! isset( $args['id'] );

        $feed = $this->feedService->save( $args, $user );

        try {
            if ( $isNew && $feed ) {
                event( new FeedSubmittedEvent( $feed, $user ) );
            }
        } catch ( \Exception $exception ) {

        }

        return $feed;
    }

}
