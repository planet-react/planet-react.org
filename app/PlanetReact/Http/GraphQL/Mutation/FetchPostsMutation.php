<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Feed\FeedService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class FetchPostsMutation extends BaseMutation {

    protected $feedService;

    protected $attributes = [
            'name' => 'fetchPosts'
    ];

    public function __construct( array $attributes = [], FeedService $feedService ) {
        $this->feedService = $feedService;

        parent::__construct( $this->attributes );
    }

    public function type() {
        return GraphQL::type( 'Count' );
    }

    public function args() {
        return [
                'feedId' => [
                        'name'  => 'feedId',
                        'type'  => Type::int(),
                        'rules' => [ 'required' ]
                ],
        ];
    }

    public function resolve( $root, $args ) {

        $result = $this->feedService->fetchPosts( $args['feedId'], $this->auth->getLoggedInUserProfile() );

        return [ 'count' => $result->getSavedPosts()->count() ];
    }

}