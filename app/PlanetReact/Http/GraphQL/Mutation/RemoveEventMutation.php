<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Event\EventService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class RemoveEventMutation extends BaseMutation {

    protected $eventService;

    protected $attributes = [
            'name' => 'removeEvent'
    ];

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );

        $this->eventService = $eventService;
    }

    public function type() {
        return GraphQL::type( 'Event' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        $this->eventService->remove( $args['id'], $user );

        return null;
    }

}
