<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Feed\FeedService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class RemoveFeedMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'removeFeed'
    ];

    protected $feedService;

    public function __construct( array $attributes = [], FeedService $feedService ) {
        parent::__construct( $this->attributes );

        $this->feedService = $feedService;
    }

    public function type() {
        return GraphQL::type( 'Feed' );
    }

    public function args() {
        return [
                'id'      => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],
                'isRemoved' => [
                        'name' => 'isRemoved',
                        'type' => Type::boolean(),
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $user    = $this->auth->getLoggedInUserProfile();
        $isAdmin = $this->auth->hasRole( [ 'admin' ] );

        if ( $args['isRemoved'] ) {
            $this->feedService->restoreFeed( $args['id'], $user, $isAdmin );

        }
        else {
            $this->feedService->removeFeed( $args['id'], $user, $isAdmin );
        }

        return null;
    }

}
