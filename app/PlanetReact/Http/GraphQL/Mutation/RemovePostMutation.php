<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Post\PostService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class RemovePostMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'removePost'
    ];

    protected $postService;

    public function __construct( array $attributes = [], PostService $postService ) {
        parent::__construct( $this->attributes );

        $this->postService = $postService;
    }

    public function type() {
        return GraphQL::type( 'Post' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int(),
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $isAdmin = $this->auth->isAdmin();
        $user    = $this->auth->getLoggedInUserProfile();
        $this->postService->removePost( $args['id'], $user, $isAdmin, true );

        return [
                'id' => $args['id']
        ];
    }

}
