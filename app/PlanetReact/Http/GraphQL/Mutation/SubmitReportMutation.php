<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Report\ReportService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class SubmitReportMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'submitReport'
    ];

    protected $reportService;

    public function __construct( array $attributes = [], ReportService $reportService ) {
        parent::__construct( $this->attributes );

        $this->reportService = $reportService;
    }

    public function type() {
        return GraphQL::type( 'Report' );
    }

    public function args() {
        return [

                'id'           => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],
                'content_id'   => [
                        'name'  => 'content_id',
                        'type'  => Type::int(),
                        'rules' => [
                                'required'
                        ]
                ],
                'title'        => [
                        'name'  => 'title',
                        'type'  => Type::string(),
                        'rules' => [
                                'required'
                        ]
                ],
                'url'          => [
                        'name'  => 'url',
                        'type'  => Type::string(),
                        'rules' => [
                                'required'
                        ]
                ],
                'reason'       => [
                        'name'  => 'reason',
                        'type'  => Type::string(),
                        'rules' => [
                                'required'
                        ]
                ],
                'other_reason' => [
                        'name' => 'other_reason',
                        'type' => Type::string()
                ],
        ];
    }

    public function resolve( $root, $args ) {

        $report = $this->reportService->save( $args );
        return $report;
    }

}
