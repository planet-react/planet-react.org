<?php

namespace App\PlanetReact\Http\GraphQL\Mutation;

use App\PlanetReact\Domain\Subscription\SubscriptionService;
use GraphQL;
use GraphQL\Type\Definition\Type;

class ToggleSubscriptionMutation extends BaseMutation {

    protected $attributes = [
            'name' => 'toggleSubscription'
    ];

    protected $subscriptionService;

    public function __construct( array $attributes = [], SubscriptionService $subscriptionService ) {
        parent::__construct( $this->attributes );

        $this->subscriptionService = $subscriptionService;
    }

    public function type() {
        return GraphQL::type( 'Subscription' );
    }

    public function args() {
        return [
                'id'    => [
                        'name' => 'id',
                        'type' => Type::int(),
                ],
                'type'  => [
                        'name'  => 'type',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ],
                'value' => [
                        'name'  => 'value',
                        'type'  => Type::string(),
                        'rules' => [ 'required' ]
                ]
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        return $this->subscriptionService->toggle( $args, $user );
    }

}
