<?php

namespace App\PlanetReact\Http\GraphQL\Query\Auth0;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Services\Auth0\Api\Auth0Connections;
use GraphQL;
use GraphQL\Type\Definition\Type;

class Auth0ConnectionsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'auth0connections'
    ];

    protected $auth0Connections;

    public function __construct( array $attributes = [], Auth0Connections $auth0Connections ) {
        parent::__construct( $this->attributes );

        $this->auth0Connections = $auth0Connections;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Auth0Connection' ) );
    }

    public function args() {
        return [];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        return $this->auth0Connections->getConnections();
    }

}