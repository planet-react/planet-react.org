<?php

namespace App\PlanetReact\Http\GraphQL\Query\Auth0;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Services\Auth0\Api\Auth0Stats;
use GraphQL;
use GraphQL\Type\Definition\Type;

class Auth0DailyStatsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'auth0DailyStats'
    ];

    protected $auth0Stats;

    public function __construct( array $attributes = [], Auth0Stats $auth0Stats ) {
        parent::__construct( $this->attributes );

        $this->auth0Stats = $auth0Stats;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Auth0DailyStat' ) );
    }

    public function args() {
        return [
                'from' => [
                        'name' => 'from',
                        'type' => Type::string()
                ],
                'to'   => [
                        'name' => 'to',
                        'type' => Type::string()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        // Just to make it more convenient for the ui
        $from = str_replace( '-', '', $args['from'] );
        $to   = str_replace( '-', '', $args['to'] );

        return $this->auth0Stats->getDailyStats( $from, $to );
    }
}