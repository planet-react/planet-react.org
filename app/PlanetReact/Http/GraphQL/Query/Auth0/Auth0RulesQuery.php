<?php

namespace App\PlanetReact\Http\GraphQL\Query\Auth0;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Services\Auth0\Api\Auth0Rules;
use GraphQL;
use GraphQL\Type\Definition\Type;

class Auth0RulesQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'auth0rules'
    ];

    protected $auth0Rules;

    public function __construct( array $attributes = [], Auth0Rules $auth0Rules ) {
        parent::__construct( $this->attributes );

        $this->auth0Rules = $auth0Rules;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Auth0Rule' ) );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        return $this->auth0Rules->getRules();
    }

}