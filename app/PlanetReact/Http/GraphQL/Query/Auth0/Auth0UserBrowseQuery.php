<?php

namespace App\PlanetReact\Http\GraphQL\Query\Auth0;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Services\Auth0\Api\Auth0Users;
use GraphQL;
use GraphQL\Type\Definition\Type;

class Auth0UserBrowseQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'auth0UserBrowse'
    ];

    protected $auth0Users;

    public function __construct( array $attributes = [], Auth0Users $auth0Users ) {
        parent::__construct( $this->attributes );

        $this->auth0Users = $auth0Users;
    }

    public function type() {
        return GraphQL::type( 'Auth0BrowseUsers' );
    }

    public function args() {
        return [
                'page'          => [
                        'name' => 'page',
                        'type' => Type::int(),
                ],
                'q'          => [
                        'name' => 'q',
                        'type' => Type::string(),
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        $page  = $this->getArg( 'page', $args, 0 );
        $query = $this->getArg( 'q', $args, '' );

        return $this->auth0Users->getUsers( 50, $page, $query );
    }

}