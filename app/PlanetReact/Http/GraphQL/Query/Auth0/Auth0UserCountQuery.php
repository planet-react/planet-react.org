<?php

namespace App\PlanetReact\Http\GraphQL\Query\Auth0;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Services\Auth0\Api\Auth0Users;
use GraphQL;

class Auth0UserCountQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'auth0UserCount'
    ];

    protected $auth0Users;

    public function __construct( array $attributes = [], Auth0Users $auth0Users ) {
        parent::__construct( $this->attributes );

        $this->auth0Users = $auth0Users;
    }

    public function type() {
        return GraphQL::type( 'Count' );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        return [ 'count' => $this->auth0Users->getUserCount() ];
    }

}