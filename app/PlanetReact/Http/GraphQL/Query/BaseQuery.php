<?php

namespace App\PlanetReact\Http\GraphQL\Query;

use App\PlanetReact\Http\Auth0\Auth0Authentication;
use Folklore\GraphQL\Support\Query;

class BaseQuery extends Query {

    protected $auth;

    public function __construct( $attributes = [] ) {
        parent::__construct( $attributes );
        $this->auth = new Auth0Authentication();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param int $perPage
     * @param int $page
     * @return array [
     * @type array $meta
     * @type \Illuminate\Contracts\Pagination\LengthAwarePaginator $data
     * ]
     */
    protected function createPayloadWithPaginationData( $builder, $page, $perPage = 10 ) {
        $paginator = $builder->paginate( $perPage, [ '*' ], 'page', $page );
        return [
                'pagination' => [
                        'total'          => $paginator->total(),
                        'per_page'       => $paginator->perPage(),
                        'current_page'   => $paginator->currentPage(),
                        'last_page'      => $paginator->lastPage(),
                        'from'           => $paginator->firstItem(),
                        'to'             => $paginator->lastItem(),
                        'has_more_pages' => $paginator->hasMorePages(),
                ],
                'data'       => $paginator
        ];
    }

    protected function getArg( $key, $args, $default = null ) {
        return isset( $args[$key] ) ? $args[$key] : $default;
    }

}