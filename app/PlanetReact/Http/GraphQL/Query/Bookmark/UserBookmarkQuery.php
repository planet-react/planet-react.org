<?php

namespace App\PlanetReact\Http\GraphQL\Query\Bookmark;

use App\PlanetReact\Domain\Bookmark\BookmarkService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class UserBookmarkQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'userBookmark'
    ];

    protected $bookmarkService;

    public function __construct( array $attributes = [], BookmarkService $bookmarkService ) {
        parent::__construct( $this->attributes );

        $this->bookmarkService = $bookmarkService;

    }

    public function type() {
        return GraphQL::type( 'Bookmark' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        return $this->bookmarkService->getBookmarkForUser( $user, $args['id'] );
    }

}