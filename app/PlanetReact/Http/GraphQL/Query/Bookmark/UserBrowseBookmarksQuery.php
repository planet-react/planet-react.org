<?php

namespace App\PlanetReact\Http\GraphQL\Query\Bookmark;

use App\PlanetReact\Domain\Bookmark\BookmarkService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class UserBrowseBookmarksQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'userBrowseBookmarks'
    ];

    protected $bookmarkService;

    public function __construct( array $attributes = [], BookmarkService $bookmarkService ) {
        parent::__construct( $this->attributes );

        $this->bookmarkService = $bookmarkService;
    }

    public function type() {
        return GraphQL::type( 'BookmarkBrowse' );
    }

    public function args() {
        return [
                'search' => [
                        'name' => 'search',
                        'type' => Type::string()
                ],
                'page'   => [
                        'name' => 'page',
                        'type' => Type::int()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        if ( ! $user ) {
            return null;
        }

        $bookmarks = $this->bookmarkService->getBookmarksForUser( $user->getUserId(), $args['search'] );

        return $this->createPayloadWithPaginationData( $bookmarks, $args['page'], 30 );
    }

}