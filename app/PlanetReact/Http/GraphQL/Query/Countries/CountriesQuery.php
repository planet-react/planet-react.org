<?php

namespace App\PlanetReact\Http\GraphQL\Query\Countries;

use App\PlanetReact\Domain\Countries\CountriesService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class CountriesQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'countries'
    ];

    protected $countriesService;

    public function __construct( array $attributes = [], CountriesService $countriesService ) {
        parent::__construct( $this->attributes );
        $this->countriesService = $countriesService;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Country' ) );
    }

    public function args() {
        return [];
    }

    public function resolve( $root, $args ) {
        return $this->countriesService->createCountriesList();
    }

}