<?php

namespace App\PlanetReact\Http\GraphQL\Query\Event;

use App\PlanetReact\Domain\Event\EventService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class BrowseEventsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'browseEvents'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );

        $this->eventService = $eventService;
    }

    public function type() {
        return GraphQL::type( 'EventBrowse' );
    }

    public function args() {
        return [
                'type'             => [
                        'name' => 'type',
                        'type' => Type::int()
                ],
                'orderBy'          => [
                        'name' => 'orderBy',
                        'type' => Type::string()
                ],
                'orderByDirection' => [
                        'name' => 'orderByDirection',
                        'type' => Type::string()
                ],
                'page'             => [
                        'name' => 'page',
                        'type' => Type::int()
                ],
                'past'             => [
                        'name' => 'past',
                        'type' => Type::boolean()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $events = $this->eventService->getEvents( $args );

        return $this->createPayloadWithPaginationData( $events, $args['page'], 5 );
    }

}