<?php

namespace App\PlanetReact\Http\GraphQL\Query\Event;

use App\PlanetReact\Domain\Event\EventService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class EventQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'event'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $service ) {
        parent::__construct( $this->attributes );

        $this->eventService = $service;
    }

    public function type() {
        return GraphQL::type( 'Event' );
    }

    public function args() {
        return [
                'id' => [
                        'name' => 'id',
                        'type' => Type::int()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        // Fetch event no matter what active status.
        $event = $this->eventService->getEvent( $args['id'], true );

        // Return event if logged in user is the owner.
        if ( $user && $user->getUserId() == $event->user_id ) {
            return $event;
        }
        else {
            // Others should only read the active event.
            return $event->active == 1 ? $event : null;
        }

    }

}