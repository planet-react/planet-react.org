<?php

namespace App\PlanetReact\Http\GraphQL\Query\Event;

use App\PlanetReact\Domain\Event\EventService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class MyEventsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'myEvents'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );
        $this->eventService = $eventService;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Event' ) );
    }

    public function args() {
        return [];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        if ( ! $user ) {
            return null;
        }

        $events = $this->eventService->getEventsForUser( $user->getUserId() );

        return $events->get();
    }

}