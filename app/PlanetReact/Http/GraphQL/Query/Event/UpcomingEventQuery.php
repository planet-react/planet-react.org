<?php

namespace App\PlanetReact\Http\GraphQL\Query\Event;

use App\PlanetReact\Domain\Event\EventService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;

class UpcomingEventQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'upcomingEvent'
    ];

    protected $eventService;

    public function __construct( array $attributes = [], EventService $eventService ) {
        parent::__construct( $this->attributes );

        $this->eventService = $eventService;
    }

    public function type() {
        return GraphQL::type( 'Event' );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        return $this->eventService->getUpcomingEvent();
    }

}