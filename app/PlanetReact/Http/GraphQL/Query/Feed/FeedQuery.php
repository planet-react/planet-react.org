<?php

namespace App\PlanetReact\Http\GraphQL\Query\Feed;

use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class FeedQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'feed'
    ];

    protected $feedService;

    public function __construct( array $attributes = [], FeedService $feedService ) {
        parent::__construct( $this->attributes );

        $this->feedService = $feedService;
    }

    public function type() {
        return GraphQL::type( 'Feed' );
    }

    public function args() {
        return [
                'slug' => [
                        'name' => 'slug',
                        'type' => Type::string()
                ],
                'includeTags' => [
                        'name' => 'includeTags',
                        'type' => Type::boolean()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $feedService       = new FeedService;
        $includeUnapproved = $this->auth->isAdmin();

        return $feedService->getFeedBySlug( $args['slug'], $includeUnapproved );
    }

}