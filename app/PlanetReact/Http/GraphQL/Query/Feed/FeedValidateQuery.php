<?php

namespace App\PlanetReact\Http\GraphQL\Query\Feed;

use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class FeedValidateQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'feedValidate'
    ];

    protected $feedService;

    public function __construct( array $attributes = [], FeedService $feedService ) {
        parent::__construct( $this->attributes );

        $this->feedService = $feedService;
    }

    public function type() {
        return GraphQL::type( 'FeedValidatorResult' );
    }

    public function args() {
        return [
                'feed_url'              => [
                        'name' => 'feed_url',
                        'type' => Type::string()
                ],
                '_skipExistsCheck' => [
                        'name' => '_skipExistsCheck',
                        'type' => Type::boolean()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $checkExists = ! $args['_skipExistsCheck'];

        return $this->feedService->validateFeedUrl( $args['feed_url'], $checkExists );
    }

}