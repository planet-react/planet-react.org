<?php

namespace App\PlanetReact\Http\GraphQL\Query\Feed;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedApprovedScope;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class FeedsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'feeds'
    ];

    public function type() {
        return Type::listOf( GraphQL::type( 'Feed' ) );
    }

    public function args() {
        return [
                'id'               => [ 'name' => 'id', 'type' => Type::int() ],
                'name'             => [ 'name' => 'name', 'type' => Type::string() ],
                'slug'             => [ 'name' => 'slug', 'type' => Type::string() ],
                'limit'            => [ 'name' => 'limit', 'type' => Type::int() ],
                'orderBy'          => [ 'name' => 'orderBy', 'type' => Type::string() ],
                'orderByDirection' => [ 'name' => 'orderByDirection', 'type' => Type::string() ],

        ];
    }

    public function resolve( $root, $args ) {

        // @todo: move to FeedService

        $feeds = Feed::query();

        if ( $this->auth->isAdmin() ) {
            $feeds->withoutGlobalScope( FeedApprovedScope::class );
        }

        if ( isset( $args['id'] ) ) {
            $feeds->where( 'id', $args['id'] );
        }

        if ( isset( $args['slug'] ) ) {
            $feeds->where( 'slug', $args['slug'] );
        }

        if ( isset( $args['orderBy'] ) ) {
            $direction = isset( $args['orderByDirection'] ) ? $args['orderByDirection'] : 'desc';
            $feeds->orderBy( $args['orderBy'], $direction );
        }

        if ( isset( $args['limit'] ) ) {
            $feeds->limit( $args['limit'] );
        }

        return $feeds->get();
    }

}