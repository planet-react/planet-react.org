<?php

namespace App\PlanetReact\Http\GraphQL\Query\Feed;

use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class UserFeedsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'userFeeds'
    ];

    protected $feedService;

    public function __construct( array $attributes = [], FeedService $feedService ) {
        parent::__construct( $this->attributes );
        $this->feedService = $feedService;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Feed' ) );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        $auth0User = $this->auth->getLoggedInUserProfile();
        return $this->feedService->getFeedsForUser( $auth0User );
    }

}