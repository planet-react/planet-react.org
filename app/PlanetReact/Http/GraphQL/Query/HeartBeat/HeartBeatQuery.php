<?php

namespace App\PlanetReact\Http\GraphQL\Query\HeartBeat;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use File;
use GraphQL;

class HeartBeatQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'heartBeat'
    ];

    public function type() {
        return GraphQL::type( 'HeartBeat' );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        $appLastUpdatedAt    = File::lastModified( resource_path( 'views/index.blade.php' ) );
        $isAggregatorRunning = File::exists( config( 'planet.aggregator.lock_file' ) );
        $latestPost          = Post::orderBy( 'pubdate', 'desc' )->first();
        $latestPostId        = $latestPost ? $latestPost->id : null;

        return [
                'timestamp'           => time(),
                'appLastUpdatedAt'    => $appLastUpdatedAt,
                'isAggregatorRunning' => $isAggregatorRunning,
                'latestPostId'        => $latestPostId,
        ];
    }

}