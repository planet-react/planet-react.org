<?php

namespace App\PlanetReact\Http\GraphQL\Query\Post;

use App\PlanetReact\Domain\Post\PostService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class BrowsePostsQuery extends BaseQuery {

    const DEFAULT_LIMIT = 100;
    const DEFAULT_ORDER_BY = 'desc';

    protected $attributes = [
            'name' => 'browsePosts'
    ];

    protected $postService;

    public function __construct( array $attributes = [], PostService $postService ) {
        parent::__construct( $this->attributes );

        $this->postService = $postService;
    }

    public function type() {
        return GraphQL::type( 'PostBrowse' );
    }

    public function args() {
        return [
                'id'               => [ 'name' => 'id', 'type' => Type::int() ],
                'search'           => [ 'name' => 'search', 'type' => Type::string() ],
                'feedId'           => [ 'name' => 'feedId', 'type' => Type::int() ],
                'slug'             => [ 'name' => 'slug', 'type' => Type::string() ],
                'lang'             => [ 'name' => 'lang', 'type' => Type::string() ],
                'pubdateYear'      => [ 'name' => 'pubdateYear', 'type' => Type::int() ],
                'pubdateMonth'     => [ 'name' => 'pubdateMonth', 'type' => Type::int() ],
                'orderBy'          => [ 'name' => 'orderBy', 'type' => Type::string() ],
                'orderByDirection' => [ 'name' => 'orderByDirection', 'type' => Type::string() ],
                'limit'            => [ 'name' => 'limit', 'type' => Type::int() ],
                'page'             => [ 'name' => 'page', 'type' => Type::int()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user  = $this->auth->getLoggedInUserProfile();
        $posts = $this->postService->getPosts( $args, $user );

        return $this->createPayloadWithPaginationData( $posts, $args['page'], 10 );
    }

}