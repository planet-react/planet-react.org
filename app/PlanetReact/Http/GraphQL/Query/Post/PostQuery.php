<?php

namespace App\PlanetReact\Http\GraphQL\Query\Post;

use App\PlanetReact\Domain\Post\PostService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class PostQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'post'
    ];

    protected $postService;

    public function __construct( array $attributes = [], PostService $postService ) {
        parent::__construct( $this->attributes );

        $this->postService = $postService;
    }

    public function type() {
        return GraphQL::type( 'Post' );
    }

    public function args() {
        return [
                'id' => [ 'name' => 'id', 'type' => Type::int() ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        return $this->postService->getPost($args['id']);
    }

}