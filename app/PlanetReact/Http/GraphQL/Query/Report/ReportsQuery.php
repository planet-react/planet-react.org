<?php

namespace App\PlanetReact\Http\GraphQL\Query\Report;

use App\PlanetReact\Domain\Report\ReportService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class ReportsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'reports'
    ];

    protected $reportService;

    public function __construct( array $attributes = [], ReportService $reportService ) {
        parent::__construct( $this->attributes );

        $this->reportService = $reportService;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Report' ) );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {
        $this->auth->authorize( [ 'admin' ] );

        return $this->reportService->getReports();
    }

}