<?php

namespace App\PlanetReact\Http\GraphQL\Query\Stats;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use App\PlanetReact\Http\GraphQL\Type\StatsType;
use GraphQL;

class StatsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'stats'
    ];

    public function type() {
        return GraphQL::type( 'Stats' );
    }

    public function args() {
        return [
        ];
    }

    public function resolve( $root, $args ) {

        $this->auth->authorize( [ 'admin' ] );

        return new StatsType;
    }

}