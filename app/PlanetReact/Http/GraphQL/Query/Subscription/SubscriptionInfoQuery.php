<?php

namespace App\PlanetReact\Http\GraphQL\Query\Subscription;

use App\PlanetReact\Domain\Subscription\SubscriptionService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class SubscriptionInfoQuery extends BaseQuery {

    protected $subscriptionService;

    protected $attributes = [
            'name' => 'getSubscriptionInfo'
    ];

    public function __construct( array $attributes = [], SubscriptionService $service ) {
        parent::__construct( $this->attributes );
        $this->subscriptionService = $service;
    }

    public function type() {
        return GraphQL::type( 'Subscription' );
    }

    public function args() {
        return [
                'id'    => [
                        'name' => 'id',
                        'type' => Type::int()
                ],
                'type'  => [
                        'name' => 'type',
                        'type' => Type::string()
                ],
                'value' => [
                        'name' => 'value',
                        'type' => Type::string()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        if ( ! $user ) {
            return null;
        }

        $type  = $this->getArg( 'type', $args, '' );
        $value = $this->getArg( 'value', $args, '' );

        $subscription = $this->subscriptionService
                ->getSubscriptionInfo( $type, $value, $user );

        return $subscription;
    }

}