<?php

namespace App\PlanetReact\Http\GraphQL\Query\Subscription;

use App\PlanetReact\Domain\Subscription\SubscriptionService;
use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;

class UserSubscriptionsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'userSubscriptions'
    ];

    protected $subscriptionService;

    public function __construct( array $attributes = [], SubscriptionService $subscriptionService ) {
        parent::__construct( $this->attributes );

        $this->subscriptionService = $subscriptionService;
    }

    public function type() {
        return Type::listOf( GraphQL::type( 'Subscription' ) );
    }

    public function args() {
        return [
                'orderBy'          => [
                        'name' => 'orderBy',
                        'type' => Type::string()
                ],
                'orderByDirection' => [
                        'name' => 'orderByDirection',
                        'type' => Type::string()
                ],
        ];
    }

    public function resolve( $root, $args ) {
        $user = $this->auth->getLoggedInUserProfile();

        if ( ! $user ) {
            return null;
        }

        $orderBy          = $this->getArg( 'orderBy', $args, 'created_at' );
        $orderByDirection = $this->getArg( 'orderByDirection', $args, 'desc' );

        $subscriptions = $this->subscriptionService
                ->getAllSubscriptionsForUser( $orderBy, $orderByDirection, $user );

        return $subscriptions;
    }

}