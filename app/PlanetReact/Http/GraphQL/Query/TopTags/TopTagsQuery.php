<?php

namespace App\PlanetReact\Http\GraphQL\Query\TopTags;

use App\PlanetReact\Http\GraphQL\Query\BaseQuery;
use DB;
use GraphQL;
use GraphQL\Type\Definition\Type;

class TopTagsQuery extends BaseQuery {

    protected $attributes = [
            'name' => 'topTags'
    ];

    public function type() {
        return Type::listOf( GraphQL::type( 'Tag' ) );
    }

    public function args() {
        return [
        ];
    }

    //@todo: refactor
    public function resolve( $root, $args ) {

        $topTags = DB::select( DB::raw( "
SELECT
  ts.tag_slug,
  count(ts.tag_slug) as `tag_count`
FROM planet_react.posts p
  LEFT JOIN planet_react.tagging_tagged ts ON p.id = ts.taggable_id
WHERE MONTH(p.pubdate) = MONTH(CURRENT_DATE())
      AND ts.tag_slug != 'react'
      AND ts.tag_slug != 'reactjs'
      AND ts.tag_slug != 'react-js'
      AND ts.tag_slug != 'javascript'
GROUP BY ts.tag_slug ORDER BY `tag_count` DESC LIMIT 5;
" ) );

        return $topTags;

    }

}