<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;

class Auth0BrowseUsersType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0BrowseUsers'
    ];

    public function fields() {
        return [
                'start'  => [
                        'type' => Type::int(),
                ],
                'limit'  => [
                        'type' => Type::int(),
                ],
                'length' => [
                        'type' => Type::int(),
                ],
                'users'  => [
                        'type' => Type::listOf( GraphQL::type( 'Auth0User' ) ),
                ]
        ];
    }

    protected function resolveUsersField( $data, $args ) {
        return $data->users;
    }


}