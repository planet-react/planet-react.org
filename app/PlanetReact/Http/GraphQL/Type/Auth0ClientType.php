<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class Auth0ClientType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0ApiClient'
    ];

    public function fields() {
        return [
                'client_id'   => [
                        'type' => Type::string(),
                ],
                'name'        => [
                        'type' => Type::string(),
                ],
                'description' => [
                        'type' => Type::string(),
                ],
        ];
    }

}