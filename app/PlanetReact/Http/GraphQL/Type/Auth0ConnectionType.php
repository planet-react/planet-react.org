<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class Auth0ConnectionType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0Connection'
    ];

    public function fields() {
        return [
                'id'              => [
                        'type' => Type::string(),
                ],
                'name'            => [
                        'type' => Type::string(),
                ],
                'strategy'        => [
                        'type' => Type::string(),
                ],
                'enabled_clients' => [
                        'type' => Type::listOf( Type::string() ),
                ],
        ];
    }

}