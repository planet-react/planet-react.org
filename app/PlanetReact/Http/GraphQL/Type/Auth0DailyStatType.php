<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class Auth0DailyStatType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0DailyStat'
    ];

    public function fields() {
        return [
                'date'             => [
                        'type' => Type::string(),
                ],
                'logins'           => [
                        'type' => Type::int(),
                ],
                'signups'          => [
                        'type' => Type::int(),
                ],
                'leaked_passwords' => [
                        'type' => Type::int(),
                ],
        ];
    }

}