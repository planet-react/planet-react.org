<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class Auth0RuleType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0Rule'
    ];

    public function fields() {
        return [
                'name'    => [
                        'type' => Type::string(),
                ],
                'id'      => [
                        'type' => Type::string(),
                ],
                'enabled' => [
                        'type' => Type::boolean(),
                ],
                'script'  => [
                        'type' => Type::string(),
                ],
                'order'   => [
                        'type' => Type::int(),
                ],
                'stage'   => [
                        'type' => Type::string(),
                ]
        ];
    }

}