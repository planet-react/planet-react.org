<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class Auth0UserType extends BaseType {

    protected $attributes = [
            'name' => 'Auth0User'
    ];

    public function fields() {
        return [
                'email'          => [
                        'type' => Type::string(),
                ],
                'email_verified' => [
                        'type' => Type::boolean(),
                ],
                'name'           => [
                        'type' => Type::string(),
                ],
                'picture'        => [
                        'type' => Type::string(),
                ],
                'updated_at'     => [
                        'type' => Type::string(),
                ],
                'user_id'        => [
                        'type' => Type::string(),
                ],
                'nickname'       => [
                        'type' => Type::string(),
                ],
                'created_at'     => [
                        'type' => Type::string(),
                ],
                'blocked'        => [
                        'type' => Type::boolean(),
                ],
                'roles'          => [
                        'type' => Type::listOf( Type::string() ),
                ],
        ];
    }

    protected function resolveEmailField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getEmail();
    }

    protected function resolveEmailVerifiedField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getEmailVerified();
    }

    protected function resolveNameField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getName();
    }

    protected function resolvePictureField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getPicture();
    }

    protected function resolveCreatedAtField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getCreatedAt();
    }

    protected function resolveUpdatedAtField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getUpdatedAt();
    }

    protected function resolveUserIdField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getUserId();
    }

    protected function resolveNicknameField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getNickname();
    }

    protected function resolveBlockedField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getBlocked();
    }

    protected function resolveRolesField( $auth0UserProfile, $args ) {
        return $auth0UserProfile->getRoles();
    }


}