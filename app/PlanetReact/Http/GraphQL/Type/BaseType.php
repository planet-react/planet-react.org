<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use Folklore\GraphQL\Support\Type as GraphQLType;

class BaseType extends GraphQLType {

    protected function resolveCreatedAtField( $model, $args ) {
        return $model->created_at->toDateTimeString();
    }

    protected function resolveUpdatedAtField( $model, $args ) {
        return $model->updated_at->toDateTimeString();
    }

    protected function resolveDeletedAtField( $model, $args ) {
        return $model->deleted_at->toDateTimeString();
    }

}