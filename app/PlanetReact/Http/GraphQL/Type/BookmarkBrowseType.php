<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;

class BookmarkBrowseType extends BaseType {

    protected $attributes = [
            'name'        => 'BookmarkBrowse',
            'description' => 'Bookmarks with pagination'
    ];

    public function fields() {
        return [
                'pagination' => [
                        'type' => GraphQL::type( 'Pagination' ),
                ],
                'data'       => [
                        'type' => Type::listOf( GraphQL::type( 'Bookmark' ) ),
                ],
        ];
    }

}