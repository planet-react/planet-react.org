<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;

class BookmarkType extends BaseType {

    protected $attributes = [
            'name'        => 'Bookmark',
            'description' => 'A Bookmark'
    ];

    public function fields() {
        return [
                'id'         => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'user_id'    => [
                        'type' => Type::string(),
                ],
                'title'      => [
                        'type' => Type::string(),
                ],
                'url'        => [
                        'type' => Type::string(),
                ],
                'notes'      => [
                        'type' => Type::string(),
                ],
                'tags'       => [
                        'type' => Type::listOf( GraphQL::type( 'Tag' ) ),
                ],
                'host'       => [
                        'type' => Type::string(),
                ],
                'created_at' => [
                        'type' => Type::string(),
                ],
                'updated_at' => [
                        'type' => Type::string(),
                ]
        ];
    }

    protected function resolveTagsField( $bookmark, $args ) {
        return $bookmark->tagged;
    }

    protected function resolveHostField( $bookmark, $args ) {
        return parse_url( $bookmark->url, PHP_URL_HOST );
    }

}