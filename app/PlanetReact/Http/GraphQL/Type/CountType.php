<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class CountType extends BaseType {

    protected $attributes = [
            'name'        => 'Count',
            'description' => 'A generic count type'

    ];

    public function fields() {
        return [
                'count' => [
                        'type' => Type::int(),
                ],
        ];
    }

}