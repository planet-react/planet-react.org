<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class CountryType extends BaseType {

    protected $attributes = [
            'name'        => 'Country',
            'description' => 'Country information'
    ];

    public function fields() {
        return [
                'commonName' => [
                        'type' => Type::string(),
                ],
                'cca2'       => [
                        'type' => Type::string(),
                ]
        ];
    }

}