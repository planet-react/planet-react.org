<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;

class EventBrowseType extends BaseType {

    protected $attributes = [
            'name'        => 'EventBrowse',
            'description' => 'Events with pagination'
    ];

    public function fields() {
        return [
                'pagination' => [
                        'type' => GraphQL::type( 'Pagination' ),
                ],
                'data'       => [
                        'type' => Type::listOf( GraphQL::type( 'Event' ) ),
                ],
        ];
    }

}