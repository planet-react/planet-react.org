<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use App\PlanetReact\Domain\Bookmark\Bookmark;
use App\PlanetReact\Http\Auth0\Auth0Authentication;
use GraphQL\Type\Definition\Type;

class EventType extends BaseType {

    protected $auth;

    protected $attributes = [
            'name'        => 'Event',
            'description' => 'An Event'
    ];

    public function __construct( $attributes = [], Auth0Authentication $auth ) {
        parent::__construct( $this->attributes );

        $this->auth = $auth;
    }

    public function fields() {
        return [
                'id'           => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'user_id'      => [
                        'type' => Type::string(),
                ],
                'active'       => [
                        'type' => Type::int(),
                ],
                'type'         => [
                        'type' => Type::int(),
                ],
                'name'         => [
                        'type' => Type::string(),
                ],
                'description'  => [
                        'type' => Type::string(),
                ],
                'start_date'   => [
                        'type' => Type::string(),
                ],
                'end_date'     => [
                        'type' => Type::string(),
                ],
                'timezone'     => [
                        'type' => Type::string(),
                ],
                'location'     => [
                        'type' => Type::string(),
                ],
                'country'      => [
                        'type' => Type::string(),
                ],
                'website_link' => [
                        'type' => Type::string(),
                ],
                'twitter_link' => [
                        'type' => Type::string(),
                ],
                'bookmarkId'   => [
                        'type' => Type::int(),
                ],
                'created_at'   => [
                        'type' => Type::string(),
                ],
                'updated_at'   => [
                        'type' => Type::string(),
                ],
        ];
    }

    protected function resolveBookmarkIdField( $event, $args ) {
        // @todo:
        // should we find another way to get the user info than using
        // Auth0Authentication? It relies on a http request.
        $user = $this->auth->getLoggedInUserProfile();
        if ( ! $user ) {
            return 0;
        }

        $found = Bookmark::where( [
                [ 'user_id', $user->getUserId() ],
                [ 'url', $event->website_link ],
        ] )->first();

        return $found ? $found->id : null;
    }

}