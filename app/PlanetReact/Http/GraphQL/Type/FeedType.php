<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Domain\Post\PostService;
use App\PlanetReact\Http\Auth0\Auth0Authentication;
use GraphQL;
use GraphQL\Type\Definition\Type;

class FeedType extends BaseType {

    protected $auth;
    protected $feedService;
    protected $postService;

    protected $attributes = [
            'name'        => 'Feed',
            'description' => 'A feed'
    ];

    public function __construct( $attributes = [], Auth0Authentication $auth ) {
        parent::__construct( $this->attributes );

        $this->auth        = $auth;
        $this->feedService = new FeedService();
        $this->postService = new PostService();
    }

    public function fields() {
        return [
                'id'               => [
                        'type' => Type::int(),
                ],
                'source_type'      => [
                        'type' => Type::int(),
                ],
                'has_owner'        => [
                        'type' => Type::int(),
                ],
                'user_id'          => [
                        'type' => Type::string(),
                ],
                'name'             => [
                        'type' => Type::string(),
                ],
                'slug'             => [
                        'type' => Type::string(),
                ],
                'feed_url'         => [
                        'type' => Type::string(),
                ],
                'website_url'              => [
                        'type' => Type::string(),
                ],
                'twitter_url'      => [
                        'type' => Type::string(),
                ],
                'repo_url'         => [
                        'type' => Type::string(),
                ],
                'image'            => [
                        'type' => Type::string(),
                ],
                'description'      => [
                        'type' => Type::string(),
                ],
                'lang'             => [
                        'type' => Type::string(),
                ],
                'comment'          => [
                        'type' => Type::string(),
                ],
                'approved'         => [
                        'type' => Type::int(),
                ],
                'failed'           => [
                        'type' => Type::boolean(),
                ],
                'total_post_count' => [
                        'type' => Type::int(),
                ],
                'month_post_count' => [
                        'type' => Type::int(),
                ],
                'created_at'       => [
                        'type' => Type::string(),
                ],
                'updated_at'       => [
                        'type' => Type::string(),
                ],
                'last_fetched_at'  => [
                        'type' => Type::string(),
                ],
                'posts'            => [
                        'type' => Type::listOf( GraphQL::type( 'Post' ) )
                ],
                'tags'             => [
                        'type' => Type::listOf( GraphQL::type( 'Tag' ) )
                ],
                'user_is_owner'    => [
                        'type' => Type::boolean()
                ]
        ];
    }

    protected function resolveTotalPostCountField( $feed, $args ) {
        return $feed->hasMany( 'App\PlanetReact\Domain\Post\Post' )->where( 'feed_id', '=', $feed->id )->count();
    }

    protected function resolveImageField( $feed, $args ) {
        return $feed->getImage();
    }

    protected function resolveMonthPostCountField( $feed, $args ) {
        return $this->postService->findPosts( $feed->id, null, date( 'Y' ), date( 'm' ) )->count();
    }

    protected function resolvePostsField( $feed, $args ) {
        return $feed->post()->get();
    }

    protected function resolveTagsField( $feed, $args ) {
        return $this->feedService->getTags( $feed->id );
    }

    protected function resolveUserIsOwnerField( $feed, $args ) {
        if ( $this->auth->isAdmin() ) {
            return true;
        }

        $user = $this->auth->getLoggedInUserProfile();
        if ( ! $user ) {
            return false;
        }
        return $feed->user_id == $user->getUserId();
    }

}