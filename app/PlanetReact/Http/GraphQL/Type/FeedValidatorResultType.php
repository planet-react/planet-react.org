<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class FeedValidatorResultType extends BaseType {

    protected $attributes = [
            'name' => 'FeedValidatorResultType'
    ];

    public function fields() {
        return [
                'is_valid'    => [
                        'type' => Type::boolean(),
                ],
                'message'     => [
                        'type' => Type::string(),
                ],
                'name'        => [
                        'type' => Type::string(),
                ],
                'description' => [
                        'type' => Type::string(),
                ],
                'website_url' => [
                        'type' => Type::string(),
                ],
                'source_type' => [
                        'type' => Type::int(),
                ],
        ];
    }

}