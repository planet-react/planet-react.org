<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class FetchCountType extends BaseType {

    protected $attributes = [
            'name' => 'FetchCount'
    ];

    public function fields() {
        return [
                'day'   => [
                        'type' => Type::int(),
                ],
                'count' => [
                        'type' => Type::int(),
                ],
                'label' => [
                        'type' => Type::int(),
                ],

        ];
    }


}