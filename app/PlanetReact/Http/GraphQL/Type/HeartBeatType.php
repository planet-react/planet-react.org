<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class HeartBeatType extends BaseType {

    protected $attributes = [
            'name' => 'HeartBeat'
    ];

    public function fields() {
        return [
                'timestamp'           => [
                        'type' => Type::int(),
                ],
                'appLastUpdatedAt'    => [
                        'type' => Type::int(),
                ],
                'isAggregatorRunning' => [
                        'type' => Type::boolean(),
                ],
                'latestPostId'        => [
                        'type' => Type::int(),
                ],
        ];
    }
}