<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class PaginationType extends BaseType {

    protected $attributes = [
            'name'        => 'Pagination',
            'description' => 'Generic meta info, pagination'
    ];

    public function fields() {
        return [
                'total'          => [
                        'type' => Type::int(),
                ],
                'per_page'       => [
                        'type' => Type::int(),
                ],
                'current_page'   => [
                        'type' => Type::int(),
                ],
                'last_page'      => [
                        'type' => Type::int(),
                ],
                'from'           => [
                        'type' => Type::int(),
                ],
                'to'             => [
                        'type' => Type::int(),
                ],
                'has_more_pages' => [
                        'type' => Type::boolean(),
                ],
        ];
    }

}