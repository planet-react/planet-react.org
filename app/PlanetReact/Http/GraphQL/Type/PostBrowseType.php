<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;

class PostBrowseType extends BaseType {

    protected $attributes = [
            'name'        => 'PostBrowse',
            'description' => 'Posts with pagination'
    ];

    public function fields() {
        return [
                'pagination' => [
                        'type' => GraphQL::type( 'Pagination' ),
                ],
                'data'       => [
                        'type' => Type::listOf( GraphQL::type( 'Post' ) ),
                ],
        ];
    }

}