<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use App\PlanetReact\Http\Auth0\Auth0Authentication;
use Carbon\Carbon;
use GraphQL;
use GraphQL\Type\Definition\Type;

class PostType extends BaseType {

    protected $auth;

    protected $attributes = [
            'name'        => 'Post',
            'description' => 'A post'
    ];

    public function __construct( $attributes = [], Auth0Authentication $auth ) {
        parent::__construct( $this->attributes );

        $this->auth = $auth;
    }

    public function fields() {
        return [
                'id'                 => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'user_is_owner'      => [
                        'type' => Type::boolean(),
                ],
                'title'              => [
                        'type' => Type::string(),
                ],
                'authors'            => [
                        'type' => Type::string(),
                ],
                'content'            => [
                        'type' => Type::string(),
                ],
                'pubdate'            => [
                        'type' => Type::string(),
                ],
                'pubdate_for_humans' => [
                        'type' => Type::string(),
                ],
                'link'               => [
                        'type' => Type::string(),
                ],
                'enclosure'          => [
                        'type' => Type::string(),
                ],
                'feed_source_type'   => [
                        'type' => Type::int(),
                ],
                'feed_id'            => [
                        'type' => Type::int(),
                ],
                'feed_name'          => [
                        'type' => Type::string(),
                ],
                'feed_slug'          => [
                        'type' => Type::string(),
                ],
                'feed_lang'          => [
                        'type' => Type::string(),
                ],
                'feed_website_url'           => [
                        'type' => Type::string(),
                ],
                'feed_image'         => [
                        'type' => Type::string(),
                ],
                'tags'               => [
                        'type' => Type::listOf( GraphQL::type( 'Tag' ) ),
                ],
                'bookmarkId'         => [
                        'type' => Type::int(),
                ],
                'host'               => [
                        'type' => Type::string(),
                ],
        ];
    }

    protected function resolveUserIsOwnerField( $post, $args ) {
        $user = $this->auth->getLoggedInUserProfile();
        if ( ! $user ) {
            return false;
        }

        return $user->getUserId() == $post->feed_user_id;
    }

    protected function resolvePubdateForHumansField( $post, $args ) {
        return $post->pubdate ? Carbon::createFromTimestamp( strtotime( $post->pubdate ) )->diffForHumans( null, false, true ) : '';
    }

    protected function resolveFeedNameField( $post, $args ) {
        return $post->feed->name;
    }

    protected function resolveFeedSlugField( $post, $args ) {
        return $post->feed->slug;
    }

    protected function resolveContentField( $post, $args ) {
        return $post->content;
    }

    protected function resolveFeedLangField( $post, $args ) {
        return $post->feed->lang;
    }

    protected function resolveFeedSourceTypeField( $post, $args ) {
        return $post->feed->source_type;
    }

    protected function resolveFeedWebsiteUrlField( $post, $args ) {
        return $post->feed->website_url;
    }

    protected function resolveFeedImageField( $post, $args ) {
        return $post->feed->getImage();
    }

    protected function resolveRssFeedUrlField( $post, $args ) {
        return $post->feed->feed_url;
    }

    protected function resolveTagsField( $post, $args ) {
        return $post->tagged;
    }

    protected function resolveBookmarkIdField( $post, $args ) {
        return $post->bookmarkId;
    }

    protected function resolveHostField( $post, $args ) {
        return parse_url( $post->link, PHP_URL_HOST );
    }

}