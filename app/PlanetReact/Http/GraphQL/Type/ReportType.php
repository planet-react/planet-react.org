<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class ReportType extends BaseType {

    protected $attributes = [
            'name'        => 'Report',
            'description' => 'A Report'
    ];

    public function fields() {
        return [
                'id'           => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'content_id'   => [
                        'type' => Type::int(),
                ],
                'title'        => [
                        'type' => Type::string(),
                ],
                'url'          => [
                        'type' => Type::string(),
                ],
                'reason'       => [
                        'type' => Type::string(),
                ],
                'other_reason' => [
                        'type' => Type::string(),
                ],
                'created_at'   => [
                        'type' => Type::string(),
                ],
                'updated_at'   => [
                        'type' => Type::string(),
                ]
        ];
    }

}