<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedApprovedScope;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Report\Report;
use App\PlanetReact\Services\Auth0\Api\Auth0Users;
use DB;
use GraphQL;
use GraphQL\Type\Definition\Type;

class StatsType extends BaseType {

    protected $attributes = [
            'name' => 'StatsType'
    ];

    public function fields() {
        return [
                'total_feeds_count'                => [
                        'type' => Type::int(),
                ],
                'feeds_waiting_for_approval_count' => [
                        'type' => Type::int(),
                ],
                'failed_feeds_count'               => [
                        'type' => Type::int(),
                ],
                'total_posts_count'                => [
                        'type' => Type::int(),
                ],
                'posts_fetched_this_month'         => [
                        'type' => Type::listOf( GraphQL::type( 'FetchCount' ) ),
                ],
                'total_users_registered'           => [
                        'type' => Type::int(),
                ],
                'total_open_reports'               => [
                        'type' => Type::int(),
                ],
        ];
    }

    protected function resolveTotalFeedsCountField( $model, $args ) {
        return Feed::withoutGlobalScope( FeedApprovedScope::class )->count();
    }

    protected function resolveFeedsWaitingForApprovalCountField( $model, $args ) {
        return Feed::withoutGlobalScope( FeedApprovedScope::class )->where( 'approved', 0 )->count();
    }

    protected function resolveFailedFeedsCountField( $model, $args ) {
        return Feed::withoutGlobalScope( FeedApprovedScope::class )->where( 'failed', 1 )->count();
    }

    protected function resolveTotalPostsCountField( $model, $args ) {
        return Post::count();
    }

    protected function resolvePostsFetchedThisMonthField( $model, $args ) {
        $data = DB::select( DB::raw( "
                              SELECT 
                                DATE_FORMAT(fetched_at,'%e') AS `day`, 
                                COUNT(id) AS `count`,
                                COUNT(id) AS `label`
                              FROM posts 
                              WHERE MONTH(fetched_at) = MONTH(NOW()) 
                              GROUP BY `day` 
                              ORDER BY  cast(`day` as unsigned) ASC" ) );

        return $data;
    }

    protected function resolveTotalUsersRegisteredField( $model, $args ) {
        $users = new Auth0Users();
        return $count = $users->getUserCount();
    }

    protected function resolveTotalOpenReportsField( $model, $args ) {
        return Report::count();
    }

}