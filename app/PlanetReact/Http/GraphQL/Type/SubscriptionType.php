<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use App\PlanetReact\Domain\Feed\Feed;
use GraphQL\Type\Definition\Type;

class SubscriptionType extends BaseType {

    protected $attributes = [
            'name'        => 'Subscription',
            'description' => 'A Subscription'
    ];

    public function fields() {
        return [
                'id'               => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'user_id'          => [
                        'type' => Type::string(),
                ],
                'user_email'       => [
                        'type' => Type::string(),
                ],
                'type'             => [
                        'type' => Type::string(),
                ],
                'value'            => [
                        'type' => Type::string(),
                ],
                'feed_name'        => [
                        'type' => Type::string(),
                ],
                'feed_description' => [
                        'type' => Type::string(),
                ],
                'feed_image'       => [
                        'type' => Type::string(),
                ],
                'created_at'       => [
                        'type' => Type::string(),
                ],
                'updated_at'       => [
                        'type' => Type::string(),
                ]
        ];
    }

    protected function resolveFeedImageField( $subscription, $args ) {
        if ( $subscription->type != 'feed' ) {
            return $subscription->feed_image;
        }
        $feed = Feed::find( $subscription->value );
        return $feed ? $feed->getImage() : null;
    }

}