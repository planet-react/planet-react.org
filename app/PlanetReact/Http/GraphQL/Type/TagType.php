<?php

namespace App\PlanetReact\Http\GraphQL\Type;

use GraphQL\Type\Definition\Type;

class TagType extends BaseType {

    protected $attributes = [
            'name'        => 'Tag',
            'description' => 'A tag'
    ];

    public function fields() {
        return [
                'id'       => [
                        'type' => Type::nonNull( Type::int() ),
                ],
                'tag_name' => [
                        'type' => Type::string(),
                ],
                'tag_slug' => [
                        'type' => Type::string(),
                ],
                'tag_count'    => [
                        'type' => Type::int(),
                ],
        ];
    }

}