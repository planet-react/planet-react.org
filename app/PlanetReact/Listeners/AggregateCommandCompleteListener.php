<?php

namespace App\PlanetReact\Listeners;

use App\PlanetReact\Domain\Subscription\SubscriptionService;
use App\PlanetReact\Events\AggregateCommandCompleteEvent;
use App\PlanetReact\Mail\Helpers\UserSubscriptionEmailPostListCreator;
use App\PlanetReact\Mail\SubscriptionUserEmail;
use Illuminate\Support\Collection;
use Mail;

class AggregateCommandCompleteListener {

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var Collection
     */
    private $posts;

    public function __construct( SubscriptionService $service ) {
        $this->subscriptionService = $service;
    }

    public function handle( AggregateCommandCompleteEvent $event ) {
        if ( $event->posts->count() == 0 ) {
            return;
        }

        $this->posts = $event->posts;

        $this->sendSubscriptionEmails();
    }

    public function sendSubscriptionEmails() {

        $subscriptions = $this->subscriptionService->getSubscriptionsForPosts( $this->posts );

        $postList = new UserSubscriptionEmailPostListCreator( $this->posts, $subscriptions );

        $emails = $subscriptions->pluck( 'user_email' )->unique()->all();

        foreach ( $emails as $email ) {

            $data = $postList->build( $email );

            Mail::send( new SubscriptionUserEmail( $email, $data ) );
        }

    }
}