<?php

namespace App\PlanetReact\Listeners;

use App\PlanetReact\Events\FeedReviewedEvent;
use App\PlanetReact\Mail\FeedReviewedEmail;
use Mail;

class FeedReviewedListener {

    public function __construct() {
    }

    public function handle( FeedReviewedEvent $event ) {
        Mail::send( new FeedReviewedEmail( $event->feed, $event->user ) );
    }

}