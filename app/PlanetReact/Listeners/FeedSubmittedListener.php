<?php

namespace App\PlanetReact\Listeners;

use App\PlanetReact\Events\FeedSubmittedEvent;
use App\PlanetReact\Mail\FeedSubmittedAdminEmail;
use App\PlanetReact\Mail\FeedSubmittedUserEmail;
use Mail;

class FeedSubmittedListener {

    public function __construct() {
    }

    public function handle( FeedSubmittedEvent $event ) {
        Mail::send( new FeedSubmittedAdminEmail( $event->feed ) );
        Mail::send( new FeedSubmittedUserEmail( $event->feed, $event->user ) );
    }

}