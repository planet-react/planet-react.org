<?php

namespace App\PlanetReact\Mail;

use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use App\PlanetReact\Domain\Feed\Feed;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

// use Illuminate\Contracts\Queue\ShouldQueue;

class FeedReviewedEmail extends Mailable {
    use Queueable, SerializesModels;

    public $feed;
    public $user;

    public function __construct( Feed $feed, Auth0UserProfile $user ) {
        $this->feed = $feed;
        $this->user = $user;
    }

    public function build() {
        $appName     = config( 'app.name' );
        $appUrl      = config( 'app.url' );
        $planetEmail = config( 'planet.email' );

        $subject = $this->feed->approved ? 'Your feed has been approved' : 'Feed has been rejected';

        return $this
                ->to( $this->user->getEmail() )
                ->subject( $subject )
                ->text( 'emails.feed-reviewed-mail' )->with( [
                        'feed'         => $this->feed,
                        'app_name'     => $appName,
                        'planet_email' => $planetEmail,
                        'app_url'      => $appUrl,
                ] );
    }

}
