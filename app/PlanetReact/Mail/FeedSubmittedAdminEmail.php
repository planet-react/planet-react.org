<?php

namespace App\PlanetReact\Mail;

use App\PlanetReact\Domain\Feed\Feed;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

// use Illuminate\Contracts\Queue\ShouldQueue;

class FeedSubmittedAdminEmail extends Mailable {
    use Queueable, SerializesModels;

    public $feed;

    public function __construct( Feed $feed ) {
        $this->feed = $feed;
    }

    public function build() {
        $subject = 'Feed Submitted: ' . $this->feed->name;

        $appUrl = config( 'app.url' );

        $feedManagerUrl = "{$appUrl}/admin/feedmanager";

        return $this
                ->to( config( 'planet.email' ) )
                ->subject( $subject )
                ->text( 'emails.feed-submitted-admin-mail' )->with( [
                        'feed' => $this->feed,
                        'app_url' => $appUrl,
                        'feed_manager_url' => $feedManagerUrl,
                ] );
    }

}
