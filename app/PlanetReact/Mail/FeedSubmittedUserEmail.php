<?php

namespace App\PlanetReact\Mail;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

// use Illuminate\Contracts\Queue\ShouldQueue;

class FeedSubmittedUserEmail extends Mailable {
    use Queueable, SerializesModels;

    public $feed;
    public $user;

    public function __construct( Feed $feed, Auth0UserProfile $user ) {
        $this->feed = $feed;
        $this->user = $user;
    }

    public function build() {
        $appName = config( 'app.name' );

        $subject = 'Thank you for submitting your feed!';

        $appUrl = config( 'app.url' );

        return $this
                ->to( $this->user->getEmail() )
                ->subject( $subject )
                ->text( 'emails.feed-submitted-user-mail' )->with( [
                        'feed'     => $this->feed,
                        'app_name' => $appName,
                        'app_url'  => $appUrl,
                ] );
    }

}
