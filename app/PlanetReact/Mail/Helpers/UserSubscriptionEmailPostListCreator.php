<?php

namespace App\PlanetReact\Mail\Helpers;

use App\PlanetReact\Domain\Subscription\SubscriptionType;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class UserSubscriptionEmailPostListCreator {

    /**
     * @var Collection|null
     */
    protected $posts = null;

    /**
     * @var Collection|null
     */
    protected $subscriptions = null;

    public function __construct( $posts, $subscriptions ) {
        $this->posts = $posts;

        $this->subscriptions = $subscriptions;
    }

    public function build( $email ) {

        $userSubscriptions = $this->subscriptions->where( 'user_email', '=', $email );

        $result = [
                'email' => $email,
                'feeds' => [],
                'tags'  => [],
        ];

        $feedSubscriptions = $userSubscriptions->where( 'type', '=', SubscriptionType::FEED );

        $feedIds = $feedSubscriptions->pluck( 'value' )->unique()->all();
        if ( count( $feedIds ) > 0 ) {
            foreach ( $feedIds as $feedId ) {
                $posts = $this->posts->where( 'feed_id', $feedId );

                $postData = [];
                foreach ( $posts as $post ) {
                    $postData[] = $this->createPostData( $post );
                }

                $result['feeds'][] = [
                        'feed_name' => $postData[0]['feed_name'],
                        'posts'     => $postData
                ];
            }
        }

        $tagSubscriptions = $userSubscriptions->where( 'type', '=', SubscriptionType::TAG );

        $tags = $tagSubscriptions->pluck( 'value' )->all();
        if ( count( $tags ) > 0 ) {
            foreach ( $tags as $tag ) {
                $posts = $this->posts->filter( function ( $post ) use ( $tag ) {
                    return $post->tags->pluck( 'slug' )->contains( $tag );
                } );

                $postData = [];
                foreach ( $posts as $post ) {
                    $postData[] = $this->createPostData( $post );
                }

                $result['tags'][] = [
                        'tag_slug' => $tag,
                        'posts'    => $postData
                ];
            }
        }

        return $result;
    }

    private function createPostData( $post ) {

        $pubDate = Carbon::parse( $post->pubdate )->format( 'jS M' );

        return [
                'title'     => $post->title,
                'link'      => $post->link,
                'pubdate'   => $pubDate,
                'feed_name' => $post->feed->name,
        ];
    }

}