<?php

namespace App\PlanetReact\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

// use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionUserEmail extends Mailable {
    use Queueable, SerializesModels;

    public $toMail;

    public $data;

    public function __construct( $toMail, $data ) {
        $this->toMail = $toMail;
        $this->data   = $data;
    }

    public function build() {

        $planetUrl = config( 'app.url' );
        $manageUrl = $planetUrl . '/profile/subscriptions';

        $subject = config( 'app.name' ) . ': New links';

        return $this
                ->to( $this->toMail )
                ->subject( $subject )
                ->text( 'emails.subscription-user-mail' )->with( [
                        'email' => $this->toMail,
                        'posts' => $this->data,
                        'planet_url' => $planetUrl,
                        'manage_url' => $manageUrl,
                ] );
    }

    private function createSubject() {
        $subject   = config( 'app.name' ) . " " . date( 'j M Y' ) . " ";
        $feeds     = collect( $this->data['feeds'] )->pluck( 'feed_name' )->unique();
        $tags      = collect( $this->data['tags'] )->pluck( 'tag_slug' )->unique();
        $allTopics = $feeds->merge( $tags );

        if ( $allTopics->count() == 2 ) {
            $topics  = $allTopics->all();
            $subject .= $topics[0] . ' and ' . $topics[1];
        }
        elseif ( $allTopics->count() >= 3 ) {
            $topics  = $allTopics->splice( 2 );
            $subject .= implode( ', ', $topics->all() ) . ' and more';
        }
        else {
            $subject .= implode( ', ', $allTopics->all() );
        }
        return $subject;
    }

}
