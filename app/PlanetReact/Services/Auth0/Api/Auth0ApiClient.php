<?php

namespace App\PlanetReact\Services\Auth0\Api;

use GuzzleHttp\Client as GuzzleClient;

class Auth0ApiClient {

    /**
     * @var GuzzleClient
     */
    protected $client;

    public function __construct() {
        $this->client = new GuzzleClient( [
                'timeout' => 7
        ] );
    }

    /**
     * @return GuzzleClient
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @param string|null $path
     * @return string
     */
    public function getDomainUrl( $path = null ) {
        return 'https://' . config( 'planet.auth0.domain' ) . '/' . ( $path ? $path : '' );
    }

    /**
     * @param string $method
     * @param string $path
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @todo: return json?
     */
    public function makeRequest( $method, $path, $options = [] ) {
        $path  = "api/v2/{$path}";
        $token = $this->getApiToken();
        $opts  = array_merge_recursive( $options, [
                        'headers' => [
                                'Authorization' => "Bearer ${token}",
                                'content-type'  => 'application/json'
                        ] ]
        );

        return $this->client->request( $method, $this->getDomainUrl( $path ), $opts );
    }

    /**
     * @return mixed
     * @throws \Exception
     * @todo: cache?
     */
    public function getApiToken() {
        $clientId     = config( 'planet.auth0.api_client_id' );
        $clientSecret = config( 'planet.auth0.api_client_secret' );
        $audience     = config( 'planet.auth0.api_client_audience' );

        try {
            $response = $this->client->request( 'POST', $this->getDomainUrl( 'oauth/token' ), [
                    'headers' => [
                            'content-type' => 'application/json'
                    ],
                    'body'    => "{\"client_id\":\"{$clientId}\",\"client_secret\":\"${clientSecret}\",\"audience\":\"${audience}\",\"grant_type\":\"client_credentials\"}"
            ] );

            return json_decode( $response->getBody()->getContents() )->access_token;

        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }

    }

}
