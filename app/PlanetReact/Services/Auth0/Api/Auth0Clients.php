<?php

namespace App\PlanetReact\Services\Auth0\Api;

class Auth0Clients {

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    public function getClients() {
        try {
            $response = $this->api->makeRequest( 'GET', 'clients', [
                    'query' => [
                            'include_fields' => 'true',
                            'fields'         => 'client_id,name,description',
                    ]
            ] );

            return json_decode( $response->getBody()->getContents() );
        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


}
