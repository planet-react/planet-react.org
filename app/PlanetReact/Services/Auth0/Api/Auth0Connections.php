<?php

namespace App\PlanetReact\Services\Auth0\Api;

class Auth0Connections {

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    public function getConnections( ) {
        try {
            // per_page:integer,
            // page:integer,
            // strategy:array/string,
            // name:string,
            // fields:string,
            //include_fields:boolean
            $response = $this->api->makeRequest( 'GET', 'connections', [] );

            return json_decode( $response->getBody()->getContents() );
        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }

}
