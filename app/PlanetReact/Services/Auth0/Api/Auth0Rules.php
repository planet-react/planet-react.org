<?php

namespace App\PlanetReact\Services\Auth0\Api;

class Auth0Rules {

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    public function getRules() {
        try {
            $response = $this->api->makeRequest( 'GET', 'rules', [
                    'query' => [
                            'include_fields' => 'true',
                            'fields'         => 'id,name,enabled,script,order,stage',
                    ]
            ] );

            return json_decode( $response->getBody()->getContents() );
        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


}
