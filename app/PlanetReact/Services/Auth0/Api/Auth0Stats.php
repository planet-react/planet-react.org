<?php

namespace App\PlanetReact\Services\Auth0\Api;

class Auth0Stats {

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    public function getDailyStats( $from = null, $to = null ) {
        $from = $from ? $from : date( 'Ymd' );
        $to   = $to ? $to : date( 'Ymd' );
        try {
            $response = $this->api->makeRequest( 'GET', 'stats/daily', [
                    'query' => [
                            'from' => $from,
                            'to'   => $to,
                    ]
            ] );

            return json_decode( $response->getBody()->getContents() );
        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }


}
