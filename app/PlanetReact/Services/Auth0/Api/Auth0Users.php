<?php

namespace App\PlanetReact\Services\Auth0\Api;

use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use Illuminate\Support\Collection;

class Auth0Users {

    protected $api;

    public function __construct() {
        $this->api = new Auth0ApiClient();
    }

    /**
     * @param int $perPage
     * @param int $page
     * @param string $q
     * @param string $includeTotals
     * @param string $sort
     * @return \stdClass
     * @throws \Exception
     */
    public function getUsers( $perPage = 50, $page = 0, $q = '', $includeTotals = 'true', $sort = 'created_at:-1' ) {
        try {
            $response = $this->api->makeRequest( 'GET', 'users', [
                    'query' => [
                            'page'           => $page,
                            'per_page'       => $perPage,
                            'q'              => $q,
                            'include_totals' => $includeTotals,
                            'sort'           => $sort,
                            'search_engine'  => 'v2'
                    ]
            ] );

            $contents = json_decode( $response->getBody()->getContents() );

            $result = new \stdClass;

            $result->total  = $contents->total;
            $result->length = $contents->length;
            $result->limit  = $contents->limit;
            $result->users  = new Collection();

            foreach ( $contents->users as $user ) {
                $result->users->push( $this->createUserProfileFromJson( $user ) );
            }

            return $result;
        } catch ( \Exception $exception ) {
            throw new \Exception( $exception->getMessage() );
        }
    }

    public function getUserById( $userId ) {
        $result = $this->getUsers( 1, 0, "user_id:\"{$userId}\"" );

        if ( $result->users->count() == 0 ) {
            return false;
        }
        return $result->users[0];
    }

    public function getUserCount() {
        // At the moment the Auth0 API does not have a method for getting the count.
        // Grab the count property from a simple user search.
        $result = $this->getUsers( 1, 0, '', 'true' );
        return $result->total;
    }

    private function createUserProfileFromJson( $json ) {
        $userProfile = new Auth0UserProfile();
        $userProfile->setUserId( $json->user_id );
        $userProfile->setName( $json->name );
        $userProfile->setNickname( $json->nickname );
        $userProfile->setPicture( $json->picture );
        $userProfile->setEmail( $json->email );
        $userProfile->setEmailVerified( $json->email_verified );
        $userProfile->setRoles( $json->app_metadata->roles );
        $userProfile->setCreatedAt( $json->created_at );
        $userProfile->setUpdatedAt( $json->updated_at );
        $userProfile->setBlocked( property_exists( $json, 'blocked' ) ? $json->blocked : false );

        return $userProfile;
    }

}
