<?php

namespace App\PlanetReact\Services\Auth0\Profile;

class Auth0UserProfile {
    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $nickname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $picture;

    /**
     * @var bool
     */
    private $emailVerified;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var string
     */
    private $updatedAt;

    /**
     * @var boolean
     */
    private $blocked;

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId( $userId ) {
        $this->userId = $userId;
    }

    public function getName() {
        return $this->name;
    }

    public function setName( $name ) {
        $this->name = $name;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public function setNickname( $nickname ) {
        $this->nickname = $nickname;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail( $email ) {
        $this->email = $email;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function setPicture( $picture ) {
        $this->picture = $picture;
    }

    public function getEmailVerified() {
        return $this->emailVerified;
    }

    public function setEmailVerified( $emailVerified ) {
        $this->emailVerified = $emailVerified;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setRoles( $roles ) {
        $this->roles = $roles;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setCreatedAt( $createdAt ) {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    public function setUpdatedAt( $updatedAt ) {
        $this->updatedAt = $updatedAt;
    }

    public function getBlocked() {
        return $this->blocked;
    }

    public function setBlocked( $blocked ) {
        $this->blocked = $blocked;
    }

}