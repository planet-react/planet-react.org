<?php

namespace App\PlanetReact\Services\YouTube\Api;

use GuzzleHttp\Client as GuzzleClient;

/**
 * Class YouTubeApi
 * @package App\PlanetReact\Services\YouTube\Api
 *
 * @todo: Work in progress.
 */
class YouTubeApi {

    protected $client;

    protected $baseUrl;

    private $apiKey;

    public function __construct() {
        $this->apiKey  = config( 'planet.google_api.key' );
        $this->baseUrl = 'https://www.googleapis.com/youtube/v3';
        $this->client  = new GuzzleClient( [ 'timeout' => 7 ] );
    }

    public function getChannel( $channelId ) {

        $result = [
                'channel' => null,
                'videos'  => [],
        ];

        $channel = $this->makeJsonRequest( "{$this->baseUrl}/channels?id={$channelId}&part=contentDetails,snippet&key={$this->apiKey}" );
        if ( !$channel) {
            throw new \Exception('No channel does not exist');
        }

        $videos = $this->makeJsonRequest( "{$this->baseUrl}/search?channelId={$channelId}&part=snippet,id&order=date&maxResults=20&key={$this->apiKey}" );

        $result['channel'] = [
                'url'         => "https://www.youtube.com/channel/{$channelId}",
                'title'       => $channel->items[0]->snippet->title,
                'description' => $channel->items[0]->snippet->description,
                'thumbnails'  => $channel->items[0]->snippet->thumbnails
        ];

        foreach ( $videos->items as $item ) {
            if ( property_exists( $item->id, 'videoId' ) ) {
                $result['videos'][] = [
                        'videoId'     => $item->id->videoId,
                        'publishedAt' => $item->snippet->publishedAt,
                        'title'       => $item->snippet->title,
                        'description' => $item->snippet->description,
                        'thumbnails'  => $item->snippet->thumbnails,
                ];
            }
        }
        return $result;
    }

    public function makeJsonRequest( $url ) {
        $response = $this->client->request( 'GET', $url, [] );
        return json_decode( $response->getBody()->getContents() );
    }

    /**
     * @param {string} $url
     * @return string|bool
     */
    public function getChannelIdFromUrl( $url ) {
        $urlComponents = parse_url( $url );
        if ( ! array_key_exists( 'path', $urlComponents ) ) {
            return false;
        }
        $pathComponents = explode( DIRECTORY_SEPARATOR, $urlComponents['path'] );
        for ( $i = 0; $i < count( $pathComponents ); $i++ ) {
            if ( $pathComponents[$i] == 'channel' && $pathComponents[$i + 1] ) {
                return $pathComponents[$i + 1];
            }
        }
        return false;
    }

}