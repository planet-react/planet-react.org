<?php

namespace App\Providers;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @param \Illuminate\Contracts\Http\Kernel $kernel
     *
     * @return void
     */
    public function boot( Kernel $kernel ) {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
//        if ( $this->app->environment() !== 'production' ) {
//            $this->app->register( \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class );
//        }
        $this->app->bind(
                \Auth0\Login\Contract\Auth0UserRepository::class,
                \Auth0\Login\Repository\Auth0UserRepository::class );

    }
}
