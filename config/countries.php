<?php

return [

        'cache' => [
                'enabled' => true,

                'service' => PragmaRX\Countries\Support\Cache::class,

                'duration' => 525600, // 1 year
        ],

        'hydrate' => [
                'before' => true,

                'after' => true,

                'elements' => [
                        'flag'       => true,
                        'currency'   => true,
                        'states'     => false,
                        'timezone'   => true,
                        'borders'    => false,
                        'topology'   => false,
                        'geometry'   => false,
                        'collection' => true,
                ],
        ],
        'maps'    => [
                'lca3'     => 'ISO639_3',
                'currency' => 'ISO4217',
        ],

        'validation' => [
                'enabled' => true,
                'rules'   => [
                        'country'        => 'name.common',
                        'cca2',
                        'cca2',
                        'cca3',
                        'ccn3',
                        'cioc',
                        'currency'       => 'ISO4217',
                        'language',
                        'language_short' => 'ISO639_3',
                ],
        ],

];
