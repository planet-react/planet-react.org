<?php


return [

    // The prefix for routes
        'prefix'               => env( 'PLANET_GRAPH_QL_ROUTE' ),

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each routes
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    //     'mutation' => 'graphiql'
    // ]
    //
    // you can also disable routes by setting routes to null
    //
    // 'routes' => null,
    //
        'routes'               => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Folklore\GraphQL\GraphQLController@query',
    //     'mutation' => '\Folklore\GraphQL\GraphQLController@mutation'
    // ]
    //
        'controllers'          => \Folklore\GraphQL\GraphQLController::class . '@query',

    // The name of the input that contain variables when you query the endpoint.
    // Some library use "variables", you can change it here. "params" will stay
    // the default for now but will be changed to "variables" in the next major
    // release.
        'variables_input_name' => 'variables',

    // Any middleware for the graphql route group
        'middleware'           => env( 'APP_ENV' ) == 'production' ? [] : [ 'debugger', 'cors' ],

    // Config for GraphiQL (https://github.com/graphql/graphiql).
    // To disable GraphiQL, set this to null.
        'graphiql'             => env( 'APP_ENV' ) == 'production' ? null : [
                'routes'     => env( 'PLANET_GRAPHI_QL_ROUTE' ),
                'middleware' => [],
                'view'       => 'graphql::graphiql'
        ],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
        'schema'               => 'default',

    // The schemas for query and/or mutation. It expects an array to provide
    // both the 'query' fields and the 'mutation' fields. You can also
    // provide directly an object GraphQL\Schema
    //
    // Example:
    //
    // 'schemas' => [
    //     'default' => new Schema($config)
    // ]
    //
    // or
    //
    // 'schemas' => [
    //     'default' => [
    //         'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //     ]
    // ]
    //
        'schemas'              => [
                'default' => [
                        'query'    => [
                                'feed'                => 'App\PlanetReact\Http\GraphQL\Query\Feed\FeedQuery',
                                'feeds'               => 'App\PlanetReact\Http\GraphQL\Query\Feed\FeedsQuery',
                                'feedValidate'        => 'App\PlanetReact\Http\GraphQL\Query\Feed\FeedValidateQuery',
                                'userFeeds'           => 'App\PlanetReact\Http\GraphQL\Query\Feed\UserFeedsQuery',
                                'post'                => 'App\PlanetReact\Http\GraphQL\Query\Post\PostQuery',
                                'browsePosts'         => 'App\PlanetReact\Http\GraphQL\Query\Post\BrowsePostsQuery',
                                'browseEvents'        => 'App\PlanetReact\Http\GraphQL\Query\Event\BrowseEventsQuery',
                                'event'               => 'App\PlanetReact\Http\GraphQL\Query\Event\EventQuery',
                                'upcomingEvent'       => 'App\PlanetReact\Http\GraphQL\Query\Event\UpcomingEventQuery',
                                'myEvents'            => 'App\PlanetReact\Http\GraphQL\Query\Event\MyEventsQuery',
                                'auth0UserBrowse'     => 'App\PlanetReact\Http\GraphQL\Query\Auth0\Auth0UserBrowseQuery',
                                'auth0UserCount'      => 'App\PlanetReact\Http\GraphQL\Query\Auth0\Auth0UserCountQuery',
                                'auth0Connections'    => 'App\PlanetReact\Http\GraphQL\Query\Auth0\Auth0ConnectionsQuery',
                                'auth0Rules'          => 'App\PlanetReact\Http\GraphQL\Query\Auth0\Auth0RulesQuery',
                                'auth0DailyStats'     => 'App\PlanetReact\Http\GraphQL\Query\Auth0\Auth0DailyStatsQuery',
                                'userBookmark'        => 'App\PlanetReact\Http\GraphQL\Query\Bookmark\UserBookmarkQuery',
                                'userBrowseBookmarks' => 'App\PlanetReact\Http\GraphQL\Query\Bookmark\UserBrowseBookmarksQuery',
                                'reports'             => 'App\PlanetReact\Http\GraphQL\Query\Report\ReportsQuery',
                                'heartBeat'           => 'App\PlanetReact\Http\GraphQL\Query\HeartBeat\HeartBeatQuery',
                                'topTags'             => 'App\PlanetReact\Http\GraphQL\Query\TopTags\TopTagsQuery',
                                'countries'           => 'App\PlanetReact\Http\GraphQL\Query\Countries\CountriesQuery',
                                'stats'               => 'App\PlanetReact\Http\GraphQL\Query\Stats\StatsQuery',
                                'subscriptionInfo'    => 'App\PlanetReact\Http\GraphQL\Query\Subscription\SubscriptionInfoQuery',
                                'userSubscriptions'   => 'App\PlanetReact\Http\GraphQL\Query\Subscription\UserSubscriptionsQuery',
                        ],
                        'mutation' => [
                                'editFeed'           => 'App\PlanetReact\Http\GraphQL\Mutation\EditFeedMutation',
                                'approveFeed'        => 'App\PlanetReact\Http\GraphQL\Mutation\ApproveFeedMutation',
                                'removeFeed'         => 'App\PlanetReact\Http\GraphQL\Mutation\RemoveFeedMutation',
                                'editEvent'          => 'App\PlanetReact\Http\GraphQL\Mutation\EditEventMutation',
                                'removeEvent'        => 'App\PlanetReact\Http\GraphQL\Mutation\RemoveEventMutation',
                                'activateEvent'      => 'App\PlanetReact\Http\GraphQL\Mutation\ActivateEventMutation',
                                'editBookmark'       => 'App\PlanetReact\Http\GraphQL\Mutation\EditBookmarkMutation',
                                'deleteBookmark'     => 'App\PlanetReact\Http\GraphQL\Mutation\DeleteBookmarkMutation',
                                'toggleBookmark'     => 'App\PlanetReact\Http\GraphQL\Mutation\ToggleBookmarkMutation',
                                'submitReport'       => 'App\PlanetReact\Http\GraphQL\Mutation\SubmitReportMutation',
                                'removePost'         => 'App\PlanetReact\Http\GraphQL\Mutation\RemovePostMutation',
                                'deleteReport'       => 'App\PlanetReact\Http\GraphQL\Mutation\DeleteReportMutation',
                                'toggleSubscription' => 'App\PlanetReact\Http\GraphQL\Mutation\ToggleSubscriptionMutation',
                                'fetchPosts'         => 'App\PlanetReact\Http\GraphQL\Mutation\FetchPostsMutation',
                                'changeFeedOwner'    => 'App\PlanetReact\Http\GraphQL\Mutation\ChangeFeedOwnerMutation',
                        ]
                ]
        ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    // or whitout specifying a key (it will use the ->name property of your type)
    //
    // 'types' => [
    //     'App\GraphQL\Type\UserType'
    // ]
    //
        'types'                => [
                'Feed'                => 'App\PlanetReact\Http\GraphQL\Type\FeedType',
                'FeedValidatorResult' => 'App\PlanetReact\Http\GraphQL\Type\FeedValidatorResultType',
                'Post'                => 'App\PlanetReact\Http\GraphQL\Type\PostType',
                'PostBrowse'          => 'App\PlanetReact\Http\GraphQL\Type\PostBrowseType',
                'Tag'                 => 'App\PlanetReact\Http\GraphQL\Type\TagType',
                'Stats'               => 'App\PlanetReact\Http\GraphQL\Type\StatsType',
                'FetchCount'          => 'App\PlanetReact\Http\GraphQL\Type\FetchCountType',
                'HeartBeat'           => 'App\PlanetReact\Http\GraphQL\Type\HeartBeatType',
                'Event'               => 'App\PlanetReact\Http\GraphQL\Type\EventType',
                'EventBrowse'         => 'App\PlanetReact\Http\GraphQL\Type\EventBrowseType',
                'Auth0BrowseUsers'    => 'App\PlanetReact\Http\GraphQL\Type\Auth0BrowseUsersType',
                'Auth0User'           => 'App\PlanetReact\Http\GraphQL\Type\Auth0UserType',
                'Auth0Connection'     => 'App\PlanetReact\Http\GraphQL\Type\Auth0ConnectionType',
                'Auth0Rule'           => 'App\PlanetReact\Http\GraphQL\Type\Auth0RuleType',
                'Auth0DailyStat'      => 'App\PlanetReact\Http\GraphQL\Type\Auth0DailyStatType',
                'Bookmark'            => 'App\PlanetReact\Http\GraphQL\Type\BookmarkType',
                'BookmarkBrowse'      => 'App\PlanetReact\Http\GraphQL\Type\BookmarkBrowseType',
                'Count'               => 'App\PlanetReact\Http\GraphQL\Type\CountType',
                'Report'              => 'App\PlanetReact\Http\GraphQL\Type\ReportType',
                'Country'             => 'App\PlanetReact\Http\GraphQL\Type\CountryType',
                'Subscription'        => 'App\PlanetReact\Http\GraphQL\Type\SubscriptionType',
                'Pagination'          => 'App\PlanetReact\Http\GraphQL\Type\PaginationType',
        ],

    // This callable will received every Error objects for each errors GraphQL catch.
    // The method should return an array representing the error.
    //
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    //
        'error_formatter'      => [ \Folklore\GraphQL\GraphQL::class, 'formatError' ],

    // Options to limit the query complexity and depth. See the doc
    // @ https://github.com/webonyx/graphql-php#security
    // for details. Disabled by default.
        'security'             => [
                'query_max_complexity'  => null,
                'query_max_depth'       => null,
                'disable_introspection' => env( 'APP_ENV' ) == 'production' ? true : false,
        ]
];
