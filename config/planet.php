<?php

if (!function_exists('dbg_log')) {
    function dbg_log() {
        $debugger = new App\PlanetReact\Http\Debugger\Debugger();
        $debugger->log( func_get_args() );
    }
}

if (!function_exists('dbg_clear')) {
    function dbg_clear() {
        $debugger = new App\PlanetReact\Http\Debugger\Debugger();
        $debugger->clearCacheFile();
    }
}

return [
    'email' => env( 'PLANET_EMAIL' ),
    'repo' => env( 'PLANET_REPO' ),
    'auth0' => [
        'client_id' => env( 'PLANET_AUTH0_CLIENT_ID' ),
        'secret' => env( 'PLANET_AUTH0_CLIENT_SECRET' ),
        'domain' => env( 'PLANET_AUTH0_DOMAIN' ),
        'api_client_id' => env( 'PLANET_AUTH0_API_CLIENT_ID' ),
        'api_client_secret' => env( 'PLANET_AUTH0_API_CLIENT_SECRET' ),
        'api_client_audience' => env( 'PLANET_AUTH0_API_CLIENT_AUDIENCE' ),
    ],
    'aggregator' => [
        'lock_file' => storage_path( 'app/.aggregator.lock' ),
        'cron' => env( 'PLANET_AGGREGATOR_CRON' ),
        'categories' => env( 'PLANET_AGGREGATOR_CATEGORY_FILTER', [] ),
        'max_posts_per_feed' => env( 'PLANET_AGGREGATOR_MAX_POSTS_PER_FEED', 10 ),
        'tags_to_discard' => env( 'PLANET_AGGREGATOR_TAGS_TO_DISCARD', [] ),
        'timeout' => env( 'PLANET_AGGREGATOR_TIMEOUT', 15 ),
        'user_agent' => env( 'PLANET_AGGREGATOR_USER_AGENT' ),
    ],
    'directories' => [
        'feed_images' => env( 'PLANET_PUBLIC_FEED_IMAGE_DIR' )
    ],
    'db_backup' => [
        // At minute 0 past every 12th hour.
        'cron' => env( 'PLANET_DB_BACKUP_CRON', '0 */12 * * *' ),
        'active' => env( 'PLANET_DB_BACKUP', false ),
        'delete_older_than' => env( 'PLANET_DB_BACKUP_DELETE_OLDER_THAN' ),
    ],
    'google_api' => [
        'key' => env('GOOGLE_API_KEY')
    ],
    'debugger' => [
        'cache_file' => storage_path( 'app/debugger-cache.json' ),
        'log_queries' => true,
    ]

];