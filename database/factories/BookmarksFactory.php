<?php

$factory->define( App\PlanetReact\Domain\Bookmark\Bookmark::class, function ( Faker\Generator $faker ) {
    return [
            'user_id' => $faker->uuid,
            'title'   => $faker->sentence( $nbWords = 6, $variableNbWords = true ),
            'url'     => $faker->url,
            'notes'   => $faker->text( $maxNbChars = 300 )
    ];
} );
