<?php

$factory->define( App\PlanetReact\Domain\Event\Event::class, function ( Faker\Generator $faker ) {
    return [
            'user_id'      => $faker->uuid,
            'active'       => $faker->biasedNumberBetween( 0, 1 ),
            'type'         => $faker->biasedNumberBetween( 1, 3 ),
            'name'         => $faker->catchPhrase,
            'description'  => $faker->text( $maxNbChars = 300 ),
            'start_date'   => $faker->dateTimeBetween( 'now', '+20days' ),
            'end_date'     => $faker->dateTimeBetween( '+1days', '+30days' ),
            'location'     => $faker->streetAddress,
            'country'      => $faker->country,
            'website_link' => $faker->url,
            'twitter_link' => $faker->url,
    ];
} );
