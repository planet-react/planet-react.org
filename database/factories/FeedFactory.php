<?php

$factory->define( App\PlanetReact\Domain\Feed\Feed::class, function ( Faker\Generator $faker ) {
    return [
            'user_id'     => $faker->uuid,
            'source_type' => $faker->numberBetween( 1, 2 ),
            'name'        => $faker->name,
            'slug'        => $faker->slug( 4, true ),
            'description' => $faker->realText( $maxNbChars = 200, $indexSize = 2 ),
            'lang'        => $faker->languageCode,
            'website_url' => $faker->unique()->url,
            'feed_url'    => $faker->unique()->url,
            'twitter_url' => $faker->unique()->url,
            'repo_url'    => $faker->unique()->url,
            'image'       => $faker->slug . '.png',
            'approved'    => 1,
            'comment'     => $faker->realText( $maxNbChars = 200, $indexSize = 2 ),
    ];
} );
