<?php

$factory->define( App\PlanetReact\Domain\Post\Post::class, function ( Faker\Generator $faker ) {
    return [
            'title'     => $faker->realText( 50, 2 ),
            'authors'   => $faker->name,
            'content'   => $faker->realText( 200, 2 ),
            'link'      => $faker->unique()->url,
            'enclosure' => $faker->unique()->url,
            'guid'      => $faker->unique()->url,
            'pubdate'   => $faker->date( 'Y-m-d H:i:s', $max = 'now' ),
            'feed_id'   => function () {
                return factory( App\PlanetReact\Domain\Feed\Feed::class )->create()->id;
            }
    ];
} );
