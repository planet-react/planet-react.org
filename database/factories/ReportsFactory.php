<?php
$factory->define( App\PlanetReact\Domain\Report\Report::class, function ( Faker\Generator $faker ) {
    return [
            'content_id'   => function () {
                return factory( App\PlanetReact\Domain\Post\Post::class )->create()->id;
            },
            'title'        => $faker->title,
            'url'          => $faker->unique()->url,
            'reason'       => $faker->sentence( 6 ),
            'other_reason' => $faker->realText( 100, 2 ),
    ];
} );
