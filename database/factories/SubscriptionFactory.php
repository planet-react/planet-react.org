<?php
$factory->define( App\PlanetReact\Domain\Subscription\Subscription::class, function ( Faker\Generator $faker ) {
    return [
            'user_id'    => $faker->uuid,
            'user_email' => $faker->email,
            'type'       => 'feed',
            'value'      => 10,
    ];
} );
