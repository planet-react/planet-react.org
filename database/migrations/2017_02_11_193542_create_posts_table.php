<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'posts', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer('blog_id')->unsigned();
            $table->string( 'title' );
            $table->mediumText( 'authors' )->nullable();
            $table->mediumText( 'description' )->nullable();
            $table->string( 'link' );
            $table->dateTime( 'pubdate' );
            $table->dateTime( 'fetched_at' )->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( 'blog_id' )->references( 'id' )->on( 'blogs' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'posts' );

    }
}
