<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentToBlog extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'blogs', function ( Blueprint $table ) {
            $table->text( 'comment' )->nullable()->after('is_rss');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'blogs', function ( Blueprint $table ) {
            $table->dropColumn( 'comment' );
        } );
    }
}
