<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBlogsTableToFeeds extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::rename('blogs', 'feeds');
        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->dropForeign('posts_blog_id_foreign');
            $table->renameColumn('blog_id', 'feed_id');
            $table->foreign( 'feed_id' )->references( 'id' )->on( 'feeds' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::rename('feeds', 'blogs');

        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->dropForeign('posts_feed_id_foreign');
            $table->renameColumn('feed_id', 'blog_id');
            $table->foreign( 'blog_id' )->references( 'id' )->on( 'blogs' );
        } );

    }
}
