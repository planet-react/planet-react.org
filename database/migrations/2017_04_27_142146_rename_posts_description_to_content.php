<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamePostsDescriptionToContent extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->renameColumn( 'description', 'content' );
        } );
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->renameColumn( 'post_description_xpath', 'post_content_xpath' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->renameColumn( 'post_content_xpath', 'post_description_xpath' );
        } );

        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->renameColumn( 'content', 'description' );
        } );
    }
}
