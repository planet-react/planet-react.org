<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'events', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'user_id' );
            $table->boolean( 'active' )->default( 0 );
            $table->integer( 'type' )->unsigned()->index();
            $table->string( 'name' );
            $table->dateTime( 'start_date' );
            $table->dateTime( 'end_date' );
            $table->string( 'location' )->nullable();
            $table->string( 'country' )->nullable();
            $table->string( 'website_link' )->nullable();
            $table->string( 'twitter_link' )->nullable();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'events' );
    }
}
