<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToEventsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'events', function ( Blueprint $table ) {
            $table->index( [ 'type', 'start_date', 'end_date' ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'events', function ( Blueprint $table ) {
            $table->dropIndex( [ 'type', 'start_date', 'end_date' ] );
        } );
    }
}

