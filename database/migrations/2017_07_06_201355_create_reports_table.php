<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'reports', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'content_id' )->unsigned();
            $table->string( 'title' );
            $table->string( 'reason' );
            $table->string( 'other_reason', 100 );
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'reports' );
    }
}
