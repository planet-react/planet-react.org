<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIsRssColumn extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->dropColumn( [
                    'is_rss',
                    'posts_xpath',
                    'post_title_xpath',
                    'post_content_xpath',
                    'post_pubdate_xpath',
                    'post_link_xpath',
            ] );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->boolean( 'is_rss' )->default( 0 );
            $table->string( 'posts_xpath' )->nullable();
            $table->string( 'post_title_xpath' )->nullable();
            $table->string( 'post_content_xpath' )->nullable();
            $table->string( 'post_pubdate_xpath' )->nullable();
            $table->string( 'post_link_xpath' )->nullable();
        } );


    }
}
