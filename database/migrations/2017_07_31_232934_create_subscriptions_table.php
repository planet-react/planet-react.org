<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'subscriptions', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'user_id' )->index();
            $table->string( 'user_email' )->index();
            $table->string( 'type', 100 )->index();
            $table->string( 'value', 100 )->index();
            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'subscriptions' );

    }
}