<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixPostsIndexes extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'posts', function ( Blueprint $table ) {
            // Don't use compound index
            $table->dropIndex( [ 'title', 'feed_id', 'pubdate' ] );

            $table->index( 'title' );
            $table->index( 'pubdate' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'posts', function ( Blueprint $table ) {
            $table->dropIndex( [ 'title' ] );
            $table->dropIndex( [ 'pubdate' ] );

            $table->index( [ 'title', 'feed_id', 'pubdate' ] );
        } );
    }
}
