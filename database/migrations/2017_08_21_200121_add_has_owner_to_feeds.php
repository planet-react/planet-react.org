<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasOwnerToFeeds extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->tinyInteger( 'has_owner' )->after( 'user_id' )->default( 0 );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->dropColumn( 'has_owner' );
        } );
    }
}
