<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTwitterUrlToFeeds extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->string( 'twitter_url' )->after( 'feed_url' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->dropColumn( 'twitter_url' );
        } );
    }
}
