<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRepoUrlFieldToFeeds extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->string( 'repo_url' )->after( 'twitter_url' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->dropColumn( 'repo_url' );
        } );
    }
}
