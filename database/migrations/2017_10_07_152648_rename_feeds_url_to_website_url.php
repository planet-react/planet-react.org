<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RenameFeedsUrlToWebsiteUrl extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->renameColumn( 'url', 'website_url' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'feeds', function ( Blueprint $table ) {
            $table->renameColumn( 'website_url', 'url' );
        } );
    }
}
