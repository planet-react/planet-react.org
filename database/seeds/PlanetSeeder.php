<?php

use Illuminate\Database\Seeder;

class PlanetSeeder extends Seeder {

    public function run() {
        factory(App\PlanetReact\Domain\Event\Event::class, 30)->create();
    }

}
