<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTypeAndValueInSubscriptions extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table( 'subscriptions', function ( Blueprint $table ) {
            $table->dropIndex( [ 'type' ] );
            $table->dropIndex( [ 'value' ] );
            $table->renameColumn( 'type', 'subscribable_type' );
            $table->renameColumn( 'value', 'subscribable_id' );

            $table->index( 'subscribable_type');
            $table->index( 'subscribable_id' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'subscriptions', function ( Blueprint $table ) {
            $table->dropIndex( [ 'subscribable_type' ] );
            $table->dropIndex( [ 'subscribable_id' ] );
            $table->renameColumn( 'subscribable_id', 'value' );
            $table->renameColumn( 'subscribable_type', 'type' );

            $table->index( 'type' );
            $table->index( 'value' );
        } );
    }
}
