<?php

namespace Deployer;

require 'recipe/common.php';
require 'vendor/deployphp/recipes/recipe/rsync.php';

/*
|-------------------------------------------------------------------------------
| Tasks
|-------------------------------------------------------------------------------
*/

require 'deployer/dep.artisan.php';
require 'deployer/dep.test.php';
require 'deployer/dep.build.php';
require 'deployer/dep.diagnose.php';
require 'deployer/dep.sync-local-feed-images.php';

/*
|-------------------------------------------------------------------------------
| Configuration
|-------------------------------------------------------------------------------
*/

set( 'keep_releases', 3 );
set( 'http_user', 'www-data' );
set( 'writable_mode', 'chmod' );
set( 'writable_chmod_mode', '0771' );
set( 'use_relative_symlink', false );
set( 'shared_dirs', [ 'storage', 'public/images/feeds' ] );
set( 'shared_files', [ '.env', ] );

host( '45.79.101.139' )
        ->stage( 'production' )
        ->set( 'deploy_path', '/var/www/html/planet-react.org/public_html' )
        ->user( 'planet-react' )
        ->set( 'rsync_src', __DIR__ )
        ->set( 'rsync_dest', '{{release_path}}' )
        ->multiplexing( true );

set( 'rsync', [
        'exclude'       => [
                '.babelrc',
                '.DS_Store',
                '.env',
                '.git',
                '.gitattributes',
                '.gitignore',
                '.idea',
                '.phpstorm.meta.php',
                '/node_modules',
                '/public/images/feeds',
                '_ide_helper.php',
                'deploy.php',
                'deploy/',
                'postcss.config.js',
                'server.php',
                'stats.json',
                'storage',
                'vendor',
                'webpack.app.*',
                'yarn.lock',
        ],
        'exclude-file'  => false,
        'include'       => [],
        'include-file'  => false,
        'filter'        => [],
        'filter-file'   => false,
        'filter-perdir' => false,
        'flags'         => 'rz', // Recursive, with compress
        'options'       => [ 'delete' ],
        'timeout'       => 120,
] );

set( 'writable_dirs', [
        'bootstrap/cache',
//        'public/images/feeds',
//        'storage',
//        'storage/app',
//        'storage/app/aggregator',
//        'storage/app/db-backup',
//        'storage/app/public',
//        'storage/framework',
//        'storage/framework/cache',
//        'storage/framework/sessions',
//        'storage/framework/views',
//        'storage/logs',
] );

/*
|-------------------------------------------------------------------------------
| Task groups
|-------------------------------------------------------------------------------
*/

desc( 'Deploy Planet React' );
task( 'deploy', [
        'deploy:prepare',
        //'test:js',
        //'test:php',
        'deploy:lock',
        'build',
        'deploy:release',
        'rsync',
        'deploy:shared',
        'deploy:vendors',
        'deploy:writable',
        'artisan:view:clear',
        'artisan:cache:clear',
        'artisan:config:cache',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
] );

after( 'deploy', 'success' );
