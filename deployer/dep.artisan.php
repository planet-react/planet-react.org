<?php

namespace Deployer;

desc( 'View artisan log' );
task( 'view:artisan-log', function () {
    $date   = date( 'Y-m-d' );
    $logfile = '{{deploy_path}}/shared/storage/logs/laravel-' . $date . '.log';
    $output = run( 'if [ -f '.$logfile.' ]; then cat '.$logfile.'; else echo "'.$logfile.' was not found"; fi' );
    writeln( $output );
} );

desc( 'Enable maintenance mode' );
task( 'artisan:down', function () {
    $output = run( 'if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan down; fi' );
    writeln( '<info>' . $output . '</info>' );
} );

desc( 'Disable maintenance mode' );
task( 'artisan:up', function () {
    $output = run( 'if [ -f {{deploy_path}}/current/artisan ]; then {{bin/php}} {{deploy_path}}/current/artisan up; fi' );
    writeln( '<info>' . $output . '</info>' );
} );

desc( 'Execute artisan migrate' );
task( 'artisan:migrate', function () {
    run( '{{bin/php}} {{release_path}}/artisan migrate --force' );
} );

desc( 'Execute artisan migrate:rollback' );
task( 'artisan:migrate:rollback', function () {
    $output = run( '{{bin/php}} {{release_path}}/artisan migrate:rollback --force' );
    writeln( '<info>' . $output . '</info>' );
} );

desc( 'Execute artisan migrate:status' );
task( 'artisan:migrate:status', function () {
    $output = run( '{{bin/php}} {{release_path}}/artisan migrate:status' );
    writeln( '<info>' . $output . '</info>' );
} );

desc( 'Execute artisan cache:clear' );
task( 'artisan:cache:clear', function () {
    run( '{{bin/php}} {{release_path}}/artisan cache:clear' );
} );

desc( 'Execute artisan config:cache' );
task( 'artisan:config:cache', function () {
    run( '{{bin/php}} {{release_path}}/artisan config:cache' );
} );

desc( 'Execute artisan config:clear' );
task( 'artisan:config:clear', function () {
    run( '{{bin/php}} {{release_path}}/artisan config:clear' );
} );

desc( 'Execute artisan view:clear' );
task( 'artisan:view:clear', function () {
    run( '{{bin/php}} {{release_path}}/artisan view:clear' );
} );

desc( 'Execute artisan optimize' );
task( 'artisan:optimize', function () {
    run( '{{bin/php}} {{release_path}}/artisan optimize' );
} );
