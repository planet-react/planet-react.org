<?php

namespace Deployer;

desc( 'Build client' );
task( 'build', function () {
    run( 'yarn run planet:build' );
} )->local();

