<?php

namespace Deployer;

// https://www.cyberciti.biz/hardware/collecting-ubuntu-linux-system-information/
desc( 'Diagnosis' );
task( 'diagnose', function () {
    $uptime   = run( 'uptime' );
    $hostname = run( 'hostname' );
    $free     = run( 'free -h' );
    $release  = run( 'lsb_release -a' );
    $uname    = run( 'uname -r' );
    $disk     = run( 'df -H' );
    $apache   = run( 'apache2ctl -v' );
    $php      = run( 'php -v' );
    $mysql    = run( 'mysql --version' );

    $outdatedProduction = run( 'cd {{release_path}} && composer outdated --direct' );

    // @todo: Commented for now as security-check is not compatible with Laravel 5.5
    // $securityCheckInfo  = run( '{{bin/php}} {{release_path}}/artisan security-check:now' );

    writeln( 'System:' );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( "Hostname: {$hostname}" );
    writeln( "Uptime: {$uptime}" );
    writeln( "Kernel version: {$uname}\n" );
    writeln( $release );

    writeln( "Apache:" );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( "{$apache}\n" );

    writeln( "PHP:" );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( "{$php}\n" );

    writeln( "MySQL:" );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( "{$mysql}\n" );

    writeln( 'Memory:' );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( $free );

    writeln( 'Disk:' );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( $disk );

    writeln( 'Composer outdated:' );
    writeln( '--------------------------------------------------------------------------------' );
    writeln( $outdatedProduction );

    //writeln( 'Security check:' );
    //writeln( '--------------------------------------------------------------------------------' );
    //writeln( $securityCheckInfo );
} );
