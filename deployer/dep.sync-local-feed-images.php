<?php

namespace Deployer;

desc( 'Syncs local images with images from production' );
task( 'sync-local-feed-images', function () {

    download( '{{release_path}}/public/images/feeds/*', dirname(__DIR__) . '/public/images/feeds' );
} );