<?php

namespace Deployer;

desc( 'Run client tests' );
task( 'test:js', function () {
    run( 'yarn test' );
} )->local();

desc( 'Run server tests' );
task( 'test:php', function () {
    run( 'phpunit --stop-on-failure' );
} )->local();