import React, { Component } from 'react';
import { withAuth0 } from 'components/services';
import { Redirect, Switch, withRouter } from 'react-router-dom';
import { Screen, ScreenError, ScreenPopover } from 'components/screen';
import Search from './screens/Search';
import Reader from 'components/post/reader';

class Routes extends Component {

    previousLocation = this.props.location;

    componentWillUpdate( nextProps ) {
        const { location } = this.props;

        if (
            nextProps.history.action !== 'POP' &&
            (!location.state || !location.state.isReader)
        ) {
            this.previousLocation = this.props.location;
        }
    }

    render() {
        const { auth0, location } = this.props;
        const { isAuthenticated, roles } = auth0;
        const redirectPath = auth0.getRedirectPath();

        const isReader = !!(
            location.state &&
            location.state.isReader &&
            this.previousLocation !== location // not initial render
        );

        return (

                <div>

                    <Switch
                        location={isReader ? this.previousLocation : location}
                    >

                        <Screen exact path="/"/>

                        <Screen path="/search" component={Search}/>

                        <Screen path="/profile" allowedRoles={[
                            roles.AUTHORIZED_USER,
                            roles.ADMIN
                        ]}
                        />

                        {/* When accessed directly */}
                        <Screen path="/post/:postId"
                                render={( props ) =>
                                    <ScreenPopover {...props}>
                                        <Reader {...props}/>
                                    </ScreenPopover>}
                        />

                        <Screen path="/events"/>

                        <Screen path="/tags/:slug/:year?/:month?"/>

                        <Screen path="/posts/:language/:year?/:month?"/>

                        <Screen path="/feeds/:slug/:year?/:month?"/>

                        <Screen path="/faq"/>

                        <Screen path="/legal"/>

                        <Screen path="/admin" allowedRoles={[
                            roles.ADMIN
                        ]}
                        />

                        <Screen path="/_callback" render={props => (
                            isAuthenticated && redirectPath ? (
                                <Redirect to={redirectPath}/>
                            ) : (
                                null
                            )
                        )}/>

                        <Screen render={( props ) => {
                            return (
                                <ScreenError statusCode={404}/>
                            )
                        }}/>

                    </Switch>

                    {isReader ?
                        <Screen path="/post/:postId" render={( props ) =>
                            <ScreenPopover {...props}>
                                <Reader {...props}/>
                            </ScreenPopover>}
                        />
                        : null}

                </div>



        )
    }
}

export default withRouter(
    withAuth0( Routes )
);