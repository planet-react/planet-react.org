import React, { Component } from 'react';
import { BrowserRouter, } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import NavBar from 'components/navbar';
import Footer from 'components/footer';
import Routes from './Routes';
import HeartBeat from 'components/heartbeat';
import { Auth0Provider, ServicesProvider } from 'components/services';
import ReactRally from 'components/banner/reactrally';
import FiveMinutesOfReact from 'components/banner/fiveminutes';
import UpcomingEvent from 'components/banner/upcomingevent';
import TopTagsList from 'components/banner/toptags';
import Box from 'components/box';

require( './shared/assets/styles/main.scss' );

export default class App extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { auth0service, apolloClient } = this.props.services;

        return (
            <ApolloProvider client={apolloClient}>
                <div>
                    <BrowserRouter>
                        <Auth0Provider auth0service={auth0service}>
                            <ServicesProvider services={this.props.services}>
                                <div>

                                    <NavBar/>

                                    <main className="content">
                                        <section className="west">
                                            <Routes/>
                                        </section>

                                        <section className="east">
                                            <Box title="Upcoming Event">
                                                <UpcomingEvent/>
                                            </Box>
                                            <Box title="Five Minutes of React">
                                                <FiveMinutesOfReact/>
                                            </Box>
                                            <Box title="React Rally Videos">
                                                <ReactRally/>
                                            </Box>
                                            <Box title="Top Tags">
                                                <TopTagsList/>
                                            </Box>
                                        </section>
                                    </main>

                                    <Footer/>

                                </div>
                            </ServicesProvider>
                        </Auth0Provider>
                    </BrowserRouter>

                    <HeartBeat pollInterval={__PLANET_HEART_BEAT_INTERVAL__}/>

                </div>
            </ApolloProvider>
        )
    }
}
