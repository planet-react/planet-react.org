import React from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';

import StatsQuery from './StatsQuery.graphql';

Numbers.PropTypes = {};

function Numbers( props ) {

    const { data: { stats } } = props;

    return (
        <div className="statistics-container">
            <div className="statistic">
                <div className="value">{stats.total_users_registered}</div>
                <div className="label">Users</div>
            </div>
            <div className="statistic">
                <div className="value">{stats.total_open_reports}</div>
                <div className="label">Open reports</div>
            </div>
            <div className="statistic">
                <div
                    className="value">{stats.feeds_waiting_for_approval_count}</div>
                <div className="label">Feeds waiting for approval</div>
            </div>
            <div className="statistic">
                <div className="value">{stats.total_feeds_count}</div>
                <div className="label">Approved feeds</div>
            </div>
            <div className="statistic">
                <div className="value">{stats.failed_feeds_count}</div>
                <div className="label">Failed feeds</div>
            </div>
            <div className="statistic">
                <div className="value">{stats.total_posts_count}</div>
                <div className="label">posts</div>
            </div>
        </div>
    )
}

export default graphql( StatsQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( Numbers ) );
