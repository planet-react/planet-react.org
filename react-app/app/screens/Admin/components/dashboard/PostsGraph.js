import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import { VictoryChart, VictoryAxis, VictoryLine, VictoryTheme, VictoryVoronoiContainer, VictoryTooltip } from 'victory';

import PostsGraphQuery from './PostsGraphQuery.graphql';

function PostsGraph( props ) {
    const { data: { stats } } = props;

    const height = 130;
    const fontSize = 7;

    const posts = stats.posts_fetched_this_month;

    return (
        <div>
            <VictoryChart
                theme={VictoryTheme.material}
                height={height}
                padding={{ top: 35, right: 30, bottom: 30, left: 30 }}
                containerComponent={<VictoryVoronoiContainer/>}>
                <VictoryAxis
                    tickValues={posts.map( ( p ) => p.day )}
                    style={{
                        tickLabels: { fontSize: fontSize }
                    }}/>
                <VictoryAxis
                    dependentAxis={true}
                    style={{
                        tickLabels: { fontSize: fontSize }
                    }}/>
                <VictoryLine
                    labelComponent={<VictoryTooltip pointerLength={5}
                                                    cornerRadius={0} style={{
                        fontSize: fontSize,
                        padding: 7
                    }}/>}
                    labels={( d ) => d.y}
                    data={posts}
                    x="day"
                    y="count"
                />
            </VictoryChart>

            <p className="align-center">Posts fetched this month</p>

        </div>
    )
}

PostsGraph.propTypes = {
    data: PropTypes.shape( {
        posts_fetched_this_month: PropTypes.array
    } )
};

export default graphql( PostsGraphQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( PostsGraph ) )
