import React from 'react';
import Numbers from './Numbers';
import PostsGraph from './PostsGraph';

require( './style.scss' );

export default function Dashboard() {
    return (
        <div>
            {typeof __PLANET_GOOGLE_SEARCH_CONSOLE_URL__ != 'undefined' &&
                <p>
                    <a href="https://www.google.com/webmasters/tools/dashboard?hl=en&siteUrl=https%3A%2F%2Fwww.planet-react.org%2F">
                        Google Search Console
                    </a>
                </p>
            }
            <Numbers/>
            <PostsGraph/>
        </div>
    )
}
