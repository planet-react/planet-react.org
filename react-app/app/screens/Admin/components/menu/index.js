import React from 'react';
import { Menu, MenuItem } from 'components/support/menu';

export default function AdminMenu() {
    return (
        <Menu type="underline">
            <MenuItem exact path="/admin">
                Admin Home
            </MenuItem>
            <MenuItem path="/admin/auth0">
                Auth 0
            </MenuItem>
            <MenuItem path="/admin/feed-manager">
                Manage Feeds
            </MenuItem>
            <MenuItem path="/admin/reports">
                Reports
            </MenuItem>
            <MenuItem path="/admin/app-assets">
                App Assets
            </MenuItem>
        </Menu>
    )
}