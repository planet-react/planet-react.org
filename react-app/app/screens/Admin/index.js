import React, { Component } from 'react';
import Header from 'components/header';
import Dashboard from './components/dashboard';

import { Screen } from 'components/screen';

import { Route } from 'react-router-dom';
import AdminMenu from './components/menu';

export default class AdminScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { match } = this.props;
        return (
            <div>
                <Route path={match.url}
                       render={( props ) => <AdminMenu/>}/>

                <Route
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header title="Admin"/>
                                <Dashboard/>
                            </div>
                        )
                    }}/>

                <Screen path={match.url + '/auth0'}/>
                <Screen path={match.url + '/feed-manager'}/>
                <Screen path={match.url + '/reports'}/>
                <Screen path={match.url + '/app-assets'}/>

            </div>
        )
    }
}