import React from "react";
import { Row, Col } from 'components/support/flex';
import Button from 'components/support/button';
import PlayIcon from 'react-icons/lib/fa/play-circle-o';
import MailIcon from 'react-icons/lib/go/mail';
import TvIcon from 'react-icons/lib/fa/television';

const onClick = () => {
};

export default function Buttons() {
    return (
        <div>
            <h2>Buttons</h2>
            <h3>Primary</h3>
            <Row>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="large">
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="medium">
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="small">
                        Small
                    </Button>
                </Col>
            </Row>
            <h4>With icon</h4>
            <Row>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="large" icon={PlayIcon}>
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="medium" icon={PlayIcon}>
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button onClick={onClick} size="small" icon={PlayIcon}>
                        Small
                    </Button>
                </Col>
            </Row>
            <h3>Secondary</h3>
            <Row>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="large"
                        color="secondary"
                    >
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="medium"
                        color="secondary"
                    >
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="small"
                        color="secondary"
                    >
                        Small
                    </Button>
                </Col>
            </Row>
            <h4>With icon</h4>
            <Row>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="large"
                        color="secondary"
                        icon={TvIcon}
                    >
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="medium"
                        color="secondary"
                        icon={TvIcon}
                    >
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="small"
                        color="secondary"
                        icon={TvIcon}
                    >
                        Small
                    </Button>
                </Col>
            </Row>

            <h3>Blue</h3>
            <Row>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="large"
                        color="blue"
                    >
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="medium"
                        color="blue"
                    >
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="small"
                        color="blue"
                    >
                        Small
                    </Button>
                </Col>
            </Row>
            <h4>With icon</h4>
            <Row>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="large"
                        color="blue"
                        icon={MailIcon}
                    >
                        Large
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="medium"
                        color="blue"
                        icon={MailIcon}
                    >
                        Medium
                    </Button>
                </Col>
                <Col flexGrow={'1'}>
                    <Button
                        onClick={onClick}
                        size="small"
                        color="blue"
                        icon={MailIcon}
                    >
                        Small
                    </Button>
                </Col>
            </Row>

        </div>
    );
}