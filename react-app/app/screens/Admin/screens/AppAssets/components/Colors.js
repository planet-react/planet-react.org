import React, { Component } from "react";
import { Row, Col } from 'components/support/flex';

const colors = [
    '#20232a',
    '#ffffff',
    '#282C34',
    '#607ba1',
    '#4285f4',
    '#7696ad',
    '#61dafb',
    '#60acd4',
    '#ea5929',
    '#e68a0d',
    '#e99442',
    '#ecd274',
    '#def4a9',
    '#f7e97c',
    '#cc7a6f',
    '#2e3a4a',
    '#888888',
    '#acacac',
    '#aaaaaa',
    '#95E1F4',
    '#b3b9be',
    '#c2cbc5',
    '#c7f9eb',
    '#cccccc',
    '#d2d2d2',
    '#d8e6e5',
    '#d1e5e3',
    '#dddddd',
    '#dedede',
    '#e2d9db',
    '#e9e9e9',
    '#e9e3ed',
    '#ecedf5',
    '#f7f7f6',
    '#fafafa',
    '#fcfcfc',
    '#726b35',
];

export default function ColorAssets( {} ) {
    return (
        <div>
            <h2>Colors</h2>
            <Row flow="wrap"
                style={{
                    backgroundColor: '#282C34',
                    padding: '.5em'
                }}>
                {colors.map( ( color, i ) => {
                    return (
                        <Col
                            key={i}
                            style={{
                                backgroundColor: color,
                                width: '7em',
                                height: '7em',
                                padding: '.1em',
                            }}
                        >
                            {color}
                        </Col>
                    );
                } )}

            </Row>
        </div>
    );
}