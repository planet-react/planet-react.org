import React, {Component} from "react";
import { Row, Col } from 'components/support/flex';
import Message from 'components/support/message';

const onClick = () => {
};

export default class MessageAsset extends Component {

    state = {
        withCloseable: false
    };

    onToggleCloseable = () => {
        this.setState( {
            withCloseable: !this.state.withCloseable
        } )
    };

    render() {
        const { withCloseable } = this.state;
        return (
            <div>
                <h2>Message</h2>

                <input type="checkbox" id="toggle-closeable" onClick={this.onToggleCloseable}/>
                <label htmlFor="toggle-closeable"> With closeable</label>

                <h3>Transparent (default)</h3>
                <Message closeable={withCloseable}
                         closeCallback={onClick}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, aliquam commodi dicta doloremque eos facilis illo maiores, quod quos repellat voluptatem voluptatum! Est in, iure iusto non quaerat quam recusandae!
                </Message>

                <h3>Info</h3>
                <Message context="info" closeable={withCloseable}
                         closeCallback={onClick}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nobis sapiente temporibus tenetur voluptas. Amet, autem blanditiis cum, excepturi modi molestias nostrum obcaecati odit omnis quasi repudiandae sunt ut voluptatem?
                </Message>

                <h3>Success</h3>
                <Message context="success" closeable={withCloseable}
                         closeCallback={onClick}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nobis sapiente temporibus tenetur voluptas. Amet, autem blanditiis cum, excepturi modi molestias nostrum obcaecati odit omnis quasi repudiandae sunt ut voluptatem?
                </Message>

                <h3>Warning</h3>
                <Message context="warning" closeable={withCloseable}
                         closeCallback={onClick}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nobis sapiente temporibus tenetur voluptas. Amet, autem blanditiis cum, excepturi modi molestias nostrum obcaecati odit omnis quasi repudiandae sunt ut voluptatem?
                </Message>

                <h3>Error</h3>
                <Message context="error" closeable={withCloseable}
                         closeCallback={onClick}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nobis sapiente temporibus tenetur voluptas. Amet, autem blanditiis cum, excepturi modi molestias nostrum obcaecati odit omnis quasi repudiandae sunt ut voluptatem?
                </Message>

            </div>
        );
    }
}