import React, { Component } from 'react';
import Popup from 'components/support/popup';
import Button from 'components/support/button';
import { Row, Col, Filler } from 'components/support/flex';

const PopupContent = () =>
    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, assumenda consectetur consequuntur dolore doloremque, dolores ea eveniet ex expedita inventore, laborum mollitia neque nulla pariatur quibusdam saepe similique velit voluptate!</div>

export default class PopupAsset extends Component {

    static propTypes = {};

    static defaultProps = {};

    state = {
        isPopupOpen: false,
        position: undefined,
        isModal: true,
        isStick: false,

        targetSize: { width: '10em', height: '10em' }
    };

    renderButtonColumns() {
        let columns = [];
        for ( let key in Popup.positions ) {
            if ( !Popup.positions.hasOwnProperty( key ) ) {
                continue;
            }
            columns.push(
                <Col key={key} style={{ padding: '.7em' }}>
                    <Button size="small"
                            onClick={this.onOpenPopup.bind( this, Popup.positions[ key ] )}
                    >
                        {key}
                    </Button>
                </Col>
            )
        }
        return columns;
    }

    onToggleModal = () => {
        this.setState( {
            isModal: !this.state.isModal
        } )
    };

    onToggleStick = () => {
        this.setState( {
            isStick: !this.state.isStick
        } )
    };

    onOpenPopup = ( position ) => {
        this.setState( {
            position: position,
        }, () => {
            this.setState( {
                isPopupOpen: true
            } );
        } )
    };

    onPopupClose = () => {
        this.setState( {
            isPopupOpen: false
        } )
    };

    onRandomizeTargetSize = () => {
        const max = 15;
        const min = 1;
        this.setState( {
            targetSize: {
                width: Math.floor( Math.random() * (max - min + 1) + max ) + 'em',
                height: Math.floor( Math.random() * (max - min + 1) + max ) + 'em',
            }
        } )
    };

    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <div>
                <h2>Popup</h2>

                <Popup
                    onClose={this.onPopupClose.bind( this, '1' )}
                    isOpen={this.state.isPopupOpen}
                    stick={this.state.isStick}
                    target=".popup-target"
                    position={this.state.position}
                    modal={this.state.isModal}
                    style={{ width: '30em', opacity: .9 }}
                >
                    <PopupContent/>
                </Popup>

                <div
                    className="popup-target"
                    style={{
                        transition: 'width 300ms, height 300ms',
                        width: this.state.targetSize.width,
                        height: this.state.targetSize.height,
                        backgroundColor: '#888888',
                        marginBottom: '1.1em',
                        marginLeft: 'auto',
                        marginRight: 'auto',
                    }}
                    onClick={this.onRandomizeTargetSize}
                >
                    Target
                </div>

                <Row>
                    <Col>
                        <input
                            type="checkbox"
                            id="is-modal"
                            onChange={this.onToggleModal}
                            checked={this.state.isModal}/>
                        <label htmlFor="is-modal"> Modal</label>
                    </Col>
                    <Filler grow=".03"/>
                    <Col>
                        <input
                            type="checkbox"
                            id="is-stick"
                            onChange={this.onToggleStick}
                            checked={this.state.isStick}/>
                        <label htmlFor="is-stick"> Stick</label>
                    </Col>
                </Row>

                <Row flow="wrap">
                    {this.renderButtonColumns()}
                </Row>

            </div>
        );
    }
}