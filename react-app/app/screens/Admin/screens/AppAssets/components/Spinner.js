import React from "react";
import Spinner from 'components/support/spinner';

export default function SpinnerAsset() {
    return (
        <div>
            <h2>Spinner</h2>
            <div style={{ border: '.1em solid #ccc' }}>
                <Spinner/>
            </div>
        </div>
    );
}