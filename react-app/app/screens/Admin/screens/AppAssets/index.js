import React, { Component } from "react";
import Header from 'components/header';

import Colors from './components/Colors';
import Typography from './components/Typography';
import Buttons from './components/Buttons';
import Spinner from './components/Spinner';
import Message from './components/message';
import Popup from "./components/Popup";

export default class AppAssets extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const marginBottom = '1.5em';
        return (
            <div>
                <Header title="Assets"/>
                <div style={{ marginBottom: marginBottom }}>
                    <Typography/>
                </div>
                <div style={{ marginBottom: marginBottom }}>
                    <Popup/>
                </div>
                <div style={{ marginBottom: marginBottom }}>
                    <Buttons/>
                </div>
                <div style={{ marginBottom: marginBottom }}>
                    <Message/>
                </div>
                <div style={{ marginBottom: marginBottom }}>
                    <Spinner/>
                </div>
                <div style={{ marginBottom: marginBottom }}>
                    <Colors/>
                </div>
            </div>
        );
    }
}
