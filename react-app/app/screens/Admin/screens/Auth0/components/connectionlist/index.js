import React from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';

import Auth0ConnectionsQuery from './Auth0ConnectionsQuery.graphql';

function Auth0ConnectionList( { data } ) {

    const { auth0Connections } = data;

    return (
        <ul className="auth0-connection-list">
            {auth0Connections.map( ( connection ) => {

                const isEnabledForSize = connection.enabled_clients.indexOf(__PLANET_AUTH0_CLIENT_ID__) > -1;
                return (
                    <li key={connection.id}
                        className="auth0-connection-list__item">
                        <div>
                            <div>Name: {connection.name}</div>
                            <div>Strategy: {connection.strategy}</div>
                            <div>Enabled: {isEnabledForSize ? 'Yes':'No'}</div>

                        </div>
                    </li>
                );
            } )}

        </ul>
    );
}

export default graphql( Auth0ConnectionsQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( Auth0ConnectionList ) );

