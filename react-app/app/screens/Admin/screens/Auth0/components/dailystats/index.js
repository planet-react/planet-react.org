import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Message from 'components/support/message';

import Auth0DailyStatsQuery from './Auth0DailyStatsQuery.graphql';

Auth0DailyStatsList.propTypes = {
    from: PropTypes.string,
    to: PropTypes.string,
};

function Auth0DailyStatsList( { data } ) {

    const { auth0DailyStats } = data;

    if ( auth0DailyStats && auth0DailyStats.length == 0 ) {
        return <Message context="info">No data found</Message>;
    }

    let stats = auth0DailyStats.slice();
    return (
        <ul>
            {stats.reverse().map( ( stat, i ) => {
                return (
                    <li key={i}>
                        <div>Date: {stat.date}</div>
                        <div>Logins: {stat.logins}</div>
                        <div>Signups: {stat.signups}</div>
                        <div>Leaked Passwords: {stat.leaked_passwords}</div>
                    </li>
                )
            } )}
        </ul>
    );
}

export default graphql( Auth0DailyStatsQuery, {
    options: ( ownProps ) => {
        return ({
            fetchPolicy: 'network-only',
            variables: {
                from: ownProps.from || null,
                to: ownProps.to || null
            }
        })
    }
} )( withLoadingIndicator()( Auth0DailyStatsList ) );

