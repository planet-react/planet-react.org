import React from 'react';
import { Menu, MenuItem } from 'components/support/menu';

export default function AdminAuth0Menu() {
    return (
        <Menu type="underline">
            <MenuItem path="/admin/auth0/info">
                Info
            </MenuItem>
            <MenuItem path="/admin/auth0/users">
                Users
            </MenuItem>
            <MenuItem path="/admin/auth0/stats">
                Statistics
            </MenuItem>
        </Menu>
    )
}