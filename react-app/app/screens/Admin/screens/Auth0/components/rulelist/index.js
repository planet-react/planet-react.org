import React  from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';

import Auth0RulesQuery from './Auth0RulesQuery.graphql';

function Auth0RulesList( { data } ) {

    const { auth0Rules } = data;

    return (
        <ul className="auth0-rules-list">
            {auth0Rules.map( ( rule, i ) => {
                return (
                    <li key={rule.id} className="auth0-rules-list__item">
                        <div>
                            {rule.name} [<a
                            href={`https://manage.auth0.com/#/rules/${rule.id}`}>
                            view </a>]
                        </div>
                        <div>Order: {rule.order}</div>
                        <div>Enabled: {String( rule.enabled )}</div>
                        <div>Stage: {rule.stage}</div>
                    </li>
                );
            } )}
        </ul>
    );
}

export default graphql( Auth0RulesQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( Auth0RulesList ) );