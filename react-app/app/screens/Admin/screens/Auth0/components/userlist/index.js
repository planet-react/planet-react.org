import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import ProfileCard from 'components/auth0/card';
import Message from 'components/support/message';

import Auth0UserBrowseQuery from './Auth0UserBrowseQuery.graphql';

function Auth0UsersList( { data } ) {

    const { auth0UserBrowse } = data;

    if ( auth0UserBrowse.users && auth0UserBrowse.users.length == 0 ) {
        return <Message align="center">No users found</Message>;
    }

    return (
        <div>
            {auth0UserBrowse.users.map( ( user ) => {
                return (
                    <ProfileCard
                        data={user}
                        key={user.user_id}
                        pictureSize="5em"
                        layout="horizontal"
                    />
                )
            } )}
        </div>
    );
}

export default graphql( Auth0UserBrowseQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( Auth0UsersList ) );

