import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import { Screen } from 'components/screen';

import Header from 'components/header';
import Auth0Menu from './components/menu';

require( './style.scss' );

export default class Auth0Screen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { match } = this.props;

        return (
            <div>
                <Route render={( props ) => {
                    return (
                        <Auth0Menu/>
                    )
                }}/>

                <Route
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header title="Auth0"/>
                                <p>
                                    <a href="https://manage.auth0.com" target="_blank">
                                        Open Manager
                                    </a>
                                </p>
                            </div>
                        )
                    }}/>

                <Screen exact path={match.url + '/users'}/>

                <Screen exact path={match.url + '/info'}/>

                <Screen exact path={match.url + '/stats'}/>

            </div>

        )
    }
}