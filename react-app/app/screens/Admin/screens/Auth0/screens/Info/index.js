import React, { Component } from 'react';
import Header from 'components/header';
import ConnectionList from '../../components/connectionlist';
import RuleList from '../../components/rulelist';

export default class Auth0InfoScreen extends Component {

    render() {
        return (
            <div>
                <Header title="Info"/>
                <h2>Connections</h2>
                <ConnectionList/>

                <h2>Rules</h2>
                <RuleList/>
            </div>
        )
    }
};