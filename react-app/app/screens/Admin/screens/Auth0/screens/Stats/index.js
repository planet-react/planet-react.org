import React, { Component } from 'react';
import Header from 'components/header';
import { withServices } from 'components/services';
import { Menu } from 'components/support/menu';
import Auth0DailyStats from '../../components/dailystats';


class Auth0StatsScreen extends Component {

    state = {
        from: null,
        to: null,
        active: null
    };

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        this.onSetLast7days();
    }

    onSetLast7days = () => {
        const fromDate = new Date();
        fromDate.setDate( fromDate.getDate() - 7 );
        const toDate = new Date();
        this.updateFromToDates( fromDate, toDate, 'last7days' );
    };

    onSetThisMonth = () => {
        const fromDate = new Date();
        fromDate.setDate( 1 );
        const toDate = new Date();
        this.updateFromToDates( fromDate, toDate, 'thisMonth' );
    };

    onSetThisYear = () => {
        const fromDate = new Date();
        fromDate.setDate( 1 );
        fromDate.setMonth( 0 );
        const toDate = new Date();
        this.updateFromToDates( fromDate, toDate, 'thisYear' );
    };

    updateFromToDates( fromDate, toDate, active ) {
        const { dateHelper } = this.props.services;
        const from = dateHelper.format( fromDate, 'YYYY-MM-DD' );
        const to = dateHelper.format( toDate, 'YYYY-MM-DD' );

        this.setState( {
            from: from,
            to: to,
            active: active
        } );
    }

    render() {
        const { from, to, active } = this.state;

        return (
            <div>
                <Header title={`Auth0 Statistics: ${from} - ${to}`}/>

                <Menu>
                    <li>
                        <a href="#" onClick={this.onSetLast7days}
                           className={active == 'last7days' ? 'active' : ''}>
                            Last 7 days
                        </a>
                    </li>
                    <li>
                        <a href="#" onClick={this.onSetThisMonth}
                           className={active == 'thisMonth' ? 'active' : ''}>
                            This Month
                        </a>
                    </li>
                    <li>
                        <a href="#" onClick={this.onSetThisYear}
                           className={active == 'thisYear' ? 'active' : ''}>
                            This Year
                        </a>
                    </li>
                </Menu>

                <div style={{ marginTop: '.9em' }}>
                    <Auth0DailyStats from={from} to={to}/>
                </div>

            </div>
        )
    }
}

export default withServices( Auth0DailyStats );