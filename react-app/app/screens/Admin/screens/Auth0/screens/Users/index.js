import React, { Component } from 'react';
import Header from 'components/header';
import UserList from '../../components/userlist';

export default class Auth0UsersScreen extends Component {

    render() {
        return (
            <div>
                <Header title="Users"/>
                <UserList/>
            </div>
        )
    }
};