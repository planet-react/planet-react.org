import { printAST } from 'react-apollo';

import ApproveFeedMutation from './ApproveFeedMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ApproveFeedMutation );
    expect( ast ).toMatchSnapshot()
} );