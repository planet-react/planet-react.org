import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, withApollo } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import A from 'components/support/a';
import { ScreenLink } from 'components/screen';
import Message from 'components/support/message';
import FeedImage from 'components/feed/feedimage';
import Popup from 'components/support/popup';
import Button from 'components/support/button';
import OwnerForm from './ownerform';

import FeedsQuery from './graphql/FeedsQuery.graphql';
import ApproveFeedMutation from './graphql/ApproveFeedMutation.graphql';
import RemoveFeedMutation from 'graphql/RemoveFeedMutation.graphql';

require( './style.scss' );

class FeedList extends Component {

    static propTypes = {
        data: PropTypes.object
    };

    state = {
        isOwnerPopupOpen: false,
        changeOwnerFeed: null,
        isApproving: false,
    };

    constructor( props ) {
        super( props );
    }

    onApproveReject = ( feed, approved ) => {
        this.setState( {
            isApproving: true
        } );

        this.props.client.mutate( {
            mutation: ApproveFeedMutation,
            variables: {
                id: feed.id,
                approved: approved,
            }
        } ).then( () => {
            this.setState( {
                isApproving: false
            } );
        } );
    };

    onRemove = ( feed ) => {
        let confirm = window.confirm( `Danger! Are you sure you want to remove the feed '${feed.name}'` );
        if ( confirm ) {
            this.props.client.mutate( {
                mutation: RemoveFeedMutation,
                variables: {
                    id: feed.id
                }
            } ).then( ( { data } ) => {
                //@todo: updateQueries
                this.props.data.refetch();
            } );
        }
    };

    onRefresh = () => {
        this.props.data.refetch();
    };

    onOpenChangeOwnerPopup = ( feed ) => {
        this.setState( {
            isOwnerPopupOpen: true,
            changeOwnerFeed: feed
        } )
    };

    onCloseChangeOwnerPopup = () => {
        this.setState( {
            isOwnerPopupOpen: false
        } )
    };

    render() {

        const { feeds } = this.props.data;
        const { isOwnerPopupOpen, changeOwnerFeed, isApproving } = this.state;

        if ( feeds.length == 0 ) {
            return <Message align="center">No feeds found</Message>;
        }

        return (
            <div>
                <Popup
                    isOpen={isOwnerPopupOpen}
                    width="50vw"
                    target="body"
                    position={Popup.positions.SCREEN_CENTER}
                    onClose={this.onCloseChangeOwnerPopup}
                >
                    <OwnerForm feed={changeOwnerFeed}/>
                </Popup>

                <p>
                    <A onClick={this.onRefresh}>
                        Refresh</A>
                </p>

                <table className="admin-feed-list">
                    <thead>
                    <tr>
                        <th className="align-center">#</th>
                        <th/>
                        <th>Name</th>
                        <th className="align-center"/>
                        <th className="align-center"/>
                    </tr>
                    </thead>
                    <tbody>

                    {feeds.map( ( feed ) => {
                        return (
                            <tr key={feed.id}
                                className={'row ' + (feed.failed == 1 ? 'failed-row' : '')}>
                                <td className="align-center">
                                    <ScreenLink
                                        path={`/feeds/${feed.slug}`}>
                                        {feed.id}
                                    </ScreenLink>
                                </td>
                                <td>
                                    <FeedImage size={'3em'} src={feed.image}
                                               alt={feed.name}/>
                                </td>
                                <td>
                                    <div>
                                        <a className="admin-feed-list__title"
                                           href={feed.website_url}
                                           title={feed.name}>
                                            {feed.name}
                                        </a>
                                    </div>

                                    <div className="admin-feed-list__info">
                                        <span>
                                            <span> Feed URL: </span>
                                            <a href={feed.feed_url}
                                               title={feed.feed_url}>
                                                Feed
                                            </a>
                                        </span>
                                        <br/>
                                        <span>
                                            <span>Twitter URL: </span>
                                            {feed.twitter_url ? (
                                                <a href={feed.twitter_url}
                                                   title={feed.twitter_url}>
                                                    {feed.twitter_url}
                                                </a>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </span>
                                        <br/>
                                        <span>
                                            <span>Repo URL: </span>
                                            {feed.repo_url ? (
                                                <a href={feed.repo_url}
                                                   title={feed.repo_url}>
                                                    {feed.repo_url}
                                                </a>
                                            ) : (
                                                <span>N/A</span>
                                            )}
                                        </span>
                                        <br/>
                                        <span>Language: </span>{feed.lang}
                                        <br/>
                                        <span>Post count: </span>{feed.total_post_count}
                                    </div>

                                    <div className="admin-feed-list__approve">
                                        {!feed.approved ? (
                                            <p>
                                                <Button
                                                    onClick={( event ) => this.onApproveReject( feed, 1 )}
                                                    size="medium"
                                                    disabled={isApproving}
                                                    style={{ marginRight: '.5em' }}
                                                >
                                                    Approve
                                                </Button>
                                                <Button
                                                    onClick={( event ) => this.onApproveReject( feed, 0 )}
                                                    size="medium"
                                                    disabled={isApproving}
                                                >
                                                    Reject
                                                </Button>
                                            </p>
                                        ) : (
                                            <div>
                                                Approved
                                            </div>
                                        )}
                                    </div>

                                    <div>
                                        Has owner: {feed.has_owner ? 'Yes' : 'No'}

                                        <span> </span>
                                        [ <A
                                        onClick={() => this.onOpenChangeOwnerPopup( feed )}>
                                        Change owner
                                    </A> ]
                                    </div>

                                </td>

                                <td className="align-center">
                                    <ScreenLink
                                        path={`/admin/feed-manager/edit/${feed.slug}`}>
                                        Edit
                                    </ScreenLink>
                                </td>

                                <td className="align-center">
                                    <A onClick={( event ) => this.onRemove( feed, event )}>Delete</A>
                                </td>
                            </tr>
                        )
                    } )}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default withApollo( graphql( FeedsQuery, {
    options: {
        fetchPolicy: 'cache-and-network'
    }
} )( withLoadingIndicator()( FeedList ) ) );


