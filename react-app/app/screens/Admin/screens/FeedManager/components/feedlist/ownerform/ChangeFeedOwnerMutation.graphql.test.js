import { printAST } from 'react-apollo';

import ChangeFeedOwnerMutation from './ChangeFeedOwnerMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ChangeFeedOwnerMutation );
    expect( ast ).toMatchSnapshot()
} );