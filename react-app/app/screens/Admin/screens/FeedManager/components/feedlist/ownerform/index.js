import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'components/support/form';
import Button from 'components/support/button';

import ChangeFeedOwnerMutation from './ChangeFeedOwnerMutation.graphql';

export default class OwnerForm extends Component {

    static propTypes = {
        feed: PropTypes.shape( {
            id: PropTypes.number,
            name: PropTypes.string,
            user_id: PropTypes.string,
            has_owner: PropTypes.number,
        } ),
    };

    constructor( props ) {
        super( props );

        this.state = {

            id: props.feed.id,
            user_id: props.feed.user_id,
            has_owner: props.feed.has_owner
        };
    }

    onChange = ( event ) => {
        let value = event.target.type != 'checkbox'
            ? event.target.value
            : event.target.checked;

        this.setState( {
            [event.target.name]: value
        } );
    };

    render() {
        const { user_id, has_owner } = this.state;

        return (
            <div>
                <div style={{marginBottom: '.7em'}}>
                    Change owner for feed: {this.props.feed.name}
                </div>
                <Form
                    data={this.state}
                    mutation={ChangeFeedOwnerMutation}
                >
                    <Input
                        name="user_id"
                        value={user_id}
                        id="feed-owner-user-id"
                        label="New Owner (User ID)"
                        required={true}
                        onChange={this.onChange}
                    />

                    <Input
                        name="has_owner"
                        type="checkbox"
                        checked={has_owner}
                        id="feed-owner-has-owner"
                        label="Is original owner"
                        onChange={this.onChange}
                    />
                    <p>
                        <Button type="submit">
                            Update
                        </Button>
                    </p>
                </Form>
            </div>
        );
    }
}