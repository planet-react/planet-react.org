import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import { Screen } from 'components/screen';

import Header from 'components/header';
import FeedList from './components/feedlist';

export default class FeedManagerScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { match } = this.props;
        return (
            <div>
                <Route
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header title="Manage Feeds"/>
                                <FeedList/>
                            </div>
                        )
                    }}/>

                <Screen exact path={match.url + '/edit/:slug'}/>

            </div>
        )
    }
}