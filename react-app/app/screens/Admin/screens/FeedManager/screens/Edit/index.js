import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import { withServices } from 'components/services';
import { withLoadingIndicator } from 'components/graphql';
import Header from 'components/header';
import SubmitFeedForm from 'components/feed/submitform'

import FeedQuery from 'graphql/FeedQuery.graphql';

class EditFeedScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { data } = this.props;

        const { feed } = data;
        const feedData = {
            id: feed.id,
            name: feed.name,
            feed_url: feed.feed_url,
            lang: feed.lang,
            description: feed.description
        };

        return (
            <div>
                <Header title={`Edit Feed: ${feedData.name}`}/>
                <SubmitFeedForm feedData={feedData}/>
            </div>
        )
    }
}

export default compose(
    withServices,
    graphql( FeedQuery, {
        options: ( ownProps ) => ({
            variables: {
                slug: ownProps.match.params.slug
            }
        }),
    } )
)( withLoadingIndicator()( EditFeedScreen ) );