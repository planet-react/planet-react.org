import { printAST } from 'react-apollo';

import DeleteReportMutation from './DeleteReportMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( DeleteReportMutation );
    expect( ast ).toMatchSnapshot()
} );