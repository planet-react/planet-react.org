import { printAST } from 'react-apollo';

import ReportsQuery from './ReportsQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ReportsQuery );
    expect( ast ).toMatchSnapshot()
} );