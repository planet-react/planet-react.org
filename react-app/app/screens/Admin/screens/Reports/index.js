import React, { Component } from "react";
import { compose, graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Spinner from 'components/support/spinner';
import Header from 'components/header';
import Message from 'components/support/message';
import { ScreenLink } from 'components/screen';
import Button from 'components/support/button';

import ReportsQuery from './ReportsQuery.graphql';
import DeleteReportMutation from './DeleteReportMutation.graphql';

require( './style.scss' );

class ReportList extends Component {

    state = {
        isDeleting: false
    };

    constructor( props ) {
        super( props );
        this.onRemovePostReport = this.onRemovePostReport.bind( this );
    }

    onRemovePostReport( reportId, postId ) {
        if ( !window.confirm( 'This will delete the post and all reports that has reported the post.\nContinue?' ) ) {
            return;
        }

        this.setState( {
            isDeleting: true
        } );

        this.props.deleteReport( {
            variables: { id: reportId },
            update: ( proxy ) => {
                const query = { query: ReportsQuery };
                const data = proxy.readQuery( query );
                const newData = data.reports.filter(
                    report => report.id != reportId
                );
                proxy.writeQuery( {
                    query: ReportsQuery,
                    data: { reports: newData }
                } );
            }
        } ).then( ( { data } ) => {
            this.setState( {
                isDeleting: false
            } );
        } ).catch( ( error ) => {
            this.setState( {
                isDeleting: false
            } );
        } );
    }

    render() {
        const { isDeleting } = this.state;
        const { reports } = this.props.data;

        if ( isDeleting ) {
            return <Spinner/>;
        }

        return (
            <div>
                <Header title="Reports"/>
                {reports.length == 0 ? (
                    <Message>No reports found</Message>
                ) : (
                    <ul className="reports-list">
                        {reports.map( ( report ) => {
                            return (
                                <li key={report.id}>
                                    <div>
                                        <ScreenLink
                                            path={`/read/${report.content_id}`}
                                        >
                                            <b>{report.title}</b>
                                            <span> (#{report.content_id})</span>
                                        </ScreenLink>
                                    </div>
                                    <div>
                                        <label>Link: </label>
                                        <a href={report.url}>
                                            {report.url}
                                        </a>
                                    </div>
                                    <div>
                                        <label>Reason: </label>{report.reason}
                                        {report.other_reason != '' && <div>
                                            {report.other_reason}
                                        </div>}
                                    </div>
                                    <div>
                                        <label>Reported: </label>{report.created_at}
                                    </div>
                                    <p>
                                        <Button
                                            onClick={this.onRemovePostReport.bind( this, report.id, report.content_id )}
                                        >
                                            Remove
                                        </Button>
                                    </p>
                                </li>
                            );
                        } )}
                    </ul>
                )}
            </div>
        );
    }
}

export default compose(
    graphql( ReportsQuery, {
            options: {
                fetchPolicy: 'network-only',
            }
        }
    ),
    graphql( DeleteReportMutation, { name: 'deleteReport' } ),
)( withLoadingIndicator()( ReportList ) );
