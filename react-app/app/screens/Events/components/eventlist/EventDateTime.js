import React from 'react';
import PropTypes from 'prop-types';
import DateFormatter from 'components/support/dateformatter';
import CalendarIcon from 'react-icons/lib/fa/calendar';
import ClockIcon from 'react-icons/lib/fa/clock-o';

EventDateTime.propTypes = {
    dateHelper: PropTypes.object.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string,
    timezone: PropTypes.string,
};

const printDate = ( dateStr, prefix = null ) => {
    return [
        <span key="col-1" style={{ paddingRight: '.5em' }}>
            <CalendarIcon
                size={15}
                color="grey"
                style={{ verticalAlign: 'text-top' }}
            />
        </span>,
        <span key="col-2">{prefix} </span>,
        <DateFormatter key="col-3" date={dateStr} format="ddd MMM Do"/>
    ]
};

const printTime = ( dateStr, showIcon = true ) => {
    return [
        <span key="col-1">
            {showIcon &&
            <span style={{ padding: '0 .5em' }}>
                <ClockIcon
                    size={15}
                    color="grey"
                    style={{ verticalAlign: 'text-top' }}
                />
            </span>}
        </span>,
        <DateFormatter key="col-2" date={dateStr} format="HH:mm"/>
    ]
};

const printTimezone = ( timezone ) => {
    return (
        <span> (UTC {timezone})</span>
    )
};

export default function EventDateTime( { dateHelper, startDate, endDate, timezone } ) {
    return (
        <div className="event-list-item__date-time">
            {dateHelper.isSameDay( startDate, endDate ) ? (
                <span>
                    {printDate( startDate )}

                    {dateHelper.format( startDate, 'HHmm' ) != '0000' &&
                    printTime( startDate )}

                    {dateHelper.format( startDate, 'HHmm' ) != '0000' &&
                    <span>
                        <span> - </span> {printTime( endDate, false )}
                    </span>}

                    {timezone && printTimezone( dateHelper.formatTimezone( timezone ) )}
                </span>
            ) : (
                <span>
                    {printDate( startDate, 'From:' )}

                    {printTime( startDate )}

                    {timezone && printTimezone( dateHelper.formatTimezone( timezone ) )}

                    <br/>

                    {printDate( endDate, 'To:' )}

                    {printTime( endDate )}

                    {timezone && printTimezone( dateHelper.formatTimezone( timezone ) )}

                </span>
            )}
        </div>

    )
}
