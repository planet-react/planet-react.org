import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import EventDateTime from './EventDateTime';

const dateHelperMock = {
    format: jest.fn(),
    isSameDay: jest.fn()
};

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <EventDateTime
                dateHelper={dateHelperMock}
                startDate="2017-01-01 10:00:00"
                endDate="2017-01-02 16:30:00"
            />
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
