import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withAuth0, withServices } from 'components/services';
import { Col, Row } from 'components/support/flex';
import CalendarIcon from 'components/calendaricon';
import BookmarkToggle from 'components/bookmark/toggle';
import { ScreenLink } from "components/screen";
import GoogleCalendarLink from './GoogleCalendarLink';
import EventDateTime from './EventDateTime';
import Message from 'components/support/message';
import Button from 'components/support/button';
import eventTypes from 'components/event/services/eventTypes';
import PencilIcon from 'react-icons/lib/fa/pencil';
import Links from 'components/links';
import A from 'components/support/a';
import EventLocation from './EventLocation';

require( './style.scss' );

class EventItem extends Component {

    static propTypes = {
        event: PropTypes.shape( {
            type: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            start_date: PropTypes.string.isRequired,
            end_date: PropTypes.string.isRequired,
            venue: PropTypes.string,
            city: PropTypes.string,
            country: PropTypes.string,
            organizer: PropTypes.string,
            website_link: PropTypes.string,
            twitter_link: PropTypes.string,
            user_id: PropTypes.string,
        } ).isRequired,
        showName: PropTypes.bool,
    };

    static defaultProps = {
        showName: true
    };

    state = {
        showFullDescription: false
    };

    maxDescriptionLength = 70;

    constructor( props ) {
        super( props );
        this.onToggleDescription = this.onToggleDescription.bind( this );
    }

    onToggleDescription() {
        this.setState( {
            showFullDescription: !this.state.showFullDescription
        } );
    }

    render() {

        const {
            event,
            showName,
            auth0: { isAuthenticated, userProfile },
            services: { dateHelper },
        } = this.props;

        const { showFullDescription } = this.state;

        const canEdit = isAuthenticated && userProfile.user_id == event.user_id;

        const eventType = eventTypes.findByKey( 'id', event.type );

        const hasLongDescription = event.description && event.description.length > this.maxDescriptionLength;

        const description = !showFullDescription && hasLongDescription
            ? event.description.substring( 0, this.maxDescriptionLength ) + ' ...'
            : event.description;

        return (
            <Row className="event-list-item">
                <Col className="event-list-item__date-col">
                    <CalendarIcon date={event.start_date}/>
                </Col>

                <Col className="event-list-item__info-col">
                    {event.active == 0 &&
                    <Message context="info">
                        <b>This event is not active.</b>
                        <br/>
                        Only you can view this event as you are the owner.
                    </Message>}

                    {showName &&
                    <div>
                        <span>{eventType.name}</span>
                        <h2>
                            <span>{event.name} </span>
                            <ScreenLink
                                path={`/events/view/${event.id}`}>
                                #
                            </ScreenLink>
                        </h2>
                    </div>}

                    <EventDateTime
                        startDate={event.start_date}
                        endDate={event.end_date}
                        timezone={event.timezone}
                        dateHelper={dateHelper}/>

                    <div className="event-list-item__tools">
                        {canEdit &&
                        <ScreenLink path={`/profile/events/edit/${event.id}`}>
                            <Button
                                icon={PencilIcon}
                                iconColor="#acacac"
                                color="transparent"
                                size="small"
                            />
                        </ScreenLink>}

                        {event.website_link &&
                        <BookmarkToggle
                            title={event.name}
                            url={event.website_link}
                            bookmarkId={event.bookmarkId}
                        />}

                        <GoogleCalendarLink
                            name={event.name}
                            startDate={event.start_date}
                            endDate={event.end_date}
                            timezone={event.timezone}
                            description={event.description}
                            location={event.location}
                        />
                    </div>

                    {description &&
                    <div className="event-list-item__description-field">
                        {description}

                        {hasLongDescription &&
                        <span>
                            <span> </span>
                            [<A onClick={this.onToggleDescription}>
                            {showFullDescription ? 'Less' : 'More'}</A>]
                        </span>}
                    </div>}

                    {(event.location || event.country) &&
                    <EventLocation
                        location={event.location}
                        country={event.country}
                    />}

                    {(event.website_link || event.twitter_link) &&
                    <div className="event-list-item__links">
                        <b>Links: </b>
                        <Links
                            siteUrl={event.website_link}
                            twitterUrl={event.twitter_link}
                        />
                    </div>}

                </Col>
            </Row>
        )
    }
}

export default withAuth0( withServices( EventItem ) );