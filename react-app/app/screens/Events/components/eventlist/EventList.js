import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EventItem from './EventItem';

require( './style.scss' );

EventList.propTypes = {
    events: PropTypes.array
};

export default function EventList( { events } ) {
    return (
        <div className="event-list">
            {events.map( ( event ) =>
                <EventItem event={event} key={event.id}/>
            )}
        </div>
    )
}
