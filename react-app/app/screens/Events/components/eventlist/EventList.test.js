import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import EventList from './EventList';

let eventShape = {
    type: 1,
    name: 'Event Name',
    start_date: '2017-01-01 10:00:00',
    end_date: '2017-01-02 16:30:00',
    venue: 'The venue 123',
    city: 'Berlin',
    country: 'DE',
    organizer: 'The Organizer',
    website_link: 'https://test.com',
    twitter_link: 'https://twitter.com/test',
    user_id: 'user-abc-987',
};

let eventsMock = [];
for ( let i = 0; i < 10; i++ ) {
    let eventMock = Object.assign( {}, { id: i }, eventShape );
    eventsMock.push( eventMock )
}

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <EventList events={eventsMock}/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
