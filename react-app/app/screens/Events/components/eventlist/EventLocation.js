import React from "react";
import PropTypes from 'prop-types';
import LocationIcon from 'react-icons/lib/md/location-on';
import { Col, Row } from 'components/support/flex';

EventLocation.propTypes = {
    location: PropTypes.string,
    country: PropTypes.string,
};

export default function EventLocation( { location, country } ) {
    return (
        <Row className="event-list-item__location">
            <Col flexBasis="2.1em">
                <LocationIcon size={21} color="#888888"/>
            </Col>
            <Col>
                {location &&
                <div className="event-list-item__location-field">
                    <b>Location:</b>
                    <div>
                        {location}
                    </div>
                </div>}

                {country &&
                <div>
                    {country}
                </div>}
            </Col>

        </Row>
    );
}
