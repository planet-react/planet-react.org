import React from 'react';
import renderer from 'react-test-renderer';

import EventLocation from './EventLocation';

it( 'renders correctly', () => {
    let tree = renderer.create(
        <EventLocation
            country="FI"
            location="GLO Hotel Kluuvi Helsinki\nKluuvikatu 4, Helsinki 00100"
        />
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
