import React from 'react';
import PropTypes from 'prop-types';
import GoogleIcon from 'react-icons/lib/fa/google';

GoogleCalendarLink.propTypes = {
    name: PropTypes.string.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string,
    timezone: PropTypes.string,
    description: PropTypes.string,
    location: PropTypes.string,
};

function pad( val ) {
    return val < 10 ? '0' + val : val;
}

function makeGoogleDate( dateStr, timezone ) {
    const dateObj = new Date( dateStr.replace( / /, 'T' ) );

    if ( timezone ) {
        let tzParts = timezone.split( '.' );
        let h = tzParts[ 0 ];
        let m = tzParts[ 1 ];

        if ( m == 5 ) {
            m = 30;
        }
        else if ( m == 75 ) {
            m = 45;
        }
        else {
            m = 0;
        }

        let hourOffset;
        let minuteOffset;
        if ( timezone < 0 ) {
            hourOffset = Math.abs( parseFloat( h ) );
            minuteOffset = Math.abs( parseFloat( m ) );
        }
        else {
            hourOffset = -Math.abs( parseFloat( h ) );
            minuteOffset = -Math.abs( parseFloat( m ) );
        }

        dateObj.setHours( dateObj.getHours() + hourOffset );
        dateObj.setMinutes( dateObj.getMinutes() + minuteOffset );
    }

    let year = dateObj.getFullYear();
    let month = pad( dateObj.getMonth() + 1 );
    let date = pad( dateObj.getDate() );
    let hours = pad( dateObj.getHours() );
    let minutes = pad( dateObj.getMinutes() );

    return year + '' + month + '' + date + 'T' + hours + minutes + '00Z';
}

export default function GoogleCalendarLink( { name, startDate, endDate, timezone, description, location } ) {
    const fromDate = makeGoogleDate( startDate, timezone );
    const toDate = makeGoogleDate( endDate, timezone );

    return (
        <a href={`http://www.google.com/calendar/event?action=TEMPLATE
&text=${encodeURIComponent( name )}
&dates=${fromDate}/${toDate}
&details=${encodeURIComponent( description )}
&location=${encodeURIComponent( location )}
&trp=false
&sprop=
&sprop=name:`} target="_blank" rel="nofollow" title="Add to Google Calendar">
            <GoogleIcon size={17} color="#4285f4"/>
        </a>

    )
}
