import { printAST } from 'react-apollo';

import BrowseEventsQuery from './BrowseEventsQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( BrowseEventsQuery );
    expect( ast ).toMatchSnapshot()
} );