import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { PaginationContainer, withLoadingIndicator } from 'components/graphql';
import Message from 'components/support/message';
import EventList from './EventList';

import BrowseEventsQuery from './graphql/BrowseEventsQuery.graphql';

EventListWithData.propTypes = {
    eventTypeId: PropTypes.number,
    past: PropTypes.bool
};

function EventListWithData( { data } ) {

    //@todo: rename .data?
    const events = data.browseEvents.data;

    if ( events.length == 0 ) {
        return <Message>No events where found</Message>
    }

    return (
        <PaginationContainer data={data} dataKey="browseEvents">
            <EventList events={events}/>
        </PaginationContainer>
    )
}

export default graphql( BrowseEventsQuery, {
    options: ( ownProps ) => {
        return ({
            fetchPolicy: 'network-only',
            variables: {
                page: 1,
                type: ownProps.eventTypeId || null,
                orderByDirection: ownProps.past ? 'desc' : 'asc',
                past: ownProps.past
            }
        })
    },
} )( withLoadingIndicator()( EventListWithData ) );