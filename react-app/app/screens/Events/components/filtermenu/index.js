import React from 'react';
import PropTypes from 'prop-types';
import { Menu, MenuItem }from 'components/support/menu';

require( './style.scss' );

Menu.propTypes = {
    eventTypes: PropTypes.array.isRequired,
    selectedEventTypeId: PropTypes.number,
    past: PropTypes.bool,
};

export default function EventsFilterMenu( { eventTypes, selectedEventTypeId, past } ) {

    return (
        <div className="events-filter-menu">

            <Menu type="underline">

                <MenuItem
                    exact
                    path={`/events?past=${past}`}
                    isActive={e => selectedEventTypeId == null}
                >
                    All
                </MenuItem>
                {eventTypes.map( ( type ) => {
                    return (
                        <MenuItem
                            key={type.id}
                            isActive={e => selectedEventTypeId == type.id}
                            path={`/events?type=${type.id}&past=${past}`}>
                            {type.name}
                        </MenuItem>
                    )
                } )}
            </Menu>

            <Menu type="underline">
                <MenuItem
                    isActive={e => !past}
                    path={`/events?type=${selectedEventTypeId}`}
                >
                    Upcoming
                </MenuItem>
                <MenuItem
                    isActive={e => past}
                    path={`/events?type=${selectedEventTypeId}&past=true` }
                >
                    Past
                </MenuItem>
            </Menu>

        </div>
    )
}