import React, { Component } from 'react';
import { Screen, ScreenLink } from 'components/screen';

import Header from 'components/header';
import EventsFilterMenu from './components/filtermenu';
import A from 'components/support/a';
import EventList from './components/eventlist';
import eventTypes from 'components/event/services/eventTypes';
import { withAuth0, withServices } from 'components/services'

class EventsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    onLogin = () => {
        const { auth0 } = this.props;
        auth0.showLogin();
    };

    render() {

        const { auth0 } = this.props;

        return (
            <div>
                <Screen exact path="/events"
                        render={( props ) => {

                            let past = location.href.match( /past=true/ ) != null;

                            const { isAuthenticated } = auth0;
                            let selectedEventType = location.href.match( /type=([\d]+)/ );
                            selectedEventType = selectedEventType ? Number( selectedEventType[ 1 ] ) : null;

                            return (
                                <div>
                                    <Header title="Events"/>
                                    <p>
                                        {isAuthenticated ? (
                                            <ScreenLink path="/profile/events">
                                                Manage Events ❱
                                            </ScreenLink>
                                        ) : (
                                            <A onClick={this.onLogin}>
                                                + Add New Event
                                            </A>
                                        )
                                        }
                                    </p>

                                    <div>

                                        <EventsFilterMenu
                                            eventTypes={eventTypes.data}
                                            selectedEventTypeId={selectedEventType}
                                            past={past}
                                        />

                                        <EventList
                                            eventTypeId={Number( selectedEventType )}
                                            past={past}
                                        />
                                    </div>
                                </div>
                            )
                        }}/>

                <Screen exact path="/events/view/:eventId"/>

            </div>
        )
    }
}

export default withAuth0( withServices( EventsScreen ) );
