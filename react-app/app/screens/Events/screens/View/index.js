import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Header from 'components/header';
import { ScreenLink, ScreenError } from "components/screen";
import EventItem from '../../components/eventlist/EventItem';
import EventQuery from 'graphql/EventQuery.graphql';

class EventViewScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        const { data } = this.props;

        if ( !data.event ) {
            return <ScreenError statusCode={404}/>;
        }

        return (
            <div>
                <Header title={`Event: ${data.event.name}`}/>
                <p>
                    <ScreenLink path="/events">
                        ❮ Back to Events
                    </ScreenLink>
                </p>

                <EventItem event={data.event} showName={false}/>
            </div>
        )
    }
}

export default graphql( EventQuery, {
    options: ( ownProps ) => {
        return ({
            variables: {
                id: Number( ownProps.match.params.eventId ) || null,
            }
        })
    },
} )( withLoadingIndicator()( EventViewScreen ) );
