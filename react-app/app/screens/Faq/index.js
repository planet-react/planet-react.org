import React, { Component } from 'react';
import Header from 'components/header';
import { ScreenLink } from 'components/screen';

export default class FAQScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        return (
            <div>
                <Header title="FAQ"/>
                <dl>
                    <dt>What is Planet React?</dt>
                    <dd>Planet React is an aggregation of blogs and news
                        from
                        the React community. The views expressed here are
                        those
                        of the author and do not necessarily reflect the
                        views
                        of Facebook.
                    </dd>

                    <dt>Who is behind it?</dt>
                    <dd>The site is maintained by Thomas Andersen. Facebook
                        has nothing to do with it.
                    </dd>

                    <dt>How are feeds selected for Planet React?</dt>
                    <dd>
                        <p>
                            There’s no real policy behind it. If an
                            interesting
                            blog is found or someone submits one, then it
                            will be
                            looked at and if it has unique content or is a
                            well
                            known developer, it will be added.
                        </p>
                        <p>
                            We also usually just take React related content
                            from
                            those blogs. This generally means, that we
                            aggregate
                            the *React* category feed of it, as the usual
                            Planet
                            React
                            reader is not interested in the well-beings of
                            your
                            cat ;) Most weblog software supports that
                            nowadays.
                        </p>
                    </dd>

                    <dt>
                        What is the criteria for post aggregation?
                    </dt>
                    <dd>
                        <ol style={{margin: '0.7em 2.1em'}}>
                            <li>The post's owner feed must have been approved.</li>
                            <li>The post must contain more than 1200 characters.</li>
                            <li>The post must have one or more of the following <a
                                href="https://validator.w3.org/feed/docs/rss2.html#ltcategorygtSubelementOfLtitemgt">sub category elements</a>: <code>{__PLANET_AGGREGATOR_CATEGORY_FILTER__}</code></li>
                        </ol>
                        <p>
                            If your posts doesn't show up or have suggestions for improvements, please contact us and we'll take a look.
                        </p>
                    </dd>

                    <dt>
                        How often are feeds aggregated?
                    </dt>
                    <dd>
                        Feeds are aggregated every hour
                    </dd>

                    <dt>What User-Agent string is used by the aggregator?
                    </dt>
                    <dd>
                        <code>{__PLANET_AGGREGATOR_USER_AGENT__}</code>
                    </dd>

                    <dt>
                        How can I add my feed to {__APP_NAME__}?
                    </dt>
                    <dd>
                        Please use <ScreenLink path="/profile/feeds/submit">this
                        submit form</ScreenLink> (requires login).
                    </dd>

                    <dt>
                        Where is the source code?
                    </dt>
                    <dd>
                        <a href={__PLANET_REPO__}>{__PLANET_REPO__}</a>
                    </dd>

                    <dt>My blog is on the list, how can I remove it?</dt>
                    <dd>
                        Send an email to {__PLANET_EMAIL__} and ask for owner ship or ask us to remove it for you
                    </dd>
                </dl>
            </div>
        )
    }
}