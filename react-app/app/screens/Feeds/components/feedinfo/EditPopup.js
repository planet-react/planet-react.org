import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';
import SubmitFeedForm from 'components/feed/submitform'

EditPopup.propTypes = {
    feed: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default function EditPopup( { feed, isOpen, onClose } ) {
    return (
        <Popup
            target=".feed-info"
            position={Popup.positions.SCREEN_CENTER}
            isOpen={isOpen}
            onClose={onClose}
        >
            <SubmitFeedForm
                feedData={feed}
                autoFocus={false}
            />
        </Popup>
    );
}