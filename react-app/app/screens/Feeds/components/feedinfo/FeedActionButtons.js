import React from "react";
import PropTypes from 'prop-types';
import { Col, Row } from 'components/support/flex';
import Button from 'components/support/button';

FeedActionButtons.propTypes = {
    feedId: PropTypes.number.isRequired,
    isRemoved: PropTypes.bool.isRequired,
    onOpenEditPopup: PropTypes.func.isRequired,
    onOpenFetchPopup: PropTypes.func.isRequired,
    onToggleRemoveFeed: PropTypes.func.isRequired,
};

FeedActionButtons.defaultProps = {};

export default function FeedActionButtons( { feedId, isRemoved, onOpenEditPopup, onOpenFetchPopup, onToggleRemoveFeed } ) {
    return (
        <Row style={{ margin: '1.5em 0' }}>
            <Col>
                <Button
                    size="small"
                    onClick={() => onOpenEditPopup()}
                    title="Edit feed info">
                    Edit
                </Button>
            </Col>
            <Col style={{ marginLeft: '.7em' }}>
                <Button
                    size="small"
                    onClick={() => onOpenFetchPopup( feedId )}
                    title="Manually fetch posts"
                >
                    Fetch
                </Button>
            </Col>
            <Col style={{ marginLeft: '1.7em' }}>
                <Button
                    size="small"
                    onClick={() => onToggleRemoveFeed( feedId )}
                    title="Remove feed">
                    {isRemoved ? 'Undo' : 'Remove'}
                </Button>
            </Col>

        </Row>
    );
}