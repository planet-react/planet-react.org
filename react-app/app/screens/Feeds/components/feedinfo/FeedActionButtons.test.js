import React from 'react';
import FeedActionButtons from './FeedActionButtons';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {

    let tree = renderer.create(
       <FeedActionButtons
           feedId={17}
           isRemoved={false}
           onToggleRemoveFeed={jest.fn()}
           onOpenEditPopup={jest.fn()}
           onOpenFetchPopup={jest.fn()}
       />
    ).toJSON();

    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
       <FeedActionButtons
           feedId={19}
           isRemoved={true}
           onToggleRemoveFeed={jest.fn()}
           onOpenEditPopup={jest.fn()}
           onOpenFetchPopup={jest.fn()}
       />
    ).toJSON();

    expect( tree ).toMatchSnapshot();

} );