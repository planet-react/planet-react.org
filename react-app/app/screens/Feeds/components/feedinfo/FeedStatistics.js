import React from "react";
import PropTypes from 'prop-types';
import { Row, Col } from 'components/support/flex';

FeedStatistics.propTypes = {
    feed: PropTypes.object.isRequired,
};

export default function FeedStatistics( {feed} ) {
    return (
        <Row className="feed-info__stats">
            <Col className="value-box">
                <em>{feed.total_post_count}</em>
                <span>Post{feed.total_post_count != 1 && 's total'}</span>
            </Col>
            <Col className="value-box">
                <em>{feed.month_post_count}</em>
                <span>
                    Post{feed.month_post_count != 1 && 's'} this month
                </span>
            </Col>
            <Col className="value-box">
                <em>{feed.lang}</em>
                <span>Language</span>
            </Col>
        </Row>
    );
}