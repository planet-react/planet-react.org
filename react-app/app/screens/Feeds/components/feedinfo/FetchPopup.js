import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';
import Button from 'components/support/button';
import { GraphQlError } from 'components/graphql';

FetchPopup.propTypes = {
    feed: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    onFetchPosts: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    fetchResult: PropTypes.number,
    fetchErrors: PropTypes.object,
};

export default function FetchPopup( { feed, isOpen, onFetchPosts, isFetching, onClose, fetchResult, fetchErrors } ) {
    return (

        <Popup
            target=".feed-info"
            position={Popup.positions.SCREEN_CENTER}
            isOpen={isOpen}
            onClose={onClose}
        >

            {fetchErrors && <GraphQlError errors={fetchErrors}/>}

            <div style={{ textAlign: 'center' }}>

                <div style={{margin:'1.1em 0'}}>
                    This will try to fetch tha latest posts from
                    <br/>
                    {feed.feed_url}
                </div>

                <div>
                    <Button
                        onClick={() => onFetchPosts( feed.id )}
                        disabled={isFetching}
                    >
                        <div>Fetch now</div>
                    </Button>

                    {isFetching ? (
                        <div>Fetching...</div>
                    ) : (
                        <div>
                            {fetchResult != null &&
                            <span>
                            <span>{fetchResult} post</span>{fetchResult > 1 ? 's' : ''}
                                <span> fetched</span>
                        </span>}
                        </div>
                    )}
                </div>
            </div>
        </Popup>
    );
}