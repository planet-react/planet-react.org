import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, withApollo } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import { withRouter } from 'react-router-dom';
import { withAuth0 } from 'components/services';
import { Col, Row } from 'components/support/flex';
import Header from 'components/header';
import FeedImage from 'components/feed/feedimage';
import SubscribeButton from 'components/subscription/button';
import Message from 'components/support/message';
import FeedActionButtons from './FeedActionButtons';
import Links from 'components/links';
import FeedStatistics from './FeedStatistics';
import EditPopup from './EditPopup';
import FetchPopup from './FetchPopup';
import Tags from 'components/tags';

import FeedQuery from 'graphql/FeedQuery.graphql';
import FetchPostsMutation from './graphql/FetchPostsMutation.graphql';
import RemoveFeedMutation from 'graphql/RemoveFeedMutation.graphql';

require( './style.scss' );

class FeedInfo extends Component {

    static propTypes = {
        data: PropTypes.object,
    };

    state = {
        isEditPopupOpen: false,
        isFetchPopupOpen: false,
        isFetchingPosts: false,
        fetchErrors: null,
        fetchResult: null,
        isRemoved: false
    };

    onOpenEditPopup = () => {
        this.setState( {
            isEditPopupOpen: true
        } );
    };

    onCloseEditPopup = () => {
        this.setState( {
            isEditPopupOpen: false
        } );
    };

    onOpenFetchPopup = () => {
        this.setState( {
            isFetchPopupOpen: true,
            fetchErrors: null
        } );
    };

    onCloseFetchPopup = () => {
        this.setState( {
            isFetchPopupOpen: false
        } );
    };

    onFetchPosts = ( id ) => {
        if ( !id ) {
            return;
        }

        this.setState( {
            isFetchingPosts: true,
            fetchResult: null,
        }, () => {
            this.props.client.mutate( {
                mutation: FetchPostsMutation,
                variables: {
                    feedId: id,
                },
                refetchQueries: [
                    'PostsQuery',
                ],
            } ).then( ( { data } ) => {
                this.setState( {
                    isFetchingPosts: false,
                    fetchResult: data.fetchPosts.count,
                    fetchErrors: null,
                } );
            } ).catch( ( error ) => {
                this.setState( {
                    isFetchingPosts: false,
                    fetchErrors: error,
                } );
            } );
        } );
    };

    onToggleRemoveFeed = ( id ) => {
        if ( !id ) {
            return;
        }

        const { isRemoved } = this.state;

        this.props.client.mutate( {
            mutation: RemoveFeedMutation,
            variables: {
                id: id,
                isRemoved: isRemoved
            }
        } ).then( ( { data } ) => {
            this.setState( {
                isRemoved: !isRemoved
            } )
        } ).catch( ( error ) => {

        } );
    };

    render() {
        const { auth0: { isAuthenticated } } = this.props;
        const {
            isEditPopupOpen,
            isFetchPopupOpen,
            isFetchingPosts,
            fetchResult,
            fetchErrors,
            isRemoved
        } = this.state;

        const { feed } = this.props.data;
        if ( !feed ) {
            return (
                <Message align="center">
                    Sorry, this feed does not exist
                </Message>
            );
        }

        const hasOwner = feed.has_owner;
        const loggedInUserIsOwner = feed.user_is_owner;

        const image = loggedInUserIsOwner
            ? feed.image + '?t=' + new Date().getTime()
            : feed.image;

        return (
            <div>
                <Header title={`${feed.name}`}/>

                <div className="feed-info">
                    <Row justifyContent="space-between" alignItems="top">
                        <Col className="feed-info__image-col">
                            <FeedImage src={image} size="7.3em"/>
                        </Col>
                        <Col flexGrow="1" style={{paddingTop: '.5em'}}>
                            <div>
                                {feed.description}
                            </div>

                            <div style={{ margin: '.7em 0' }}>
                                <Links
                                    siteUrl={feed.website_url}
                                    feedUrl={feed.feed_url}
                                    twitterUrl={feed.twitter_url}
                                    repoUrl={feed.repo_url}
                                />
                            </div>

                            <div style={{ margin: '1.5em 0' }}>
                                <SubscribeButton
                                    type="feed"
                                    value={feed.id}
                                    title={feed.name}
                                />
                            </div>

                            {isAuthenticated && loggedInUserIsOwner &&
                            <FeedActionButtons
                                feedId={feed.id}
                                isRemoved={isRemoved}
                                onOpenEditPopup={this.onOpenEditPopup}
                                onOpenFetchPopup={this.onOpenFetchPopup}
                                onToggleRemoveFeed={this.onToggleRemoveFeed}
                                onFetchPosts={this.onFetchPosts}
                            />}

                        </Col>
                    </Row>
                    {!hasOwner &&
                    <Message context="info">
                        Owner of this blog/site?<br/>
                        Send an email to {__PLANET_EMAIL__} and request the ownership over it's content here at {__APP_NAME__}
                    </Message>}

                    <h3>Stats</h3>
                    <FeedStatistics feed={feed}/>

                    {(feed.tags && feed.tags.length > 0) &&
                    <div style={{marginBottom:'.7em'}}>
                        <h3>Tags</h3>
                        <Tags path="/tags" tags={feed.tags}/>
                    </div>}

                </div>

                {isEditPopupOpen &&
                <EditPopup
                    feed={feed}
                    isOpen={isEditPopupOpen}
                    onClose={this.onCloseEditPopup}
                />}

                {isFetchPopupOpen &&
                <FetchPopup
                    feed={feed}
                    isOpen={isFetchPopupOpen}
                    isFetching={isFetchingPosts}
                    onFetchPosts={this.onFetchPosts}
                    onClose={this.onCloseFetchPopup}
                    fetchResult={fetchResult}
                    fetchErrors={fetchErrors}
                />}
            </div>
        )
    }
}

export default withRouter( withApollo( graphql( FeedQuery, {
    options: ( ownProps ) => {
        return ({
            variables: {
                slug: ownProps.slug,
                includeTags: true
            }
        })
    },
} )( withLoadingIndicator()( withAuth0( FeedInfo ) ) ) ) );