import React, { Component } from 'react';
import FeedInfo from './components/feedinfo';
import PostList from 'components/post/postlist';
import MonthPaginator from 'components/monthpaginator';

export default class FeedsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        let { slug, year, month } = this.props.match.params;
        const date = new Date();
        year = year ? parseInt( year, 10 ) : date.getFullYear();
        month = month ? parseInt( month, 10 ) : date.getMonth() + 1;

        return (
            <div>
                <FeedInfo slug={slug}/>

                <div className="month-paginator-wrapper">
                    <MonthPaginator
                        startMonth={month - 1}
                        startYear={year}
                        path={`/feeds/${slug}`}/>
                </div>

                <PostList
                    path={`/feed/${slug}`}
                    slug={slug}
                    year={year}
                    month={month}
                />
            </div>
        )
    }
}