import React from 'react';
import { Menu, MenuItem }from 'components/support/menu';

export default function LegalMenu() {
    return (
        <Menu type="underline">
            <MenuItem exact path="/legal/terms">
                Terms and Conditions
            </MenuItem>
            <MenuItem exact path="/legal/privacy">
                Privacy Policy
            </MenuItem>
        </Menu>
    )
}