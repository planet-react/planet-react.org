import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import { Screen } from 'components/screen';

import { withAuth0, withServices } from 'components/services';
import LegalMenu from './components/menu'

class LegalScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        const { auth0, match } = this.props;

        return (
            <div>
                <Route
                    path={match.url}
                    render={( props ) => <LegalMenu/>}
                />

                <Screen exact path="/legal/terms"/>

                <Screen exact path="/legal/privacy"/>

            </div>
        )
    }
}

export default withAuth0(
    withServices( LegalScreen )
);
