import React, { Component } from 'react';
import Header from 'components/header';
import { ScreenLink } from 'components/screen';

export default class TermsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        return (
            <div>
                <Header title="Terms and Conditions"/>
                <p>
                    Last updated: August 7, 2017
                </p>
                <p>
                    These Terms and Conditions (“Terms”, “Terms and Conditions”) govern your relationship with https://www.planet-react.org website (the “Service”) operated by Thomas Andersen (“us”, “we”, or “our”).
                </p>
                <p>
                    Please read these Terms and Conditions carefully before using the Service.
                </p>
                <p>
                    Your access to and use of the Service is based on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.
                </p>
                <p>
                    By accessing or using the Service you agree to be bound by these Terms and accept all legal consequences. If you do not agree to these terms and conditions, in whole or in part, please do not use the Service.
                </p>

                <h2>
                    Disclaimer
                </h2>
                <p>
                    The materials on Planet React's web site are provided "as is". Planet React makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Planet React does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.
                </p>

                <h2>
                    Limitations
                </h2>
                <p>
                    In no event shall Planet React or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Planet React's Internet site, even if Planet React or a Planet React authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
                </p>

                <h2>
                    Removal links from our website
                </h2>
                <p>
                    If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.
                </p>
                <p>
                    Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.
                </p>

                <h2>Site Terms of Use Modifications</h2>
                <p>
                    Planet React may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.
                </p>

                <h2>
                    Governing Law
                </h2>
                <p>
                    These Terms shall be governed and construed in accordance with the laws of Norway, without regard to its conflict of law provisions.
                </p>
                <p>
                    Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.
                </p>

                <h2>
                    Privacy Policy
                </h2>
                <p>
                    See: <ScreenLink
                    path="/legal/privacy">Privacy Policy</ScreenLink>
                </p>

                <h2>Contact Us</h2>
                <p>
                    If you have any questions about these Terms, please contact us at: {__PLANET_EMAIL__}
                </p>
            </div>
        )
    }
}