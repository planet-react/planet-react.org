import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Reader from 'components/post/reader';
import { withServices } from 'components/services';

export class PostScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <Reader {...this.props}/>
        )
    }
}

export default withRouter(
    withServices( PostScreen )
);

