import React, { Component } from 'react';
import { withServices } from 'components/services';
import Header from 'components/header';
import PostList from 'components/post/postlist';
import MonthPaginator from 'components/monthpaginator';

class PostsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        const { services: { dateHelper }, match } = this.props;
        let { language, year, month, slug } = match.params;
        year = year ? year : new Date().getFullYear();
        month = month ? month : new Date().getMonth() + 1;

        month = parseInt( month, 10 );
        year = parseInt( year, 10 );

        const date = new Date( `${year}/${month}/1` );

        const title = `${dateHelper.format( date, 'MMMM YYYY' )}`;

        return (
            <div>
                <div className="month-paginator-wrapper">
                    <MonthPaginator
                        startMonth={month - 1}
                        startYear={year}
                        path={`/posts/${language}`}
                    />
                </div>

                <Header title={title}/>

                <PostList
                    path={'/posts'}
                    language={language}
                    slug={slug}
                    year={year}
                    month={month}
                />
            </div>
        )
    }
}

export default withServices( PostsScreen );