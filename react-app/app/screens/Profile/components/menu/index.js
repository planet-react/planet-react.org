import React from 'react';
import { Menu, MenuItem } from 'components/support/menu';

export default function ProfileMenu() {
    return (
        <Menu type="underline">
            <MenuItem exact path="/profile">
                Profile
            </MenuItem>
            <MenuItem path="/profile/feeds">
                Feeds
            </MenuItem>
            <MenuItem path="/profile/events">
                Events
            </MenuItem>
            <MenuItem path="/profile/subscriptions">
                Subscriptions
            </MenuItem>
            <MenuItem path="/profile/bookmarks">
                Bookmarks
            </MenuItem>
        </Menu>
    )
}