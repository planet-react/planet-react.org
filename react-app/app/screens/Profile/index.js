import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { withAuth0, withServices } from 'components/services';
import { Screen } from 'components/screen';
import Header from 'components/header';
import ProfileMenu from './components/menu'
import ProfileCard from 'components/auth0/card'

class ProfileMainScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { auth0, match } = this.props;
        const { isAuthenticated, userProfile } = auth0;

        if ( !isAuthenticated ) {
            return null;
        }

        const loggedInUserIsAdmin = auth0.userHasRole( auth0.roles.ADMIN );

        const userData = {
            name: userProfile.name,
            nickname: userProfile.nickname,
            picture: userProfile.picture,
            email: userProfile.email,
            created_at: userProfile.created_at,
            email_verified: userProfile.email_verified,
            identities: userProfile.identities,
            roles: loggedInUserIsAdmin ? userProfile.roles : null
        };

        return (
            <div>
                <Route
                    path={match.url}
                    render={( props ) => <ProfileMenu/>}
                />

                <Route
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header
                                    title={userData.name}
                                    hideHeader={true}
                                />
                                <ProfileCard
                                    data={userData}
                                    layout="horizontal"
                                />
                            </div>
                        )
                    }}/>

                <Screen path={match.url + '/feeds'}/>

                <Screen path={match.url + '/events'}/>

                <Screen path={match.url + '/bookmarks'}/>

                <Screen path={match.url + '/subscriptions'}/>

            </div>
        )
    }
}

export default withAuth0(
    withServices( ProfileMainScreen )
);

