import { printAST } from 'react-apollo';

import EditBookmarkMutation from './EditBookmarkMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( EditBookmarkMutation );
    expect( ast ).toMatchSnapshot()
} );