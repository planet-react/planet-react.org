import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withServices } from 'components/services';
import { withApollo } from 'react-apollo';
import { Form, Input, Tags, TextArea } from 'components/support/form';
import Button from 'components/support/button';

import EditBookmarkMutation from './graphql/EditBookmarkMutation.graphql';

class BookmarkForm extends Component {

    static propTypes = {
        formData: PropTypes.object
    };

    initialState = {
        id: null,
        title: '',
        url: '',
        notes: '',
        tags: []
    };

    constructor( props ) {
        super( props );
        this.state = this.initialState;
    }

    componentDidMount() {
        const { formData } = this.props;
        if ( formData ) {
            this.setState(
                Object.assign( this.state, formData )
            );
        }
    }

    onChange = ( event ) => {
        let value;
        if ( event.target.type == 'checkbox' ) {
            value = event.target.checked ? 1 : 0;
        }
        else {
            value = event.target.value;
        }
        this.setState( {
            [event.target.name]: value
        } );
    };

    onBeforeSubmit = () => {
        return new Promise( ( resolve, reject ) => {
            let tags = this.state.tags.map( ( tag, i ) => {
                return tag.tag_slug
            } ).join( ',' );

            resolve( { formData: { tags } } );
        } );
    };

    onSubmitSuccess = ( data ) => {
        return new Promise( ( resolve, reject ) => {
            this.setState( {
                id: data.editBookmark.id,
            }, () => {
                resolve( true );
            } );
        } );
    };

    render() {
        return (
            <div className="bookmarks-edit-form">

                <Form
                    mutation={EditBookmarkMutation}
                    data={this.state}
                    onBeforeSubmit={this.onBeforeSubmit}
                    onSubmitSuccess={this.onSubmitSuccess}
                >
                    <Input
                        name="title"
                        value={this.state.title}
                        id="bookmarks-input-title"
                        required
                        label="Title"
                        onChange={this.onChange}
                    />

                    <Input
                        name="url"
                        value={this.state.url}
                        id="bookmarks-input-url"
                        type="url"
                        label="URL"
                        required
                        onChange={this.onChange}
                    />

                    <TextArea
                        name="notes"
                        value={this.state.notes}
                        id="bookmarks-input-notes"
                        label="Notes"
                        rows={5}
                        onChange={this.onChange}
                    />

                    <Tags
                        name="tags"
                        value={this.state.tags}
                        id="bookmarks-input-tags"
                        label="Tags"
                        onChange={this.onChange}
                    />
                    <p>
                        <Button type="submit">Save</Button>
                    </p>
                </Form>
            </div>
        )
    }
}

export default withApollo(
    withServices( BookmarkForm )
);