import React from 'react';
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen';
import Button from 'components/support/button';
import Tags from 'components/tags';
import DateFormatter from 'components/support/dateformatter';
import FavIcon from 'components/favicon';
import PencilIcon from 'react-icons/lib/fa/pencil';
import CancelIcon from 'react-icons/lib/io/android-cancel';

require( './style.scss' );

MyBookmarkItem.propTypes = {
    bookmark: PropTypes.object,
    onDelete: PropTypes.func
};

export default function MyBookmarkItem( { bookmark, onDelete } ) {

    return (
        <li key={bookmark.id} className="my-bookmarks-list__item">
            <div className="my-bookmarks-list__icon-col">
                <FavIcon host={bookmark.host}/>
            </div>
            <div className="my-bookmarks-list__content-col">
                <h2>
                    <a href={bookmark.url} title={bookmark.title}>
                        {bookmark.title}
                    </a>
                </h2>
                <div className="my-bookmarks-list__meta">
                    {bookmark.host} - Created: <DateFormatter
                    date={bookmark.created_at}/>
                </div>
                <div className="bookmarks-tag-list-wrapper">
                    { (bookmark.tags && bookmark.tags.length > 0) &&
                    <Tags tags={bookmark.tags}
                          path="/profile/bookmarks/tags"/> }
                </div>
            </div>
            <div>
                <ScreenLink
                    path={`/profile/bookmarks/edit/${bookmark.id}`}
                >
                    <Button
                        icon={PencilIcon}
                        iconColor="#acacac"
                        color="transparent"
                        size="medium"
                    />
                </ScreenLink>

                <Button
                    icon={CancelIcon}
                    iconColor="#726b35"
                    color="transparent"
                    size="medium"
                    onClick={() => {
                        onDelete( bookmark.id )
                    }}
                />
            </div>
        </li>
    );
}
