import { printAST } from 'react-apollo';

import DeleteBookmarkMutation from './DeleteBookmarkMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( DeleteBookmarkMutation );
    expect( ast ).toMatchSnapshot()
} );