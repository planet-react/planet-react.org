import { printAST } from 'react-apollo';

import UserBrowseBookmarksQuery from './UserBrowseBookmarksQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( UserBrowseBookmarksQuery );
    expect( ast ).toMatchSnapshot()
} );