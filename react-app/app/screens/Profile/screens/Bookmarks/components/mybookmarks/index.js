import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import MyBookmarkItem from './MyBookmarkItem';
import { PaginationContainer, withLoadingIndicator } from 'components/graphql';
import Message from 'components/support/message';

import UserBrowseBookmarksQuery from './graphql/UserBrowseBookmarksQuery.graphql';
import DeleteBookmarkMutation from './graphql/DeleteBookmarkMutation.graphql';

require( './style.scss' );

MyBookmarks.propTypes = {
    data: PropTypes.object,
    onDelete: PropTypes.func,
    search: PropTypes.string,
};

function MyBookmarks( { data, onDelete, search } ) {

    const onDestroy = ( id ) => {
        if ( window.confirm( 'Do you want to delete this bookmark?' ) ) {
            onDelete( id )
        }
    };

    const bookmarks = data.userBrowseBookmarks.data;

    if ( bookmarks.length == 0 ) {
        return <Message>No bookmarks found</Message>;
    }

    return (
        <PaginationContainer data={data} dataKey="userBrowseBookmarks">
            <ul className="my-bookmarks-list">
                {bookmarks.map( ( bookmark ) => {
                    return (
                        <MyBookmarkItem
                            key={bookmark.id}
                            bookmark={bookmark}
                            onDelete={onDestroy}
                        />
                    )
                } )}
            </ul>
        </PaginationContainer>
    );
}

const deleteBookmarkMutation = graphql( DeleteBookmarkMutation, {
    props: ( { mutate } ) => ({
        onDelete: ( id ) => mutate( {
            variables: { id },
            refetchQueries: [ 'UserBrowseBookmarksQuery' ]
        } ),
    })
} );

export default graphql( UserBrowseBookmarksQuery, {
    options: ( ownProps ) => {
        return ({
            fetchPolicy: 'network-only',
            variables: {
                page: 1,
                search: ownProps.search || null
            }
        })
    }

} )( deleteBookmarkMutation( withLoadingIndicator()( MyBookmarks ) ) );

