import React, { Component } from 'react';
import { Screen, ScreenLink } from 'components/screen';
import { withServices } from 'components/services';
import Header from 'components/header';
import MyBookmarks from './components/mybookmarks';

class BookmarksScreen extends Component {

    state = {
        tempSearch: '',
        search: '',
    };

    constructor( props ) {
        super( props );
    }

    onSearch = ( event ) => {
        const value = event.target.value.trim();
        this.setState( {
            tempSearch: value
        }, this.props.services.util.debounce( () => {
            this.setState( { search: this.state.tempSearch } );
        }, 500 ) );
    };

    render() {
        const { match } = this.props;

        return (
            <div>

                <Screen
                    exact
                    path={match.url}
                    render={( props ) => {

                        const { tempSearch, search } = this.state;

                        return (
                            <div>
                                <Header title="My Bookmarks"/>
                                <p>
                                    <ScreenLink path={'/profile/bookmarks/new'}>
                                        + Add New Bookmark
                                    </ScreenLink>
                                </p>
                                <p>
                                    <input
                                        type="text"
                                        value={tempSearch}
                                        placeholder="Search Bookmarks"
                                        onChange={this.onSearch}
                                    />
                                </p>
                                <MyBookmarks search={search}/>
                            </div>
                        )
                    }}/>

                <Screen exact path={match.url + '/new'}/>

                <Screen exact path={match.url + '/edit/:bookmarkId'}/>

                <Screen exact path={match.url + '/tags/:slug'}/>

            </div>
        )
    }
}

export default withServices( BookmarksScreen );