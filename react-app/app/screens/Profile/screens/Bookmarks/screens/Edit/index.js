import React, { Component } from 'react';
import { withAuth0 } from 'components/services';
import { withServices } from 'components/services';
import { compose, graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Header from 'components/header';
import BookmarkForm from '../../components/bookmarkform';
import { ScreenLink, ScreenError } from 'components/screen';

import UserBookmarkQuery from './graphql/UserBookmarkQuery.graphql'

class EditBookmarkScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { data, auth0 } = this.props;
        const { userBookmark } = data;

        if ( auth0.userProfile && auth0.userProfile.user_id != userBookmark.user_id ) {
            return <ScreenError statusCode={401}/>
        }

        return (
            <div>
                <Header title={`Edit Bookmark: ${userBookmark.title}`}/>
                <p>
                    <ScreenLink path="/profile/bookmarks">
                        ❮ Back to Bookmarks
                    </ScreenLink>
                </p>

                <BookmarkForm formData={userBookmark}/>
            </div>
        )
    }
}

export default compose(
    withAuth0,
    withServices,
    graphql( UserBookmarkQuery, {
        options: ( ownProps ) => {
            return ({
                variables: {
                    id: Number( ownProps.match.params.bookmarkId ) || null,
                }
            })
        },
    }),

)( withLoadingIndicator()( EditBookmarkScreen ) );
