import React, { Component } from 'react';
import Header from 'components/header';
import BookmarkForm from '../../components/bookmarkform';
import { ScreenLink } from 'components/screen';

export default class NewBookmarkScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <div>
                <Header title="New Bookmark"/>
                <p>
                    <ScreenLink path="/profile/bookmarks">
                        ❮ Back to Bookmarks
                    </ScreenLink>
                </p>

                <BookmarkForm/>
            </div>
        )
    }
}