import React, { Component } from 'react';
import Header from 'components/header';
import MyBookmarks from '../../components/mybookmarks';

export default class BookmarksTagsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { slug } = this.props.match.params;

        return (
            <div>
                <Header title={`My Bookmarks tagged with: ${slug}`}/>
                <MyBookmarks search={`_tag:${slug}`}/>
            </div>
        )
    }
}