import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withServices } from 'components/services';
import { compose, withApollo } from "react-apollo";
import { withRouter } from "react-router-dom";
import { CountrySelector, DatePicker, Form, Input, Select, TextArea, TimePicker, TimeZoneSelector } from "components/support/form";
import { Col, Row } from "components/support/flex";
import Button from "components/support/button";
import PreviewIcon from 'react-icons/lib/fa/eye';

import eventTypes from 'components/event/services/eventTypes';

import EditFeedMutation from './graphql/EditEventMutation.graphql';

class EventForm extends Component {

    static propTypes = {
        formData: PropTypes.object
    };

    initialState = {

        id: null,
        active: 0,
        type: 1,
        name: '',
        description: '',

        start_date: '',

        temp_start_date: '',
        temp_start_time: '',

        end_date: '',

        temp_end_date: '',
        temp_end_time: '',

        timezone: String( -(new Date().getTimezoneOffset() / 60) ).indexOf( '.' ) > -1 ? String( -(new Date().getTimezoneOffset() / 60) ) : String( -(new Date().getTimezoneOffset() / 60) ) + '.0',
        location: '',
        country: 'US',
        website_link: '',
        twitter_link: '',
    };

    constructor( props ) {
        super( props );
        this.state = this.initialState;
    }

    componentDidMount() {
        const { formData } = this.props;
        if ( formData ) {

            const {
                temp_start_date,
                temp_start_time,
                temp_end_date,
                temp_end_time
            } = this.parseDatesOnMount( formData );

            this.setState(
                Object.assign( this.state, formData, {
                    temp_start_date,
                    temp_start_time,
                    temp_end_date,
                    temp_end_time,
                } )
            );
        }
    }

    isNew() {
        return this.state.id == null;
    }

    parseDatesOnMount( formData ) {
        const { services: { dateHelper } } = this.props;
        return {
            temp_start_date: dateHelper.format( formData.start_date, 'YYYY-MM-DD' ),
            temp_start_time: dateHelper.format( formData.start_date, 'h:mm A' ),
            temp_end_date: dateHelper.format( formData.end_date, 'YYYY-MM-DD' ),
            temp_end_time: dateHelper.format( formData.end_date, 'h:mm A' ),
        }
    }

    parseDatesOnSubmit() {
        const { services: { dateHelper } } = this.props;

        let start_date = this.state.temp_start_date + ' ' + dateHelper.timeTo24Format( this.state.temp_start_time );
        let end_date = this.state.temp_end_date + ' ' + dateHelper.timeTo24Format( this.state.temp_end_time );

        start_date = dateHelper.format( start_date, 'YYYY-MM-DD\THH:mm:ss' );
        end_date = dateHelper.format( end_date, 'YYYY-MM-DD\THH:mm:ss' );

        return {
            start_date,
            end_date
        }
    }

    onPreview = ( id ) => {
        window.open( `/events/${id}` )
    };

    onBeforeSubmit = () => {
        return new Promise( ( resolve ) => {
            const { start_date, end_date } = this.parseDatesOnSubmit();
            this.setState( {
                start_date,
                end_date
            }, resolve );
        } );
    };

    onSubmitSuccess = ( data ) => {
        const isNew = this.isNew();

        return new Promise( ( resolve ) => {
            this.initialState = this.state;
            const { id } = data.editEvent;
            if ( id && isNew ) {
                this.props.history.push( `/profile/events/edit/${id}` )
            }
            else {
                this.setState( {
                    id: id
                } );
            }

            resolve();
        } );
    };

    onChange = ( event ) => {
        let value;
        if ( event.target.type == 'checkbox' ) {
            value = event.target.checked ? 1 : 0;
        }
        else {
            value = event.target.value;
        }
        this.setState( {
            [event.target.name]: value
        } );
    };

    onBlurStartDate = () => {
        const { temp_start_date, temp_end_date } = this.state;
        if ( temp_start_date.trim() != '' && temp_end_date.trim() == '' ) {
            this.setState( {
                temp_end_date: temp_start_date
            } );
        }
    };

    render() {
        const { id } = this.state;
        const isNew = this.isNew();
        return (
            <div className="events-submit-form">

                <Form
                    mutation={EditFeedMutation}
                    data={this.state}
                    onBeforeSubmit={this.onBeforeSubmit}
                    onSubmitSuccess={this.onSubmitSuccess}
                    submitSuccessText={`The event was successfully ${isNew ? 'created' : 'updated'}`}
                >
                    <Input
                        name="active"
                        type="checkbox"
                        id="event-input-active"
                        checked={this.state.active == 1}
                        label="Active"
                        onChange={this.onChange}
                    />

                    <Input
                        name="name"
                        value={this.state.name}
                        id="event-input-name"
                        required
                        label="Event Name"
                        onChange={this.onChange}
                    />

                    <Select
                        name="type"
                        value={this.state.type}
                        id="event-input-type"
                        label="Event Type"
                        onChange={this.onChange}
                    >
                        {eventTypes.data.map( ( type ) => <option
                            value={type.id}
                            key={type.id}>{type.name}</option> )}
                    </Select>

                    <TextArea
                        name="description"
                        value={this.state.description}
                        id="event-input-description"
                        label="Description"
                        rows={5}
                        onChange={this.onChange}
                    />

                    <Row>
                        <Col flexBasis="50%" style={{ marginRight: '.7em' }}>
                            <DatePicker
                                name="temp_start_date"
                                value={this.state.temp_start_date}
                                id="event-input-start-date"
                                placeholder="YYYY-MM-DD"
                                required
                                label="Start Date"
                                onBlur={this.onBlurStartDate}
                                onChange={this.onChange}
                            />
                        </Col>
                        <Col flexBasis="50%">
                            <TimePicker
                                name="temp_start_time"
                                value={this.state.temp_start_time}
                                id="event-input-start-time"
                                placeholder="h:mm AM/PM"
                                label="Start Time"
                                onChange={this.onChange}
                            />
                        </Col>
                    </Row>

                    <Row>
                        <Col flexBasis="50%" style={{ marginRight: '.7em' }}>
                            <DatePicker
                                name="temp_end_date"
                                value={this.state.temp_end_date}
                                id="event-input-end-date"
                                placeholder="YYYY-MM-DD"
                                required
                                label="End Date"
                                onChange={this.onChange}
                            />
                        </Col>
                        <Col flexBasis="50%">
                            <TimePicker
                                name="temp_end_time"
                                value={this.state.temp_end_time}
                                id="event-input-end-time"
                                placeholder="h:mm AM/PM"
                                label="End Time"
                                onChange={this.onChange}
                            />
                        </Col>
                    </Row>

                    <TimeZoneSelector
                        name="timezone"
                        value={this.state.timezone}
                        id="event-input-timezone"
                        onChange={this.onChange}
                    />

                    <TextArea
                        name="location"
                        value={this.state.location}
                        id="event-input-location"
                        label="Location"
                        rows={5}
                        onChange={this.onChange}
                    />

                    <Input
                        name="website_link"
                        value={this.state.website_link}
                        id="event-input-website-link"
                        type="url"
                        label="Web Site"
                        onChange={this.onChange}
                    />

                    <Input
                        name="twitter_link"
                        value={this.state.twitter_link}
                        id="event-input-twitter-link"
                        type="url"
                        label="Twitter Link"
                        onChange={this.onChange}/>

                    <CountrySelector
                        name="country"
                        value={this.state.country}
                        id="event-input-country"
                        label="Country"
                        onChange={this.onChange}
                    />

                    <p>
                        <Button type="submit">Save</Button>

                        {id &&
                        <Button
                            style={{marginLeft:'.7em'}}
                            onClick={() => this.onPreview( id )}
                            color="secondary"
                            icon={PreviewIcon}
                        >
                            Preview
                        </Button>}

                    </p>
                </Form>
            </div>
        )
    }
}

export default compose(
    withRouter,
    withApollo,
    withServices
)( EventForm )
