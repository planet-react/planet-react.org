import React from 'react';
import PropTypes from 'prop-types';
import MyEventListItem from './MyEventListItem';
import Message from 'components/support/message';

MyEventList.propTypes = {
    visible: PropTypes.bool,
    events: PropTypes.array,
    onActivate: PropTypes.func,
    onRemove: PropTypes.func,
};

export default function MyEventList( { events, onActivate, onRemove } ) {

    return (
        <div>
            { events.length == 0 &&
            <Message>You have not added any events</Message> }

            {events.map( ( event ) => {
                return (
                    <MyEventListItem
                        key={event.id}
                        event={event}
                        onActivate={onActivate}
                        onRemove={onRemove}/>
                )
            } )}
        </div>
    )
}