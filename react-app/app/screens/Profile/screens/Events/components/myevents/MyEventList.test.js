import React from 'react';
import MyEventList from './MyEventList';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../../../internals/testutils/MockProviders'

it( 'should render', () => {

    const onActivate = () => {
    };
    const onRemove = () => {
    };

    const events = [
        {
            id: 1,
            user_id: 'abc|123',
            active: 1,
            type: 1,
            name: 'My Event',
            start_date: '2017-06-06 10:00:00',
            end_date: '2017-06-07 10:00:00',
            location: 'The location',
            country: 'US',
            website_link: 'https://www.planet-react.org',
            twitter_link: '',
            created_at: '2017-01-01 11:00:00',
            updated_at: '2017-01-01 12:00:00',
        },
        {
            id: 2,
            user_id: 'abc|123',
            active: 1,
            type: 1,
            name: 'My Event',
            start_date: '2017-06-06 10:00:00',
            end_date: '2017-06-07 10:00:00',
            location: 'The location',
            country: 'US',
            website_link: 'https://www.planet-react.org',
            twitter_link: '',
            created_at: '2017-01-01 11:00:00',
            updated_at: '2017-01-01 12:00:00',
        }
    ];

    const tree = renderer.create(
        <MockProviders>
            <MyEventList
                onActivate={onActivate}
                onRemove={onRemove}
                visible={true}
                events={events}/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );
