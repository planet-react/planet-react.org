import React from 'react';
import PropTypes from 'prop-types';
import { ScreenLink } from "components/screen";
import { Row, Col } from 'components/support/flex';
import A from 'components/support/a';

MyEventListItem.propTypes = {
    event: PropTypes.object.isRequired,
    onActivate: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
};

export default function MyEventListItem( { event, onActivate, onRemove } ) {
    const active = event.active;

    return (
        <Row
            className={'my-events__item'}>
            <Col className={'my-events__name-col' + (!active ? ' my-events__inactive' : '')}>
                <ScreenLink path={`/events/view/${event.id}`}>
                    {event.name}
                </ScreenLink>
            </Col>
            <Col className="my-events__edit-col">
                <ScreenLink
                    path={`/profile/events/edit/${event.id}`}>
                    Edit
                </ScreenLink>
            </Col>
            <Col className="my-events__activate-col">
                <A onClick={() => {
                    onActivate( event )
                }}>
                    {event.active == 1 ? 'Deactivate' : 'Activate'}
                </A>
            </Col>
            <Col className="my-events__remove-col">
                <A onClick={() => {
                    onRemove( event )
                }}>Remove</A>
            </Col>
        </Row>
    )
}