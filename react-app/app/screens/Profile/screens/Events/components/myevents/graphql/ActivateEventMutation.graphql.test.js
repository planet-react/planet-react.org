import { printAST } from 'react-apollo';

import ActivateEventMutation from './ActivateEventMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ActivateEventMutation );
    expect( ast ).toMatchSnapshot()
} );