import { printAST } from 'react-apollo';

import MyEventsQuery from './MyEventsQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( MyEventsQuery );
    expect( ast ).toMatchSnapshot()
} );