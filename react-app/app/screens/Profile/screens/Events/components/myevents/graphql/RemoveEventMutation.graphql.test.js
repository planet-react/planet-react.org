import { printAST } from 'react-apollo';

import RemoveEventMutation from './RemoveEventMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( RemoveEventMutation );
    expect( ast ).toMatchSnapshot()
} );