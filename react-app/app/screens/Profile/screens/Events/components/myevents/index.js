import React, { Component } from 'react';
import { graphql, withApollo } from 'react-apollo';
import MyEventList from './MyEventList';
import { withLoadingIndicator } from 'components/graphql';

import MyEventsQuery from './graphql/MyEventsQuery.graphql';
import RemoveEventMutation from './graphql/RemoveEventMutation.graphql';
import ActivateEventMutation from './graphql/ActivateEventMutation.graphql';

require( './style.scss' );

class MyEvents extends Component {

    state = {
        visible: false,
    };

    constructor( props ) {
        super( props );
    }

    onRemove = ( eventData ) => {
        if ( !window.confirm( `Are you sure you want to remove ${eventData.name}?` ) ) {
            return;
        }

        this.props.client.mutate( {
            mutation: RemoveEventMutation,
            refetchQueries: [ 'EventsQuery' ],
            variables: {
                id: eventData.id
            }
        } ).then( ( { data } ) => {
            this.props.data.refetch();
        } );
    };

    onToggleActivate = ( eventData ) => {
        this.props.client.mutate( {
            mutation: ActivateEventMutation,
            refetchQueries: [ 'BrowseEventsQuery' ],
            variables: {
                id: eventData.id,
                active: eventData.active == 1 ? 0 : 1
            }
        } ).then( ( { data } ) => {
            this.props.data.refetch();
        } );
    };

    render() {
        const { data: { myEvents } } = this.props;

        return (
            <div className="my-events">
                <MyEventList
                    events={myEvents}
                    onActivate={this.onToggleActivate}
                    onRemove={this.onRemove}
                />
            </div>
        )
    }
}

export default withApollo( graphql( MyEventsQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( MyEvents ) ) );