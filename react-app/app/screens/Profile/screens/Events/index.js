import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Screen, ScreenLink } from 'components/screen';
import MyEvents from './components/myevents';
import Header from 'components/header';

export default class ProfileEventsScreen extends Component {

    render() {
        const { match } = this.props;

        return (
            <div>
                <Route
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header title="Manage Events"/>
                                <ScreenLink path="/profile/events/new">
                                    + Add New Event
                                </ScreenLink>

                                <MyEvents/>
                            </div>
                        )
                    }}/>

                <Screen exact path={`${match.url}/new`}/>

                <Screen exact path={`${match.url}/edit/:eventId`}/>
            </div>

        )
    }
}

