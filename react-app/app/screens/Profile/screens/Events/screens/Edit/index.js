import React, { Component } from 'react';
import { withAuth0 } from 'components/services';
import { compose, graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Header from 'components/header';
import EventForm from '../../components/eventform';
import { ScreenError, ScreenLink } from "components/screen";

import EventQuery from 'graphql/EventQuery.graphql';

class EditEvent extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { data: { event }, auth0 } = this.props;
        const { userProfile } = auth0;

        if ( userProfile && userProfile.user_id != event.user_id ) {
            return <ScreenError statusCode={401}/>
        }

        return (
            <div>
                <Header title={`Edit: ${event.name}`}/>
                <p>
                    <ScreenLink path="/profile/events">
                        ❮ Back to My Events
                    </ScreenLink>
                </p>
                <EventForm formData={event}/>
            </div>
        )
    }
}

export default compose(
    withAuth0,
    graphql( EventQuery, {
        options: ( ownProps ) => {
            return ({
                variables: {
                    id: Number( ownProps.match.params.eventId ) || null,
                }
            })
        },
    } )
)( withLoadingIndicator()( EditEvent ) );