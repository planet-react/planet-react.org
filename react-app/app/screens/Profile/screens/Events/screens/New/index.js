import React, { Component } from 'react';
import Header from 'components/header';
import EventForm from '../../components/eventform';
import { ScreenLink } from 'components/screen';

export default class NewEventScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {

        return (
            <div>
                <Header title="New Event"/>
                <p>
                    <ScreenLink path="/profile/events">
                        ❮ Back to My Events
                    </ScreenLink>
                </p>
                <EventForm/>
            </div>
        )
    }
}