import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withApollo, graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import FeedImage from 'components/feed/feedimage';
import { Row, Col, Filler } from 'components/support/flex';
import { ScreenLink } from 'components/screen'
import Button from 'components/support/button';
import Message from 'components/support/message';

import UserFeedsQuery from './graphql/UserFeedsQuery.graphql';
import RemoveFeedMutation from 'graphql/RemoveFeedMutation.graphql';

require( './style.scss' );

class UserFeedsList extends Component {

    static propTypes = {};

    static defaultProps = {};

    constructor( props ) {
        super( props );

        this.onRemove = this.onRemove.bind( this );
    }

    onRemove = ( id ) => {
        if ( !id ) {
            return;
        }

        const confirmed = window.confirm(
            'This will remove this feed and all posts.\n\n'
            + 'Continue?'
        );

        if ( confirmed ) {
            this.props.client.mutate( {
                mutation: RemoveFeedMutation,
                variables: {
                    id: id,
                    isRemoved: false
                }
            } ).then( ( { data } ) => {
                this.props.data.refetch();
            } ).catch( ( error ) => {

            } );
        }
    };

    render() {

        const { userFeeds } = this.props.data;

        if (userFeeds.length == 0 ) {
            return <Message>You have no feeds</Message>
        }

        return (
            <div className="user-feed-list">
                {userFeeds.map( ( feed ) => {
                    return (
                        <Row
                            key={feed.id}
                            className="user-feed-row"
                            alignItems="center"
                            justifyContent="space-between"
                        >
                            <Col flexBasis="3%">
                                <ScreenLink path={`/feeds/${feed.slug}`}>
                                    <FeedImage
                                        size={'1.7em'}
                                        src={feed.image}
                                        alt={feed.name}
                                    />
                                </ScreenLink>
                            </Col>
                            <Col flexBasis="27%">
                                <ScreenLink path={`/feeds/${feed.slug}`}>
                                    {feed.name}
                                </ScreenLink>
                            </Col>
                            <Col flexBasis="25%">
                                {feed.created_at}
                            </Col>
                            <Col flexBasis="25%">
                                {feed.updated_at}
                            </Col>
                            <Col>
                                <Button
                                    size="medium"
                                    onClick={this.onRemove.bind( this, feed.id )}>
                                    Remove
                                </Button>
                            </Col>
                        </Row>
                    )
                } )}
            </div>
        );
    }
}

export default withApollo( graphql( UserFeedsQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( UserFeedsList ) ) );

