import React, { Component } from 'react';
import { Screen, ScreenLink } from 'components/screen';
import Header from 'components/header/index';
import UserFeedsList from './components/userfeeds'

export default class ProfileFeedsScreen extends Component {

    render() {
        const { match } = this.props;

        return (
            <div>
                <Screen
                    exact
                    path={match.url}
                    render={( props ) => {
                        return (
                            <div>
                                <Header title="Feeds"/>
                                <p>
                                    <ScreenLink path={`/profile/feeds/submit`}>
                                        + Submit Feed
                                    </ScreenLink>
                                </p>
                                <UserFeedsList/>
                            </div>
                        )
                    }}/>

                <Screen exact path={`${match.url}/submit`}/>

            </div>
        )
    }
}

