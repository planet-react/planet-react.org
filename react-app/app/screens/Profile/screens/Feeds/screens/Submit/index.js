import React, { Component } from 'react';
import { withServices } from 'components/services';
import Header from 'components/header';
import SubmitFeedForm from 'components/feed/submitform'

class SubmitFeedScreen extends Component {

    feedUrlParameter = null;

    constructor( props ) {
        super( props );
        this.feedUrlParameter = (props.location.search.split( 'feedUrl=' )[ 1 ] || '').split( '&' )[ 0 ];
    }

    render() {
        return (
            <div>
                <Header title="Submit Feed"/>
                <div>
                    <SubmitFeedForm feedUrl={this.feedUrlParameter}/>
                </div>
            </div>
        )
    }
}

export default withServices( SubmitFeedScreen );