import React from "react";
import PropTypes from 'prop-types';
import { Row, Col } from 'components/support/flex'
import FeedImage from 'components/feed/feedimage';
import SubscribeButton from 'components/subscription/button';
import { withServices } from 'components/services'
import TagIcon from 'react-icons/lib/fa/tag';

SubscriptionListItem.propTypes = {
    subscription: PropTypes.object.isRequired
};

require( './style.scss' );

function SubscriptionListItem( { subscription, services: { dateHelper } } ) {
    const title = subscription.type == 'feed' ? subscription.feed_name : subscription.value;
    return (
        <Row
            className="subscription-list__item"
            justifyContent="space-between"
            alignItems="top"
        >
            <Col flexBasis="23%" style={{ textAlign: 'center' }}>
                {
                    subscription.type == 'feed' ? (
                        <FeedImage
                            size={'3.7em'}
                            src={subscription.feed_image}
                            alt={subscription.feed_name}
                        />
                    ) : (
                        <TagIcon size={25}/>
                    )
                }
            </Col>
            <Col flexBasis="85%">
                <h2>
                    {title}
                </h2>
                { subscription.feed_description &&
                    <div className="subscription-list__description">
                        {subscription.feed_description}
                    </div>
                }
                <span>Subscribed: </span>
                <time dateTime={subscription.created_at}>
                    {dateHelper.format( subscription.created_at, 'Do MMMM YYYY' )}
                </time>
            </Col>
            <Col flexBasis={'33%'} style={{paddingLeft:'1.5em'}}>
                <SubscribeButton
                    title={title}
                    type={subscription.type}
                    value={subscription.value}
                />
            </Col>
        </Row>
    );
}

export default withServices( SubscriptionListItem );