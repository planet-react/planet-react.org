import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ListItem from './SubscriptionListItem';

require( './style.scss' );

SubscriptionList.propTypes = {
    subscriptions: PropTypes.array.isRequired
};

export default function SubscriptionList( { subscriptions } ) {
    return (
        <div className="subscription-list">
            {
                subscriptions.map( ( subscription ) =>
                    <ListItem
                        key={subscription.id}
                        subscription={subscription}
                    /> )
            }
        </div>
    );
}
