import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../../../internals/testutils/MockProviders'

import SubscriptionList from './'

const subscriptionsMockData = [
    {
        id: 1,
        user_email: 'foo@test.com',
        type: 'tag',
        value: 'mobx',
        feed_name: '',
        feed_description: '',
        feed_image: '',
        created_at: '2017-08-06 20:12:33',
        updated_at: '2017-08-07 12:05:14',
    },
    {
        id: 2,
        user_email: 'foo@test.com',
        type: 'feed',
        value: '12',
        feed_name: 'Ben Daglish',
        feed_description: 'Some description',
        feed_image: 'images/feeds/ben-daglish.png',
        created_at: '2017-08-03 19:15:09',
        updated_at: '2017-08-03 19:15:09',
    }
];

it( 'renders correctly', () => {

    let tree = renderer.create(
        <MockProviders>
            <SubscriptionList subscriptions={subscriptionsMockData}/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );