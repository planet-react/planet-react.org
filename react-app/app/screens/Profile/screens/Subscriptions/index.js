import React, { Component } from 'react';
import { graphql } from 'react-apollo'
import { withLoadingIndicator } from 'components/graphql'
import Header from 'components/header';
// import DailyWeeklySubscriptionButtons from 'components/subscription/dailyweekly'
import Message from 'components/support/message';
import SubscriptionList from './components/list'

import UserSubscriptionsQuery from './graphql/UserSubscriptionsQuery.graphql';

class SubscriptionsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { userSubscriptions } = this.props.data;

        if ( userSubscriptions.length == 0 ) {
            return <Message>No subscriptions found</Message>
        }

        return (
            <div>
                <Header title="Subscriptions"/>
                {/*<div style={{ marginBottom: '1.5em' }}>*/}
                {/*<DailyWeeklySubscriptionButtons/>*/}
                {/*</div>*/}
                <SubscriptionList subscriptions={userSubscriptions}/>
            </div>
        )
    }
}

export default graphql( UserSubscriptionsQuery, {
    options: ( ownProps ) => {
        return ({
            fetchPolicy: 'cache-and-network',
        })
    },
} )( withLoadingIndicator()( SubscriptionsScreen ) ) ;

