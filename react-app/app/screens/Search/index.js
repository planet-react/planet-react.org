import React, { Component } from 'react';
import PostList from 'components/post/postlist';
import Header from 'components/header';

export default class SearchScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { language, year, month, slug } = this.props.match.params;
        const search = (this.props.location.search.split( 'q=' )[ 1 ] || '').split( '&' )[ 0 ];

        return (
            <div>

                <Header title={`Search results for: '${search}'`}/>

                <PostList
                    language={language}
                    slug={slug}
                    year={year}
                    month={month}
                    search={search}
                />
            </div>
        )
    }
}