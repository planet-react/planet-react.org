import React, { Component } from 'react';
import { graphql, withApollo } from 'react-apollo';
import Header from 'components/header';
import PostList from 'components/post/postlist';
import SubscribeButton from 'components/subscription/button';

export default class TagsScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { pathname } = this.props.location;
        let { slug, year, month } = this.props.match.params;

        year = Number( year );
        month = Number( month );

        return (
            <div>
                <Header title={`Posts tagged with ${slug}`}/>

                <div style={{ margin: '1.5em 0' }}>
                    <SubscribeButton
                        type="tag"
                        value={slug}
                        title={slug}
                    />
                </div>

                <PostList
                    path={pathname}
                    search={`_tag:${slug}`}
                    year={year}
                    month={month}
                />
            </div>
        )
    }
}
