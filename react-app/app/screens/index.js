import React, { Component } from 'react';
import { withServices } from 'components/services';
import Header from 'components/header';
import PostList from 'components/post/postlist';
import MonthPaginator from 'components/monthpaginator';

class HomeScreen extends Component {

    constructor( props ) {
        super( props );
    }

    render() {
        const { services: { dateHelper } } = this.props;
        const date = new Date();
        const title = `${dateHelper.format( date, 'MMMM YYYY' )}`;
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        return (
            <div>
                <div className="month-paginator-wrapper">
                    <MonthPaginator
                        startMonth={month - 1}
                        startYear={year}
                        path="/posts/EN"
                    />
                </div>

                <Header title="Home" header={title} hideHeader={false}/>

                <PostList
                    language={'EN'}
                    year={year}
                    month={month}/>
            </div>
        )
    }
}

export default withServices( HomeScreen );