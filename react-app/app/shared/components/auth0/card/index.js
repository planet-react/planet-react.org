import React from 'react';
import PropTypes from 'prop-types';
import ProfilePicture from '../picture';
import { Row, Col } from 'components/support/flex';

require( './style.scss' );

ProfileCard.propTypes = {
    layout: PropTypes.oneOf( [ 'vertical', 'horizontal' ] ),
    pictureSize: PropTypes.string,

    data: PropTypes.shape( {
        name: PropTypes.string,
        nickname: PropTypes.string,
        picture: PropTypes.string,
        email: PropTypes.string,
        created_at: PropTypes.string,
        email_verified: PropTypes.bool,
        blocked: PropTypes.bool,
        roles: PropTypes.array,
        identities: PropTypes.array,
    } )
};

ProfileCard.defaultProps = {
    pictureSize: '10em',
    layout: 'vertical',
};

export default function ProfileCard( { data, pictureSize, layout } ) {

    const isVerticalLayout = layout == 'vertical';

    return (
        <Row direction={isVerticalLayout ? 'column' : 'row'}
             alignItems={isVerticalLayout ? 'center' : 'flex-start'}
             className="profile-card">

            <ProfilePicture src={data.picture} size={pictureSize} alt={data.name}/>

            <Col style={!isVerticalLayout ? {marginLeft:'1em'}:null}>
                <div
                    style={{ textAlign: isVerticalLayout ? 'center' : null, margin: isVerticalLayout ? '1em':'0 0 1em 0' }}>

                    <div className="profile-card__name">{data.name}</div>
                    {data.blocked &&
                    <div className="profile-card__blocked">User is
                        blocked</div>}
                </div>

                <ul>
                    <li>
                        Nickname: {data.nickname}
                    </li>
                    <li>
                        Email: {data.email}
                    </li>
                    <li>
                        Created: {data.created_at}
                    </li>
                    <li>
                        Email verified: {data.email_verified ? 'Yes' : 'No'}
                    </li>

                    {data.roles &&
                    <li>
                        Roles: {data.roles.map( role => <span
                        key={role}>{role}</span> )}
                    </li>}
                    {data.identities &&
                    <li>
                        Identities: {data.identities.map( identity => <span
                        key={identity.provider}>{identity.provider}</span> )}
                    </li>}
                </ul>

            </Col>


        </Row>
    );
}