import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

ProfilePicture.propTypes = {
    src: PropTypes.string,
    size: PropTypes.string,
    alt: PropTypes.string,
};

ProfilePicture.defaultProps = {
    size: '3.5em'
};

export default function ProfilePicture( { src, size, alt } ) {
    const style = {
        width: size,
        height: size,
    };

    return (
        <img className="profile-picture" style={style} src={src} alt={alt}/>
    )
};