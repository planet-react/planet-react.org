import { printAST } from 'react-apollo';

import FiveMinutesReactQuery from './FiveMinutesReactQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( FiveMinutesReactQuery );
    expect( ast ).toMatchSnapshot()
} );