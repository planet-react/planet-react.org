import React from "react";
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import Radio from 'components/radio';

import FiveMinutesReactQuery from './FiveMinutesReactQuery.graphql';

FiveMinutesOfReact.propTypes = {};
FiveMinutesOfReact.defaultProps = {};

function FiveMinutesOfReact( { data } ) {

    if ( data.loading ) {
        return <span>...</span>;
    }

    const posts  = data.browsePosts.data;
    return (
        <Radio name="five-minutes-of-react" post={posts[ 0 ]}/>
    );
}

export default graphql( FiveMinutesReactQuery )( FiveMinutesOfReact );