import React from 'react';
import Message from 'components/support/message';
import UpcomingEvent from './upcomingevent';
import FiveMinutesOfReact from './fiveminutes';
import TopTagsList from './toptags';
import ReactRally from './reactrally';

export default function Banner() {

    const hour = new Date().getHours();

    let DisplayComponent;
    switch ( true ) {
        case hour >= 0 && hour <= 2:
            DisplayComponent = FiveMinutesOfReact;
            break;
        case hour >= 3 && hour <= 5:
            DisplayComponent = FiveMinutesOfReact;
            break;
        case hour >= 6 && hour <= 11:
            DisplayComponent = UpcomingEvent;
            break;
        case hour >= 12 && hour <= 13:
            DisplayComponent = FiveMinutesOfReact;
            break;
        case hour >= 14 && hour <= 15:
            DisplayComponent = UpcomingEvent;
            break;
        case hour >= 16 && hour <= 17:
            DisplayComponent = UpcomingEvent;
            break;
        case hour >= 18 && hour <= 19:
            DisplayComponent = UpcomingEvent;
            break;
        case hour >= 20 && hour <= 21:
            DisplayComponent = TopTagsList;
            break;
        case hour >= 22 && hour <= 23:
            DisplayComponent = ReactRally;
            break;
        default:
            DisplayComponent = UpcomingEvent;
    }

    return (
        <Message>
            <DisplayComponent/>
        </Message>
    )
}

