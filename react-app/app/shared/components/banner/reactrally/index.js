import React from 'react';
import { VideoPlayList } from "components/video";

const videos = require( './data.json' );

export default function ReactRally() {
    return (
        <VideoPlayList data={videos}/>
    )
}

