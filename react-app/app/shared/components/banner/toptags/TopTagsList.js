import React from "react";
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen'

TopTagsList.propTypes = {
    tags: PropTypes.array,
};

TopTagsList.defaultProps = {};

export default function TopTagsList( { tags, year, month } ) {
    return (
        <ol className="top-tags" style={{marginTop:0}}>
            {tags.map( ( tag ) => {
                return (
                    <li key={tag.tag_slug}>
                        <ScreenLink
                            path={`/tags/${tag.tag_slug}/:${year}/${month}`}
                        >
                            {tag.tag_slug}
                            <span> ({tag.tag_count} posts)</span>

                        </ScreenLink>
                    </li>
                )
            } )}
        </ol>
    );
}