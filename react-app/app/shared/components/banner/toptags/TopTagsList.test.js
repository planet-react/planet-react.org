import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import TopTagsList from './TopTagsList';

const mockTagData = [
    { tag_slug: "redux", count: 12 },
    { tag_slug: "web-development", count: 9 },
    { tag_slug: "front-end-development", count: 5 },
    { tag_slug: "tdd", count: 4 },
    { tag_slug: "webdriverio", count: 4 }
];

it( 'should render correctly', () => {
    const tree = renderer.create(
        <MockProviders>
            <TopTagsList tags={mockTagData}/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );