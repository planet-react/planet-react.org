import { printAST } from 'react-apollo';

import TopTagsQuery from './TopTagsQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( TopTagsQuery );
    expect( ast ).toMatchSnapshot()
} );