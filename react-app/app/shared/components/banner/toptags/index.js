import React from "react";
import TopTagsList from './TopTagsList';
import { graphql } from 'react-apollo';
import { withServices } from 'components/services';

import TopTagsQuery from './TopTagsQuery.graphql';

function TopTagsWithData( { data, services: { dateHelper } } ) {
    if ( data.loading ) {
        return <span>...</span>;
    }

    const tags = data.topTags;
    const date = new Date();
    const year = dateHelper.format( date, 'YYYY' );
    const month = dateHelper.format( date, 'MM' );

    return (
        <TopTagsList
            tags={tags}
            year={year}
            month={month}
        />
    );
}

export default graphql( TopTagsQuery )(
    withServices( TopTagsWithData )
);