import React from 'react';
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen';
import DateFormatter from 'components/support/dateformatter';
import CalendarIcon from 'react-icons/lib/fa/calendar';

UpcomingEvent.propTypes = {
    event: PropTypes.shape( {
        id: PropTypes.number,
        name: PropTypes.string,
        start_date: PropTypes.string,
    } )
};

export default function UpcomingEvent( { event } ) {
    return (
        <div>
            <span style={{ paddingRight: '.7em' }}>
                <CalendarIcon
                    size={14}
                    color="grey"
                />
            </span>
            <ScreenLink path={`/events/${event.id}`}>
                {event.name}
            </ScreenLink>
            <span>, </span>
            <DateFormatter date={event.start_date} format="ddd MMM Do"/>
        </div>
    )
}