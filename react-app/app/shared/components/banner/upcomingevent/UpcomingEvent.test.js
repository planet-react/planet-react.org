import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import UpcomingEvent from './UpcomingEvent';

const eventShape = {
    id: 7,
    name: 'React Event 2023',
    start_date: '2023-10-11 08:30:00',
};

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <UpcomingEvent event={eventShape}/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
