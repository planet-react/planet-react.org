import { printAST } from 'react-apollo';

import UpcomingEventQuery from './UpcomingEventQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( UpcomingEventQuery );
    expect( ast ).toMatchSnapshot()
} );