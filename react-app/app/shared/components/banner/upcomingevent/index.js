import React from 'react';
import { graphql } from 'react-apollo';
import UpcomingEvent from './UpcomingEvent';

import UpcomingEventQuery from './UpcomingEventQuery.graphql';

function UpcomingEventWithData( { data } ) {

    if ( data.loading ) {
        return <span>...</span>;
    }
    if ( data.error ) {
        return <span>Something went wrong</span>;
    }

    const { upcomingEvent } = data;

    if ( !upcomingEvent ) {
        return (<div>No event found</div>);
    }

    return (
        <UpcomingEvent event={upcomingEvent}/>
    )
}

export default graphql( UpcomingEventQuery )( UpcomingEventWithData );