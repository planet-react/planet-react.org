import { printAST } from 'react-apollo';

import ToggleBookmarkMutation from './ToggleBookmarkMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ToggleBookmarkMutation );
    expect( ast ).toMatchSnapshot()
} );