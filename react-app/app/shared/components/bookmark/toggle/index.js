import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import Button from 'components/support/button';
import { withAuth0 } from 'components/services';
import BookmarkIcon from 'react-icons/lib/fa/bookmark';
import BookmarkEmptyIcon from 'react-icons/lib/fa/bookmark-o';

import ToggleBookmarkMutation from './graphql/ToggleBookmarkMutation.graphql';

class BookmarkToggle extends Component {

    static propTypes = {
        bookmarkId: PropTypes.number,
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        tags: PropTypes.array,
        size: PropTypes.number,
    };

    static defaultProps = {
        size: 17,
    };

    constructor( props ) {
        super( props );
        this.state = {
            isSaving: false,
            bookmarkId: props.bookmarkId
        };
    }

    isBookmarked() {
        //@todo: check against content id too?
        return this.state.bookmarkId != null;
    }

    tagsToString( tags ) {
        if ( !tags ) {
            return null;
        }
        return tags.map( tag => tag.tag_slug ).join( ',' );
    }

    onToggle = () => {
        const { title, url, tags, onToggle } = this.props;
        const { bookmarkId } = this.state;
        const tagString = this.tagsToString( tags );

        this.setState( { isSaving: true }, () => {

            onToggle( bookmarkId, title, url, tagString ).then( ( { data } ) => {
                this.setState( {
                    isSaving: false,
                    // 'Optimistically' set content as bookmarked
                    bookmarkId: data.toggleBookmark ? data.toggleBookmark.id : null
                } )
            } ).catch( ( error ) => {
                this.setState( {
                    isSaving: false
                } )
            } );
        } )
    };

    onLogin = () => {
        const { auth0 } = this.props;
        auth0.showLogin();
    };

    render() {
        const { auth0 } = this.props;
        const { isAuthenticated } = auth0;
        const { isSaving } = this.state;
        const isBookmarked = this.isBookmarked();
        return (
            <span>
                {isAuthenticated ? (
                    <Button
                        onClick={this.onToggle}
                        disabled={isSaving}
                        color="transparent"
                        title={isBookmarked ? 'Remove Bookmark' : 'Bookmark'}
                        icon={isBookmarked ? BookmarkIcon : BookmarkEmptyIcon}
                        iconColor={isBookmarked ? '#e99442' : '#acacac'}
                        size="small"
                    />
                ) : (
                    <Button
                        onClick={this.onLogin}
                        title="Log in to save bookmark"
                        color="transparent"
                        size="small"
                        icon={BookmarkEmptyIcon}
                        iconColor="#acacac"
                    >
                    </Button>
                )}
            </span>
        );
    }
}

const withMutations = graphql( ToggleBookmarkMutation, {
    props: ( { mutate, ownProps } ) => ({
        onToggle: ( id, title, url, tags ) => mutate( {
            variables: { id, title, url, tags }
        } )
    }),
} );

export default withMutations( withAuth0( BookmarkToggle ) );