import React from "react";
import PropTypes from 'prop-types';

require( './style.scss' );

Box.propTypes = {
    title: PropTypes.string,
    children: PropTypes.func.isRequired,
};


export default function Box( { title, children } ) {
    return (
        <div className="box">
            <h3>{title}</h3>
            <div>
                {children}
            </div>
        </div>
    )
}