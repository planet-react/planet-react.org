import React from 'react';
import PropTypes from 'prop-types';
import DateFormatter from 'components/support/dateformatter'
require( './style.scss' );

CalendarIcon.propTypes = {
    date: PropTypes.string.isRequired,
};

export default function CalendarIcon( { date } ) {
    return (
        <div className="calendar-icon">
            <b><DateFormatter date={date} format="MMM"/></b>
            <i><DateFormatter date={date} format="DD"/></i>
        </div>
    );
}