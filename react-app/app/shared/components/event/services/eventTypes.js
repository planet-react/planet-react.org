export default {

    data: [
        {
            id: 1,
            name: 'Conference',
            slug: 'conference',
        },
        {
            id: 2,
            name: 'Meet Up',
            slug: 'meetup'
        },
        {
            id: 3,
            name: 'Online',
            slug: 'online'
        },
    ],

    findByKey( key, value ) {
        return this.data.find( t => t[key] == value );
    },

    getEventIdBySlug( slug ) {
        const event = this.data.find( t => t.slug == slug );
        if ( !event ) {
            return null;
        }
        return event.id;
    },

    getEventById( id ) {
        return this.data.find( type => type.id == id );
    }

}