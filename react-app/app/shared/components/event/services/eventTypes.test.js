import eventTypes from './eventTypes';

//@todo: use snapshot?

describe( 'eventTypes', () => {
    describe( 'data', () => {
        it( 'should have the expected structure', () => {
            const expected = [
                {
                    id: 1,
                    name: 'Conference',
                    slug: 'conference',
                },
                {
                    id: 2,
                    name: 'Meet Up',
                    slug: 'meetup'
                },
                {
                    id: 3,
                    name: 'Online',
                    slug: 'online'
                },
            ];
            expect( eventTypes.data ).toEqual( expected );

        } );

    } );
} );