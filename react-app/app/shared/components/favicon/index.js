import React from 'react';
import PropTypes from 'prop-types';

FavIcon.propTypes = {
    host: PropTypes.string.isRequired,
    style: PropTypes.object,
    grayscale: PropTypes.bool
};

export default function FavIcon( { host, grayscale, style, ...rest } ) {
    const defaultStyle = {
        width: '16px',
        height: '16px',
        filter: grayscale ? 'grayscale(100%)' : null
    };

    const iconStyle = Object.assign( {}, defaultStyle, style );

    return (
        <img
            src={`https://www.google.com/s2/favicons?domain=${host}`}
            alt="Favicon"
            style={iconStyle}
            {...rest}
        />
    )
};