import feedSourceTypes from './feedSourceTypes.json';

describe( 'feedSourceTypes', () => {
    it( 'should have the expected values', () => {
        expect( feedSourceTypes[ 0 ] ).toEqual( {
            id: 1,
            name: 'RSS'
        } );
        expect( feedSourceTypes[ 1 ] ).toEqual( {
            id: 2,
            name: 'You Tube Channel'
        } );
    } )
} );