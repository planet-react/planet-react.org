import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

const FeedIcon = ( { feed, style } ) => {
    const url = feed.feed_url
        ? feed.feed_url
        : feed.website_url;

    return (
        <a href={url} className="feed-icon" title="Feed"
           style={style}> </a>
    )
};

FeedIcon.propTypes = {
    feed: PropTypes.object.isRequired,
    style: PropTypes.object
};

export default FeedIcon;