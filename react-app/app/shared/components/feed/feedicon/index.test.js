import React from 'react';
import FeedIcon from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const feed = {
        feed_url: 'https://app.url',
    };

    let tree = renderer.create(
        <FeedIcon feed={feed}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <FeedIcon feed={feed} style={{margin:'1em'}}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );