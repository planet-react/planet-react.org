import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

FeedImage.propTypes = {
    src: PropTypes.string,
    size: PropTypes.string,
    alt: PropTypes.string,
};

FeedImage.defaultProps = {
    size: '3.5em'
};

export default function FeedImage( { src, size, alt } ) {
    const url = src && src.startsWith( 'data:image' )
        ? src
        : '/' + __PLANET_PUBLIC_FEED_IMAGE_DIR__ + '/' + (src ? src : '_default.png');

    const style = {
        width: size,
        height: size,
    };

    return (
        <img className="feed-image" style={style} src={url} alt={alt}/>
    )
};