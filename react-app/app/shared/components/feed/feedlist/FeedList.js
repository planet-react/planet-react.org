import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'components/support/flex';
import FeedListItem from './FeedListItem';

require( './style.scss' );

FeedList.propTypes = {
    feeds: PropTypes.array,
};

export default function FeedList( { feeds } ) {
    return (
        <div className="feed-list">
            <Row className="list-header">
                <Col className="image">Image</Col>
                <Col className="name">Name</Col>
                <Col className="url">URL</Col>
                <Col className="post-count">Posts</Col>
                <Col className="language">Lang</Col>
                <Col className="feed"></Col>
            </Row>

            {feeds.map( ( feed ) => {
                return (
                    <FeedListItem key={feed.id} feed={feed}/>
                )
            } )}
        </div>
    );
}
