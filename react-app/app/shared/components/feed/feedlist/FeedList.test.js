import React from 'react';
import FeedList from './FeedList';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {

    const feeds = [
        {
            id: 1,
            slug:"alessandro-boneparte",
            name: "Alessandro Boneparte",
            website_url: "https://site.com/",
            feed_url: "https://site.com/feed.xml",
            image: "image-1.png",
            lang: "en",
            total_post_count: 5,
        },
        {
            id: 2,
            slug: "Branden-mc-mac",
            name: "Branden McMac",
            website_url: "https://domain.com/",
            feed_url: "https://domain.com/feed.xml",
            image: "image-2.png",
            lang: "es",
            total_post_count: 9,
        }
    ];

    const tree = renderer.create(
        <MockProviders>
            <FeedList feeds={feeds}/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

} );