import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'components/support/flex';
import FeedIcon from 'components/feed/feedicon';
import { ScreenLink } from 'components/screen';
import FeedImage from 'components/feed/feedimage';

require( './style.scss' );

FeedListItem.propTypes = {
    feed: PropTypes.shape( {
        id: PropTypes.number,
        name: PropTypes.string,
        description: PropTypes.string,
        url: PropTypes.string,
        feed_url: PropTypes.string,
        image: PropTypes.string,
        lang: PropTypes.string,
        created_at: PropTypes.string,
        last_fetched_at: PropTypes.string,
        month_post_count: PropTypes.number,
        total_post_count: PropTypes.number,
    } ),
};

export default function FeedListItem( { feed } ) {
    return (
        <Row className="list-item">
            <Col className="image">
                <FeedImage size={'3em'} src={feed.image} alt={feed.name}/>
            </Col>
            <Col className="name">
                <ScreenLink path={`/feeds/${feed.slug}`}>
                    {feed.name}
                </ScreenLink>
            </Col>
            <Col className="url">
                <a href={feed.website_url}>
                    {feed.url}
                </a>
            </Col>
            <Col className="post-count">
                {feed.total_post_count}
            </Col>
            <Col className="language">
                {feed.lang}
            </Col>
            <Col className="feed">
                <FeedIcon feed={feed}
                          style={{ verticalAlign: 'middle' }}/>
            </Col>
        </Row>
    )
}
