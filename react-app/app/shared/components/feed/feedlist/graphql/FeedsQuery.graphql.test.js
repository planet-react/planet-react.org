import { printAST } from 'react-apollo';

import FeedsQuery from './FeedsQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( FeedsQuery );
    expect( ast ).toMatchSnapshot()
} );