import React from 'react';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import FeedList from './FeedList';

import FeedsQuery from './graphql/FeedsQuery.graphql';

require( './style.scss' );

function FeedListWithData( { data: { feeds } } ) {
    return (
        <FeedList feeds={feeds}/>
    );
}

export default graphql( FeedsQuery, {
    options: {
        fetchPolicy: 'network-only',
    }
} )( withLoadingIndicator()( FeedListWithData ) );
