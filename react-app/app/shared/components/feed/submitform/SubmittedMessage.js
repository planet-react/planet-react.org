import React from 'react';
import PropTypes from 'prop-types';
import Message from 'components/support/message';
import { ScreenLink } from 'components/screen';

SubmittedMessage.propTypes = {
    isUpdateMode: PropTypes.bool,
    isAdmin: PropTypes.bool,
};

export default function SubmittedMessage( { isUpdateMode, isAdmin } ) {
    return (
        <div>
            {isUpdateMode ? (
                <Message context="success">
                    The feed was successfully updated
                </Message>
            ) : (
                <Message context="success">
                    The feed has been successfully
                    submitted. Aggregation
                    will
                    start once the feed has been approved.
                    <p>
                        <ScreenLink path="/">Home</ScreenLink>
                        {isAdmin &&
                        <span> | <ScreenLink path="/admin/feed-manager">
                                    Feed Manager (admin)
                                </ScreenLink>
                        </span>
                        }
                    </p>
                </Message>
            )}
        </div>
    );
}