import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FeedImage from 'components/feed/feedimage';

require( './style.scss' );

export default class ImageUploader extends Component {

    static propTypes = {
        image: PropTypes.string,
        onChangeCallback: PropTypes.func
    };

    static defaultProps = {};

    static ids = 0;
    static componentName = null;

    state = {
        image: null
    };

    constructor( props ) {
        super( props );
        this.componentName = 'image-uploader-' + (ImageUploader.ids++);

        this.onChange = this.onChange.bind( this );
    }

    componentDidMount() {
        this.setImage( this.props.image );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.image != this.props.image ) {
            this.setImage( nextProps.image );
        }
    }

    setImage( image ) {
        this.setState( {
            image: image
        }, () => {
            if ( this.props.onChangeCallback ) {
                this.props.onChangeCallback( image );
            }
        } )
    }

    onChange( event ) {
        const input = event.target;
        const url = input.value;
        const ext = url.substring( url.lastIndexOf( '.' ) + 1 ).toLowerCase();
        if ( input.files && input.files[ 0 ] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") ) {
            const reader = new FileReader();
            reader.onload = ( e ) => {
                this.setImage( e.target.result );
            };
            reader.readAsDataURL( input.files[ 0 ] );
        } else {
            this.setImage( null );
        }
    }

    render() {
        let { image } = this.state;
        if ( image && !image.startsWith( 'data:image' ) ) {
            image = image + '?' + new Date().getTime();
        }

        return (
            <div className="image-uploader">
                <label htmlFor={this.componentName}>
                    <FeedImage src={image} size="7.5em"/>
                </label>

                <input
                    id={this.componentName}
                    type="file"
                    name="image"
                    onChange={this.onChange}
                />
            </div>
        );
    }
}