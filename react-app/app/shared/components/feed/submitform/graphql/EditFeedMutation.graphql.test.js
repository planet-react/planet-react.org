import { printAST } from 'react-apollo';

import EditFeedMutation from './EditFeedMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( EditFeedMutation );
    expect( ast ).toMatchSnapshot()
} );