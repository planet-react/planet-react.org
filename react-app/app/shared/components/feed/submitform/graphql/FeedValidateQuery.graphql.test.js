import { printAST } from 'react-apollo';

import FeedValidateQuery from './FeedValidateQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( FeedValidateQuery );
    expect( ast ).toMatchSnapshot()
} );