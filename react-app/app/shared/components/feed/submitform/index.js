import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withApollo } from 'react-apollo';
import { withAuth0 } from 'components/services';
import { ScreenLink } from 'components/screen';
import { CountrySelector, FeedUrl, Form, Input, TextArea } from 'components/support/form';
import Button from 'components/support/button';
import Message from 'components/support/message';
import ImageUploader from './components/imageuploader';
import SubmittedMessage from './SubmittedMessage';

import EditFeedMutation from './graphql/EditFeedMutation.graphql';
import FeedValidateQuery from './graphql/FeedValidateQuery.graphql';

export class FeedSubmitForm extends Component {

    static propTypes = {
        feedUrl: PropTypes.string,
        autoFocus: PropTypes.bool,
        feedData: PropTypes.shape( {
            image: PropTypes.string,
            source_type: PropTypes.number,
            name: PropTypes.string,
            lang: PropTypes.string,
            feed_url: PropTypes.string,
            website_url: PropTypes.string,
            repo_url: PropTypes.string,
            twitter_url: PropTypes.string,
            description: PropTypes.string,
        } )
    };

    static defaultProps = {
        autoFocus: true
    };

    initialState = {
        isValidatingFeed: false,
        isFeedValid: false,
        feedValidatorErrorMessage: null,
        isSubmitting: false,

        source_type: 1,
        image: null,
        name: '',
        lang: 'EN',
        feed_url: this.props.feedUrl ? decodeURIComponent( this.props.feedUrl ) : '',
        website_url: '',
        twitter_url: '',
        repo_url: '',
        description: ''
    };

    isSubmitted = false;

    constructor( props ) {
        super( props );
        this.state = this.initialState;
    }

    isUpdateMode() {
        return this.props.feedData != undefined;
    }

    componentDidMount() {
        if ( this.isUpdateMode() ) {
            this.setState( this.props.feedData, this.onValidateFeedUrl );
        }
    }

    onChange = ( event ) => {
        this.setState( {
            [event.target.name]: event.target.value
        } );
    };

    onChangeImage = ( image ) => {
        this.setState( {
            image: image
        } );
    };

    onBlurFeedUrl = ( event ) => {
        const { value } = event.target;

        if ( value.trim() == '' ) {
            this.setState( {
                feedValidatorErrorMessage: null,
                isFeedValid: false
            } );
            return;
        }

        if ( this.state.isFeedValid ) {
            this.onValidateFeedUrl();
        }

    };

    onValidateFeedUrl = () => {
        const { feed_url } = this.state;

        if ( feed_url == '' ) {
            return;
        }

        this.setState( {
            isValidatingFeed: true,
            isFeedValid: false,
            feedValidatorErrorMessage: null,
        } );

        this.props.client.query( {
            query: FeedValidateQuery,
            variables: {
                feed_url: feed_url,
                //@todo: remove
                _skipExistsCheck: this.isUpdateMode(),
            }
        } ).then( ( { data } ) => {
            const { feedValidate } = data;

            this.setState( {
                isValidatingFeed: false,

                isFeedValid: feedValidate.is_valid,
                feedValidatorErrorMessage: feedValidate.is_valid ? null : feedValidate.message,

                name: feedValidate.name != this.state.name ? feedValidate.name : this.state.name,

                website_url: feedValidate.website_url  != this.state.website_url ? feedValidate.website_url : this.state.website_url,
                description: feedValidate.description != this.state.description ? feedValidate.description : this.state.description,
                source_type: feedValidate.source_type != this.state.source_type ? feedValidate.source_type : this.state.source_type,

            } );
        } ).catch( ( error ) => {
            console.log('error', error);
            this.setState( {
                isValidatingFeed: false,
                isFeedValid: false,
                feedValidatorErrorMessage: error.toString(),
            } );
        } );
    };

    onBeforeSubmit = () => {
        return new Promise( ( resolve, reject ) => {
            this.setState( {
                isSubmitting: true,
                feedValidatorErrorMessage: null,
            }, () => {
                resolve( {
                    formData: {
                        id: this.isUpdateMode() ? this.props.feedData.id : null,
                        source_type: this.state.source_type,
                        name: this.state.name,
                        image: this.state.image,
                        lang: this.state.lang != '' ? this.state.lang : 'EN',
                        feed_url: this.state.feed_url,
                        website_url: this.state.website_url,
                        twitter_url: this.state.twitter_url,
                        repo_url: this.state.repo_url,
                        description: this.state.description
                    }
                } );
            } );
        } );
    };

    onSubmitSuccess = () => {
        return new Promise( ( resolve, reject ) => {
            this.isSubmitted = true;
            this.setState( this.initialState );

            resolve( {} );
        } );
    };

    onSubmitError = () => {
        return new Promise( ( resolve, reject ) => {
            this.isSubmitted = false;
            this.setState( this.initialState );

            resolve( {} );
        } );
    };

    render() {
        const { auth0, autoFocus } = this.props;
        const { isFeedValid, feedValidatorErrorMessage, isValidatingFeed, isSubmitting } = this.state;
        const isAdmin = auth0.userHasRole( [ auth0.roles.ADMIN ] );
        const isUpdateMode = this.isUpdateMode();
        const showIntroMessage = !isUpdateMode && !this.isSubmitted;

        return (
            <div className="submit-feed-form">
                {this.isSubmitted ? (
                    <SubmittedMessage
                        isAdmin={isAdmin}
                        isUpdateMode={isUpdateMode}
                    />
                ) : (
                    <div>
                        {showIntroMessage &&
                        <Message>
                            Please read the <ScreenLink
                            path="/faq">FAQ</ScreenLink> if you want to know more about how feeds are approved and aggregated.
                            Thank you for submitting your feed!
                        </Message>}

                        <Form
                            mutation={EditFeedMutation}
                            data={this.state}
                            onBeforeSubmit={this.onBeforeSubmit}
                            onSubmitSuccess={this.onSubmitSuccess}
                            onSubmitError={this.onSubmitError}
                        >
                            {isUpdateMode &&
                            <div style={{ textAlign: 'center' }}>
                                <ImageUploader
                                    image={this.state.image}
                                    onChangeCallback={this.onChangeImage}
                                />
                            </div>}

                            {/*{this.renderSourceTypes()}*/}

                            <FeedUrl
                                value={this.state.feed_url}
                                id="feed-input-feed-url"
                                isValidating={isValidatingFeed}
                                isFeedValid={isFeedValid}
                                showStatusIcon={!isValidatingFeed && (isFeedValid || feedValidatorErrorMessage != null)}
                                required={true}
                                autoFocus={autoFocus}
                                onChange={this.onChange}
                                onBlur={this.onBlurFeedUrl}
                                errorMessage={feedValidatorErrorMessage}
                            />

                            {!isFeedValid &&
                            <Button
                                disabled={this.state.feed_url == '' || isValidatingFeed}
                                onClick={this.onValidateFeedUrl}
                            >
                                Next
                            </Button>}

                            <div style={{ display: isFeedValid ? '' : 'none' }}>

                                <Input
                                    name="name"
                                    value={this.state.name}
                                    id="feed-input-name"
                                    label="Name"
                                    required={true}
                                    onChange={this.onChange}
                                />

                                <TextArea
                                    name="description"
                                    value={this.state.description}
                                    id="feed-input-description"
                                    label="Description"
                                    onChange={this.onChange}
                                />

                                <Input
                                    name="website_url"
                                    value={this.state.website_url}
                                    id="feed-input-website-url"
                                    label="Website URL"
                                    required={true}
                                    onChange={this.onChange}
                                />

                                <Input
                                    name="twitter_url"
                                    type="url"
                                    value={this.state.twitter_url}
                                    id="feed-twitter-url"
                                    label="Twitter URL"
                                    onChange={this.onChange}
                                />

                                <Input
                                    name="repo_url"
                                    type="url"
                                    value={this.state.repo_url}
                                    id="feed-repo-url"
                                    label="Repo URL"
                                    onChange={this.onChange}
                                />

                                <CountrySelector
                                    name="lang"
                                    value={this.state.lang}
                                    id="feed-input-country"
                                    label="Language (spoken language in the posts)"
                                    onChange={this.onChange}
                                />

                                <p>
                                    <Button type="submit">
                                        {isUpdateMode ? 'Update' : 'Submit'}
                                    </Button>
                                </p>
                            </div>

                        </Form>
                    </div>
                )}
            </div>
        )
    }
}

export default withAuth0( withApollo( FeedSubmitForm ) )