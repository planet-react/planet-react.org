import React from 'react';
import { ScreenLink } from 'components/screen'
require( './style.scss' );

export default function Footer() {
    return (
        <footer>
            <span>
                {__APP_NAME__} {process.env.NODE_ENV == 'development' && '(development mode)'}
                <span> - </span>
                <ScreenLink path={'/legal/screens/terms'}>Terms and Conditions</ScreenLink>
                <span> - </span>
                <ScreenLink path={'/legal/privacy'}>Privacy Policy</ScreenLink>
                <span> - </span>
                <ScreenLink path="/faq">FAQ</ScreenLink>
                <span> - </span>
                <a href={__PLANET_REPO__}>Source code and issues</a>
                <span> - </span>
                <span><a href="/api/feed">Atom Feed</a></span>
            </span>
        </footer>
    );
};

