import React from 'react';
import Footer from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <Footer/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );