import React, { Component } from "react";
import PropTypes from 'prop-types';
import Button from 'components/support/button';
import Spinner from 'components/support/spinner';

export default class PaginationContainer extends Component {

    static propTypes = {
        data: PropTypes.shape( {
            error: PropTypes.object,
            fetchMore: PropTypes.func,
            loading: PropTypes.bool,
            networkStatus: PropTypes.number,
            refetch: PropTypes.func,
            startPolling: PropTypes.func,
            stopPolling: PropTypes.func,
            subscribeToMore: PropTypes.func,
            updateQuery: PropTypes.func,
            variables: PropTypes.object,
        } ).isRequired,
        dataKey: PropTypes.string.isRequired
    };

    state = {
        loading: false
    };

    onClick = ( data, nextPage ) => {
        this.setState( {
            loading: true
        } );

        data.loadMoreEntries( nextPage ).then( () => {
            this.setState( {
                loading: false
            } );
        } );
    };

    render() {
        const { loading } = this.state;
        const data = DataLoadMoreDecorator( this.props.data, this.props.dataKey );
        const { pagination } = data[ this.props.dataKey ];
        const nextPage = pagination.current_page + 1;

        return (
            <div>
                {this.props.children}

                {pagination.has_more_pages &&
                <div style={{ textAlign: 'center' }}>
                    <Button
                        onClick={this.onClick.bind( this, data, nextPage )}
                        disabled={loading}
                    >
                        Show more results
                    </Button>
                    {loading && <Spinner verticalSpace={'1.3em'} text="Loading more results"/>}
                </div>}
                <div>Results: {pagination.total}</div>
            </div>
        );
    }
}

function DataLoadMoreDecorator( data, dataKey ) {
    const { loading, variables, fetchMore, networkStatus, error } = data;

    return {
        loading,
        [dataKey]: data[ dataKey ],

        networkStatus,
        variables,
        error,
        loadMoreEntries( page ) {
            return fetchMore( {
                variables: {
                    page: page,
                    type: variables.type,
                    past: variables.past,
                },
                updateQuery: ( previousResult, { fetchMoreResult } ) => {
                    if ( !fetchMoreResult ) {
                        return previousResult;
                    }
                    return Object.assign( {}, previousResult, {
                        // Append the new feed results to the old one
                        [dataKey]: {
                            __typename: previousResult[ dataKey ].__typename,
                            pagination: fetchMoreResult[ dataKey ].pagination,
                            data: [ ...previousResult[ dataKey ].data, ...fetchMoreResult[ dataKey ].data ]
                        },
                    } );
                },
            } );
        },
    };
}