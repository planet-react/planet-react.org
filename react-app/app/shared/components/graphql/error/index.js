import React from 'react';
import PropTypes from 'prop-types';
import Message from 'components/support/message';

GraphQlError.propTypes = {
    errors: PropTypes.object.isRequired
};

function processValidationObject( errors ) {
    let result = [];
    for ( let key in errors ) {
        if ( !errors.hasOwnProperty( key ) ) {
            continue;
        }

        result.push( key + ': ' + errors[ key ] );
    }
    return result;
}

export default function GraphQlError( { errors } ) {

    return (
        <Message context="error">
            { errors.graphQLErrors &&
            <div>
                <h3>: ( Something went wrong</h3>
                <ul>
                    {errors.graphQLErrors.map( ( error, i ) => (
                        <li key={i}>
                            {error.message}

                            {error.validation && <ul>
                                {processValidationObject( error.validation ).map( ( message, j ) => {
                                    return (
                                        <li key={j}>
                                            {message}
                                        </li>
                                    )
                                } )}
                            </ul>}
                        </li>
                    ) )}
                </ul>
            </div> }
        </Message>
    )
}
