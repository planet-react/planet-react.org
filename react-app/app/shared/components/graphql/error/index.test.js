import React from 'react';
import renderer from 'react-test-renderer';
import GraphQlError from '../error';

const mockError = {
    "graphQLErrors": [
        {
            "message": "validation",
            "locations": [
                {
                    "line": 2,
                    "column": 3
                }
            ],
            "validation": {
                "url": [
                    "The url format is invalid."
                ]
            }
        }
    ],
    "networkError": null,
    "message": "GraphQL error: validation"
};

it( 'renders correctly', () => {
    const tree = renderer.create(
        <GraphQlError errors={mockError}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
