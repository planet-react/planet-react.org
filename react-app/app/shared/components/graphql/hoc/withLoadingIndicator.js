import React, { Component } from "react";
import Spinner from "components/support/spinner";
import GraphQLError from '../error';

export default function withwithLoadingIndicator( loadingText ) {
    return function withLoadingIndicator( Component ) {
        function WithLoadingIndicator( props ) {
            const { data } = props;

            if ( data.loading ) {
                return <Spinner text={loadingText}/>
            }

            if ( data.error ) {
                return <GraphQLError errors={data.error}/>
            }

            return <Component {...props}/>
        }

        return WithLoadingIndicator
    }
}