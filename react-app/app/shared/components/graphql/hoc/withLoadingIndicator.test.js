import React, { Component } from 'react';
import renderer from 'react-test-renderer';

import { withLoadingIndicator } from '../';

let dataMock = {
    loading: true,
    error: null,
    resultData: {
        name: ' Nathan Stone',
        email: ' nathan@vault11.com',
    }
};

class MockComponent extends Component {
    constructor( props ) {
        super( props );
    }

    render() {
        const { resultData } = this.props.data;
        return <div>{resultData.name} - {resultData.email}</div>;
    }
}


it( 'renders as expected when loading', () => {
    const MockComponentWithData = withLoadingIndicator()( MockComponent );

    const tree = renderer.create(
        <MockComponentWithData data={dataMock}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'renders as expected on loading success', () => {
    const MockComponentWithData = withLoadingIndicator()( MockComponent );

    dataMock.loading = false;

    const tree = renderer.create(
        <MockComponentWithData data={dataMock}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );

it( 'render error message on loading failure', () => {
    const MockComponentWithData = withLoadingIndicator()( MockComponent );

    dataMock.loading = false;
    dataMock.error = {
        graphQLErrors: [
            { message: 'Error 1' },
            { message: 'Error 2' },
            { message: 'Error 3' },
        ]
    };

    const tree = renderer.create(
        <MockComponentWithData data={dataMock}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );

it( 'supports loading text', () => {
    const MockComponentWithData = withLoadingIndicator('Loading text')( MockComponent );

    dataMock.loading = true;

    const tree = renderer.create(
        <MockComponentWithData data={dataMock}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );