import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withServices } from 'components/services';

require( './style.scss' );

class Header extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        metaDescription: PropTypes.string,
        header: PropTypes.string,
        hideHeader: PropTypes.bool
    };

    static defaultProps = {
        title: null,
        metaDescription: __PLANET_HTML_DEFAULT_META_DESCRIPTION__,
        hideHeader: false
    };

    constructor( props ) {
        super( props );
    }

    componentWillMount() {
        this.setDocumentTitle( this.props.title );
        this.setMeta( this.props.metaDescription );
    }

    componentWillReceiveProps( nextProps ) {
        this.setDocumentTitle( nextProps.title );
        this.setMeta( nextProps.metaDescription );
    }

    componentWillUnmount() {
        this.removeMeta();
    }

    setDocumentTitle( title ) {
        document.title = title + ' - ' + __APP_NAME__;
    }

    setMeta( metaDescription ) {
        const head = document.querySelector( 'head' );
        if ( !head ) {
            return;
        }
        let metaDescriptionElem = document.querySelector( 'meta[name="description"]' );
        if ( metaDescriptionElem ) {
            this.removeMeta();
        }
        metaDescriptionElem = document.createElement( 'meta' );
        metaDescriptionElem.setAttribute( 'name', 'description' );
        metaDescriptionElem.setAttribute( 'content', metaDescription );
        head.appendChild( metaDescriptionElem );
    }

    removeMeta() {
        const metaDescriptionElem = document.querySelector( 'meta[name="description"]' );
        if ( metaDescriptionElem ) {
            metaDescriptionElem.parentNode.removeChild( metaDescriptionElem );
        }
    }

    render() {
        const { header, hideHeader, title } = this.props;

        if ( hideHeader ) {
            return null;
        }

        return (
            <section className="header">
                <h1>
                    {header ? header : title}
                </h1>
            </section>
        );
    }
}

export default withServices( Header );