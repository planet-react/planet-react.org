import React from 'react';
import Header from './';
import renderer from 'react-test-renderer';
import MockDate from '../../../../internals/testutils/MockDate';
import MockProviders from '../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {

    MockDate.setTime( 1434319925275 );

    const tree = renderer.create(
        <MockProviders>
            <Header title="Foo" header="Bar"/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

    MockDate.reset();
} );