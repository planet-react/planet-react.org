import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';
import A from 'components/support/a';
import LoopIcon from 'react-icons/lib/md/loop';

AppUpdatedPopup.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    appLastUpdatedAt: PropTypes.instanceOf(Date),
    onClose: PropTypes.func.isRequired,
    onReload: PropTypes.func,
};

AppUpdatedPopup.defaultProps = {};

export default function AppUpdatedPopup( { isOpen, onClose, appLastUpdatedAt, onReload } ) {
    return (
        <Popup
            target={'#navbar'}
            position={Popup.positions.SCREEN_CENTER}
            isOpen={isOpen}
            onClose={onClose}
        >
            <div style={{
                textAlign: 'center',
                padding: '2.3em 4.1em',
            }}>
                <div style={{ margin: '0 0 1.5em 0' }}>
                    <A onClick={onReload}>
                        <LoopIcon size={67}/>
                    </A>
                </div>
                <h1>{__APP_NAME__} has been updated!</h1>
                <p>
                    {appLastUpdatedAt.toUTCString()}
                </p>
                <div>
                    <A onClick={onReload}>
                        The application will reload on next navigation
                    </A>
                </div>
            </div>
        </Popup>
    );
}