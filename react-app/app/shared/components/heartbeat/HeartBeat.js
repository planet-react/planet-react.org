import React, { Component } from 'react';
import AppUpdatedPopup from './AppUpdatedPopup';
import StatusPopup from './StatusPopup';

export default class HeartBeat extends Component {

    state = {
        hasAppUpdate: false,
        isAggregatorRunning: false,
        hasNewPost: false,
    };

    /**
     * Timestamp, when the app was last updated.
     * @type {number|null}
     */
    appLastUpdatedAt = null;

    latestKnownPostId = null;

    constructor( props ) {
        super( props );
    }

    componentWillReceiveProps( nextProps ) {
        const { loading, error } = nextProps.data;
        if ( loading || error ) {
            return;
        }
        const { heartBeat: { appLastUpdatedAt, isAggregatorRunning, latestPostId } } = nextProps.data;

        this.setState( { isAggregatorRunning: isAggregatorRunning } );

        if ( !isAggregatorRunning ) {
            this.checkHasNewPosts( latestPostId );
        }

        this.checkAppIsUpdated( appLastUpdatedAt );
    }

    checkHasNewPosts( latestPostId ) {
        this.setState( {
            hasNewPost: this.latestKnownPostId != null
            && this.latestKnownPostId < latestPostId,
        }, () => {
            this.latestKnownPostId = latestPostId;
        } );
    }

    checkAppIsUpdated( appLastUpdatedAt ) {
        if ( !this.appLastUpdatedAt ) {
            this.appLastUpdatedAt = appLastUpdatedAt;
        }
        else {
            if ( appLastUpdatedAt > this.appLastUpdatedAt ) {
                this.appLastUpdatedAt = appLastUpdatedAt;
                window.swUpdate = true;
                this.setState( {
                    hasAppUpdate: true
                } );
            }
        }
    }

    onCloseAppUpdatedPopup = () => {
        this.setState( { hasAppUpdate: false } )
    };

    onCloseAggregatorPopup = () => {
        void(0);
    };

    onCloseNewPostPopup = () => {
        this.setState( {
            hasNewPost: false
        } );
    };

    onReload = () => {
        window.location.reload();
    };

    render() {
        const { hasAppUpdate, isAggregatorRunning, hasNewPost } = this.state;
        const appLastUpdatedAt = new Date( this.appLastUpdatedAt * 1000 );

        return (
            <span>
                <AppUpdatedPopup
                    isOpen={hasAppUpdate}
                    appLastUpdatedAt={appLastUpdatedAt}
                    onReload={this.onReload}
                    onClose={this.onCloseAppUpdatedPopup}
                />

                <StatusPopup
                    isOpen={isAggregatorRunning}
                    onClose={this.onCloseAggregatorPopup}
                >
                    <div>Aggregator is running</div>
                </StatusPopup>

               <StatusPopup
                   isOpen={hasNewPost}
                   onClose={this.onCloseNewPostPopup}
               >
                    <div>Aggregator is running</div>
                </StatusPopup>
            </span>
        );
    }

}