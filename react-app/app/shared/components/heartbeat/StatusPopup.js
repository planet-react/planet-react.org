import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';

StatusPopup.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

StatusPopup.defaultProps = {};

const style = {
    border: 'none',
    borderRadius: '0',
    color: '#1cbcf0',
    fontSize: '.7rem',
    padding: '.3em 1em',
};

export default function StatusPopup( { isOpen, onClose, children } ) {
    return (
        <Popup
            target={'#navbar'}
            position={Popup.positions.TARGET_TOP_LEFT_INSIDE}
            isOpen={isOpen}
            showCloseButton={false}
            modal={false}
            backgroundColor="#2d2d2d"
            style={style}
            onClose={onClose}
        >
            {children}
        </Popup>
    );
}