import { printAST } from 'react-apollo';

import SwUpdateQuery from './HeartBeatQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( SwUpdateQuery );
    expect( ast ).toMatchSnapshot()
} );