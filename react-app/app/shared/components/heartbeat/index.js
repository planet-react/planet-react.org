import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import HeartBeat from './HeartBeat';
import HeartBeatQuery from './graphql/HeartBeatQuery.graphql';

window.swUpdate = null;

const HeartBeatWithData = graphql( HeartBeatQuery, {
    options: ( props ) => ({
        pollInterval: props.pollInterval
    }),
} )( HeartBeat );

HeartBeatWithData.propTypes = {
    pollInterval: PropTypes.number
};

HeartBeatWithData.defaultProps = {
    pollInterval: 3000 * 60 * 2
};

export default HeartBeatWithData;