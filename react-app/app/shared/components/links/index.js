import React from "react";
import PropTypes from 'prop-types';
import TwitterIcon from 'react-icons/lib/fa/twitter';
import FeedIcon from 'react-icons/lib/fa/feed';
import HomeIcon from 'react-icons/lib/fa/home';
import RepoIcon from 'react-icons/lib/go/repo';

require( './style.scss' );

Links.propTypes = {
    siteUrl: PropTypes.string,
    feedUrl: PropTypes.string,
    twitterUrl: PropTypes.string,
    repoUrl: PropTypes.string,
};

export default function Links( { siteUrl, feedUrl, twitterUrl, repoUrl } ) {
    const iconSize = 17;
    const iconColor = '#acacac';
    return (
        <ul className="links">
            {siteUrl &&
            <li>
                <HomeIcon size={iconSize} color={iconColor}/>
                <a href={siteUrl}>Website</a>
            </li>}

            {twitterUrl &&
            <li>
                <TwitterIcon size={iconSize} color={iconColor}/>
                <a href={twitterUrl}>Twitter</a>
            </li>}

            {feedUrl &&
            <li>
                <FeedIcon size={iconSize} color={iconColor}/>
                <a href={feedUrl}>Feed</a>
            </li>}
            {repoUrl &&
            <li>
                <RepoIcon size={iconSize} color={iconColor}/>
                <a href={repoUrl}>Repository</a>
            </li>}
        </ul>
    );
}