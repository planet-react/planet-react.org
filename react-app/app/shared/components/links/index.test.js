import React from 'react';
import FeedLinks from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const tree = renderer.create(
       <FeedLinks
           siteUrl="https://test.com/"
           feedUrl="https://test.com/feed.xml"
           twitterUrl="https://twitter.com/twittrer"
       />
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'renders correctly when feedUrl prop is not is passed', () => {
    const tree = renderer.create(
       <FeedLinks
           siteUrl="https://test.com/"
       />
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );