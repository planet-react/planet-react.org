import React from 'react';
import { ScreenLink } from 'components/screen';

require( './style.scss' );

import SVGLogo from './react.svg';

export default function Logo() {
    return (
        <ScreenLink path="/" className="logo">
            <img src={SVGLogo} alt="React"/>
            <span>Planet React</span>
        </ScreenLink>
    )
};