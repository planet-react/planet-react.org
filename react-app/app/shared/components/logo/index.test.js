import React from 'react';
import Logo from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {

    const tree = renderer.create(
        <MockProviders>
            <Logo/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );