import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Col, Row } from 'components/support/flex';
import { MonthList } from 'react-util-components';

require( './style.scss' );


export class MonthPaginator extends Component {

    static propTypes = {
        startMonth: PropTypes.number,
        startYear: PropTypes.number,
        disableFromIndex: PropTypes.number,
        path: PropTypes.string
    };

    isTouchDevice = false;
    scrollWrapper = null;
    isMouseDown = false;
    isMoving = false;
    lastClientX = 0;
    newScrollX = 0;
    maxScroll = 0;
    name = 'month-paginator-' + new Date().getTime();

    constructor( props ) {
        super( props );

        this.onPaginate = this.onPaginate.bind( this );
        this.onPaginateToYear = this.onPaginateToYear.bind( this );
        this.onMouseDown = this.onMouseDown.bind( this );
        this.onTouchStart = this.onMouseDown.bind( this );
        this.onMouseMove = this.onMouseMove.bind( this );
        this.onTouchMove = this.onMouseMove.bind( this );
        this.onMouseUp = this.onMouseUp.bind( this );
        this.onTouchEnd = this.onMouseUp.bind( this );
        this.onMouseLeave = this.onMouseLeave.bind( this );
        this.onWheel = this.onWheel.bind( this );
    }

    componentDidMount() {
        this.isTouchDevice = (('ontouchstart' in window)
            || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

        this.scrollWrapper = document.querySelector( '[data-name="' + this.name + '"]' );

        // Use timeout as it seems like the computed widths are not computed yet.
        setTimeout( () => {
            this.maxScroll = this.scrollWrapper.scrollWidth - this.scrollWrapper.offsetWidth;
            this.scrollToSelectedMonth();
        }, 100 );
    }

    /**
     * @todo: figure out why undefined is in the path.
     */
    parseRoutePath( path ) {
        return path.replace( /\/undefined/, '' );
    }

    onMouseDown( event ) {
        this.isMouseDown = true;
        this.lastClientX = this.isTouchDevice
            ? event.touches[ 0 ].clientX
            : event.clientX;
    }

    onMouseUp() {
        this.isMouseDown = false;
        setTimeout( () => {
            this.isMoving = false;
        }, 300 )
    }

    onMouseMove( event ) {
        if ( this.isMouseDown ) {
            this.isMoving = true;
            let clientX = this.isTouchDevice
                ? event.touches[ 0 ].clientX
                : event.clientX;

            this.scrollWrapper.scrollLeft -= this.newScrollX
                = (-this.lastClientX + (this.lastClientX = clientX));
        }
    }

    onMouseLeave() {
        this.isMouseDown = false;
        this.isMoving = false;
    }

    onWheel( event ) {
        event.stopPropagation();
        event.preventDefault();
        if ( event.deltaX < 0 || event.deltaX > 0 ) {
            return;
        }

        if ( event.deltaY < 0 ) {
            this.scrollWrapper.scrollLeft -= 3;
        }
        else {
            this.scrollWrapper.scrollLeft += 3;
        }
    }

    onPaginate( data ) {
        if ( data.disabled ) {
            return;
        }
        if ( this.isMoving ) {
            return;
        }
        const { path, history, startYear } = this.props;
        let month = data.value + 1;
        month = month < 10 ? '0' + month : month;
        history.push( `${this.parseRoutePath( path )}/${startYear}/${month}` )
    }

    onPaginateToYear( direction ) {
        if ( this.isMoving ) {
            return;
        }
        this.scrollWrapper.scrollLeft = 0;
        const { path, history, startYear } = this.props;
        const year = direction == 'next' ? startYear + 1 : startYear - 1;
        history.push( `${this.parseRoutePath( path )}/${year}/01` );
    }

    scrollToSelectedMonth() {
        const selectedMonthElem = this.scrollWrapper.querySelector( '.pr-month-list__item--selected' );
        if ( !selectedMonthElem ) {
            return;
        }

        this.scrollWrapper.scrollLeft = (selectedMonthElem.offsetLeft - this.scrollWrapper.offsetLeft)
            - (this.scrollWrapper.offsetWidth / 2)
            + (selectedMonthElem.offsetWidth / 2);
    }

    render() {
        const { startMonth, startYear } = this.props;
        const today = new Date();
        const disableNextYearButton = startYear >= today.getFullYear();
        const disableMonthButtons = startYear > today.getFullYear();
        const indexToDisableFrom = today.getFullYear() == startYear ? today.getMonth() : undefined;

        return (
            <Row className="month-paginator" alignItems="baseline">
                <Col className="month-paginator__button-col">
                    <button
                        onClick={() => this.onPaginateToYear( 'previous' )}>
                        〈 {startYear - 1}</button>
                </Col>
                <Col
                    className="month-paginator__list-wrapper"
                    data-name={this.name}
                    onMouseDown={this.onMouseDown}
                    onTouchStart={this.onMouseDown}
                    onMouseMove={this.onMouseMove}
                    onTouchMove={this.onMouseMove}
                    onMouseUp={this.onMouseUp}
                    onTouchEnd={this.onMouseUp}
                    onMouseLeave={this.onMouseLeave}
                    onWheel={this.onWheel}
                >
                    <MonthList
                        value={disableMonthButtons ? -1 : startMonth}
                        disableFromIndex={disableMonthButtons ? -1 : indexToDisableFrom}
                        onSelect={this.onPaginate}/>
                </Col>
                <Col className="month-paginator__button-col">
                    <button disabled={disableNextYearButton}
                            onClick={disableNextYearButton ? null : () => this.onPaginateToYear( 'next' )}>{startYear + 1}
                        〉
                    </button>
                </Col>
            </Row>
        )
    }

}

export default withRouter(MonthPaginator);