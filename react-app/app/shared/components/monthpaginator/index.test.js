import React from 'react';
import { MonthPaginator } from './';

describe( 'MonthPaginator', () => {
    it( 'should render without throwing an error', () => {
        expect( shallow( <MonthPaginator/> )
            .is( '.month-paginator' ) ).toBe( true );
    } );

    it( 'should render 12 li html elements', () => {
        expect( mount( <MonthPaginator/> ).find('li').length).toBe(12);
    } );
} );