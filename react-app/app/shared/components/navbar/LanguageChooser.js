import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import CountrySelector from 'components/support/CountrySelector';

class LanguageChooser extends Component {

    state = {
        lang: ''
    };

    unListen = null;

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        // Make sure the language is set on navigation change.
        this.unListen = this.props.history.listen( ( location ) => {
            this.onSetLanguage( location.pathname );
        } );
        this.onSetLanguage( this.props.location.pathname );
    }

    componentWillUnmount() {
        this.unListen();
    }

    onSetLanguage = ( pathname ) => {
        if ( !pathname ) {
            return;
        }
        const lang = pathname.split( '/' )[ 2 ];

        this.setState( {
            lang: lang
        } );
    };

    onChange = ( event ) => {
        const { value } = event.target;
        this.setState( {
            lang: value
        }, () => {
            const route = value == '' ? '' : '/posts/' + value;
            this.props.history.push( `${route}` );
        } );
    };

    render() {
        return (
            <CountrySelector
                onChange={this.onChange}
                selected={this.state.lang}/>
        )
    }
}

export default withRouter( LanguageChooser )
