import React, { Component } from 'react';
import { withAuth0, withServices } from 'components/services';
import { withRouter, } from 'react-router-dom';
import Logo from 'components/logo';
import LanguageChooser from './LanguageChooser';
import { ScreenLink } from 'components/screen';
import ProfileMenu from './profilemenu';

require( './style.scss' );

export class NavBar extends Component {

    constructor( props ) {
        super( props );
        let search = (props.location.search.split( 'q=' )[ 1 ] || '').split( '&' )[ 0 ];
        if ( search ) {
            search = decodeURIComponent( search );
        }
        this.state = {
            search: search || ''
        };
    }

    componentWillReceiveProps( nextProps ) {
        if ( !nextProps.location.pathname.startsWith( '/search' ) ) {
            this.setState( { search: '' } );
        }
    }

    debounceSearch = this.props.services.util.debounce( () => {
        this.props.history.push( `/search?q=${this.state.search}` );
    }, 500 );

    onSearch = ( event ) => {
        event.persist();
        const value = event.target.value;
        this.setState( { search: value }, this.debounceSearch );
    };

    onLogin = () => {
        const { auth0 } = this.props;
        auth0.showLogin();
    };

    onNavigateToProfile = () => {
        this.props.history.push( '/profile' );
    };

    onLogout = () => {
        const { auth0 } = this.props;
        auth0.logout();
    };

    render() {
        const { auth0, data } = this.props;
        const { search } = this.state;
        const { isAuthenticated } = auth0;
        return (

            <header id="navbar" className={'navbar'}>
                <div className="navbar__inner">

                    <Logo/>

                    <div className="navbar__description">
                        News and information aggregated from the React community
                    </div>
                </div>
                <nav>
                    <ul>
                        <li>
                            <ScreenLink exact path="/">
                                Home
                            </ScreenLink>
                        </li>
                        <li>
                            <ScreenLink
                                path="/events"
                            >
                                Events
                            </ScreenLink>
                        </li>
                        <li className="navbar__search-menu-item">
                            <input type="text" name="search" value={search}
                                   aria-label="Search posts"
                                   className={'search ' + (search != '' ? 'has-value' : '')}
                                   placeholder="Search posts"
                                   onChange={this.onSearch}/>
                        </li>
                        <li>
                            <LanguageChooser/>
                        </li>
                    </ul>
                </nav>

                <div className="profile-menu-wrapper">
                    <ProfileMenu
                        isAuthenticated={isAuthenticated}
                        profile={auth0.userProfile}
                        isAdmin={auth0.userHasRole( auth0.roles.ADMIN )}
                        onNavigateToProfile={this.onNavigateToProfile}
                        onLogin={this.onLogin}
                        onLogout={this.onLogout}
                    />
                </div>

            </header>

        )
    }
}

export default withServices(
    withAuth0(
        withRouter( NavBar )
    )
)

