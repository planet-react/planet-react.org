import React from "react";
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen';
import A from 'components/support/a';

require( './style.scss' );

FlyOutMenu.propTypes = {
    open: PropTypes.bool.isRequired,
    isAdmin: PropTypes.bool.isRequired,
    onLogout: PropTypes.func.isRequired,
};

export default function FlyOutMenu( { open, isAdmin, onLogout } ) {
    return (
        <ul className="profile-menu__flyout"
            style={{ display: open ? '' : 'none' }}>
            {isAdmin &&
            <li>
                <ScreenLink
                    path="/admin"
                    className="profile-menu-item"
                >
                    Admin
                </ScreenLink>
            </li>}
            <li>
                <ScreenLink
                    path="/profile"
                    className="profile-menu-item"
                >
                    Profile
                </ScreenLink>
            </li>
            <li>
                <A onClick={onLogout} className="profile-menu-item">
                    Logout
                </A>
            </li>
        </ul>
    );
}