import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import FlyOutMenu from './FlyOutMenu'

const onLogout = jest.fn();

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <FlyOutMenu
                open={true}
                onLogout={onLogout}
                isAdmin={true}
            />
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'renders correctly when not open', () => {
    let tree = renderer.create(
        <MockProviders>
            <FlyOutMenu
                open={false}
                onLogout={onLogout}
                isAdmin={true}
            />
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'does not render menu items only visible to admin when `isAdmin` = false', () => {
    let tree = renderer.create(
        <MockProviders>
            <FlyOutMenu
                open={true}
                onLogout={onLogout}
                isAdmin={false}
            />
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );