import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Picture from 'components/auth0/picture';
import FlyOutMenu from './FlyOutMenu';

require( './style.scss' );

export default class ProfileMenu extends Component {

    static propTypes = {
        isAuthenticated: PropTypes.bool.isRequired,
        onNavigateToProfile: PropTypes.func.isRequired,
        onLogin: PropTypes.func.isRequired,
        onLogout: PropTypes.func.isRequired,
        profile: PropTypes.object,
        isAdmin: PropTypes.bool,
    };

    state = {
        isMouseOver: false,
        isOpen: false
    };

    componentDidMount() {
        document.addEventListener( 'click', this.onClickOutside )
    }

    componentWillUnmount() {
        document.removeEventListener( 'click', this.onClickOutside );
    }

    onClickOutside = ( event ) => {
        let target = event.target;
        while ( target ) {
            if ( target.classList ) {
                if ( target.classList.contains( 'profile-menu-item' ) ) {
                    break;
                }
                if ( target.classList.contains( 'profile-menu-button' )
                    && !target.classList.contains( 'profile-menu-picture' )  ) {
                    return;
                }
            }
            target = target.parentNode;
        }

        this.setState( { isOpen: false } );
    };

    onToggleMenu = () => {
        this.setState( {
            isOpen: !this.state.isOpen
        } )
    };

    onMouseOverProfile = () => {
        this.setState( {
            isMouseOver: true
        } );
    };

    onMouseOutProfile = () => {
        this.setState( {
            isMouseOver: false
        } );
    };

    render() {
        const { isAuthenticated, onLogin } = this.props;
        if ( !isAuthenticated ) {
            return (
                <div className="profile-menu profile-menu--not-logged-in">
                    <button
                        className="profile-menu-button"
                        onClick={onLogin}
                    >
                        Login
                    </button>
                </div>
            );
        }

        const { isOpen, isMouseOver } = this.state;
        const { isAdmin, onLogout, onNavigateToProfile, profile } = this.props;
        const picture = profile.picture;
        const nickname = profile.nickname;

        return (
            <div className="profile-menu">
                <button
                    className="profile-menu-button profile-menu-picture"
                    onClick={onNavigateToProfile}
                    onMouseOver={this.onMouseOverProfile}
                    onMouseOut={this.onMouseOutProfile}
                    style={{ transform: (isMouseOver ? 'scale(1.3)' : 'scale(1)') }}
                >
                    <Picture size="1.7em" src={picture} alt={nickname}/>
                </button>

                <button className="profile-menu-button"
                        onClick={this.onToggleMenu}>
                    {nickname}
                </button>

                <FlyOutMenu
                    open={isOpen}
                    isAdmin={isAdmin}
                    onLogout={onLogout}
                />

            </div>
        )
    }
}