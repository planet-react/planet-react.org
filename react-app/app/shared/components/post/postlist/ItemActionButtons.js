import React from "react";
import PropTypes from 'prop-types';
import A from 'components/support/a';
import Button from 'components/support/button';
import BookmarkToggle from 'components/bookmark/toggle';
import OpenIcon from 'react-icons/lib/md/open-in-new';
import VideoCameraIcon from 'react-icons/lib/io/ios-videocam';

ItemActionButtons.propTypes = {
    post: PropTypes.object.isRequired,
    isOwner: PropTypes.bool.isRequired,
    onRead: PropTypes.func.isRequired,
    onOpenVideoPopup: PropTypes.func.isRequired,
    onToggleReportForm: PropTypes.func.isRequired,
    onOpenRemovePostWindow: PropTypes.func.isRequired,
};

export default function ItemActionButtons( { post, isOwner, onOpenVideoPopup, onRead, onToggleReportForm, onOpenRemovePostWindow } ) {
    const isYouTubeChannel = post.feed_source_type == 2;
    return (

        <div>
            {isYouTubeChannel &&
            <Button
                color="transparent"
                size="medium"
                onClick={() => onOpenVideoPopup( post )}
                icon={VideoCameraIcon}
                title="Play Video"
                iconColor="#acacac"
            />}

            <Button
                color="transparent"
                size="small"
                onClick={() => onRead( post )}
                icon={OpenIcon}
                title="Read"
                iconColor="#acacac"
            />

            <BookmarkToggle
                bookmarkId={post.bookmarkId}
                title={post.title}
                url={post.link}
                tags={post.tags}
                size={17}
            />

            <A onClick={onToggleReportForm}>
                Report
            </A>

            {isOwner &&
            <A
                style={{ marginLeft: '1.3em' }}
                onClick={() => onOpenRemovePostWindow( post )}
            >
                Remove
            </A>}
        </div>
    );
}