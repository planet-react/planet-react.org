import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withAuth0, withServices } from 'components/services';
import { Col, Row } from 'components/support/flex';
import FeedImage from 'components/feed/feedimage';
import Tags from 'components/tags';
import { ScreenLink } from 'components/screen';
import ItemActionButtons from './ItemActionButtons';
import ReportForm from 'components/report/form';

export class PostListItem extends Component {

    static propTypes = {
        index: PropTypes.number,
        post: PropTypes.shape( {
            id: PropTypes.number,
            title: PropTypes.string,
            authors: PropTypes.string,
            pubdate: PropTypes.string,
            pubdate_for_humans: PropTypes.string,
            link: PropTypes.string,
            enclosure: PropTypes.string,
            feed_id: PropTypes.number,
            feed_source_type: PropTypes.number,
            feed_name: PropTypes.string,
            feed_slug: PropTypes.string,
            feed_lang: PropTypes.string,
            feed_url: PropTypes.string,
            feed_image: PropTypes.string,
            bookmarkId: PropTypes.number,
            tags: PropTypes.array
        } ),
        onOpenRemovePostWindow: PropTypes.func.isRequired,
        onOpenVideoPopup: PropTypes.func.isRequired
    };

    state = {
        time: null,
        isReportFormVisible: false,
    };

    renderInterval = null;

    constructor( props ) {
        super( props );
    }

    componentWillMount() {

    }

    componentWillUnmount() {
        clearInterval( this.renderInterval );
    }

    onRead = ( post ) => {
        this.props.history.push( `/post/${post.id}`, { isReader: true } );
    };

    onToggleReportForm = () => {
        this.setState( ( prevState ) => {
            return { isReportFormVisible: !prevState.isReportFormVisible }
        } );
    };

    render() {
        const { isReportFormVisible } = this.state;
        const { post, auth0, services: { dateHelper }, onOpenVideoPopup, onOpenRemovePostWindow } = this.props;
        const isToday = dateHelper.isToday( post.pubdate );
        const cls = 'post-list__item ' + (isToday ? ' today' : '');

        return (
            <Row className={cls}>
                <Col className="post-list__image-col">
                    <ScreenLink path={`/feeds/${post.feed_slug}`}>
                        <FeedImage
                            size={'3.95em'}
                            src={post.feed_image}
                            alt={post.feed_name}
                        />
                    </ScreenLink>
                </Col>
                <Col className="post-list__main-col">

                    <h2 className="title">
                        <a href={post.link}>
                            {post.title}
                        </a>
                    </h2>

                    <div className="meta">
                        <div>
                            <span>By </span>
                            <ScreenLink
                                path={`/feeds/${post.feed_slug}`}
                            >
                                {post.feed_name}
                            </ScreenLink>

                            <time
                                dateTime={post.pubdate}
                                title={post.pubdate}
                            >
                                <span> {post.pubdate_for_humans}</span>
                            </time>

                        </div>
                    </div>

                    {(post.tags && post.tags.length > 0) &&
                    <div className="posts-tag-list-wrapper">
                        <Tags tags={post.tags} path={`/tags`}/>
                    </div>}

                    <ItemActionButtons
                        post={post}
                        isOwner={auth0.isAuthenticated && post.user_is_owner}
                        onRead={this.onRead}
                        onOpenVideoPopup={onOpenVideoPopup}
                        onToggleReportForm={this.onToggleReportForm}
                        onOpenRemovePostWindow={onOpenRemovePostWindow}
                    />

                    {isReportFormVisible &&
                    <div className="post-list-report-form-wrapper">
                        <ReportForm
                            contentId={post.id}
                            title={post.title}
                            url={post.link}
                            cancelCallback={this.onToggleReportForm}
                        />
                    </div>}

                </Col>

            </Row>
        );
    }
}

export default withAuth0(
    withServices(
        withRouter( PostListItem )
    )
);



