import React from "react";
import PropTypes from 'prop-types';
import { Col, Row } from 'components/support/flex';
import Popup from 'components/support/popup';
import Button from 'components/support/button';

RemovePostPopup.propTypes = {
    post: PropTypes.object,
    isRemovingPost: PropTypes.bool,
    onRemovePost: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default function RemovePostPopup( { post, isRemovingPost, onRemovePost, onClose } ) {
    return (
        <Popup
            target="body"
            isOpen={post != null}
            position={Popup.positions.SCREEN_CENTER}
            onClose={onClose}
        >
            {post &&
            <div style={{ textAlign: 'center' }}>
                <div style={{ marginBottom: '.7em' }}>
                    Remove your post: "{post.title}"?
                </div>
                <Row justifyContent="center">
                    <Col style={{ marginRight: '.7em' }}>
                        <Button
                            disabled={isRemovingPost}
                            onClick={onClose}
                            color="secondary"
                        >
                            Cancel
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            disabled={isRemovingPost}
                            onClick={onRemovePost}
                        >
                            Ok
                        </Button>
                    </Col>
                </Row>
                {isRemovingPost &&
                <div style={{ marginTop: '.7em' }}>
                    Removing post...
                </div>}
            </div>}
        </Popup>
    );
}