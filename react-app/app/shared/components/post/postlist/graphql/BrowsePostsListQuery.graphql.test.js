import { printAST } from 'react-apollo';

import PostsListQuery from './BrowsePostsListQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( PostsListQuery );
    expect( ast ).toMatchSnapshot()
} );