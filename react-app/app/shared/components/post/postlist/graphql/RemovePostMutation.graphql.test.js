import { printAST } from 'react-apollo';

import RemovePostMutation from './RemovePostMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( RemovePostMutation );
    expect( ast ).toMatchSnapshot()
} );