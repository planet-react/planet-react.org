import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, withApollo } from 'react-apollo';
import { PaginationContainer, withLoadingIndicator } from 'components/graphql';
import PostListItem from './PostListItem';
import RemovePostPopup from './RemovePostPopup';
import Message from 'components/support/message';

import BrowsePostsListQuery from './graphql/BrowsePostsListQuery.graphql';
import RemovePostMutation from './graphql/RemovePostMutation.graphql';
import VideoPopup from "../../video/popup/index";

require( './style.scss' );

class PostListWithData extends Component {

    static propTypes = {
        feedId: PropTypes.number,
        slug: PropTypes.string,
        search: PropTypes.string,
        year: PropTypes.number,
        month: PropTypes.number,
        language: PropTypes.string,
        data: PropTypes.object,
    };

    state = {
        postToRemove: null,
        isRemovingPost: null,
        isVideoPopupOpen: false,
        selectedVideo: null,
    };

    onRemovePost = () => {
        const id = this.state.postToRemove.id;
        this.setState( {
            isRemovingPost: true,
        } );

        this.props.client.mutate( {
            mutation: RemovePostMutation,
            variables: {
                id: id,
            },
            update: ( proxy, { data: { removePost } } ) => {
                let options = {
                    query: PostsListQuery,
                    variables: this.props.data.variables
                };

                const data = proxy.readQuery( options );

                options.data = {
                    posts: data.posts.filter( ( post ) => post.id != removePost.id )
                };

                proxy.writeQuery(
                    options
                );
            },
        } ).then( ( { data } ) => {
            this.setState( {
                isRemovingPost: false,
                postToRemove: null,
            } );
        } ).catch( ( error ) => {
            this.setState( {
                isRemovingPost: false,
                postToRemove: null,
            } );
        } );
    };

    onOpenVideoPopup = ( post ) => {
        let videoId = post.link.split( 'https://www.youtube.com/watch?v=' )[ 1 ];
        this.setState( {
            selectedVideo: {
                videoId: videoId,
                title: post.title
            },
            isVideoPopupOpen: true
        } );
    };

    onCloseVideoPopup = () => {
        this.setState( {
            isVideoPopupOpen: false,
            selectedVideo: null
        } );
    };

    onOpenRemovePostWindow = ( post ) => {
        this.setState( {
            postToRemove: post
        } );
    };

    onCloseRemovePostWindow = () => {
        this.setState( {
            postToRemove: null
        } );
    };

    render() {
        const { postToRemove, isRemovingPost, isVideoPopupOpen, selectedVideo } = this.state;
        const { data } = this.props;

        const posts = data.browsePosts.data;

        if ( posts.length == 0 ) {
            return (
                <Message align="center">No posts found for this month</Message>
            );
        }

        return (
            <div>
                <VideoPopup
                    isOpen={isVideoPopupOpen}
                    onCloseVideoPopup={this.onCloseVideoPopup}
                    videoId={selectedVideo && selectedVideo.videoId}
                />

                <RemovePostPopup
                    post={postToRemove}
                    onClose={this.onCloseRemovePostWindow}
                    isRemovingPost={isRemovingPost}
                    onRemovePost={this.onRemovePost}
                />

                <PaginationContainer data={data} dataKey="browsePosts">
                    <div className="post-list">
                        {posts.map( ( post, i ) => {
                            return (
                                <PostListItem
                                    key={post.id}
                                    post={post}
                                    index={i}
                                    onOpenVideoPopup={this.onOpenVideoPopup}
                                    onOpenRemovePostWindow={this.onOpenRemovePostWindow}
                                />
                            )
                        } )}
                    </div>
                </PaginationContainer>
            </div>
        );
    }
}

export default withApollo( graphql( BrowsePostsListQuery, {
    options: ( ownProps ) => {
        return ({
            //fetchPolicy: 'cache-and-network',
            fetchPolicy: 'network-only',
            variables: {
                page: 1,
                feedId: ownProps.feedId,
                slug: ownProps.slug,
                lang: ownProps.language,
                year: ownProps.year,
                month: Number( ownProps.month ),
                search: ownProps.search,
            }
        })
    },
} )( withLoadingIndicator()( PostListWithData ) ) );

