import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/header';
import DateFormatter from 'components/support/dateformatter';
import BookmarkToggle from 'components/bookmark/toggle';
import FavIcon from 'components/favicon';
import Button from 'components/support/button';
import Tags from 'components/tags';
import LinkIcon from 'react-icons/lib/go/link';
import VideoPlayer from "../../video/player/index";

Reader.propTypes = {
    post: PropTypes.object.isRequired,
};

export default function Reader( { post } ) {

    const openLink = () => {
        window.open( post.link );
    };

    let videoId = post.feed_source_type == 2 && post.link.split( 'https://www.youtube.com/watch?v=' )[ 1 ];

    return (
        <div className="reader">
            <div className="reader__content">

                <Header title={post.title}/>

                <p className="reader__content-byline">
                    <span>By {post.feed_name}, </span>
                    <DateFormatter
                        date={post.pubdate}
                        format={'MMM D, YYYY'}
                    />

                    <span className="reader__content-domain">
                        <FavIcon
                            host={post.link}
                            grayscale
                            style={{ verticalAlign: 'middle' }}
                        /> {post.host}
                    </span>
                </p>
                <p>
                    <BookmarkToggle
                        bookmarkId={post.bookmarkId}
                        title={post.title}
                        url={post.link}
                        tags={post.tags}
                    />
                    <Button
                        color="transparent"
                        size="small"
                        onClick={openLink}
                        icon={LinkIcon}
                        iconColor="#acacac"
                    />
                </p>
                <div
                    dangerouslySetInnerHTML={{ __html: post.content }}/>

                {post.feed_source_type == 2 &&
                <div className="reader__content-video">
                    <VideoPlayer
                        videoId={videoId}
                        autoPlay={false}
                    />
                </div>}


                {/*<div className="reader__content-tail"> More articles*/}
                {/*from Nader Dabit*/}
                {/*</div>*/}
            </div>

            <div className="reader__tags">
                <Tags tags={post.tags} path="/tags"/>
            </div>

        </div>
    )
}

