import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { withLoadingIndicator } from 'components/graphql';
import Reader from './Reader';

import PostQuery from './graphql/PostQuery.graphql';

require( './style.scss' );

ReaderWithData.propTypes = {
    history: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
};

function ReaderWithData( { history, data } ) {

    const post = data.post;

    const back = ( e ) => {
        e.stopPropagation();
        const { state } = history.location;
        if ( state && state.isReader ) {
            history.goBack();
            return;
        }
        history.push( '/' );
    };

    return (
        <Reader post={post} onClose={back}/>
    )
}

export default graphql( PostQuery, {
    options: ( ownProps ) => {
        return ({
            variables: {
                postId: ownProps.match.params.postId,
            }
        })
    },
} )( withLoadingIndicator()( ReaderWithData ) );