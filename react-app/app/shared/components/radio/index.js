import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'components/support/flex';
import A from 'components/support/a';
import PauseIcon from 'react-icons/lib/fa/pause-circle-o';
import PlayIcon from 'react-icons/lib/fa/play-circle-o';

require( './style.scss' );

export default class Radio extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        post: PropTypes.shape( {
            feed_name: PropTypes.string,
            title: PropTypes.string,
            link: PropTypes.string,
            enclosure: PropTypes.string
        } ).isRequired
    };

    state = {
        isPlaying: false,
        progressBarWidth: '0%',
    };

    audioElement = null;
    isActivated = false;

    constructor( props ) {
        super( props );

        this.onTogglePlay = this.onTogglePlay.bind( this )
    }

    componentDidMount() {
        const audioElement = this.getAudio();
        if ( audioElement ) {
            audioElement.addEventListener( 'timeupdate', this.onAudioTimeUpdate );
            audioElement.addEventListener( 'ended', this.onAudioEnded );
        }
    }

    componentWillUnmount() {
        const audioElement = this.getAudio();
        if ( audioElement ) {
            audioElement.removeEventListener( 'timeupdate', this.onAudioTimeUpdate );
            audioElement.removeEventListener( 'ended', this.onAudioEnded );
        }
    }

    onTogglePlay() {
        this.setState( prevState => ({
            isPlaying: !prevState.isPlaying
        }), () => {
            const audioElement = this.getAudio();
            this.state.isPlaying ? audioElement.play() : audioElement.pause();
            if ( this.state.isPlaying && !this.isActivated ) {
                this.isActivated = true;
            }
        } );
    };

    onAudioTimeUpdate = ( event ) => {
        const audio = event.target;
        const duration = audio.duration;
        if ( duration > 0 ) {
            this.updateProgressBarWidth( ((audio.currentTime / duration) * 100) )
        }
    };

    onAudioEnded = ( event ) => {
        this.updateProgressBarWidth( 0 );
    };

    getAudio() {
        return this.audioElement;
    }

    updateProgressBarWidth( width ) {
        this.setState( {
            progressBarWidth: width + '%'
        } );

        if ( width == 0 ) {
            this.setState( { isPlaying: false } );
        }
    }

    render() {
        const { post, name } = this.props;

        const { feed_name, title, link, enclosure } = post;
        const displayTitle = `${feed_name}: ${title}`;

        const { isPlaying, progressBarWidth } = this.state;

        const color = isPlaying ? '#61dafb' : '#acacac';
        return (
            <div className="radio">
                <Row alignItems="center">
                    <Col>
                        <A onClick={this.onTogglePlay}
                           className={'radio__toggle' + (isPlaying ? ' radio__toggle--is-playing' : '')}>
                            {isPlaying ? (
                                <PauseIcon size={17} color={color} style={{verticalAlign:'sub'}}/>
                            ) : (
                                <PlayIcon size={17} color={color} style={{verticalAlign:'sub'}}/>
                            )}
                        </A>
                    </Col>
                    <Col>
                        {link ? (
                            <a href={link} target="_blank"
                               title={displayTitle}>{displayTitle}</a>
                        ) : (
                            <span title={displayTitle}>{displayTitle}</span>
                        )}

                        <div className="radio__audio">
                            <audio preload="none" data-name={'radio-' + name}
                                   ref={node => this.audioElement = node}>
                                <source
                                    src={enclosure}
                                    type="audio/mp3"/>
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                    </Col>
                </Row>
                <div
                    className={this.isActivated ? 'radio__progress-bar-wrapper__activated' : 'radio__progress-bar-wrapper'}>
                    <div className="radio__progress-bar"
                         style={{
                             width: progressBarWidth,
                             backgroundColor: color
                         }}/>
                </div>
            </div>
        );
    }
}