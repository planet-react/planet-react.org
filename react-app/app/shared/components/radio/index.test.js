import React from 'react';
import Radio from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../internals/testutils/MockProviders'

const mockPost = {
    feed_name: 'Feed Name',
    title: 'Title',
    link: 'https://test.com',
    enclosure: 'https://test.com/file.mp3'
};

it( 'renders correctly', () => {
    const tree = renderer.create(
        <Radio name="radio1" post={mockPost}/>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

//@todo: why does enzyme report 2 instead of 1 (see commented expect)
it( 'toggles play/pause on click', () => {

    const wrapper = mount(
        <Radio name="radio2" post={mockPost}/>
    );

    expect( wrapper.state().isPlaying ).toBe( false );

    //expect( wrapper.find( '.radio__toggle' ).length ).toBe( 1 );

    expect( wrapper.find( '.radio__toggle--is-playing' ).length ).toBe( 0 );
    expect( wrapper.find( '.radio__progress-bar' ).prop( 'style' ).backgroundColor ).toBe( '#acacac' );

    wrapper.find( '.radio a' ).at(0).simulate( 'click' );

    expect( wrapper.state().isPlaying ).toBe( true );
    //expect( wrapper.find( '.radio__toggle' ).length ).toBe( 1 );
    //expect( wrapper.find( '.radio__toggle--is-playing' ).length ).toBe( 1 );
    expect( wrapper.find( '.radio__progress-bar' ).prop( 'style' ).backgroundColor ).toBe( '#61dafb' );
} );
