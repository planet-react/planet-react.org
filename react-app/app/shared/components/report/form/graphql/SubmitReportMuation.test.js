import { printAST } from 'react-apollo';

import SubmitReportMuation from './SubmitReportMuation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( SubmitReportMuation );
    expect( ast ).toMatchSnapshot()
} );