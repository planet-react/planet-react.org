import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withApollo } from 'react-apollo';
import { Form, Input } from 'components/support/form';
import Button from 'components/support/button';

import SubmitReportMuation from './graphql/SubmitReportMuation.graphql';

class ReportForm extends Component {

    static propTypes = {
        contentId: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        cancelCallback: PropTypes.func,
    };

    reasons = [
        { value: 'This is spam' },
        { value: 'This is personal and confidential information' },
        { value: 'This is threatening, harassing, or inciting violence' },

        // Should always be the last array element. See logic.
        { value: 'Other (max 100 characters)' }
    ];

    state = {
        otherReasonChosen: false,
        isValid: false,
        isSubmitted: false,

        content_id: null,
        title: '',
        url: '',
        reason: '',
        other_reason: ''
    };

    constructor( props ) {
        super( props );
        this.isValid = this.isValid.bind( this );
        this.isOtherReasonChosen = this.isOtherReasonChosen.bind( this );
        this.onReasonChange = this.onReasonChange.bind( this );
        this.enableSubmit = this.enableSubmit.bind( this );
        this.onOtherReasonChange = this.onOtherReasonChange.bind( this );
        this.onSubmitSuccess = this.onSubmitSuccess.bind( this );
    }

    componentDidMount() {
        this.setState( {
            content_id: this.props.contentId,
            title: this.props.title,
            url: this.props.url,
        } );
    }

    isValid() {
        const { reason, other_reason } = this.state;
        if ( this.isOtherReasonChosen( reason ) ) {
            return other_reason != '';
        }
        else {
            return reason != '';
        }
    }

    enableSubmit( enable ) {
        this.setState( {
            isValid: enable,
            isSubmitted: false
        } );
    }

    isOtherReasonChosen( value ) {
        //@todo: may robustify this
        return value == this.reasons[ this.reasons.length - 1 ].value
    }

    renderReasonsRadioButtons() {
        const { content_id } = this.state;
        return this.reasons.map( ( reason, i ) => {
            return (
                <Input
                    key={i}
                    id={'reason-' + content_id + '-' + i}
                    type="radio"
                    name="reason"
                    label={reason.value}
                    value={reason.value}
                    checked={this.state.reason == reason.value}
                    onChange={this.onReasonChange}
                />
            )
        } )
    }

    onSubmitSuccess() {
        return new Promise( ( resolve ) => {
            this.setState( {
                isSubmitted: true
            } );
            resolve();
        } );
    }

    onReasonChange( event ) {
        const { value } = event.target;
        const isOtherReason = this.isOtherReasonChosen( value );
        this.setState( {
            otherReasonChosen: isOtherReason,
            reason: value
        }, () => {
            if ( !isOtherReason ) {
                this.setState( {
                    other_reason: ''
                } );
            }
            this.enableSubmit( this.isValid() );
        } );
    }

    onOtherReasonChange( event ) {
        const { value } = event.target;
        this.setState( {
            other_reason: value
        }, () => {
            this.enableSubmit( this.isValid() );
        } );
    }

    render() {

        const { otherReasonChosen, other_reason, isValid, isSubmitted } = this.state;

        if ( isSubmitted ) {
            return (
                <p style={{ marginTop: '0' }}>
                    The post has been reported.
                </p>
            );
        }

        return (
            <Form
                mutation={SubmitReportMuation}
                data={this.state}
                onSubmitSuccess={this.onSubmitSuccess}
            >
                <div
                    style={{ marginBottom: '.7em' }}>We're sorry something's wrong. How can we help?
                </div>
                <div>
                    {this.renderReasonsRadioButtons()}
                    <Input
                        type="text"
                        name="other_reason"
                        value={other_reason}
                        maxLength={100}
                        disabled={!otherReasonChosen}
                        onChange={this.onOtherReasonChange}
                    />
                </div>
                <p>
                    <b>The report is sent anonymously</b>
                </p>
                <p>
                    <Button
                        type="button"
                        color="secondary"
                        onClick={this.props.cancelCallback}
                        style={{ marginRight: '.35em' }}
                    >
                        Cancel
                    </Button>
                    <Button
                        type="submit"
                        disabled={!isValid}
                    >
                        Submit
                    </Button>
                </p>
            </Form>
        );
    }
}

export default withApollo( ReportForm );