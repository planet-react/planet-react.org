import React, { Component } from 'react';
import Message from 'components/support/message';

export default class ErrorBoundary extends Component {
    constructor( props ) {
        super( props );
        this.state = { error: null, info: null };
    }

    componentDidCatch( error, info ) {
        this.setState( { error, info } );
    }

    render() {

        if ( this.state.error )
            console.log( this.state.error );

        return this.state.error
            ? (
                <Message context="error">
                    <h1 key="header">
                        Something went wrong.
                    </h1>
                    {/*<pre key="error" style={{whiteSpace:'pre-wrap'}}>*/}
                    {/*{this.state.error.stack}*/}
                    {/*</pre>*/}
                    <pre key="componentStack"
                         style={{ whiteSpace: 'pre-wrap' }}>
                        {this.state.info.componentStack}
                    </pre>
                </Message>
            )
            : this.props.children;
    }
}
