import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/header'

ScreenError.propTypes = {
    statusCode: PropTypes.oneOf( [ 401, 403, 404 ] ).isRequired,
};

export default function ScreenError( { statusCode } ) {

    let statusText;
    switch ( statusCode ) {
        case 401:
            statusText = 'Unauthorized';
            break;
        case 403:
            statusText = 'Forbidden';
            break;
        case 404:
            statusText = 'Not Found';
            break;
        default:
            statusText = 'Not Found';
    }

    return (
        <Header title={`${statusCode} - ${statusText}`}/>
    )
}