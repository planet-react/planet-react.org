import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { gql } from 'react-apollo';
import { withServices } from 'components/services'
import { NavLink, withRouter } from 'react-router-dom';
import { loadScreen } from '../screenHelper';

class ScreenLink extends Component {

    static propTypes = {
        path: PropTypes.string.isRequired,
        exact: PropTypes.bool,
        className: PropTypes.string,
        isActive: PropTypes.func,
        onClick: PropTypes.func,
        onBlockedClick: PropTypes.func,
        children: PropTypes.node,
        role: PropTypes.string,
        ariaLabel: PropTypes.string,
        target: PropTypes.string,
    };

    routePath = null;
    match = null;

    constructor( props ) {
        super( props );
        this.routePath = props.path;
        this.match = props.match;
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.path != this.props.path ) {
            this.routePath = nextProps.path;
            this.match = nextProps.match;
        }
    }

    async loadBeforeNavigate( blockPush ) {
        const { history, services: { apolloClient } } = this.props;

        apolloClient.writeQuery( {
            data: { isScreenLoading: true },
            query: gql`{ isScreenLoading }`
        } );

        // Remove query string
        // let routeMatchPathWithoutQueryString = this.routePath.split( '?' )[ 0 ].replace( /\/$/, '' );

        await loadScreen( this.match );

        if ( blockPush ) {
            return;
        }

        //@todo: move before blockPush check?
        window.scrollTo( 0, 0 );

        apolloClient.writeQuery( {
            data: { isScreenLoading: false },
            query: gql`{ isScreenLoading }`
        } );

        return history.push( this.routePath );
    }

    render() {
        let routePath = this.routePath;
        let {
            onClick, className, onBlockedClick, children, exact, isActive,
            role, ariaLabel, target
        } = this.props;

        return (
            <NavLink
                exact={exact}
                to={routePath}
                isActive={isActive}
                role={role}
                aria-label={ariaLabel}
                className={className}
                onClick={event => {
                    event.preventDefault();

                    if ( target && target == '_blank' ) {
                        window.open( routePath );
                        return;
                    }

                    if ( window.swUpdate ) {
                        window.location = routePath;
                        return;
                    }

                    if ( onBlockedClick ) {
                        return onBlockedClick( event )
                    }

                    this.loadBeforeNavigate();

                    if ( onClick ) {
                        onClick( event );
                    }
                }}>
                {children}
            </NavLink>
        )
    }
}

export default withRouter( withServices( ScreenLink ) );
