import React from 'react';
import ScreenLink from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

it( 'should render', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink path="/somewhere"/>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should support the role attribute', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink role="button" path="/somewhere">
                    Link
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should support the role target', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink target="_blank" path="/somewhere">
                    Link
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should support the aria-label attribute', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink ariaLabel="Read more about it"
                            path="/somewhere">
                    Link
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should support the class attribute', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink className="menu-item" path="/somewhere">
                    Link
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should set "active" as class name when isActive prop is used', () => {
    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink isActive={e => 2 == 2} path="/somewhere">
                    Link 1
                </ScreenLink>
                <ScreenLink isActive={e => 2 == 3} path="/elsewhere">
                    Link 2
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should have class "active" and given classNames when isActive prop is used', () => {
    const tree = renderer.create(
        <MockProviders>
            <div>
                <ScreenLink isActive={e => 3 == 3}
                            className="main-nav menu-item"
                            path="/elsewhere">
                    Link 2
                </ScreenLink>
                <ScreenLink isActive={e => 3 == 4}
                            className="main-nav menu-item"
                            path="/elsewhere">
                    Link 2
                </ScreenLink>
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );
