import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { gql } from 'react-apollo';
import { withAuth0, withServices } from 'components/services';
import cache from '../screenCache';
import { loadScreen } from '../screenHelper';
import ScreenError from '../error';
import Button from 'components/support/button';
import LoadingBar from '../loadingbar';

class Loader extends Component {

    static propTypes = {
        path: PropTypes.string.isRequired,
        allowedRoles: PropTypes.array,
    };

    constructor( props ) {
        super( props );

        if ( cache.hasComponent( props.path ) ) {
            this.state = { ...window.components[ props.path ] }
        }
        else {
            this.state = {};
        }
    }

    componentDidMount() {
        const { auth0, allowedRoles, match } = this.props;

        if ( allowedRoles && !auth0.isAuthenticated ) {
            auth0.showLogin();
        }

        if ( !this.state.component ) {
            this.load( this.props );
        }
    }

    componentWillReceiveProps( nextProps ) {
        const { auth0, allowedRoles, match } = nextProps;
        const isNewRoute = this.props.match.url != match.url;
        if ( isNewRoute && allowedRoles && !auth0.isAuthenticated ) {
            auth0.showLogin();
        }

        if ( this.props.path !== nextProps.path ) {
            this.load( nextProps );
        }
    }

    componentWillUnmount() {
        this.props.services.apolloClient.writeQuery( {
            data: { isScreenLoading: false },
            query: gql`{ isScreenLoading }`
        } );
    }

    async load( { match } ) {
        const { apolloClient } = this.props.services;

        apolloClient.writeQuery( {
            data: { isScreenLoading: true },
            query: gql`{ isScreenLoading }`
        } );

        let { component } = await loadScreen( match );

        apolloClient.writeQuery( {
            data: { isScreenLoading: false },
            query: gql`{ isScreenLoading }`
        } );

        if ( !component ) {
            return;
        }

        this.setState( {
            component: component.default
        } );

        this.log();
    }

    /**
     * Log route request to terminal. See internals/cmd-dev-start.js
     */
    log() {
        if ( process.env.NODE_ENV == 'development' ) {
            fetch( `/?planet-route=${encodeURIComponent( window.location.href )}` );
        }
    }

    onLogin = () => {
        const { auth0 } = this.props;
        auth0.showLogin();
    };

    render() {
        const { auth0, allowedRoles } = this.props;
        const { isAuthenticated } = auth0;
        const requireAuthentication = allowedRoles && !isAuthenticated;
        const userIsNotAuthorized = isAuthenticated && allowedRoles
            && !auth0.userHasRole( allowedRoles );

        return (
            <div>
                <LoadingBar/>

                {requireAuthentication ? (
                    <div>
                        <ScreenError statusCode={403}/>
                        <p>
                            <Button onClick={this.onLogin}>Log in</Button>
                        </p>
                    </div>
                ) : (
                    <div>
                        {this.state.component ? (
                            <div>
                                {(userIsNotAuthorized) ? (
                                    <ScreenError statusCode={401}/>

                                ) : (
                                    <this.state.component {...this.props} />
                                )}
                            </div>
                        ) : (
                            null
                        )}
                    </div>
                )}
            </div>
        )
    }
}

export default withAuth0(
    withServices( Loader )
);