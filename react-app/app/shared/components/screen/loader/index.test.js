import React from 'react';
import Loader from '../loader';
import { Route } from 'react-router-dom';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

//@todo: improve tests

it( 'should render', () => {

    const tree = renderer.create(
        <MockProviders>
            <div>
                <Route path="/"
                       render={( props ) =>
                           <Loader path="/Y" {...props}/>}
                />

                <Route path="/x"
                       render={( props ) =>
                           <Loader path="/X" {...props}/>}
                />
            </div>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );
