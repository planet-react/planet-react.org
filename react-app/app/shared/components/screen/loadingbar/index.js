import React from 'react';
import { graphql, gql } from 'react-apollo';

require( './style.scss' );

function LoadingBar( { data } ) {
    let isLoading = data.isScreenLoading;
    return (
        <div
            className={'loading-bar' + (isLoading ? ' loading-bar--loading' : '') }>
            <div/>
        </div>
    );
}

export default graphql(  gql`{ isScreenLoading }` )( LoadingBar );
