import React from 'react';
import LoadingBar from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <LoadingBar/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <MockProviders>
            <LoadingBar/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );