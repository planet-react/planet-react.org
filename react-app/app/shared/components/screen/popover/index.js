import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/support/button';
import { ScreenLink } from 'components/screen';
import { Col, Filler, Row } from 'components/support/flex';
import CloseIcon from 'react-icons/lib/fa/close';

require( './style.scss' );

export default class ScreenPopover extends Component {

    static propTypes = {
        children: PropTypes.node,
        size: PropTypes.oneOf( [ 'full', 'large', 'medium', 'small' ] )
    };

    static defaultProps = {
        size: 'full'
    };

    componentDidMount() {
        document.addEventListener( 'keydown', this.onKeyDown );
    }

    componentWillUnmount() {
        document.removeEventListener( 'keydown', this.onKeyDown );
    }

    onKeyDown = ( event ) => {
        if ( event.keyCode == 27 ) {
            this.onClose( event );
        }
    };

    onClose = ( event ) => {
        event.stopPropagation();
        const { history } = this.props;
        const { state } = history.location;
        if ( state && state.isReader ) {
            history.goBack();
            return;
        }
        history.push( '/' );
    };

    render() {
        const { children, size } = this.props;

        let positions = [];
        switch ( size ) {
            case 'full':
                positions = [ 0, 0, 0, 0 ];
                break;
            case 'large':
                positions = [ 10, 10, 10, 10 ];
                break;
            case 'medium':
                positions = [ 20, 20, 20, 20 ];
                break;
            case 'small':
                positions = [ 40, 40, 40, 40 ];
                break;
            default:
                positions = [ 0, 0, 0, 0 ];
        }

        const style = {
            top: positions[ 0 ] + '%',
            right: positions[ 1 ] + '%',
            bottom: positions[ 2 ] + '%',
            left: positions[ 3 ] + '%',
        };

        return (
            <div className="screen-popover" style={style}>
                <Row className="screen-popover__toolbar">
                    <Col style={{ padding: '.37em 0 0 .7em' }}>
                        <ScreenLink path="/">
                            {__APP_NAME__}
                        </ScreenLink>
                    </Col>
                    <Filler/>
                    <Col>
                        <Button
                            color="transparent"
                            size="medium"
                            onClick={this.onClose}
                            icon={CloseIcon}
                            iconColor="#acacac"
                        >
                        </Button>
                    </Col>
                </Row>
                <div className="screen-popover__content">
                    {children}
                </div>
            </div>
        );
    }

}