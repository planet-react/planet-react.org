import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import Loader from '../loader';

/**
 * Wraps react-router's Route component and adds support for dynamic loading
 *
 * @todo: make stateless component?
 */
export default class Screen extends Component {

    static propTypes = {
        exact: PropTypes.bool,
        path: PropTypes.string,
        component: PropTypes.func,
        render: PropTypes.func,
        allowedRoles: PropTypes.array,
    };

    static defaultProps = {};

    state = {};

    constructor( props ) {
        super( props );
    }

    render() {
        const { exact, path, component, render, allowedRoles } = this.props;
        const Component = component;

        return (
            <Route exact={exact} path={path}
                   render={( props ) => {
                       return (
                           <div>
                               {Component ? (
                                   <Component {...props}/>
                               ) : (
                                   <div>
                                       {render ? (
                                           <div>{render( props )}</div>
                                       ) : (
                                           <Loader
                                               path={path}
                                               allowedRoles={allowedRoles}
                                               {...props}
                                           />
                                       )}
                                   </div>
                               )}
                           </div>

                       )
                   }}/>
        );
    }
}