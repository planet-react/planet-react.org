/**
 * Cache for loaded components.
 * @type {{}}
 */
window.components = {};

/**
 * @param {string} key
 * @param defaultComponent
 */
function setComponent( key, defaultComponent ) {
    window.components[ key ] = { component: defaultComponent };
}

/**
 * @param {string} key
 * @returns {*}
 */
function getComponent( key ) {
    return window.components[ key ];
}

/**
 * @param {string} key
 * @returns {boolean}
 */
function hasComponent( key ) {
    return getComponent( key ) != undefined;
}

/**
 * @returns {{}}
 */
function getAll( ) {
    return window.components;
}
function clear( ) {
    return window.components = {};
}

export default {
    setComponent,
    getComponent,
    hasComponent,
    getAll,
    clear,
}