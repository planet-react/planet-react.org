import cache from './screenCache';

describe( 'screenCache', () => {

    beforeEach( () => {
        cache.clear();
    } );

    describe( '.setComponent()', () => {
        it( 'should store components in the cache', () => {
            cache.setComponent( '/a/b/c', {} );
            expect( Object.keys( cache.getAll() ).length ).toBe( 1 );
            expect( cache.getComponent( '/a/b/c' ) ).toEqual( { component: {} } );

            cache.setComponent( '/d/e/f', {} );
            cache.setComponent( '/g/h/i', {} );
            expect( Object.keys( cache.getAll() ).length ).toBe( 3 );
            expect( cache.getComponent( '/d/e/f' ) ).toEqual( { component: {} } )

        } );


        it( 'should only store component with given key once', () => {

            cache.setComponent( '/a/b/c', {} );
            cache.setComponent( '/a/b/c', {} );
            cache.setComponent( '/another/key', {} );

            expect( Object.keys( cache.getAll() ).length ).toBe( 2 );
        } );
    });

    describe( '.hasComponent()', () => {
        it( 'should return true when cache has component', () => {

            cache.setComponent( '/a/b/c', {} );

            expect( cache.hasComponent( '/a/b/c' ) ).toBe( true );
            expect( cache.hasComponent( '/j/k/l' ) ).toBe(false);
        } );
    });

} );