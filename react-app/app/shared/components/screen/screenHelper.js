import cache from './screenCache';

/**
 * Translates the React Router's match.path to it's corresponding screen path.
 *
 * @param {Object} match
 * @param {bool} match.isExact
 * @param {Object} match.params
 * @param {string} match.path
 * @param {string} match.url
 * @returns {string}
 */
export function translateRoutePathToScreenPath( match ) {

    const { path } = match;

    if ( isIndexRoute( path ) ) {
        return '';
    }

    let screenPath = path
    // Remove start slash.
        .replace( /^\//, '' )
        // Remove router params.
        .replace( /:.+?($|\/)/g, '' )
        // Remove trailing slash.
        .replace( /\/$/, '' );

    // Uppercase each part.
    screenPath = screenPath.split( '/' )
        .map( ( segment, index ) =>
            (index > 0 ? 'screens/' : '') + kebabToCamel( segment ) )
        .join( '/' );

    return screenPath;
}

/**
 * Imports and caches the screen component.
 *
 * @param {Object} match
 * @param {bool} match.isExact
 * @param {Object} match.params
 * @param {string} match.path
 * @param {string} match.url
 * @returns {Promise.<*>}
 */
export async function loadScreen( match ) {

    const screenPath = translateRoutePathToScreenPath( match );

    try {
        // if (cache.hasComponent(path)) {
        //     console.log('has')
        //     let component = cache.getComponent(path);
        //     return component;
        // }
        // console.log('has not')

        const [ component ] = await Promise.all( [
            importScreen( screenPath ),
        ] );

        // Cache
        if ( component ) {
            cache.setComponent( screenPath, component.default );
        }
        return { component };
    } catch ( e ) {
        console.warn( `Screen: ${screenPath} does not exist.` );
        return {}
    }
}

/**
 * Loads the screen component.
 *
 * @param {string} screenPath
 * @returns {Promise.<*>}
 */
async function importScreen( screenPath ) {
    if ( isIndexRoute( screenPath ) ) {
        return await import( `../../../screens/index.js` )
    }
    return await import( `../../../screens/${screenPath}/index.js` )
}

/**
 * Returns true if the route path is the index route (/)
 *
 * @param {string} path
 * @returns {boolean}
 */
function isIndexRoute( path ) {
    return path == '' || path == '/'
}

/**
 * CamelCases the given string.
 *
 * @param {string} str
 * @returns {*}
 */
function kebabToCamel( str ) {
    if ( str.indexOf( '-' ) == -1 ) {
        return upperCaseFirst( str );
    }
    return str
        .split( '-' ).map( ( c ) => upperCaseFirst( c ) )
        .join( '' );
}

/**
 * Uppercase the first letter in the string
 *
 * @param {string} str
 * @returns {string}
 */
function upperCaseFirst( str ) {
    return str[ 0 ].toUpperCase() + str.substring( 1 );
}
