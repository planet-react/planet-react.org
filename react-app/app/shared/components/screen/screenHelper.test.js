import { translateRoutePathToScreenPath } from './screenHelper';

describe( 'screenHelper', () => {
    describe( '.translateRoutePathToScreenPath()', () => {

        let match = { isExact: true, params: {}, path: '/', url: '/' };

        it( 'should translate a route path to a screen path', () => {
            expect( translateRoutePathToScreenPath( match ) ).toEqual( '' );

            match.path = '/a/b/c';
            expect( translateRoutePathToScreenPath( match ) ).toEqual( 'A/screens/B/screens/C' );

            match.path = '/a/b/c/:contentKey';
            expect( translateRoutePathToScreenPath( match ) ).toEqual( 'A/screens/B/screens/C' );

            match.path = '/a/b/path-with-many-words';
            expect( translateRoutePathToScreenPath( match ) ).toEqual( 'A/screens/B/screens/PathWithManyWords' );

            match.path = '/a/b/path-with-many-words/c/d';

            console.time( 'translateRoutePathToScreenPath' );
            let result = translateRoutePathToScreenPath( match );
            console.timeEnd( 'translateRoutePathToScreenPath' );
            expect( result ).toEqual( 'A/screens/B/screens/PathWithManyWords/screens/C/screens/D' );
        } );

    } );

} );