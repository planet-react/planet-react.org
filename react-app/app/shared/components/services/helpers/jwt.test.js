import jwt from './jwt';

describe( 'jwtHelper', () => {
    it( 'should return true when token has expired', () => {
        const dummyToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE0ODg2MjA3NDgsImV4cCI6MTMzMDg1NDM0OCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSJ9.eNkqKC3XJ6fwSbPnn1VqK1FtkDXW3cnK_LRwHra7ie0';
        expect( jwt.isTokenExpired( dummyToken ) ).toBe( true );
    } );
} );
