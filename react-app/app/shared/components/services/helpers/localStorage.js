function getRedirectPath() {
    return window.localStorage.getItem( 'redirect_path' );
}

function setRedirectPath( path ) {
    return window.localStorage.setItem( 'redirect_path', path );
}

function deleteRedirectPath() {
    return window.localStorage.removeItem( 'redirect_path' );
}

function getAccessToken() {
    return window.localStorage.getItem( 'access_token' );
}

function setAccessToken( accessToken ) {
    return window.localStorage.setItem( 'access_token', accessToken );
}

function deleteAccessToken() {
    return window.localStorage.removeItem( 'access_token' );
}

function getIdToken() {
    return window.localStorage.getItem( 'id_token' );
}

function setIdToken( idToken ) {
    window.localStorage.setItem( 'id_token', idToken );
}

function deleteIdToken() {
    return window.localStorage.removeItem( 'id_token' );
}

export default {
    getRedirectPath,
    setRedirectPath,
    deleteRedirectPath,
    getAccessToken,
    setAccessToken,
    deleteAccessToken,
    getIdToken,
    setIdToken,
    deleteIdToken
}
