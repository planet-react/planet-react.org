const ADMIN = 'admin';
const AUTHORIZED_USER = 'authorized_user';

export default {
    ADMIN,
    AUTHORIZED_USER
}