import roles from './roles';

describe( 'roles', () => {

    it( 'should have AUTHORIZED_USER', () => {
        expect( roles.AUTHORIZED_USER ).toBe( 'authorized_user' );
    } );

    it( 'should have ADMIN', () => {
        expect( roles.ADMIN ).toBe( 'admin' );
    } );

} );
