import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default function withAuth0( WrappedComponent ) {

    class WithAuth0 extends Component {

        static contextTypes = {
            auth0: PropTypes.object
        };

        render() {
            return <WrappedComponent
                {...this.props}
                auth0={this.context.auth0}/>
        }
    }

    return WithAuth0;
};