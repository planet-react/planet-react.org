import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default function withServices( WrappedComponent ) {

    class WithServices extends Component {

        static contextTypes = {
            services: PropTypes.object
        };

        render() {
            return <WrappedComponent
                {...this.props}
                services={this.context.services}/>
        }
    }

    return WithServices;
};