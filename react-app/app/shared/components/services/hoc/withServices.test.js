import React, { Component } from 'react';
import { ServicesProvider, withServices } from '../';

const servicesMock = {

    mock: {
        mockFunc() {
            return 'Hello, World!'
        }
    },
    dateHelper: {
        isSameDay: jest.fn(),
        format: jest.fn(),
    },

};


class MockComponent extends Component {
    constructor( props ) {
        super( props );
    }

    render() {
        const { services: { mock } } = this.props;
        const str = mock.mockFunc();
        return (
            <div>{str}</div>
        );
    }
}

const MockComponentWithServices = withServices( MockComponent );

it( 'it has the expected markup', () => {
    const wrapper = mount(
        <ServicesProvider services={servicesMock}>
            <div className="content">
                <MockComponentWithServices/>
            </div>
        </ServicesProvider>
    );

    expect( wrapper.find( '.content' ).children().length ).toBe( 1 );
} );
