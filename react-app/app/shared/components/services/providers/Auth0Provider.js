import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose, gql, graphql, withApollo } from 'react-apollo';
import jwt from '../helpers/jwt'
import roles from '../helpers/roles';
import storage from '../helpers/localStorage';

class Auth0Provider extends Component {

    static propTypes = {
        auth0service: PropTypes.object.isRequired
    };

    static childContextTypes = {
        auth0: PropTypes.object
    };

    lock = null;

    isAuthenticating = false;

    getChildContext() {
        const t = this;
        return {
            auth0: {
                isAuthenticated: t.props.data.isAuthenticated,
                userProfile: t.props.data.userProfile,
                roles: roles,
                showLogin() {
                    t.onShowLogin()
                },
                logout() {
                    t.logout()
                },
                userHasRole( roles ) {
                    return t.userHasRole( roles )
                },
                getRedirectPath() {
                    return storage.getRedirectPath()
                },
            }
        }
    }

    constructor( props, context ) {
        super( props, context );

        this.lock = props.auth0service.lock;
        this.lock.on( 'hide', () => {
            storage.deleteRedirectPath();
        } );
        this.lock.on( 'authenticated', ( result ) => {
            this.doAuthentication( result.accessToken, result.idToken )
        } );

        this.resume();
    }

    resume() {
        return new Promise( ( resolve ) => {
            if ( this.isIdTokenValid() ) {
                const accessToken = storage.getAccessToken();
                const idToken = storage.getIdToken();
                this.doAuthentication( accessToken, idToken ).then( () => {
                    resolve( true );
                } ).catch( () => {
                    //reject( false )
                    resolve( false );
                } );
            }
            else {
                resolve( false );
            }
        } );
    }

    onShowLogin() {
        storage.setRedirectPath( location.pathname );
        if ( !this.isAuthenticating ) {
            this.lock.show();
        }
    };

    logout() {
        this.setUserProfile( null );
        this.setIsAuthenticated( false );
        storage.deleteAccessToken();
        storage.deleteIdToken();
    };

    doAuthentication( accessToken, idToken ) {
        this.isAuthenticating = true;
        return new Promise( ( resolve, reject ) => {
            storage.setAccessToken( accessToken );
            storage.setIdToken( idToken );

            this.props.auth0service.fetchUserProfile( accessToken ).then( ( auth0UserData ) => {

                this.setUserProfile( auth0UserData );
                this.setIsAuthenticated( true );

                this.isAuthenticating = false;

                resolve( true );
            } ).catch( ( error ) => {
                this.logout();
                reject( false );
            } );
        } );
    }

    userHasRole( rolesToCheck ) {
        const user = this.getUserProfile();
        if ( !user || !user.roles ) {
            return false;
        }

        const roles = user.roles;

        if ( typeof rolesToCheck == 'string' ) {
            return roles.indexOf( rolesToCheck ) > -1;
        }

        if ( Array.isArray( rolesToCheck ) ) {
            for ( let i = 0; i < roles.length; i++ ) {
                if ( rolesToCheck.indexOf( roles[ i ] ) > -1 ) {
                    return true;
                }
            }
        }

        return false;
    }

    isIdTokenValid() {
        const idToken = storage.getIdToken();
        return !!idToken && !jwt.isTokenExpired( idToken );
    }

    setIsAuthenticated( value ) {
        this.props.client.writeQuery(
            {
                query: gql`{ isAuthenticated }`,
                data: { isAuthenticated: value }
            }
        );
    };

    getIsAuthenticated() {
        return this.props.client.readQuery( { query: gql`{isAuthenticated}` } ).isAuthenticated;
    };

    setUserProfile( userProfile ) {
        this.props.client.writeQuery(
            {
                query: gql`{ userProfile }`,
                data: { userProfile: userProfile }
            }
        );
    };

    getUserProfile() {
        return this.props.client.readQuery( { query: gql`{userProfile}` } ).userProfile;
    };

    render() {
        return this.props.children
    }

}

export default compose(
    withApollo,
    graphql( gql`{isAuthenticated, userProfile}` )
)( Auth0Provider );