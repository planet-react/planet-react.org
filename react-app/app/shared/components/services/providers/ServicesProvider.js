import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ServicesProvider extends Component {

    static propTypes = {
        services: PropTypes.object.isRequired
    };

    static childContextTypes = {
        services: PropTypes.object
    };

    getChildContext() {
        return {
            services: this.props.services
        }
    }

    render() {
        return this.props.children
    }

}