import React from 'react';
import { ServicesProvider } from '../';

it( 'it has the expected markup', () => {
    const wrapper = mount(
        <ServicesProvider services={{}}>
            <div>
                <div>Child 1</div>
                <div>Child 2</div>
                <div>Child 3</div>
            </div>
        </ServicesProvider>
    );

    expect( wrapper.find( 'div > div' ).length ).toBe( 3 );
} );
