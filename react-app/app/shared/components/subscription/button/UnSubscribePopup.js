import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';
import Button from 'components/support/button';
import { Row, Col } from 'components/support/flex';

UnSubscribePopup.propTypes = {
    target: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onUnSubscribe: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

require( './style.scss' );

export default function UnSubscribePopup( { target, isOpen, onClose, onUnSubscribe, children } ) {
    return (
        <Popup
            target={target}
            position={Popup.positions.SCREEN_CENTER}
            isOpen={isOpen}
            onClose={onClose}
        >
            <div className="unsubscribe-popup__body">
                <div>
                    {children ? (
                        <span>{ children }</span>
                    ) : (
                        'Unsubscribe?'
                    )}
                </div>
                <Row
                    className="unsubscribe-popup__actions"
                    justifyContent={'space-between'}
                >
                    <Col style={{ marginRight: '.7em' }}>
                        <Button
                            onClick={onClose}
                            color="secondary"
                        >
                            Cancel
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            onClick={onUnSubscribe}
                        >
                            Unsubscribe
                        </Button>
                    </Col>
                </Row>
            </div>
        </Popup>
    );
}