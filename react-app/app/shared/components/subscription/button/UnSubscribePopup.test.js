import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import UnSubscribePopup from './UnSubscribePopup'

const onUnSubscribe = jest.fn();
const onClose = jest.fn();

it( 'renders correctly', () => {

    let tree = renderer.create(
        <UnSubscribePopup
            onUnSubscribe={onUnSubscribe}
            onClose={onClose}
            isOpen={false}
            target="popup-target-elem"
        >
            Content
        </UnSubscribePopup>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );