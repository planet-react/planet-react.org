import { printAST } from 'react-apollo';

import SubscriptionInfoQuery from './SubscriptionInfoQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( SubscriptionInfoQuery );
    expect( ast ).toMatchSnapshot()
} );