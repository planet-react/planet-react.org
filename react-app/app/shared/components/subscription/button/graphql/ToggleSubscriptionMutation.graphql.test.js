import { printAST } from 'react-apollo';

import ToggleSubscriptionMutation from './ToggleSubscriptionMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( ToggleSubscriptionMutation );
    expect( ast ).toMatchSnapshot()
} );