import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withAuth0 } from 'components/services';
import { withApollo } from 'react-apollo';
import Button from 'components/support/button';
import UnSubscribePopup from './UnSubscribePopup';
import MailIcon from 'react-icons/lib/go/mail';
import CheckMarkIcon from 'react-icons/lib/io/checkmark';
import CloseIcon from 'react-icons/lib/fa/close';

import ToggleSubscriptionMutation from './graphql/ToggleSubscriptionMutation.graphql';
import SubscriptionInfoQuery from './graphql/SubscriptionInfoQuery.graphql';

class SubscribeButton extends Component {

    static propTypes = {
        type: PropTypes.oneOf( [ 'feed', 'tag', 'daily', 'weekly' ] ),
        value: PropTypes.oneOfType( [
            PropTypes.string,
            PropTypes.number,
        ] ),
        title: PropTypes.string,
    };

    static componentIds = 0;

    componentName = null;

    constructor( props ) {
        super( props );

        // @todo: function for getting button label and button icon
        this.state = {
            subscriptionId: null,
            isSubmitting: false,
            buttonLabel: 'Subscribe',
            buttonIcon: MailIcon,
            isPopupOpen: false,
        };

        this.componentName = 'subscription-button-' + (SubscribeButton.componentIds++);

        this.onClick = this.onClick.bind( this );
        this.onMouseOver = this.onMouseOver.bind( this );
        this.onMouseOut = this.onMouseOut.bind( this );
        this.onCloseUnSubscribePopup = this.onCloseUnSubscribePopup.bind( this );
        this.onUnSubscribe = this.onUnSubscribe.bind( this );
        this.isSubscribed = this.isSubscribed.bind( this );
        this.createToolTipText = this.createToolTipText.bind( this );
    }

    componentDidMount() {
        const { type, value, auth0: { isAuthenticated } } = this.props;
        if ( !isAuthenticated ) {
            return;
        }
        this.fetchSubsciptionInfo( type, value );
    }

    componentWillReceiveProps( nextProps ) {
        const { type, value } = this.props;
        if ( nextProps.type == type && nextProps.value == value ) {
            return;
        }
        this.fetchSubsciptionInfo( nextProps.type, nextProps.value );
    }

    fetchSubsciptionInfo( type, value ) {
        if ( !(type && value) ) {
            return;
        }
        const { subscriptionId } = this.state;
        const { client } = this.props;
        this.setState( {
            isSubmitting: true
        }, () => {
            client.query( {
                query: SubscriptionInfoQuery,
                fetchPolicy: 'network-only',
                variables: {
                    id: subscriptionId,
                    type: type,
                    value: value,
                }
            } ).then( ( result ) => {
                const id = result.data.subscriptionInfo ? result.data.subscriptionInfo.id : null;
                this.setState( {
                    subscriptionId: id,
                    isSubmitting: false,
                    buttonLabel: id ? 'Subscribed' : 'Subscribe',
                    buttonIcon: id ? CheckMarkIcon : MailIcon,
                } );
            } ).catch( ( error ) => {

            } );
        } );
    }

    isSubscribed() {
        return this.state.subscriptionId != null;
    }

    onMouseOver() {
        this.setState( {
            buttonLabel: this.isSubscribed() ? 'Unsubscribe' : this.state.buttonLabel,
            buttonIcon: this.isSubscribed() ? CloseIcon : this.state.buttonIcon,
        } );
    }

    onMouseOut() {
        this.setState( {
            buttonLabel: this.isSubscribed() ? 'Subscribed' : 'Subscribe',
            buttonIcon: this.isSubscribed() ? CheckMarkIcon : MailIcon,
        } );
    }

    onClick() {
        const { auth0: { isAuthenticated, showLogin } } = this.props;

        if ( !isAuthenticated ) {
            showLogin()
        }

        if ( this.isSubscribed() ) {
            this.setState( {
                isPopupOpen: true
            } );
            return;
        }
        this.onSubmit();
    }

    onSubmit() {
        const { subscriptionId } = this.state;
        const { client, type, value } = this.props;

        this.setState( {
            isSubmitting: true
        }, () => {
            client.mutate( {
                mutation: ToggleSubscriptionMutation,
                variables: {
                    id: subscriptionId,
                    type: type,
                    value: value,
                }
            } ).then( ( result ) => {
                const id = result.data.toggleSubscription ? result.data.toggleSubscription.id : null
                this.setState( {
                    subscriptionId: id,
                    isSubmitting: false,
                    buttonLabel: id ? 'Subscribed' : 'Subscribe',
                    buttonIcon: id ? CheckMarkIcon : MailIcon,
                } );
            } ).catch( ( error ) => {

            } );
        } );
    }

    createToolTipText() {
        const { type, title } = this.props;
        if ( type == 'feed' ) {
            return this.isSubscribed()
                ? `Unsubscribe from ${title}`
                : `Receive an email when ${title} has new posts`;
        }
        else {
            return this.isSubscribed()
                ? `Unsubscribe from ${title}`
                : `Receive an email when posts tagged with ${title} has been aggregated`;
        }
    }

    onUnSubscribe() {
        this.onSubmit();
        this.onCloseUnSubscribePopup();
    }

    onCloseUnSubscribePopup() {
        this.setState( {
            isPopupOpen: false
        } )
    };

    render() {
        const {
            buttonLabel,
            buttonIcon,
            isSubmitting,
            isPopupOpen,
        } = this.state;

        const { title } = this.props;

        return (
            <span>
                <Button
                    id={this.componentName}
                    onClick={this.onClick}
                    title={this.createToolTipText()}
                    size="small"
                    icon={buttonIcon}
                    onMouseOver={this.onMouseOver}
                    onMouseOut={this.onMouseOut}
                    disabled={isSubmitting}
                >
                    {buttonLabel}
               </Button>

                <UnSubscribePopup
                    target={`#${this.componentName}`}
                    isOpen={isPopupOpen}
                    onClose={this.onCloseUnSubscribePopup}
                    onUnSubscribe={this.onUnSubscribe}
                >
                    <div>Unsubscribe from {title}?</div>
                </UnSubscribePopup>
            </span>
        );
    }
}

export default withAuth0(
    withApollo( SubscribeButton )
);
