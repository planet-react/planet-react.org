import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import SubscribeButton from './'

it( 'renders correctly', () => {

    let tree = renderer.create(
        <MockProviders>
            <SubscribeButton
                title="Hello, World!"
                type="feed"
                value="3"
            />
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );