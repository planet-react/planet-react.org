import React from "react";
import PropTypes from 'prop-types';
import SubscribeButton from 'components/subscription/button';
import { Row, Col, Filler } from 'components/support/flex';

DailyWeeklySubscriptionButtons.propTypes = {};

DailyWeeklySubscriptionButtons.defaultProps = {};

export default function DailyWeeklySubscriptionButtons( {} ) {
    return (
        <div>
            <div>Subscribe me to:</div>
            <Row>
                <Col>
                    <div>Daily</div>
                    <div style={{marginBottom:'.7em'}}>Posts aggregated daily</div>
                    <SubscribeButton type="daily" value="1" title="Weekly"/>
                </Col>
                <Filler grow=".07"/>
                <Col>
                    <div>Weekly</div>
                    <div style={{marginBottom:'.7em'}}>Posts aggregated weekly</div>
                    <SubscribeButton type="weekly" value="1" title="Daily"/>
                </Col>
            </Row>
        </div>
    );
}