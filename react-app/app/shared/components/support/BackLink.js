import React from 'react';
import A from 'components/support/a';
import { withRouter } from 'react-router-dom';

const BackLink = withRouter( ( props ) => {
    return (
        <A onClick={props.history.goBack}>❮ Back</A>
    )
} );

export default BackLink;