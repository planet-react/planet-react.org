import React from 'react';
import PropTypes from 'prop-types';

import { graphql } from 'react-apollo';
import CountriesQuery from 'graphql/CountriesQuery.graphql';

CountrySelector.propTypes = {
    onChange: PropTypes.func.isRequired,
    selected: PropTypes.string,
    name: PropTypes.string,
    id: PropTypes.string,
};

CountrySelector.defaultProps = {
    name: 'lang'
};

function CountrySelector( props ) {

    const { countriesQuery, selected, name, id } = props;

    return (
        <div className="select-wrapper">
            <select name={name} value={selected} id={id}
                    onChange={props.onChange}>
                {!countriesQuery.loading && countriesQuery.countries.map( ( country ) => {
                    return (
                        <option key={country.cca2} value={country.cca2}>
                            {country.commonName}
                        </option>
                    );
                } )}
            </select>
        </div>
    )
}

export default graphql( CountriesQuery, {
    name: 'countriesQuery',
    fetchPolicy: 'cache-first'
} )( CountrySelector );
