import React from 'react';
import CountrySelector from './CountrySelector';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../internals/testutils/MockProviders'


it( 'renders correctly', () => {
    const onChange = ( event ) => {
        void(0);
    };
    const countriesQueryMock = {
        "loading": false,
        "countries": [
            {
                "cca2": "",
                "commonName": "All"
            },
            {
                "cc2": "AF",
                "commonName": "Afghanistan"
            },
            {
                "cc2": "AX",
                "commonName": "Åland Islands"
            }
        ],
    };

    const tree = renderer.create(
        <MockProviders>
            <CountrySelector countriesQuery={countriesQueryMock} onChange={onChange} selected="en"/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );