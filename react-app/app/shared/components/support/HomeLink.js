import React, { Component } from 'react';
import { ScreenLink } from 'components/screen';

const HomeLink = ( props ) => {
    return (
        <ScreenLink path="/">❮ Home</ScreenLink>
    )
};

export default HomeLink;