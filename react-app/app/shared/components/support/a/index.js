import React from 'react';
import PropTypes from 'prop-types';

const A = ( { onClick, children, ...rest } ) => {
    const clickHandler = ( event ) => {
        event.preventDefault();
        if ( onClick ) {
            onClick(event);
        }
    };
    return (
        <a href="#" onClick={clickHandler} {...rest}>
            {children}
        </a>
    )
};

A.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.node
};

export default A;