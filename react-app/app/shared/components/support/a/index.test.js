import React from 'react';
import A from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const onClick = ( event ) => {
        void(0);
    };

    let tree = renderer.create(
        <A onClick={onClick}>Link</A>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <A onClick={onClick} className="link">Link</A>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );