import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

Button.propTypes = {
    onClick: PropTypes.func,
    color: PropTypes.oneOf( [ 'primary', 'secondary', 'blue', 'transparent' ] ),
    type: PropTypes.string,
    size: PropTypes.oneOf( [ 'large', 'medium', 'small' ] ),
    className: PropTypes.string,
    children: PropTypes.node,
    icon: PropTypes.func,
    iconColor: PropTypes.string,
};

export default function Button( { children, onClick, className, size = 'large', color = 'primary', type = 'button', icon, iconColor, ...rest } ) {

    let classNames = [ 'button', 'button--' + size ];

    classNames.push( 'button--' + color );

    if ( className ) {
        classNames.push( className );
    }
    classNames = classNames.join( ' ' );

    let iconSize;
    switch ( size ) {
        case 'large':
            iconSize = 19;
            break;
        case 'medium':
            iconSize = 18;
            break;
        case 'small':
            iconSize = 14;
            break;
        default:
            iconSize = 19;
            break;
    }

    let iconVerticalAlign;
    switch ( size ) {
        case 'large':
            iconVerticalAlign = 'bottom';
            break;
        case 'medium':
            iconVerticalAlign = 'top';
            break;
        case 'small':
            iconVerticalAlign = 'text-top';
            break;
        default:
            iconVerticalAlign = 'top';
            break;
    }

    const Icon = icon;

    return (
        <button onClick={onClick} type={type} className={classNames} {...rest}>
            {icon && (<span className="button__icon">
                <Icon
                    size={iconSize}
                    color={iconColor}
                    style={{ verticalAlign: iconVerticalAlign }}
                />
            </span>)}
            {children}
        </button>
    )
};