import React from 'react';
import Button from './';
import renderer from 'react-test-renderer';
import MailIcon from 'react-icons/lib/go/mail';

const onClick = ( event ) => {
    void(0);
};

it( 'renders correctly', () => {
    let tree = renderer.create(
        <Button onClick={onClick}>Link</Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports size=medium', () => {
    let tree = renderer.create(
        <Button onClick={onClick} size="medium">Link</Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports type', () => {
    let tree = renderer.create(
        <Button type="button" onClick={onClick} size="medium">Link</Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports size=small', () => {
    let tree = renderer.create(
        <Button onClick={onClick} size="small">Link</Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports custom className', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            size="small"
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports color=secondary', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            size="small"
            color="secondary"
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports color=transparent', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            size="small"
            color="transparent"
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports color=blue', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            color="blue"
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports icon', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            color="blue"
            icon={MailIcon}
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports icon color', () => {
    let tree = renderer.create(
        <Button
            onClick={onClick}
            className="fancy"
            color="blue"
            icon={MailIcon}
            iconCcon="#f00"
        >
            Link
        </Button>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );