import React from 'react';
import PropTypes from 'prop-types';
import { withServices } from 'components/services';

DateFormatter.propTypes = {
    date: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.instanceOf( Date )
    ] ),
    format: PropTypes.string,
};

DateFormatter.defaultProps = {
    format: 'MMM D, YYYY'
};

function DateFormatter( { date, format, services: { dateHelper } } ) {
    return <span>{dateHelper.format( date, format )}</span>;
}

export default withServices( DateFormatter );