import React from 'react';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

import DateFormatter from './';

it( 'renders correctly using a date object', () => {

    const date = new Date( 2017, 7, 1 );

    let tree = renderer.create(
        <MockProviders>
            <DateFormatter date={date} format="YYYY-MM-DD"/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'renders correctly using a date string', () => {

    const date = '2017-07-10 10:10:11';

    let tree = renderer.create(
        <MockProviders>
            <DateFormatter date={date} format="YYYY-MM-DD HH:mm:ss"/>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'renders supports formats', () => {

    let tree = renderer.create(
        <MockProviders>
            <div>
                <DateFormatter
                    date="2019-03-04 09:30:00"
                    format="dddd MMMM Do"
                />
                <DateFormatter
                    date="2019-03-04 09:30:00"
                    format="HHmm"
                />
                <DateFormatter
                    date="2019-03-04 09:30:00"
                    format="MMM"
                />
            </div>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
