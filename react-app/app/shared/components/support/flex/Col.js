import React from 'react';
import PropTypes from 'prop-types';

Col.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    tag: PropTypes.string,
    style: PropTypes.object,
    flexBasis: PropTypes.string,
    flexGrow: PropTypes.string,
    flexShrink: PropTypes.string,
};

export default function Col( { children, className = '', tag = '', style, flexBasis, flexGrow, flexShrink, ...rest } ) {
    const Tag = tag ? `${tag}` : 'div';
    const classes = [ 'flex-col', className ].join( ' ' ).trim();

    const styles = Object.assign( {}, style, {
        flexBasis: flexBasis,
        flexGrow: flexGrow,
        flexShrink: flexShrink
    } );

    return (
        <Tag className={classes} {...rest} style={styles}>
            {children}
        </Tag>
    )
};

