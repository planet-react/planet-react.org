import React from 'react';
import { Col } from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    let tree = renderer.create(
        <Col>A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'it supports custom className', () => {
    let tree = renderer.create(
        <Col className="fancy box">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'it supports custom style', () => {
    let tree = renderer.create(
        <Col style={{backgroundColor: '#aacc00', color: 'green'}}>A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'it supports flexBasis', () => {
    let tree = renderer.create(
        <Col flexBasis="1">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Col flexBasis="max-content">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'it supports flexGrow', () => {
    let tree = renderer.create(
        <Col flexGrow="3">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Col flexGrow="0.6">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'it supports flexShrink', () => {
    let tree = renderer.create(
        <Col flexShrink="2">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Col flexGrow="0.3">A</Col>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );