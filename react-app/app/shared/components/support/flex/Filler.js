import React from 'react';
import PropTypes from 'prop-types';

Filler.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    grow: PropTypes.string,
    tag: PropTypes.string
};

Filler.defaultProps = {
    tag: '',
    grow: '1'
};

export default function Filler( { tag, grow, ...rest } ) {
    const Tag = tag ? `${tag}` : 'div';

    const realStyle = {
        flexGrow: grow
    };

    return (
        <Tag style={realStyle} {...rest}/>
    )
};

