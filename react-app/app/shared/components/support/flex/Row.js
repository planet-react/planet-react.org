import React from 'react';
import PropTypes from 'prop-types';

Row.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    tag: PropTypes.string,
    style: PropTypes.object,
    flow: PropTypes.string,
    direction: PropTypes.string,
    justifyContent: PropTypes.string,
    alignContent: PropTypes.string,
    alignItems: PropTypes.string,

};

export default function Row( { children, className = '', tag = 'div', style, flow, direction = 'row', justifyContent, alignContent, alignItems, ...rest } ) {
    const Tag = tag ? `${tag}` : 'div';

    const classes = [
        'flex-row',
        `flex-direction-${direction}`,
        className ].join( ' ' ).trim();

    const styles = Object.assign( {}, style, {
        flexFlow: flow ? direction + ' ' + flow : null,
        justifyContent: justifyContent,
        alignContent: alignContent,
        alignItems: alignItems,
    } );

    return (
        <Tag className={classes} style={styles} {...rest}>
            {children}
        </Tag>
    )
};