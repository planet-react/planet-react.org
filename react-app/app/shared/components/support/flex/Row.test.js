import React from 'react';
import { Row } from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    let tree = renderer.create(
        <Row></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports custom className', () => {
    let tree = renderer.create(
        <Row className="nice custom-row"></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports custom styles', () => {
    let tree = renderer.create(
        <Row style={{border: '1px solid green', background: 'skyblue', color:'#ffffff'}}></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports justifyContent prop', () => {
    let tree = renderer.create(
        <Row justifyContent="space-around"></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports alignItems prop', () => {
    let tree = renderer.create(
        <Row alignItems="flex-start"></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports alignContent prop', () => {
    let tree = renderer.create(
        <Row alignContent="center"></Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports direction prop', () => {
    let tree = renderer.create(
        <Row direction="row"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row direction="row-reverse"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row direction="column"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row direction="column-reverse"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );
