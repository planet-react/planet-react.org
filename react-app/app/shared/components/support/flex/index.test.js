import React from 'react';
import { Row, Col, Filler } from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    let tree = renderer.create(
        <Row className={'foo'}>
            <Col>A</Col>
            <Col>B</Col>
            <Col className={'bar baz'}>C</Col>
        </Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row className={'bar'} id="unique">
            <Col title="A column">A</Col>
            <Col style={{ color: '#ccff99' }}>B</Col>
        </Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row tag="article" className={'zot'}>
            <Col tag="header">A</Col>
            <Col tag="section">B</Col>
            <Col tag="footer">C</Col>
        </Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Row tag="article" className={'zot'}>
            <Col tag="header">A</Col>
            <Filler/>
            <Col tag="footer">C</Col>
        </Row>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );