import React from "react";
import PropTypes from "prop-types";

FieldLayout.propTypes = {
    children: PropTypes.node
};

export default function FieldLayout( { children } ) {
    return (
        <div className="form__row">
            {children}
        </div>
    )
}
