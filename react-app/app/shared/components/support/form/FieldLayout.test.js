import React from 'react';
import FieldLayout from './FieldLayout';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const tree = renderer.create(
        <FieldLayout>
            <div>Child 1</div>
            <div>Child 2</div>
            <div>Child 3</div>
        </FieldLayout>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

