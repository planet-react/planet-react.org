import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withApollo } from 'react-apollo';
import Spinner from 'components/support/spinner';
import { GraphQlError } from 'components/graphql';
import Message from 'components/support/message';

export class Form extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        mutation: PropTypes.object.isRequired,
        onBeforeSubmit: PropTypes.func,
        onSubmitSuccess: PropTypes.func,
        onSubmitError: PropTypes.func,
        submitText: PropTypes.string,
        submitSuccessText: PropTypes.string
    };

    static defaultProps = {
        submitText: 'Submitting form...',
        submitSuccessText: 'The form was successfully submitted',
        onBeforeSubmit: ( formStateCopy ) => {
            return new Promise( ( resolve ) => {
                resolve( {
                    formData: null
                } );
            } )
        },
        onSubmitSuccess: ( data ) => {
            return new Promise( ( resolve ) => {
                resolve( {
                    data: null
                } );
            } )
        },
        onSubmitError: ( errors ) => {
            return new Promise( ( resolve ) => {
                resolve( {
                    data: null
                } );
            } )
        }
    };

    constructor( props ) {
        super( props );

        this.state = {
            isSubmitting: false,
            isSubmitSuccess: false,
            errors: null,
            data: Object.assign( {}, props.data )
        };

        this.onSubmit = this.onSubmit.bind( this );
        this.onSubmitSuccess = this.onSubmitSuccess.bind( this );
        this.onSubmitError = this.onSubmitError.bind( this );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.data != this.props.data ) {
            this.setState( {
                data: Object.assign( {}, nextProps.data )
            } )
        }
    }

    onSubmit( event ) {
        event.preventDefault();

        this.setState( {
            errors: null,
            isSubmitSuccess: false,
            isSubmitting: true,
        }, () => {
            const formStateCopy = Object.assign( {}, this.state );
            this.props.onBeforeSubmit( formStateCopy ).then( ( result ) => {
                this.setState( {
                        data: Object.assign( this.state.data, result ? result.formData : null )
                    }, () => {
                        this.props.client.mutate( {
                            mutation: this.props.mutation,
                            variables: this.state.data
                        } )
                            .then( this.onSubmitSuccess )
                            .catch( this.onSubmitError );
                    }
                )

            } );

        } );
    }

    onSubmitSuccess( { data } ) {
        this.setState( {
            isSubmitting: false,
            isSubmitSuccess: true,
        }, () => {
            this.props.onSubmitSuccess( data ).then( () => {
                //
            } );
        } );
    }

    onSubmitError( errors ) {
        this.setState( {
            isSubmitting: false,
            errors: errors
        }, () => {
            this.props.onSubmitError( errors ).then( ( data ) => {
                //
            } );
        } );
    }

    render() {
        const { errors, isSubmitting, isSubmitSuccess } = this.state;
        const { children, submitText, submitSuccessText } = this.props;

        if ( !errors && isSubmitting ) {
            return <Spinner text={submitText}/>
        }

        return (
            <div>
                {errors && <GraphQlError errors={errors}/>}

                {isSubmitSuccess && <Message context="success">
                    {submitSuccessText}
                </Message>}

                <form onSubmit={this.onSubmit}>
                    {children}
                </form>
            </div>
        );
    }
}

export default withApollo( Form );