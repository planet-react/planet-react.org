import React from 'react';
import { Form } from './Form';


const apolloClientMock = {
    mutate( options ) {
        return new Promise( ( resolve, reject ) => {
            resolve( { data: {} } );
        } )
    }
};

describe( 'Form', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <Form
                mutation={{}}
                data={{ a: 1, b: 3, c: 'check' }}
            /> );

        expect( wrapper.find( 'form' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();


    } );

    it( 'has the expected defaultProps', () => {
        const wrapper = shallow(
            <Form
                mutation={{}}
                data={{ a: 1, b: 3, c: 'check' }}
            /> );

        expect( Form.defaultProps.submitText ).toBe( 'Submitting form...' );
        expect( Form.defaultProps.submitSuccessText ).toBe( 'The form was successfully submitted' );
        expect( Form.defaultProps.onBeforeSubmit ).toBeDefined();
        expect( Form.defaultProps.onSubmitSuccess ).toBeDefined();
        expect( Form.defaultProps.onSubmitError ).toBeDefined();

    } );

    it( 'resolves .defaultProps.onBeforeSubmit as expected', () => {
        return expect( Form.defaultProps.onBeforeSubmit( { a: 1 } ) ).resolves.toEqual( {
            formData: null
        } );
    } );

    it( 'resolves .defaultProps.onSubmitSuccess as expected', () => {
        return expect( Form.defaultProps.onSubmitSuccess( {} ) ).resolves.toEqual( {
            data: null
        } );
    } );

    it( 'resolves .defaultProps.onBeforeSubmit as expected', () => {
        return expect( Form.defaultProps.onSubmitError( {} ) ).resolves.toEqual( {
            data: null
        } );
    } );

    it( 'has the expected state', () => {
        const wrapper = shallow(
            <Form
                mutation={{}}
                data={{ a: 1, b: 3, c: 'check' }}
            /> );

        expect( wrapper.state() ).toEqual( {
                isSubmitting: false,
                isSubmitSuccess: false,
                errors: null,
                data: { a: 1, b: 3, c: 'check' }
            }
        );

    } );

    it( 'renders the children', () => {
        const wrapper = shallow(
            <Form
                mutation={{}}
                data={{ a: 1, b: 3, c: 'check' }}
            >
                <div className="dummy-input">a</div>
                <div className="dummy-input">b</div>
                <div className="dummy-input">c</div>
            </Form> );

        expect( wrapper.find( 'form div.dummy-input' ).length ).toBe( 3 );
    } );

    it( 'submits the form', () => {
        Form.prototype.onSubmit = jest.fn( Form.prototype.onSubmit );

        const wrapper = mount(
            <Form
                mutation={{}}
                data={{ a: 1, b: 3, c: 'check' }}
                client={apolloClientMock}
            >
                <div className="dummy-input">a</div>

                <button type="submit">Submit</button>
            </Form> );

        expect( wrapper.find( '.spinner' ).length ).toBe( 0 );

        wrapper.find( 'form' ).simulate( 'submit' );

        expect( Form.prototype.onSubmit ).toHaveBeenCalled();
        expect( wrapper.state().isSubmitting ).toBe( true );
        expect( wrapper.find( '.spinner' ).length ).toBe( 1 );

    } );
} );

