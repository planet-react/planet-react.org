import React from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';
import CountrySelector from "components/support/CountrySelector";

InputCountrySelector.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    id: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
};

InputCountrySelector.defaultProps = {};

export default function InputCountrySelector( { name, value, id, label = 'Language', onChange } ) {
    return (
        <FieldLayout>
            <label htmlFor={id}>{label}:</label>
            <br/>
            <CountrySelector
                name={name}
                id={id}
                selected={value}
                onChange={onChange}
            />
        </FieldLayout>
    )
}
