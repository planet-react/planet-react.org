import React, { Component } from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';
import { Calendar } from 'react-util-components';
import { formatDate, isOutside, isPopupOutsideViewportBottom } from './util';
import CalendarIcon from 'react-icons/lib/fa/calendar';

export default class DatePicker extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.any,
        label: PropTypes.string,
        required: PropTypes.bool,
        autoFocus: PropTypes.bool,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
        className: PropTypes.string,
        id: PropTypes.string,
        placeholder: PropTypes.string,
        format: PropTypes.string,
    };

    static defaultProps = {
        required: false,
        autoFocus: false,
        format: 'YYYY-MM-DD',
        className: 'pr-date-picker-input',
    };

    input = null;

    componentElem = null;

    listElem = null;

    constructor( props ) {
        super( props );
        this.state = {
            visible: false,
            value: props.value,
            listStyle: {}
        };
    }

    componentDidMount() {
        this.componentElem = document.querySelector( '[data-name="date-picker-' + this.props.name + '"' );
        this.listElem =  this.componentElem.querySelector( '.pr-calendar' );

        document.addEventListener( 'click', this.onClickOutside );
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            value: nextProps.value
        } );
    }

    componentWillUnmount() {
        document.removeEventListener( 'click', this.onClickOutside );
    }

    isValidDate( date ) {
        return !isNaN( Date.parse( date.toString() ) );
    }

    onShow = () => {
        this.setState( { visible: true }, () => {
            const isOutside = isPopupOutsideViewportBottom( this.listElem );
            const inputRect = this.componentElem.querySelector( 'input' ).getBoundingClientRect();
            const listStyle = isOutside ? {
                bottom: inputRect.height
            } : {
                top: 0
            };

            this.setState( { listStyle } )
        } );
    };

    onHide = () => {
        this.setState( { visible: false, listStyle: {} } );
    };

    onChange = ( event ) => {
        if ( this.props.onChange ) {
            this.props.onChange( event );
        }
    };

    onSelect = ( date ) => {
        const value = formatDate( date, this.props.format );
        this.onChange( {
            target: {
                name: this.props.name,
                type: 'text',
                value: value
            }
        } );
        this.input.focus();
    };

    onClickOutside = ( event ) => {
        const hide = isOutside( event.target, this.props.name, [ 'pr-calendar__header' ] );
        if ( hide ) {
            this.onHide();
        }
    };

    render() {
        const { name, label, required, onBlur, autoFocus, className, id, placeholder } = this.props;
        const { value, listStyle } = this.state;

        let startDate = new Date( value );
        if ( !this.isValidDate( startDate ) ) {
            startDate = new Date();
        }

        const style = {
            position: 'relative',
            display: this.state.visible ? '' : 'none'
        };

        return (
            <FieldLayout>
                {label && <label htmlFor={id}>
                    {label}: {required ? '*' : ''}
                </label>}
                <div data-name={'date-picker-' + name}>
                    <div className="picker-input-wrapper">
                        <div className="picker-input-wrapper__icon">
                            <CalendarIcon size={15}/>
                        </div>
                        <input
                            name={name}
                            type="text"
                            value={value}
                            id={id}
                            required={required}
                            autoFocus={autoFocus}
                            onClick={this.onShow}
                            onChange={this.onChange}
                            onBlur={onBlur ? onBlur : null}
                            className={className}
                            placeholder={placeholder}
                            ref={node => this.input = node}
                        />
                    </div>

                    <div style={style}>
                        <Calendar
                            onSelect={this.onSelect}
                            startDate={startDate}
                            style={listStyle}
                        />
                    </div>
                </div>
            </FieldLayout>
        )
    }
}
