import React from "react";
import PropTypes from "prop-types";
import { Row, Col } from 'components/support/flex';
import Spinner from 'components/support/spinner';
import FieldLayout from '../FieldLayout';
import InputError from './InputError';

FeedUrl.propTypes = {
    isValidating: PropTypes.bool.isRequired,
    showStatusIcon: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func,
    id: PropTypes.string,
    isFeedValid: PropTypes.bool,
    value: PropTypes.any,
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    errorMessage: PropTypes.string
};

FeedUrl.defaultProps = {
    errorMessage: null,
};

// @todo: fix so showStatusIcon shows 'checked' icon
// @todo: graphql here?

export default function FeedUrl( { isValidating, showStatusIcon, isFeedValid, value, required, autoFocus, onChange, onBlur, id, errorMessage } ) {

    //const showFeedErrorMessage = !(isValidating || isSubmitting) && !isFeedValid && feedValidatorError != null;
    const showFeedErrorMessage = !(isValidating) && !isFeedValid && errorMessage != null;

    return (
        <FieldLayout>
            <label htmlFor={id}>Feed URL: {required ? '*' : ''}</label>
            <Row className="feed-url-container">
                <Col>
                    {isValidating &&
                    <Spinner
                        verticalSpace={0} size={16}
                        center={false}
                    />}

                    {showStatusIcon &&
                    <div>
                        { !isFeedValid ?
                            (
                                <div
                                    className="validation-icon icon-error"
                                >
                                    ⚠</div>
                            ) : (
                                <div
                                    className="validation-icon icon-success"
                                >
                                    ✓</div>
                            )
                        }
                    </div>
                    }
                </Col>
                <Col>
                    <input name="feed_url"
                           value={value}
                           id={id}
                           required={required}
                           type="url"
                           autoFocus={autoFocus}
                           onChange={onChange}
                           onBlur={onBlur}
                    />
                </Col>
            </Row>

            { showFeedErrorMessage && <InputError message={errorMessage}/> }

        </FieldLayout>
    )
}
