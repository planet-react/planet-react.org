import React from 'react';
import { FeedUrl } from '../';
import renderer from 'react-test-renderer';

const onChange = jest.fn();
const onClick = jest.fn();
const onBlur = jest.fn();

describe( '<FeedUrl/>', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <FeedUrl
                isValidating={false}
                required
                showStatusIcon={false}
                onChange={onChange}
                onBlur={onBlur}
                id="feed-url"
            />
        );
        expect( wrapper.find( 'FieldLayout' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );
} );

// it( 'renders correctly when prop `isValidating` is true', () => {
//     const tree = renderer.create(
//         <FeedUrl
//             isValidating={true}
//             showStatusIcon={false}
//             onChange={onChange}
//             onBlur={onBlur}
//             id="feed-url"
//         />
//     ).toJSON();
//     expect( tree ).toMatchSnapshot();
// } );
//
// it( 'displays the correct icon when feed url is valid', () => {
//     const tree = renderer.create(
//         <FeedUrl
//             isValidating={false}
//             isFeedValid={true}
//             showStatusIcon={true}
//             onChange={onChange}
//             onBlur={onBlur}
//             id="feed-url"
//         />
//     ).toJSON();
//     expect( tree ).toMatchSnapshot();
// } );
//
// it( 'displays the correct icon when feed url is not valid', () => {
//     const tree = renderer.create(
//         <FeedUrl
//             isValidating={false}
//             isFeedValid={false}
//             showStatusIcon={true}
//             onChange={onChange}
//             onBlur={onBlur}
//             id="feed-url"
//         />
//     ).toJSON();
//     expect( tree ).toMatchSnapshot();
// } );
//
// it( 'displays the error message when message is given and feed url is not valid', () => {
//     const tree = renderer.create(
//         <FeedUrl
//             isValidating={false}
//             isFeedValid={false}
//             showStatusIcon={true}
//             errorMessage="Invalid url"
//             onChange={onChange}
//             onBlur={onBlur}
//             id="feed-url"
//         />
//     ).toJSON();
//     expect( tree ).toMatchSnapshot();
// } );
