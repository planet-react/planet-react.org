import React from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';

Input.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    value: PropTypes.any,
    maxLength: PropTypes.number,
    label: PropTypes.string,
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    checked: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number
    ]),
    className: PropTypes.string,
    id: PropTypes.string,
    placeholder: PropTypes.string,
};

Input.defaultProps = {
    type: 'text',
    required: false,
    autoFocus: false,
};

export default function Input( { name, type, value, maxLength, label, required, autoFocus, disabled, onClick, onChange, onFocus, onBlur, checked, className, id, placeholder } ) {
    const isCheckbox = type == 'checkbox' || type == 'radio';
    return (
        <FieldLayout>
            {!isCheckbox && label &&
            <label htmlFor={id}>{label}: {required ? '*' : ''}</label>}

            <input
                id={id}
                name={name}
                type={type}
                maxLength={maxLength}
                value={type != 'checkbox' ? value || '' : ''}
                placeholder={placeholder}
                required={required}
                autoFocus={autoFocus}
                disabled={disabled}
                onClick={onClick}
                onChange={onChange ? onChange : null}
                onFocus={onFocus ? onFocus : null}
                onBlur={onBlur ? onBlur : null}
                checked={type == 'checkbox' || type == 'radio' ? checked : null}
                className={className}
            />

            {isCheckbox && label &&
            <label htmlFor={id}> {label} {required ? '*' : ''}</label>}
        </FieldLayout>
    )
}
