import React from 'react';
import { Input } from '../';


describe( '<Input/>', () => {

    const onChange = jest.fn();
    const onClick = jest.fn();
    const onBlur = jest.fn();

    it( 'renders correctly', () => {
        const wrapper = shallow(
            <Input
                name="age"
                type="text"
                value="8"
                maxLength={255}
                placeholder="Age"
                id="age"
                onChange={onChange}
                onClick={onClick}/>
        );

        expect( wrapper.find( 'input' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'handles onClick', () => {
        const wrapper = mount(
            <Input
                name="email"
                type="text"
                value="test@acme.com"
                onChange={onChange}
                onClick={onClick}
            />
        );
        wrapper.find( 'input' ).simulate( 'click' );
        expect( onClick ).toHaveBeenCalled();
    } );


    it( 'handles onChange', () => {
        const wrapper = mount(
            <Input
                name="email"
                type="text"
                value="test@acme.com"
                onChange={onChange}
            />
        );
        const event = { target: { name: 'email', value: 'tester@acme.com' } };
        wrapper.find( 'input' ).simulate( 'change', event );
        expect( onChange ).toHaveBeenCalled();
    } );


    it( 'handles onBlur', () => {
        const wrapper = mount(
            <Input
                name="email"
                type="text"
                value="test@acme.com"
                onChange={onChange}
                onBlur={onBlur}
            />
        );
        wrapper.find( 'input' ).simulate( 'blur' );
        expect( onBlur ).toHaveBeenCalled();
    } );

} );


