import React from "react";
import PropTypes from "prop-types";
import Message from 'components/support/message';

InputError.propTypes = {
    message: PropTypes.string
};

export default function InputError( { message } ) {
    return (
        <Message context="error" className="form-error-message">
            {message}
        </Message>
    )
}
