import React from 'react';
import InputError from './InputError';

describe( '<InputError/>', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <InputError message="Message here.."/>
        );
        expect( wrapper.find( 'Message' ).dive().find( 'div.message--error' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );
} );
