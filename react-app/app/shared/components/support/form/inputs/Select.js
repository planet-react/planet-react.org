import React from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';

Select.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    label: PropTypes.string,
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    className: PropTypes.string,
    id: PropTypes.string,
    children: PropTypes.node,
};

Select.defaultProps = {
    required: false,
    autoFocus: false,
};

export default function Select( { name, value, label, required, autoFocus, onClick, onChange, onBlur, className, id, children } ) {
    return (
        <FieldLayout>
            {label &&
            <label htmlFor={id}>{label}: {required ? '*' : ''}</label>}
            <div className="select-wrapper">
                <select
                    name={name}
                    value={value || ''}
                    id={id}
                    onClick={onClick}
                    onChange={onChange}
                    onBlur={onBlur}
                    autoFocus={autoFocus}
                    className={className}
                >
                    {children}
                </select>
            </div>
        </FieldLayout>
    )
}
