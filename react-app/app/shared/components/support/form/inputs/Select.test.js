import React from 'react';
import { Select } from '../';


describe( '<Select/>', () => {

    const onChange = jest.fn();
    const onBlur = jest.fn();

    it( 'renders correctly', () => {
        const wrapper = shallow(
            <Select
                label="Country"
                name="country"
                value="en"
                id="country"
                onChange={onChange}
            />
        );

        expect( wrapper.find( 'select' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'handles onChange', () => {
        const wrapper = mount(
            <Select
                label="Country"
                name="country"
                value="en"
                onChange={onChange}
            >
                <option value="en">EN</option>
                <option value="de">DE</option>
                <option value="us">US</option>
            </Select>
        );
        const event = { target: { name: 'country', value: 'de' } };
        wrapper.find( 'select' ).simulate( 'change', event );
        expect( onChange ).toHaveBeenCalled();
    } );

    it( 'handles onBlur', () => {
        const wrapper = mount(
            <Select
                label="Country"
                name="country"
                value="en"
                onChange={onChange}
                onBlur={onBlur}
            />
        );
        const event = { target: { name: 'country', value: 'de' } };
        wrapper.find( 'select' ).simulate( 'blur', event );
        expect( onBlur ).toHaveBeenCalled();
    } );

} );

