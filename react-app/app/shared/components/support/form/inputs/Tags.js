import React, { Component } from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';

export default class TagsInput extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.array,
        id: PropTypes.string,
        label: PropTypes.string,
        required: PropTypes.bool,
        autoFocus: PropTypes.bool,
        onChange: PropTypes.func,
        className: PropTypes.string,
    };

    static defaultProps = {
        required: false,
        autoFocus: false,
        className: 'pr-tag-input',
    };

    constructor( props ) {
        super( props );
        this.state = {
            tags: props.value || []
        }
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            tags: nextProps.value || []
        } );
    }

    addTag( value ) {
        this.setState( {
            tags: this.state.tags.concat( { tag_slug: value } )
        }, () => {
            this.onChange();
        } );
    }

    removeTag( index ) {
        this.setState( {
            tags: this.state.tags.filter( ( item, i ) => i != index )
        }, () => {
            this.onChange();
        } );
    }

    onChange = () => {
        if ( this.props.onChange ) {
            this.props.onChange( {
                target: {
                    name: this.props.name,
                    value: this.state.tags
                }
            } );
        }
    };

    onClick = ( event ) => {
        if ( event.target.dataset && event.target.dataset.action ) {
            const index = Number( event.target.dataset.id );
            this.removeTag( index );
        }
        document.querySelector( '[data-name="pr-tag-input-' + this.props.name + '"] input' ).focus();
    };

    onInputKeyDown = ( event ) => {
        const { target, keyCode } = event;
        const value = target.value.trim();

        const addKeys = [ 9, 13 ];
        const backSpace = 8;

        if ( addKeys.indexOf( keyCode ) > -1 ) {
            if ( value == '' ) {
                return;
            }
            event.preventDefault();
            target.value = '';
            this.addTag( value );
        }

        if ( keyCode == backSpace ) {
            if ( value == '' ) {
                event.preventDefault();
                this.removeTag( this.state.tags.length - 1 );
            }
        }
    };

    onInputBlur = ( event ) => {
        const { target } = event;
        const value = target.value.trim();

        if ( value != '' ) {
            target.value = '';
            this.addTag( value );
        }
    };

    render() {

        const { tags } = this.state;
        const { name, label, id, required, autoFocus, className } = this.props;

        return (
            <FieldLayout>
                {label &&
                <label htmlFor={id}>{label}: {required ? '*' : ''}</label>}

                <div className={className} onClick={this.onClick}
                     data-name={'pr-tag-input-' + name}>
                    <ul className="pr-tag-input__display-values">
                        {tags.map( ( tag, i ) => {
                            return (
                                <li key={i} className="pr-tag-input__tag">
                                    <span>{tag.tag_slug}</span>
                                    <button type="button" data-action="remove"
                                            data-id={i}>
                                        ×
                                    </button>
                                </li>
                            )
                        } )}
                        <li className="pr-tag-input__input-wrap">
                            <input
                                type="text"
                                id={id}
                                autoFocus={autoFocus}
                                onKeyDown={this.onInputKeyDown}
                                onBlur={this.onInputBlur}
                            />
                        </li>
                    </ul>
                </div>
            </FieldLayout>
        )
    }
}
