import React from 'react';
import { Tags } from '../';

describe( '<Tags/>', () => {

    it( 'renders correctly', () => {
        const wrapper = shallow(
            <Tags name="tags"/>
        );

        expect( wrapper.find( 'div.pr-tag-input' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );

} );

