import React from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';

TextArea.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    label: PropTypes.string,
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    className: PropTypes.string,
    id: PropTypes.string,
    placeholder: PropTypes.string,
    rows: PropTypes.number,
};

TextArea.defaultProps = {
    required: false,
    autoFocus: false,
    id: null,
};

export default function TextArea( { name, value, label, required, autoFocus, onClick, onChange, onFocus, onBlur, className, rows, id, placeholder } ) {
    return (
        <FieldLayout>
            {label &&
            <label htmlFor={id}>{label}: {required ? '*' : ''}</label>}
            <textarea
                name={name}
                id={id}
                required={required}
                autoFocus={autoFocus}
                onClick={onClick}
                onChange={onChange}
                onFocus={onFocus}
                onBlur={onBlur}
                value={value || ''}
                placeholder={placeholder}
                className={className}
                rows={rows}/>
        </FieldLayout>
    )
}
