import React from 'react';
import { TextArea } from '../';

const onChange = jest.fn();
const onClick = jest.fn();
const onBlur = jest.fn();

describe( '<TextArea/>', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <TextArea
                label="Description"
                name="description"
                value="Hello, World!"
                placeholder="Type something"
                id="description-1"
                onChange={onChange}
                onClick={onClick}
            />
        );

        expect( wrapper.find( 'textarea' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();

    } );

    it( 'handles onClick', () => {
        const wrapper = mount(
            <TextArea
                label="Description"
                name="description"
                value="Hello, World!"
                onChange={onChange}
                onClick={onClick}
            />
        );
        wrapper.find( 'textarea' ).simulate( 'click' );
        expect( onClick ).toHaveBeenCalled();
    } );

    it( 'handles onChange', () => {
        const wrapper = mount(
            <TextArea
                label="Description"
                name="description"
                value="Hello, World!"
                onChange={onChange}
            />
        );
        const event = {
            target: {
                name: 'description',
                value: 'Bye Bye, Cruel World!'
            }
        };
        wrapper.find( 'textarea' ).simulate( 'change', event );
        expect( onChange ).toHaveBeenCalled();
    } );

    it( 'handles onBlur', () => {
        const wrapper = mount(
            <TextArea
                label="Description"
                name="description"
                value="Hello, World!"
                onChange={onChange}
                onBlur={onBlur}
            />
        );
        wrapper.find( 'textarea' ).simulate( 'blur' );
        expect( onBlur ).toHaveBeenCalled();
    } );

} );
