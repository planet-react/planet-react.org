import React, { Component } from "react";
import PropTypes from "prop-types";
import FieldLayout from '../FieldLayout';
import { TimeList } from 'react-util-components';
import { isOutside, isPopupOutsideViewportBottom } from './util';
import ClockIcon from 'react-icons/lib/fa/clock-o';

export default class TimePicker extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        type: PropTypes.string,
        value: PropTypes.any,
        label: PropTypes.string,
        required: PropTypes.bool,
        autoFocus: PropTypes.bool,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
        steps: PropTypes.array,
        className: PropTypes.string,
        id: PropTypes.string,
        placeholder: PropTypes.string,
    };

    static defaultProps = {
        steps: [ 30 ],
        className: 'pr-time-picker-input',
    };

    inputElem = null;

    componentElem = null;

    listElem = null;

    constructor( props ) {
        super( props );
        this.state = {
            visible: false,
            value: props.value,
            listStyle: {}
        };
    }

    componentDidMount() {

        this.componentElem = document.querySelector( '[data-name="time-picker-' + this.props.name + '"]' );
        this.listElem = this.componentElem.querySelector( '.time-picker-wrapper ul' );

        document.addEventListener( 'click', this.onClickOutside );
    }

    componentWillReceiveProps( nextProps ) {
        this.setState( {
            value: nextProps.value
        } );
    }

    componentWillUnmount() {
        document.removeEventListener( 'click', this.onClickOutside );
    }

    onShow = () => {
        this.setState( { visible: true }, () => {

            const selected = this.componentElem.querySelector( '.time-picker-wrapper [data-selected="true"]' );

            this.listElem.scrollTop = selected ? selected.offsetTop : 0;

            const isOutside = isPopupOutsideViewportBottom( this.listElem );

            const inputRect = this.componentElem.querySelector( 'input' ).getBoundingClientRect();

            const listStyle = isOutside ? {
                bottom: inputRect.height
            } : {
                top: 0
            };

            this.setState( { listStyle } )
        } );
    };

    onHide = () => {
        this.setState( { visible: false, listStyle: {} } );
    };

    onChange = ( event ) => {
        if ( this.props.onChange ) {
            this.props.onChange( event );
        }
    };

    onSelect = ( value ) => {
        this.onChange( {
            target: {
                name: this.props.name,
                type: 'text',
                value: value
            }
        } );
        this.inputElem.focus();
    };

    onClickOutside = ( event ) => {
        const hide = isOutside( event.target, this.props.name );
        if ( hide ) {
            this.onHide();
        }
    };

    render() {
        const { name, label, required, onBlur, autoFocus, className, id, placeholder } = this.props;
        const { value, listStyle } = this.state;

        const style = {
            position: 'relative',
            display: this.state.visible ? '' : 'none'
        };

        return (
            <FieldLayout>
                {label &&
                <label htmlFor={id}>
                    {label}: {required ? '*' : ''}
                </label>}
                <div
                    data-name={'time-picker-' + name}
                    className="time-picker-wrapper"
                >

                    <div className="picker-input-wrapper">
                        <div className="picker-input-wrapper__icon">
                            <ClockIcon size={15}/>
                        </div>
                        <input
                            name={name}
                            type="text"
                            value={value}
                            placeholder={placeholder}
                            id={id}
                            required={required}
                            autoFocus={autoFocus}
                            onClick={this.onShow}
                            onChange={this.onChange}
                            onBlur={onBlur ? onBlur : null}
                            className={className}
                            ref={node => this.inputElem = node}
                        />

                    </div>

                    <div style={style}>
                        <TimeList
                            value={value}
                            steps={[ 30 ]}
                            style={listStyle}
                            onSelect={this.onSelect}
                        />
                    </div>
                </div>
            </FieldLayout>
        )
    }
}
