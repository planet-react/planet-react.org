import React from "react";
import PropTypes from "prop-types";
import Select from './Select';

TimezoneSelector.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    className: PropTypes.string,
    id: PropTypes.string,
};

TimezoneSelector.defaultProps = {
    required: false,
    autoFocus: false,
};

export default function TimezoneSelector( { name, value, required, autoFocus, onClick, onChange, onBlur, className, id } ) {
    return (
        <Select
            name={name}
            value={value}
            id={id}
            label="Timezone"
            autoFocus={autoFocus}
            required={required}
            className={className}
            onClick={onClick}
            onBlur={onBlur}
            onChange={onChange}
        >

            <option value="">None</option>
            <option value="-12.0">(UTC -12:00) Eniwetok, Kwajalein</option>
            <option value="-11.0">(UTC -11:00) Midway Island, Samoa</option>
            <option value="-10.0">(UTC -10:00) Hawaii</option>
            <option value="-9.0">(UTC -9:00) Alaska</option>
            <option value="-8.0">(UTC -8:00) Pacific Time (US &amp; Canada)
            </option>
            <option value="-7.0">(UTC -7:00) Mountain Time (US &amp; Canada)
            </option>
            <option
                value="-6.0">(UTC -6:00) Central Time (US &amp; Canada), Mexico City
            </option>
            <option
                value="-5.0">(UTC -5:00) Eastern Time (US &amp; Canada), Bogota, Lima
            </option>
            <option
                value="-4.0">(UTC -4:00) Atlantic Time (Canada), Caracas, La Paz
            </option>
            <option value="-3.5">(UTC -3:30) Newfoundland</option>
            <option value="-3.0">(UTC -3:00) Brazil, Buenos Aires, Georgetown
            </option>
            <option value="-2.0">(UTC -2:00) Mid-Atlantic</option>
            <option value="-1.0">(UTC -1:00 hour) Azores, Cape Verde Islands
            </option>
            <option
                value="0.0">(UTC) Western Europe Time, London, Lisbon, Casablanca
            </option>
            <option
                value="1.0">(UTC +1:00 hour) Brussels, Copenhagen, Madrid, Paris
            </option>
            <option value="2.0">(UTC +2:00) Kaliningrad, South Africa</option>
            <option
                value="3.0">(UTC +3:00) Baghdad, Riyadh, Moscow, St. Petersburg
            </option>
            <option value="3.5">(UTC +3:30) Tehran</option>
            <option value="4.0">(UTC +4:00) Abu Dhabi, Muscat, Baku, Tbilisi
            </option>
            <option value="4.5">(UTC +4:30) Kabul</option>
            <option
                value="5.0">(UTC +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent
            </option>
            <option
                value="5.5">(UTC +5:30) Bombay, Calcutta, Madras, New Delhi
            </option>
            <option value="5.75">(UTC +5:45) Kathmandu</option>
            <option value="6.0">(UTC +6:00) Almaty, Dhaka, Colombo</option>
            <option value="7.0">(UTC +7:00) Bangkok, Hanoi, Jakarta</option>
            <option
                value="8.0">(UTC +8:00) Beijing, Perth, Singapore, Hong Kong
            </option>
            <option
                value="9.0">(UTC +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk
            </option>
            <option value="9.5">(UTC +9:30) Adelaide, Darwin</option>
            <option
                value="10.0">(UTC +10:00) Eastern Australia, Guam, Vladivostok
            </option>
            <option
                value="11.0">(UTC +11:00) Magadan, Solomon Islands, New Caledonia
            </option>
            <option
                value="12.0">(UTC +12:00) Auckland, Wellington, Fiji, Kamchatka
            </option>
        </Select>
    )
}
