import React from 'react';
import { TimeZoneSelector } from '../';

describe( '<TimeZoneSelector/>', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <TimeZoneSelector
                className="tz-selector"
                name="tz" value="7.0"
            />
        );

        expect( wrapper.find( 'Select' ).dive().find( 'select' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();

    } );

} );
