//@todo: refactor / rename
function isOutside( target, triggerInputName, skipChildrenOfTarget = [] ) {
    let hide = true;

    if ( target.nodeName == 'INPUT' && target.name && target.name == triggerInputName ) {
        hide = false;
    } else {
        while ( target ) {
            if ( target.className && skipChildrenOfTarget.indexOf( target.className ) > -1 ) {
                hide = false;
                break;
            }
            target = target.parentNode;
        }
    }

    return hide;
}

const formatters = {
    'YYYY': ( date ) => {
        return date.getFullYear();
    },
    'YY': ( date ) => {
        return date.getFullYear().toString().substr( -2 );
    },
    'MM': ( date ) => {
        let m = date.getMonth() + 1;
        return m < 10 ? '0' + m : m;
    },
    'M': ( date ) => {
        return date.getMonth() + 1;
    },
    'DD': ( date ) => {
        let d = date.getDate();
        return d < 10 ? '0' + d : d;
    },
    'D': ( date ) => {
        return date.getDate();
    },
};

function formatDate( date, format = 'YYYY-MM-DD' ) {
    let match = format.match( /(M+)|(\W)|(D+)|(\W)|(Y+)|(\W)/gi );
    let result = '';
    let error = false;
    if ( match && match.length > 0 ) {
        match.forEach( ( m ) => {
            if ( /\w/.test( m ) ) {
                let formatted = formatters[ m ]( date );
                if ( isNaN( formatted ) ) {
                    error = true;
                }
                result += formatted;
            }
            else {
                // Separator
                result += m;
            }
        } )
    }

    return !error ? result : null;
}

function isPopupOutsideViewportBottom( elem ) {
    return elem.getBoundingClientRect().bottom > window.innerHeight;
}

export  {
    isOutside,
    formatters,
    formatDate,
    isPopupOutsideViewportBottom
}