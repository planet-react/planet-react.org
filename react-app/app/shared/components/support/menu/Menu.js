import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

Menu.propTypes = {
    type: PropTypes.oneOf( [ 'pill', 'underline' ] ),
    alignItems: PropTypes.string,
    children: PropTypes.node,
};

Menu.defaultProps = {
    type: 'pill',
    alignItems: 'left'
};

export default function Menu( { type, alignItems, children, ...rest } ) {

    let classes = [ 'menu' ];
    switch ( type ) {
        case 'pill':
            classes.push( 'menu-pill' );
            break;
        case 'underline':
            classes.push( 'menu-underline' );
            break;
        default:
            classes.push( 'menu-pill' );
    }

    switch ( alignItems ) {
        case 'right':
            classes.push( 'menu-align-right' );
            break;
        case 'center':
            classes.push( 'menu-align-center' );
            break;
        default:
            classes.push( 'menu-left' );
    }

    classes = classes.join( ' ' );

    return (
        <ul className={classes} {...rest}>
            {children}
        </ul>
    )
}