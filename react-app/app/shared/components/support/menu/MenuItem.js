import React from 'react';
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen';

require( './style.scss' );

MenuItem.propTypes = {
    path: PropTypes.string,
    exact: PropTypes.bool,
    children: PropTypes.node,
    isActive: PropTypes.func,
};

export default function MenuItem( { path, exact, isActive, children, ...rest } ) {
    return (
        <li {...rest}>
            <ScreenLink path={path} exact={exact} isActive={isActive}>
                {children}
            </ScreenLink>
        </li>
    )
}

