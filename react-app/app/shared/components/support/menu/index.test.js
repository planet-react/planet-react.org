import React from 'react';
import { Menu, MenuItem } from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {
    let tree = renderer.create(
        <MockProviders>
            <Menu type="pill">
                <MenuItem exact path="a">
                    Menu Item 1
                </MenuItem>
                <MenuItem path="a/b">
                    Menu Item 2
                </MenuItem>
                <MenuItem path="a/b/c">
                    Menu Item 3
                </MenuItem>
            </Menu>
        </MockProviders>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );