import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from 'react-icons/lib/fa/close';

require( './style.scss' );

export default class Message extends Component {

    static propTypes = {
        context: PropTypes.oneOf( [ 'warning', 'success', 'error', 'info', 'transparent' ] ),
        align: PropTypes.string,
        closeable: PropTypes.bool,
        closeCallback: PropTypes.func,
        children: PropTypes.node,
        className: PropTypes.string,
        style: PropTypes.object,
    };

    static defaultProps = {
        context: 'transparent',
        align: 'left',
        closeable: false
    };

    state = {
        hidden: false
    };

    onClose() {
        this.setState( { hidden: true } );
        if ( this.props.closeCallback ) {
            this.props.closeCallback();
        }
    }

    render() {

        const { context, align, closeable, children,className, style } = this.props;

        let classes = [
            'message',
            context ? `message--${context}` : null,
            `message--${align}`
        ];
        if ( className ) {
            classes.push( className );
        }
        classes = classes.join( ' ' );

        let realStyle = Object.assign( {}, style, {
            display: this.state.hidden ? 'none' : ''
        } );

        return (
            <div className={classes} style={realStyle}>
                <div>
                    {children}
                </div>
                {closeable &&
                <button
                    className="message-close"
                    onClick={this.onClose.bind( this )}
                >
                    <CloseIcon size={10}/>
                </button>}
            </div>
        )
    }
}