import React from 'react';
import Message from './';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    let tree = renderer.create(
        <Message>Hello, World!</Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'can render nodes in children', () => {
    let tree = renderer.create(
        <Message>
            This is a <b>bold</b> message!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'has renders with the correct `align` css class', () => {
    let tree = renderer.create(
        <Message>
            Default is left
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message align="left">
            This is a left aligned message!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message align="center">
            This is a centered message!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message align="right">
            This is a right aligned message!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );

it( 'supports attributes', () => {
    let tree = renderer.create(
        <Message style={{margin: '1em'}} data-custom-data="cheese">
            Hello, World!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
});
it( 'supports className', () => {
    let tree = renderer.create(
        <Message className="extra-class-name-1 extra-class-name-2">
            Hello, World!
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
});

it( 'has renders with the correct `context` css class', () => {
    let tree = renderer.create(
        <Message>
            Default none
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message context="transparent">
            This message is has a transparent style
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message context="warning">
            This is a warning message
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message context="success">
            This is a success message
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message context="error">
            This is a error message
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Message context="info">
            This is a information message
        </Message>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

} );

it( 'it has the expected markup', () => {
    const wrapper = mount(
        <Message closeable>The message</Message>
    );

    expect( wrapper.find( '.message > div' ).length ).toBe( 1 );
    expect( wrapper.find( '.message > button' ).length ).toBe( 1 );
} );

it( 'it is hidden when X button is clicked', () => {
    const wrapper = mount(
        <Message closeable>The message</Message>
    );

    expect( wrapper.state().hidden ).toBe( false );
    wrapper.find( 'button' ).simulate( 'click' );
    expect( wrapper.state().hidden ).toBe( true );
} );
