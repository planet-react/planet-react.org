import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/support/button';
import { createPositionStyles, positions } from './popupPositions';
import CloseIcon from 'react-icons/lib/fa/close';

require( './style.scss' );

//@todo: close button and press escape for close

export default class Popup extends Component {

    static propTypes = {
        isOpen: PropTypes.bool,
        target: PropTypes.string.isRequired,
        position: PropTypes.string,
        onClose: PropTypes.func.isRequired,
        showCloseButton: PropTypes.bool,
        stick: PropTypes.bool,
        modal: PropTypes.bool,
        width: PropTypes.string,
        title: PropTypes.string,
        children: PropTypes.node,
        backgroundColor: PropTypes.string,
        className: PropTypes.string,
        openBodyClass: PropTypes.string,
        exitAnimationName: PropTypes.string,
    };

    static defaultProps = {
        position: positions.TARGET_TOP_LEFT,
        showCloseButton: true,
        modal: true,
        backgroundColor: '#fff',
        openBodyClass: 'body-popup-open',
        exitAnimationName: 'ani-zoom-out'
    };

    static positions = positions;

    static ids = 0;

    static componentName = null;

    defaultClassName = 'popup';

    initialState = {
        transform: null,
        isVisible: false,
        display: 'none',
    };

    constructor( props ) {
        super( props );
        this.state = this.initialState;
        this.componentName = 'popup-' + (Popup.ids++);
    }

    componentDidMount() {
        const popup = this.getPopupElement();
        if ( popup ) {
            this.getPopupElement().addEventListener( 'animationend', this.onExitAnimationEnd );
        }

        if ( this.props.isOpen ) {
            this.updatePosition();
            this.setState( {
                isVisible: true
            } );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.isOpen ) {
            this.updatePosition();

            document.querySelector( 'body' ).classList.add( nextProps.openBodyClass );
        }
        this.setState( {
            isVisible: nextProps.isOpen
        } );
    }

    componentWillUnmount() {
        const popup = this.getPopupElement();
        if ( popup ) {
            this.getPopupElement().removeEventListener( 'animationend', this.onExitAnimationEnd );
        }
        window.removeEventListener( 'resize', this.onResizeWindow );
        window.removeEventListener( 'keydown', this.onClose );
    }

    getTargetElement() {
        return document.querySelector( this.props.target );
    }

    getPopupElement() {
        return document.querySelector( `[data-name="${this.componentName}"]` );
    }

    updatePosition() {
        const { position, isOpen, stick } = this.props;
        const targetRect = this.getTargetElement().getBoundingClientRect();
        const scrollTop = document.documentElement.scrollTop;

        this.setState( {
            display: '',
        }, () => {
            const popupRect = this.getPopupElement().getBoundingClientRect();
            let positionStyles = createPositionStyles( position, popupRect, targetRect, scrollTop, isOpen, stick );
            this.setState( positionStyles );
        } )
    }

    onExitAnimationEnd = ( event ) => {
        const { onClose, openBodyClass, exitAnimationName } = this.props;
        if ( event.animationName.indexOf( exitAnimationName ) > -1 ) {
            this.setState( this.initialState );
            document.querySelector( 'body' ).classList.remove( openBodyClass );
            if ( onClose ) {
                onClose();
            }
        }
    };

    onResizeWindow = () => {
        if ( this.props.isOpen ) {
            this.updatePosition();
        }
    };

    onClose = ( event ) => {
        if ( event.type == 'keydown' ) {
            if ( event.keyCode != 27 ) {
                return;
            }
        }
        this.setState( {
            isVisible: false
        } );
    };

    render() {
        const {
            title,
            children,
            isOpen,
            position,
            showCloseButton,
            modal,
            stick,
            width,
            className,
            backgroundColor,
            style
        } = this.props;

        const { top, left, transform, isVisible } = this.state;

        let popupClass = [ this.defaultClassName, className ];
        popupClass.push( stick ? 'popup--stick' : '' );
        popupClass.push( isVisible ? 'popup--enter' : 'popup--exit' );
        popupClass = popupClass.join( ' ' );

        const popupRealStyle = Object.assign( {
            top: top,
            left: left,
            display: isOpen ? '' : 'none',
            transform: transform,
            backgroundColor: backgroundColor
        }, { width: width }, style );

        return (
            <div>

                {modal &&
                <div
                    className={'popup-modal-wrapper' + (isVisible ? ' popup-modal-wrapper--open' : '')}
                    onClick={this.onClose}
                >
                </div>}

                <div
                    data-name={this.componentName}
                    data-position-type={position.startsWith( 'screen' ) ? 'at-screen' : 'at-target'}
                    className={popupClass}
                    style={popupRealStyle}
                >
                    {title &&
                    <h4>{title}</h4>}

                    {showCloseButton &&
                    <Button
                        className="popup__close-button"
                        color="transparent"
                        size="medium"
                        onClick={this.onClose}
                        icon={CloseIcon}
                        iconColor="#acacac"
                    />
                    }

                    {isOpen && children}
                </div>
            </div>
        );
    }
}