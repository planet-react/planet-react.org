export const positions = {
    TARGET_TOP_LEFT: 'target-top-left',
    TARGET_TOP_CENTER: 'target-top-center',
    TARGET_TOP_RIGHT: 'target-top-right',
    TARGET_TOP_LEFT_INSIDE: 'target-top-left-inside',
    TARGET_MIDDLE_LEFT: 'target-middle-left',
    TARGET_MIDDLE_CENTER: 'target-middle-center',
    TARGET_MIDDLE_RIGHT: 'target-middle-right',
    TARGET_BOTTOM_LEFT: 'target-bottom-left',
    TARGET_BOTTOM_CENTER: 'target-bottom-center',
    TARGET_BOTTOM_RIGHT: 'target-bottom-right',
    SCREEN_CENTER: 'screen-center',
};

function getTop( targetRect, popupRect ) {
    return (targetRect.top - popupRect.height);
}

function getMiddle( targetRect, popupRect ) {
    return targetRect.top + (targetRect.height / 2) - (popupRect.height / 2);
}

function getCenter( targetRect, popupRect ) {
    return targetRect.right - (targetRect.width / 2) - (popupRect.width / 2);
}

/**
 * @param {string} position
 * @param {DOMRect} popupRect
 * @param {DOMRect} targetRect
 * @param {number} scrollTop
 * @param {boolean} isOpen
 * @param {boolean} stick
 * @returns {*}
 */
export function createPositionStyles( position, popupRect, targetRect, scrollTop, isOpen, stick ) {

    let style;
    switch ( position ) {
        case positions.TARGET_TOP_LEFT_INSIDE:
            style = {
                top: targetRect.top,
                left: targetRect.left
            };
            break;

        case positions.TARGET_TOP_LEFT:
            style = {
                top: getTop( targetRect, popupRect ),
                left: targetRect.left - popupRect.width
            };
            break;
        case positions.TARGET_TOP_CENTER:
            style = {
                top: getTop( targetRect, popupRect ),
                left: getCenter( targetRect, popupRect )
            };
            break;
        case positions.TARGET_TOP_RIGHT:
            style = {
                top: getTop( targetRect, popupRect ),
                left: targetRect.right
            };
            break;

        case positions.TARGET_MIDDLE_LEFT:
            style = {
                top: getMiddle( targetRect, popupRect ),
                left: targetRect.left - popupRect.width
            };
            break;
        case positions.TARGET_MIDDLE_CENTER:
            style = {
                top: getMiddle( targetRect, popupRect ),
                left: getCenter( targetRect, popupRect )
            };
            break;
        case positions.TARGET_MIDDLE_RIGHT:
            style = {
                top: getMiddle( targetRect, popupRect ),
                left: targetRect.right
            };
            break;

        case positions.TARGET_BOTTOM_LEFT:
            style = {
                top: targetRect.bottom,
                left: targetRect.left - popupRect.width
            };
            break;
        case positions.TARGET_BOTTOM_CENTER:
            style = {
                top: targetRect.bottom,
                left: getCenter( targetRect, popupRect )
            };
            break;
        case positions.TARGET_BOTTOM_RIGHT:
            style = {
                top: targetRect.bottom,
                left: targetRect.right
            };
            break;

        case positions.SCREEN_CENTER:
            style = {
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
            };
            break;
        default:
            style = {
                top: (targetRect.top - popupRect.height),
                left: targetRect.left - popupRect.width
            };
            break;
    }

    if ( position != positions.SCREEN_CENTER ) {
        style.top = style.top + (stick ? scrollTop : 0) + 'px';
        style.left = style.left + 'px';
        style.transform = null;
    }

    if ( isOpen ) {
        style.display = '';
    }

    return style;
}