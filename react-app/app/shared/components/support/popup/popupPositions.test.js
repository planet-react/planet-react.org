import { createPositionStyles, positions } from './popupPositions';

describe( 'popupPositions', () => {
    describe( 'positions', () => {
        it( 'have the expected constants', () => {
            expect( positions.TARGET_TOP_LEFT ).toBe( 'target-top-left' );
            expect( positions.TARGET_TOP_CENTER ).toBe( 'target-top-center' );
            expect( positions.TARGET_TOP_RIGHT ).toBe( 'target-top-right' );
            expect( positions.TARGET_TOP_LEFT_INSIDE ).toBe( 'target-top-left-inside' );
            expect( positions.TARGET_MIDDLE_LEFT ).toBe( 'target-middle-left' );
            expect( positions.TARGET_MIDDLE_CENTER ).toBe( 'target-middle-center' );
            expect( positions.TARGET_MIDDLE_RIGHT ).toBe( 'target-middle-right' );
            expect( positions.TARGET_BOTTOM_LEFT ).toBe( 'target-bottom-left' );
            expect( positions.TARGET_BOTTOM_CENTER ).toBe( 'target-bottom-center' );
            expect( positions.TARGET_BOTTOM_RIGHT ).toBe( 'target-bottom-right' );
            expect( positions.SCREEN_CENTER ).toBe( 'screen-center' );

        } );

    } );
    describe( 'createPositionStyles()', () => {

        it( 'should return', () => {
            expect( createPositionStyles(
                positions.TARGET_TOP_LEFT,
                {
                    width: 100,
                    height: 100,
                    top: 10,
                    right: 20,
                    bottom: 42,
                    left: 19,
                    x: 19,
                    y: 10
                },
                {
                    width: 50,
                    height: 30,
                    top: 433,
                    right: 21,
                    bottom: 466,
                    left: 23,
                    x: 23,
                    y: 433
                },
                30,
                true,
                true,
            ) ).toEqual( {
                "display": "",
                "left": "-77px",
                "top": "363px",
                "transform": null
            } );
        } );
    } );
} );