import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

//@todo: fix verticalSpace
Spinner.propTypes = {
    size: PropTypes.number,
    text: PropTypes.string,
    verticalSpace: PropTypes.oneOfType( [
        PropTypes.string,
        PropTypes.number
    ] ),
    children: PropTypes.node
};
Spinner.defaultProps = {
    size: 39,
    text: '',
    verticalSpace: '87px 87px',
    center: true
};

export default function Spinner( { size, text, verticalSpace, center } ) {
    const style = {
        padding: verticalSpace
    };

    const spinnerSize = {
        width: size + 'px',
        height: size + 'px'
    };

    const wrapperStyle = {
        display: center ? 'inline-block' : 'flex',
        margin: center ? '0 auto' : ''
    };

    const textStyle = {
        margin: center ? '' : '0 0 0 .7em'
    };

    return (
        <div className="spinner" style={style}>
            <div className="spinner-wrapper" style={wrapperStyle}>
                <div className="spinner-spinner" style={spinnerSize}>

                </div>
                {text != '' &&
                <div className="spinner-text" style={textStyle}>{text}</div>}
            </div>
        </div>
    )
}
