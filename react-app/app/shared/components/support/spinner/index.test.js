import React from 'react';
import renderer from 'react-test-renderer';
import Spinner from './';

it( 'should render', () => {
    let tree = renderer.create(
        <Spinner/>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Spinner size={16}/>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Spinner size={22} text="Hello, World"/>
    ).toJSON();

    expect( tree ).toMatchSnapshot();
} );

it( 'should render', () => {
    let tree = renderer.create(
        <Spinner/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Spinner size={16}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();

    tree = renderer.create(
        <Spinner size={22} text="Hello, World" verticalSpace={'20px 20px'}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );
