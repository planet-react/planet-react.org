import React from 'react';
import PropTypes from 'prop-types';
import { ScreenLink } from 'components/screen';

require( './style.scss' );

Tags.propTypes = {
    path: PropTypes.string.isRequired,
    tags: PropTypes.array,
};

export default function Tags( { tags, path } ) {
    if ( !tags ) {
        return <div/>;
    }
    return (
        <ul className="tag-list">
            {tags.map( ( tag ) => {
                const tagCount = tag.tag_count || 0;
                const fontSize = 1 + (1.1 * tagCount / 33);

                return (
                    <li
                        key={tag.tag_slug}
                        className="tag-list__tag"
                        style={tagCount ? { fontSize: (fontSize) + 'em' } : null}
                    >
                        <ScreenLink
                            path={`${path}/${tag.tag_slug}`}
                        >
                            {tag.tag_slug}

                            {tag.tag_count &&
                            <span> ({tag.tag_count})</span>}
                        </ScreenLink>
                    </li>
                )
            } )}
        </ul>
    );
}