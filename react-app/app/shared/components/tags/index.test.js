import React from 'react';
import Tags from './';
import renderer from 'react-test-renderer';
import MockProviders from '../../../../internals/testutils/MockProviders'

it( 'renders correctly', () => {

    const tags = [
        {
            tag_slug: 'javascript',
            tag_name: 'Javascript',
        },
        {
            tag_slug: 'react',
            tag_name: 'React',
        },
        {
            tag_slug: 'react-router',
            tag_name: 'React-Router',
        },

    ];

    const tree = renderer.create(
        <MockProviders>
            <Tags tags={tags} path="/tags"/>
        </MockProviders>
    ).toJSON();

    expect( tree ).toMatchSnapshot();

} );