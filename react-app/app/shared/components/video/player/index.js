import React from 'react';
import PropTypes from 'prop-types';

require( './style.scss' );

VideoPlayer.propTypes = {
    videoId: PropTypes.string,
    autoPlay: PropTypes.bool,
};

VideoPlayer.defaultProps = {
    autoPlay: false
};

export default function VideoPlayer( { videoId, autoPlay } ) {
    const play = autoPlay ? 1 : 0;
    const autoPlayParam = videoId.indexOf( '?' ) == -1 ? `?autoplay=${play}` : `&autoplay=${play}`;
    const src = `https://www.youtube.com/embed/${videoId}${autoPlayParam}`;

    return (
        <div className="video-player-container">
            <iframe
                src={src}
                frameBorder="0"
                allowFullScreen
            />
        </div>
    );
}
