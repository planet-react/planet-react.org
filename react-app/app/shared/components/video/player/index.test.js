import React from 'react';
import VideoPlayer from './';

describe( 'VideoPlayer', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow( <VideoPlayer videoId="abcGfe"/> );
        expect( wrapper.find( '.video-player-container' ).exists() ).toBe( true );
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'has autoplay', () => {
        const wrapper = shallow( <VideoPlayer videoId="jklMzy" autoPlay/> );
        expect( wrapper ).toMatchSnapshot();
    } );

} );

