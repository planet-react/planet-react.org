import React, { Component } from 'react';
import PropTypes from 'prop-types';
import A from 'components/support/a';
import Popup from 'components/support/popup';
import { VideoPlayer } from 'components/video';
import VideoCameraIcon from 'react-icons/lib/io/ios-videocam';

export default class VideoPlayList extends Component {

    static propTypes = {
        title: PropTypes.string,
        data: PropTypes.shape( {
            tabs: PropTypes.array,
        } ).isRequired,
    };

    state = {
        selectedTabIndex: 0,
        showAll: false,
        isVideoPopupOpen: false,
        selectedVideo: null,
    };

    onSelectTab = ( event ) => {
        const tabIndex = event.currentTarget.getAttribute('data-tabindex')
            ? event.currentTarget.getAttribute('data-tabindex')
            : null;

        if ( !tabIndex ) {
            return;
        }

        this.setState( {
            selectedTabIndex: Number(tabIndex)
        } )
    };

    onOpenVideoPopup = ( event ) => {
        const link = event.currentTarget.getAttribute('data-videolink');
        const title = event.currentTarget.getAttribute('data-videolink');
        let [ videoId, time ] = link.split( 'https://www.youtube.com/watch?v=' )[ 1 ]
            .split( '&amp;t=' );
        time = time.replace( 's', '' );

        this.setState( {
            selectedVideo: {
                videoId: `${videoId}?start=${time}`,
                title: title
            },
            isVideoPopupOpen: true
        } );
    };

    onCloseVideoPopup = () => {
        this.setState( {
            isVideoPopupOpen: false,
            selectedVideo: null
        } );
    };

    onToggleShowAll = () => {
        this.setState( {
            showAll: !this.state.showAll
        } )
    };

    renderTabs( selectedTabIndex ) {
        const { tabs } = this.props.data;
        return (
            <ul className="playlist-tabs horizontal-list">
                {tabs.map( ( tab, i ) => {
                    return (
                        <li key={i}>
                            <A
                                data-tabindex={i}
                                onClick={this.onSelectTab}
                                style={{
                                    fontWeight: selectedTabIndex == i ? 'bold' : '',
                                    fontSize: selectedTabIndex == i ? '1rem' : '',
                                }}>
                                {tab.title}
                            </A>
                        </li>)
                } )}
            </ul>
        );
    }

    renderTabPanel( selectedTab ) {
        const { showAll } = this.state;

        let sessions = showAll
            ? selectedTab.sessions
            : selectedTab.sessions.filter( ( session, i ) => i < 3 );

        return (
            <div className="playlist-panels">
                <ul>
                    {sessions.map( ( session, i ) =>
                        <li key={i}>

                            <a href={session.link}>
                                {session.title}
                            </a>

                            <A
                                onClick={this.onOpenVideoPopup}
                                data-videolink={session.link}
                                data-videotitle={session.title}
                                style={{marginLeft:'.5em'}}
                            >
                                <VideoCameraIcon size={19} color="#acacac"/>
                                <span>Watch</span>
                            </A>
                        </li>
                    )}
                </ul>
                <div>
                    <A onClick={this.onToggleShowAll}>
                        {showAll ? 'Show less' : 'Show more'}
                    </A>
                </div>
            </div>
        )
    }

    render() {
        const { title, data } = this.props;
        const { selectedTabIndex, isVideoPopupOpen, selectedVideo } = this.state;
        const selectedTabData = data.tabs[ selectedTabIndex ];
        const tabs = this.renderTabs( selectedTabIndex );
        const panel = this.renderTabPanel( selectedTabData );
        return (
            <div>
                {title &&
                <h3>
                    {title}
                </h3>}

                {tabs}
                {panel}

                <Popup
                    isOpen={isVideoPopupOpen}
                    onClose={this.onCloseVideoPopup}
                    target="body"
                    position={Popup.positions.SCREEN_CENTER}
                    style={{width: '75vw'}}
                >
                    <VideoPlayer
                        autoPlay
                        videoId={selectedVideo && selectedVideo.videoId}
                    />
                </Popup>
            </div>
        )
    }
}

