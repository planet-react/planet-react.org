import React from 'react';
import VideoPlayList from './';

const mockData = {
    "tabs": [
        {
            "title": "Day 1",
            "sessions": [
                {
                    "link": "https://www.youtube.com/watch?v=GverPLbSqEE&amp;t=4778s",
                    "title": "Intro",
                    "twitter": null
                },
                {
                    "link": "https://www.youtube.com/watch?v=GverPLbSqEE&amp;t=4950s",
                    "title": "Michael Jackson Talk (UNPKG The CDN for everything on nom)",
                    "twitter": "https://twitter.com/mjackson"
                },
                {
                    "link": "https://www.youtube.com/watch?v=GverPLbSqEE&amp;t=6940s",
                    "title": "Shirley Wu (d3 & React)",
                    "twitter": "https://twitter.com/sxywu"
                },
                {
                    "link": "https://www.youtube.com/watch?v=GverPLbSqEE&amp;t=9922s",
                    "title": "Devon Lindsey (A hand wave of React for all your Internet of Thangs)",
                    "twitter": "https://twitter.com/devonbl"
                },
            ]
        },
        {
            "title": "Day 2",
            "sessions": [
                {
                    "link": "https://www.youtube.com/watch?v=HRJ_VjkmyiE&amp;t=3290s",
                    "title": "Preethi Kasireddy (We All Started Somewhere)",
                    "twitter": "https://twitter.com/iam_preethi"
                },
                {
                    "link": "https://www.youtube.com/watch?v=HRJ_VjkmyiE&amp;t=4802s",
                    "title": "Evan Czaplicki (Convergent Evolution)",
                    "twitter": "https://twitter.com/czaplic"
                },
                {
                    "link": "https://www.youtube.com/watch?v=HRJ_VjkmyiE&amp;t=8832s",
                    "title": "Henry Zhu (So how does Babel even work?)",
                    "twitter": "https://twitter.com/left_pad"
                },
                {
                    "link": "https://www.youtube.com/watch?v=HRJ_VjkmyiE&amp;t=10239s",
                    "title": "Jennifer Van (Scaling My First Enterprise React App! )",
                    "twitter": "https://twitter.com/sugargreenbean"
                },
                {
                    "link": "https://www.youtube.com/watch?v=HRJ_VjkmyiE&amp;t=11720s",
                    "title": "Sean Larkin (Everything is a plugin!! Mastering webpack from the inside out)",
                    "twitter": "https://twitter.com/thelarkinn"
                },

            ]
        }
    ]
};

describe( 'VideoPlayList', () => {
    it( 'renders correctly', () => {
        const wrapper = shallow(
            <VideoPlayList
                title="Play List Test"
                data={mockData}
            />
        );

        expect( wrapper.find( 'h3' ).text() ).toBe( 'Play List Test' );
        expect( wrapper.find( 'ul.playlist-tabs li' ).length ).toBe( 2 );
        expect( wrapper.find( 'div.playlist-panels' ).length ).toBe( 1 );
        expect( wrapper.state() ).toEqual( {
            selectedTabIndex: 0,
            showAll: false,
            isVideoPopupOpen: false,
            selectedVideo: null,
        } );
        expect( wrapper ).toMatchSnapshot();
    } );

    it( 'should render video list', () => {
        const wrapper = mount(
            <VideoPlayList
                title="Play List Test"
                data={mockData}
            />
        );

        expect( wrapper.find( '.playlist-panels ul li' ).length ).toBe( 3 );
    } );

    it( 'should show more', () => {
        const wrapper = mount(
            <VideoPlayList
                title="Play List Test"
                data={mockData}
            />
        );

        expect( wrapper.state().showAll ).toBe( false );

        expect( wrapper.find( '.playlist-panels ul li' ).length ).toBe( 3 );

        wrapper.find( '.playlist-panels > div a' ).simulate( 'click' );

        expect( wrapper.state().showAll ).toBe( true );

        expect( wrapper.find( '.playlist-panels ul li' ).length ).toBe( 4 );

    } );

    it( 'should select the second tab', () => {
        const wrapper = mount(
            <VideoPlayList
                title="Play List Test"
                data={mockData}
            />
        );
        expect( wrapper.state().selectedTabIndex ).toBe( 0 );

        wrapper.find( '.playlist-tabs a[data-tabindex=1]' ).simulate( 'click' );

        expect( wrapper.state().selectedTabIndex ).toBe( 1 );

    } );

    //@todo: create enzyme test for popup first (fix getBoundingClientRect issue)
    // it( 'opens the popup', () => {
    //     const wrapper = mount(
    //         <VideoPlayList
    //             title="Play List Test"
    //             data={mockData}
    //         />
    //     );
    //     expect( wrapper.state().isVideoPopupOpen ).toBe( false);
    //
    //     wrapper.find( '.playlist-panels ul li a[data-videolink]' ).first().simulate( 'click' );
    //
    //     //expect( wrapper.state().selectedTabIndex ).toBe( 1 );
    //
    // } );

} );

