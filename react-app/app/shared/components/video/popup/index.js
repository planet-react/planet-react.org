import React from "react";
import PropTypes from 'prop-types';
import Popup from 'components/support/popup';
import VideoPlayer from '../player';

VideoPopup.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onCloseVideoPopup: PropTypes.func.isRequired,
    videoId: PropTypes.string,
    autoPlay: PropTypes.bool,
};

VideoPopup.defaultProps = {
    autoPlay: false
};

export default function VideoPopup( { isOpen, onCloseVideoPopup, videoId, autoPlay } ) {
    return (
        <Popup
            isOpen={isOpen}
            onClose={onCloseVideoPopup}
            target="body"
            position={Popup.positions.SCREEN_CENTER}
            style={{ width: '75vw' }}
        >
            <VideoPlayer
                autoPlay={autoPlay}
                videoId={videoId}
            />
        </Popup>
    );
}