import { printAST } from 'react-apollo';

import CountriesQuery from './CountriesQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( CountriesQuery );
    expect( ast ).toMatchSnapshot()
} );