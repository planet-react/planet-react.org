import { printAST } from 'react-apollo';

import EventQuery from './EventQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( EventQuery );
    expect( ast ).toMatchSnapshot()
} );