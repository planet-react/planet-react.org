import { printAST } from 'react-apollo';

import FeedQuery from './FeedQuery.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( FeedQuery );
    expect( ast ).toMatchSnapshot()
} );