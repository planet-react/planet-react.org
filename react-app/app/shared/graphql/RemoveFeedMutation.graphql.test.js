import { printAST } from 'react-apollo';

import RemoveFeedMutation from './RemoveFeedMutation.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( RemoveFeedMutation );
    expect( ast ).toMatchSnapshot()
} );