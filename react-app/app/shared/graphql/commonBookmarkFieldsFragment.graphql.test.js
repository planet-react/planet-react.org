import { printAST } from 'react-apollo';

import commonBookmarkFieldsFragment from './commonBookmarkFieldsFragment.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( commonBookmarkFieldsFragment);
    expect( ast ).toMatchSnapshot()
} );