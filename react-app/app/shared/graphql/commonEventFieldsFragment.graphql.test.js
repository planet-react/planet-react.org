import { printAST } from 'react-apollo';

import commonEventFieldsFragment from './commonEventFieldsFragment.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( commonEventFieldsFragment );
    expect( ast ).toMatchSnapshot()
} );