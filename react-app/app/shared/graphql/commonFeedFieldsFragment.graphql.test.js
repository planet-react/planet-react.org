import { printAST } from 'react-apollo';

import commonFeedFieldsFragment from './commonFeedFieldsFragment.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( commonFeedFieldsFragment );
    expect( ast ).toMatchSnapshot()
} );