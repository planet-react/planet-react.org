import { printAST } from 'react-apollo';

import commonPostFieldsFragment from './commonPostFieldsFragment.graphql';

it( 'has a known query shape', () => {
    let ast = printAST( commonPostFieldsFragment );
    expect( ast ).toMatchSnapshot()
} );