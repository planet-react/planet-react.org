import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './app';
import services from './services';

const props = { services };

/**
 * App entry
 */
(async function () {

    // Start the application.
    ReactDOM.render(
        <AppContainer>
            <App {...props}/>
        </AppContainer>,
        document.getElementById( 'root' )
    );

    // Hot Module Replacement API
    if ( module.hot ) {
        module.hot.accept( './app', () => {
            const NextApp = require( './app' ).default;
            ReactDOM.render(
                <AppContainer>
                    <NextApp {...props}/>
                </AppContainer>
                ,
                document.getElementById( 'root' )
            );
        } );
    }
}());
