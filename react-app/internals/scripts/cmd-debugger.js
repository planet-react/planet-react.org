const fs = require( 'fs' );
const watch = require( 'node-watch' );
const sqlFormatter = require( 'sql-formatter' );
const banner = require( './utils/banner.js' );
const CACHE_FILE = './storage/app/debugger-cache.json';
const SQL_TOKEN = '[SQL]';

console.log( banner );
console.log( 'Debugger started' );
console.log( '' );

function printArgs( args, count ) {
    args.forEach( ( arg ) => {
        console.log( `[${count+1}]` );
        for ( let i = 0; i < arg.length; i++ ) {

            let value = typeof arg[ i ] == 'object' ? JSON.stringify( arg[ i ], null, 4 ) : arg[ i ];
            if ( value.indexOf( SQL_TOKEN) == 0 ) {
                value = sqlFormatter.format( value.split( '[SQL]' )[ 1 ] );
            }
            console.log( value );
        }
        console.log( '--------------------------------------------------------------------------------' );
    } );
}

watch( CACHE_FILE, {}, ( eventType, filename ) => {
    if ( filename && eventType == 'update' ) {

        const contents = fs.readFileSync( CACHE_FILE ).toString();

        try {
            const json = JSON.parse( contents );
            process.stdout.write( '\033c=START==========================================================================\n' );
            json.entries
                .forEach( printArgs );
        } catch ( exception ) {
            console.log( 'Debugger ERROR' );
            console.log( exception )
        }
    }
} );