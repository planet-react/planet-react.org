require( 'dotenv' ).config();

const webpack = require( 'webpack' );
const WebpackDevServer = require( 'webpack-dev-server' );
const webpackDevConfig = require( '../../webpack.app.development.config' );
const config = require( '../../webpack.app.shared.config' );
const exec = require( './utils/exec' );
const ip = require( './utils/ip' );
const exitHandler = require( './utils/exitHandler' );
const { findProcessLine, getPid } = require( './utils/activeProcesses' );
const log = require( './utils/log' );
const banner = require( './utils/banner.js' );

const APP_DEBUG = process.env.APP_DEBUG;
const DEV_SERVER_URL = config.devServer.url;
const DEV_SERVER_HOST = config.devServer.host;
const DEV_SERVER_PORT = config.devServer.port;
const API_SERVER_URL = config.devServer.proxy.url;
const API_PREFIX = '/' + process.env.PLANET_API_PREFIX;
const IS_REQUEST_LOGGING_ENABLED = process.argv.length > 2 && process.argv[ 2 ] == '-l';
const LARAVEL_VERSION = exec( 'php artisan --version' ).trim();
const DEV_SERVER_VERSION = require( '../../../node_modules/webpack-dev-server/package.json' ).version;

// Check if MySQL is running.
if ( process.env.DB_CONNECTION == 'mysql' && !findProcessLine( 'mysql' ) ) {
    log( '' );
    log( 'Mysql needs to be running. Please start MySQL' );
    log( '' );
    exitHandler( { exit: true } );
}

// Check if the php server is already running.
const phpProcess = findProcessLine( 'php -S 127.0.0.1:' + API_SERVER_URL );
if ( phpProcess ) {
    const pid = getPid( phpProcess );
    log( '' );
    log( '  PHP server is already running' );
    log( '' );
    log( '  Use `kill -9 ' + pid + '` to terminate the process' );
    log( '' );
    exitHandler( { exit: true } );
}

// So the program will not close instantly
process.stdin.resume();

// Handle exit
process.on( 'exit', exitHandler.bind( null, { cleanup: true } ) );
process.on( 'SIGINT', exitHandler.bind( null, { exit: true } ) );
process.on( 'uncaughtException', exitHandler.bind( null, { exit: true } ) );

// Print the banner.
console.log( banner );

// Start the servers.
log( 'Staring PHP server' );
exec( 'nohup php artisan serv --host 127.0.0.1 > /dev/null 2>&1 &' );
log( 'Staring webpack dev server' );

const options = {
    publicPath: webpackDevConfig.output.publicPath,
    hot: true,
    compress: true,
    historyApiFallback: true,
    watchOptions: {
        ignored: /node_modules/
    },
    proxy: [ {
        context: [ API_PREFIX, '/images' ],
        target: API_SERVER_URL,
        secure: false,
        changeOrigin: true
    } ]
};

// Make the dev server available to other devices. Eg. mobile
const ipAddress = ip();
if ( ipAddress && DEV_SERVER_HOST == '0.0.0.0' ) {
    options.public = ipAddress
}

Object.assign( options, { stats: config.devServer.stats } );

const server = new WebpackDevServer( webpack( webpackDevConfig ), options );

server.listen( DEV_SERVER_PORT, DEV_SERVER_HOST, function ( err, result ) {
    if ( err ) {
        return console.log( err );
    }
    log( 'Listening at ' + DEV_SERVER_URL + '/' );
    log( 'Proxy ' + API_SERVER_URL + '/' );
    log( 'IP: ' + ipAddress );
    log( 'DevServer: ' + DEV_SERVER_VERSION );
    log( LARAVEL_VERSION );
    log( `Debug: ${APP_DEBUG}` );
    log( `Request log: ${IS_REQUEST_LOGGING_ENABLED}` );
    if ( APP_DEBUG ) {
        log( '💡 Run `yarn planet:debugger` in a new console window to view debug information' );
    }
    log( '' );
} );

if ( IS_REQUEST_LOGGING_ENABLED ) {
    server.on( 'request', onRequest );
}

function onRequest( req, res ) {
    const url = req.url;
    const responseStatus = res.statusCode;
    const prefix = `[${responseStatus}]: `;

    // [Tue Apr 11 10:37:53 2017] 127.0.0.1:50766 [200]:
    // Log api requests.
    if ( url.indexOf( API_PREFIX ) > -1 ) {
        log( prefix + API_SERVER_URL + url, 'yellow' );
        return;
    }

    // Log router requests.
    if ( req.query[ 'planet-route' ] ) {
        log( prefix + req.query[ 'planet-route' ], 'blue' );
        return;
    }

    // Log anything else.
    log( prefix + DEV_SERVER_URL + url, 'dim' );
}
