#!/usr/bin/env node

const path = require("path");
const exec = require("./utils/exec");

if (process.argv.length < 3) {
    process.exit();
    console.log("Please provide path to file");
}

const prettierCmd = path.resolve(
    __dirname,
    "../../../node_modules/prettier/bin/prettier.js"
);

const configFile = path.resolve(
    __dirname,
    "../../../react-app/prettier.config.json"
);

const fileName = path.resolve(process.argv[2]);

exec(`${prettierCmd} --config ${configFile} --write ${fileName}`);
