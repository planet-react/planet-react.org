const fs = require( 'fs' );

class File {
    static exists( file ) {
        return fs.existsSync( file );
    }

    static getContents( file ) {
        if ( !File.exists( file ) ) {
            return false;
        }

        return fs.readFileSync( file, { encoding: 'utf8' } ).toString('utf8');
    }

    static getLines( file ) {
        return File.getContents(file).toString().trim().split('\n');
    }

    static copy( source, target ) {
        if ( !File.exists( source ) ) {
            return false;
        }
        fs.writeFileSync( target, fs.readFileSync( source ) );
        return true;
    }

    static write( file, contents ) {
        if ( !File.exists( file ) ) {
            return false;
        }
        fs.writeFileSync( file, contents );
        return true;
    }

    static create( file ) {
        fs.writeFileSync( file, '' );
        return true;
    }

    static del( file ) {
        if ( !File.exists( file ) ) {
            return false;
        }
        fs.unlinkSync( file );
        return true;
    }
}

module.exports = File;