const execSync = require( 'child_process' ).execSync;

/**
 * Return a line containing `str` from the ps command output.
 * @param str
 * @return {string|boolean}
 */
function findProcessLine( str ) {
    const ps = execSync( 'ps aux' ).toString( 'utf-8' ).trim();
    if ( ps.indexOf( str ) == 0 ) {
        return false;
    }
    const line = ps.split( '\n' ).filter( l => l.indexOf( str ) > -1 );
    return line && line[ 0 ];
}

function getPid( processLine ) {
    return processLine.trim().match( /^\d+/ )[ 0 ]
}

module.exports = {
    findProcessLine: findProcessLine,
    getPid: getPid
};