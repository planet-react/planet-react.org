const execSync = require( 'child_process' ).execSync;

function exec( command ) {
    //return execSync( command, {stdio: [0, 1, 2]} )
    return execSync( command ).toString();
}

module.exports = exec;
