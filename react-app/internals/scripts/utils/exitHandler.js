/**
 * Called when exit occurs (eg. CTRL+C)
 * @param options
 * @param err
 */
function exitHandler( options, err ) {
    if ( err ) {
        console.log( err.stack );
    }
    if ( options.exit ) {
        process.exit();
    }
}

module.exports = exitHandler;