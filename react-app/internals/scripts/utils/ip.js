function ip() {
    const interfaces = require('os').networkInterfaces();
    let result = [];
    for (let key in interfaces) {
        if (interfaces.hasOwnProperty(key)) {
            let iPv4 = interfaces[key].filter((iface) => {
                return iface.family == 'IPv4' && iface.internal == false;
            });
            if (iPv4.length) {
                result.push(iPv4[0])
            }
        }
    }

    return result[0] ? result[0].address : null;
}

module.exports = ip;
