const chalk = require('chalk');
const format = require('date-fns/format');

function log( val, color = 'reset' ) {

    const date = format(new Date(), 'DD-MMM-YYYY HH:mm:ss.SSS');

    let message = '';
    if ( val instanceof Buffer ) {
        message = val.toString( 'utf8' ).trim().replace( '\n', '' );
    }
    else if ( val instanceof Array || val instanceof Object ) {
        message = JSON.stringify( val );
    }
    else {
        message = String( val );
    }

    if ( message ) {
        process.stdout.write( `[PLANET] ${date} ${chalk[color](message)}\n` );
    }
}

module.exports = log;