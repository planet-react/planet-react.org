const MockDate = require( 'mockdate' );

function setTime( time ) {
    MockDate.set( time );
}

function reset() {
    MockDate.reset();
}

export default {
    setTime,
    reset
}