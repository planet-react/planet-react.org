import React from 'react';
import { ApolloClient } from 'react-apollo';
import { MockedProvider as ApolloProvider } from 'react-apollo/test-utils';
import { MemoryRouter } from 'react-router-dom';
import { ServicesProvider } from 'components/services';
import { Auth0Provider } from 'components/services';
import auth0service from '../../services/auth0/auth0service';
import dateHelper from '../../services/date/dateHelper';

const servicesMock = {
    dateHelper: dateHelper,
};

const apolloClient = new ApolloClient();

export default function MockProviders( { children } ) {
    return (
        <ApolloProvider client={apolloClient}>
            <MemoryRouter>
                <Auth0Provider auth0service={auth0service}>
                    <ServicesProvider services={servicesMock}>
                        {children}
                    </ServicesProvider>
                </Auth0Provider>
            </MemoryRouter>
        </ApolloProvider>
    );
}


