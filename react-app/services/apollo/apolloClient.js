import { ApolloClient, createNetworkInterface } from 'react-apollo';

const networkInterface = createNetworkInterface( {
    uri: __PLANET_GRAPH_QL_ROUTE__,
    opts: {
        credentials: 'same-origin'
    }
} );

networkInterface.use( [ {
    applyMiddleware( req, next ) {
        if ( !req.options.headers ) {
            req.options.headers = {};
            req.options.headers[ 'Authorization' ] = 'Bearer ' + localStorage.getItem( 'id_token' );
        }
        next();
    }
} ] );

const client = new ApolloClient( {
    networkInterface,
    initialState: {
        apollo: {
            data: {
                'ROOT_QUERY': {
                    isScreenLoading: false,
                    isAuthenticated: false,
                    userProfile: null
                },
            },
        },
    }
} );

export default client;

