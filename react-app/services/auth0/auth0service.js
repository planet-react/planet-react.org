import Auth0Lock from 'auth0-lock'

if ( process.env.NODE_ENV == 'development' ) {
    if ( typeof __PLANET_AUTH0_DOMAIN__ == 'undefined' || __PLANET_AUTH0_DOMAIN__ == ''
        || typeof __PLANET_AUTH0_CLIENT_ID__ == 'undefined' || __PLANET_AUTH0_CLIENT_ID__ == ''
        || typeof __PLANET_AUTH0_CLIENT_REDIRECT_URL__ == 'undefined' || __PLANET_AUTH0_CLIENT_REDIRECT_URL__ == ''
    ) {
        console.warn( '%c Warning: Auth0 is not properly configured. See the .env file',
            'background: #222; color: #bada55; padding: .3em' );
    }
}

export default {
    lock: new Auth0Lock( __PLANET_AUTH0_CLIENT_ID__, __PLANET_AUTH0_DOMAIN__, {
        auth: {
            redirectUrl: __PLANET_AUTH0_CLIENT_REDIRECT_URL__,
            responseType: 'token'
        },
        allowForgotPassword: false
    } ),

    fetchUserProfile( accessToken ) {
        return new Promise( ( resolve, reject ) => {
            this.lock.getUserInfo( accessToken, ( error, profile ) => {
                if ( error ) {
                    reject( error );
                } else {
                    resolve( profile );
                }
            } );
        } );
    }

};