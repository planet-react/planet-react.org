import format from 'date-fns/format';
import addMonths from 'date-fns/add_months';
import isSameDay from 'date-fns/is_same_day';
import isSameMonth from 'date-fns/is_same_month';
import isToday from 'date-fns/is_today';

function formatTimezone( tz ) {

    const isNegative = tz.match( /^-/ );
    const parts = tz.split( '.' );

    let hour = parts[ 0 ].replace( /^-/, '' );
    let minute = parts[ 1 ];

    hour = hour < 10 ? '0' + hour : hour;

    switch ( minute ) {
        case '25':
            minute = '15';
            break;
        case '5':
            minute = '30';
            break;
        case '75':
            minute = '45';
            break;
        default:
            minute = '00';
            break;
    }

    return isNegative ? '-' + (hour + '' + minute) : (hour + '' + minute);
}

function timeTo24Format( time ) {
    if ( time.indexOf( 'PM' ) > -1 ) {
        time = time.replace( / PM/gim, '' );

        let parts = time.split( ':' );
        // 0, 2, 4, 7

        let h = 12;
        for ( let i = 0; i < parts[ 0 ]; i++ ) {
            h++;

        }
        return h + ':' + parts[ 1 ];

    }
    else {
        time = time.replace( / AM/gim, '' );
        let parts = time.split( ':' );
        let h = parts[ 0 ] < 10 ? '0' + parts[ 0 ] : parts[ 0 ];
        return h + ':' + parts[ 1 ];
    }
}

export default {
    format,
    addMonths,
    isSameDay,
    isSameMonth,
    isToday,
    formatTimezone,
    timeTo24Format,
}