import dateHelper from './dateHelper';

describe( 'dateHelper', () => {

    describe( '.format()', () => {
        it( 'should format the date string as expected', () => {
            expect( dateHelper.format( '2017-05-28 10:43:12', 'ddd MMM D - h:mma' ) ).toBe( 'Sun May 28 - 10:43am' );
            expect( dateHelper.format( '2016-07-09 23:12:02', 'ddd MMM D - HH:mma' ) ).toBe( 'Sat Jul 9 - 23:12pm' );
        } );
        it( 'should format the date object as expected', () => {
            let date = new Date( 2014, 3, 9 );
            expect( dateHelper.format( date, 'MMM D YYYY, HH:mm' ) ).toBe( 'Apr 9 2014, 00:00' );
            date = new Date( 2017, 4, 28 );
            expect( dateHelper.format( date, 'MMM YYYY' ) ).toBe( 'May 2017' );
        } );
    } );

    describe( '.addMonths()', () => {
        it( 'should add month to date string', () => {
            expect( dateHelper.addMonths( '2016-07-09', 2 ).getMonth() ).toBe( 8 );
        } );
        it( 'should add month to date object', () => {
            expect( dateHelper.addMonths( new Date( 2011, 3, 5 ), 5 ).getMonth() ).toBe( 8 );
        } );
    } );

    describe( '.isSameDay()', () => {
        it( 'should return true when days in the given date string is the same', () => {
            expect( dateHelper.isSameDay( '2017-05-20 07:10:17', '2017-05-20 10:43:19' ) ).toBe( true );
        } );
        it( 'should return true when days in the given date object is the same', () => {
            expect( dateHelper.isSameDay( new Date( 2012, 9, 4 ), new Date( 2012, 9, 4 ) ) ).toBe( true );
        } );
    } );

    describe( '.isSameMonth()', () => {
        it( 'should return true when months in the given date string is the same', () => {
            expect( dateHelper.isSameMonth( '2017-02-19 07:12:34', '2017-02-24 12:43:09' ) ).toBe( true );
        } );
        it( 'should return true when months in the given date object is the same', () => {
            expect( dateHelper.isSameMonth( new Date( 2009, 12, 1 ), new Date( 2009, 12, 13 ) ) ).toBe( true );
        } );
    } );

    describe( '.isToday()', () => {
        it( 'should return if the given date string is today', () => {
            const dateStr = new Date().toString();
            expect( dateHelper.isToday( dateStr ) ).toBe( true );
        } );
        it( 'should return if the given date object is today', () => {
            expect( dateHelper.isToday( new Date() ) ).toBe( true );
        } );
    } );

    describe( '.formatTimezone()', () => {
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '-11.0' ) ).toBe( '-1100' );
        } );
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '-3.5' ) ).toBe( '-0330' );
        } );
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '0.0' ) ).toBe( '0000' );
        } );
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '5.75' ) ).toBe( '0545' );
        } );
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '8.0' ) ).toBe( '0800' );
        } );
        it( 'should format the given timezone value', () => {
            expect( dateHelper.formatTimezone( '9.5' ) ).toBe( '0930' );
        } );
    } );

} );
