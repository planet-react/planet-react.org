export default function URLHelper( originalUrl ) {

    let url = originalUrl;

    function hasParams() {
        return url.indexOf( '?' ) > -1;
    }

    function urlDecode( value ) {
        return decodeURIComponent( value.replace( /\+/g, '%20' ) );
    }

    function urlEncode( value ) {
        return encodeURIComponent( value );
    }

    function getParam( name ) {
        return urlDecode( (new RegExp( '[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)' ).exec( url ) || [ null, '' ])[ 1 ] ) || null;
    }

    function buildUrl( params ) {
        let result = url;
        if ( !hasParams() ) {
            result += '?';
        }
        else {
            result = result.split( '?' )[ 0 ] + '?';
        }
        params.forEach( ( p ) => {
            result += p.name + '=' + urlEncode( p.value ) + '&';
        } );

        url = result.replace( /&$/, '' );
    }

    return {

        getParam( name ) {
            return getParam( name );
        },

        //@todo: if name exists delete old.
        setParam( name, value ) {
            if ( this.hasParam( name ) ) {
                this.deleteParam( name );
            }

            let params = this.getParams();

            params.push( { name: name, value: value } );
            buildUrl( params );
        },

        hasParam( name ) {
            return getParam( name ) != null;
        },

        getParams() {
            if ( !hasParams() ) {
                return [];
            }
            return url.split( '?' )[ 1 ].split( '&' ).map( ( p ) => {
                let x = p.split( '=' );
                return {
                    // @todo: do we need to decode `key`?
                    name: urlDecode( x[ 0 ] ),
                    value: urlDecode( x[ 1 ] ),
                };
            } );
        },

        deleteParam( name ) {
            let params = this.getParams();
            let newParams = params.filter( p => p.name != name );
            buildUrl( newParams );
        }
    }
}

// let url = URLHelper( 'https://www.youtube.com/results' );
//
// url.setParam( '_what', 'Mobile' );
// url.setParam( '_name', 'Thomas Andersen' );
//
// console.log( url.hasParam( 'search_query' ) );
// console.log( url.hasParam( '_name' ) );
// console.log( url.hasParam( 'xiv' ) );
//
// console.log( url.getParams() );
// console.log( '---------------' );
// url.setParam( '_what', 'PC' );
// console.log( url.getParams() );
// console.log( '---------------' );
//
// url.deleteParam( '_what' );
//
// console.log( url.getParams() );

