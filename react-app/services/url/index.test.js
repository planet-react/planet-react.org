import URLHelper from './';

describe( 'url', () => {

    describe( '.getParam(name)', () => {
        it( 'should return the param value', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            expect( url.getParam( 'param1' ) ).toBe( 'a' );
            expect( url.getParam( 'param2' ) ).toBe( '3' );
            expect( url.getParam( 'param3' ) ).toBe( 'John Doe' );
        } );
        it( 'should return `null` if not found', () => {
            const url = URLHelper( 'https://test.com' );
            expect( url.getParam( 'param4' ) ).toBeNull();
        } );
    } );
    describe( '.setParam(name, value)', () => {
        it( 'should set the parameter', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            url.setParam( 'param4', 'abc æøå' );
            expect( url.getParam( 'param4' ) ).toBe( 'abc æøå' );
        } );

        it( 'should replace the param value if name exists', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            expect( url.getParam( 'param3' ) ).toBe( 'John Doe' );
            url.setParam( 'param3', 'Joe Bloggs' );
            expect( url.getParam( 'param3' ) ).toBe( 'Joe Bloggs' );
        } );
    } );

    describe( '.hasParam(name)', () => {
        it( 'should return true when the param exist', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            expect( url.hasParam( 'param2' ) ).toBe( true );
            expect( url.hasParam( 'param9' ) ).toBe( false );
        } );
    } );

    describe( '.getParams()', () => {
        it( 'should return an array of name/value tuples', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            let expected = [
                {
                    name: 'param1',
                    value: 'a'
                },
                {
                    name: 'param2',
                    value: '3'
                }, {
                    name: 'param3',
                    value: 'John Doe'
                },
            ];
            expect( url.getParams() ).toEqual( expected );

            url.setParam( 'param4', 'a b c' );
            expected.push( { name: 'param4', value: 'a b c' } );

            expect( url.getParams() ).toEqual( expected );
        } );
    } );

    describe( '.deleteParam(name)', () => {
        it( 'should delete the param', () => {
            const url = URLHelper( 'https://test.com?param1=a&param2=3&param3=John+Doe' );
            let expected = [
                {
                    name: 'param1',
                    value: 'a'
                },
                {
                    name: 'param2',
                    value: '3'
                }, {
                    name: 'param3',
                    value: 'John Doe'
                },
            ];
            expect( url.getParams() ).toEqual( expected );

            url.deleteParam( 'param1' );
            expected.shift();

            expect( url.getParams() ).toEqual( expected );
        } );
    } );

    // describe( '.setParam()', () => {
    //     it( 'should format the date string as expected', () => {
    //         expect( dateHelper.format( '2016-07-09 23:12:02', 'ddd MMM D - HH:mma' ) ).toBe( 'Sat Jul 9 - 23:12pm' );
    //     } );
    // } );
    //
    // describe( '.hasParam()', () => {
    //     it( 'should format the date string as expected', () => {
    //         expect( dateHelper.format( '2016-07-09 23:12:02', 'ddd MMM D - HH:mma' ) ).toBe( 'Sat Jul 9 - 23:12pm' );
    //     } );
    // } );
    //
    // describe( '.getParams()', () => {
    //     it( 'should format the date string as expected', () => {
    //         expect( dateHelper.format( '2016-07-09 23:12:02', 'ddd MMM D - HH:mma' ) ).toBe( 'Sat Jul 9 - 23:12pm' );
    //     } );
    // } );
    //
    // describe( '.deleteParam()', () => {
    //     it( 'should format the date string as expected', () => {
    //         expect( dateHelper.format( '2016-07-09 23:12:02', 'ddd MMM D - HH:mma' ) ).toBe( 'Sat Jul 9 - 23:12pm' );
    //     } );
    // } );

} );
