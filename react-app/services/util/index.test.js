import util from './';

describe( 'util', () => {
    describe( '.debounce()', () => {

        it( 'should return the param value', (done) => {

            let val = null;

            let debounce = util.debounce( () => {
                val = 5;
                expect( val ).toBe( 5 );
                done();
            }, 1500 );

            debounce();
        } )


    } );
} );