const path = require( 'path' );
const webpack = require( 'webpack' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const precss = require( 'precss' );
const autoprefixer = require( 'autoprefixer' );
const config = require( './webpack.app.shared.config' );
const FaviconPlugin = require( 'webpack-favicon-plugin' );
const BundleAnalyzerPlugin = require( 'webpack-bundle-analyzer' ).BundleAnalyzerPlugin;
const processArgs = process.argv.slice( 2 );

/**
 * Webpack configuration for production.
 * See .env file for constants.
 *
 * https://webpack.js.org/
 */
const webpackConfig = {
    /**
     * Cache generated modules and chunks
     */
    cache: true,
    /**
     * webpack creates a graph of all of your application's dependencies.
     * The starting point of this graph is known as an entry point.
     * The entry point tells webpack where to start and follows the graph of
     * dependencies to know what to bundle. You can think of your application's
     * entry point as the contextual root or the first file to kick off your app.
     *
     * https://webpack.js.org/concepts/#entry
     */
    entry: {
        /**
         * Vendor bundle.
         * Add packages that should be added a separate vendor bundle.
         */
        vendor: [
            'react',
            'react-dom',
            'react-router',
            'react-apollo',
            'date-fns',
        ],

        /**
         * Application bundle.
         */
        app: [
            'babel-polyfill',
            path.resolve(  config.entry )
        ]
    },

    /**
     * Once you've bundled all of your assets together, we still need to tell
     * webpack where to bundle our application. The webpack output property
     * describes to webpack how to treat bundled code.
     *
     * https://webpack.js.org/concepts/#output
     */
    output: {
        /**
         * Path to where the static assets should be created.
         */
        path: path.resolve( config.output.js ),

        /**
         * publicPath specifies the public URL of the output directory when
         * referenced in a browser.
         *
         * https://webpack.js.org/configuration/output/#output-publicpath
         */
        publicPath: config.output.publicPath,

        /**
         * This option determines the name of each output bundle.
         * The bundle is written to the directory specified by the output.path option.
         *
         * https://webpack.js.org/configuration/output/#output-filename
         */
        filename: '[name].[hash].js',

        /**
         * The filename of non-entry chunks as a relative path inside the
         * output.path directory.
         *
         * https://webpack.js.org/concepts/output/#output-chunkfilename
         */
        chunkFilename: '[name].[chunkhash].js'
    },

    /**
     * This option controls if and how Source Maps are generated.
     *
     * cheap-module-source-map
     * SourceMap without column-mappings.
     * SourceMaps from loaders are simplified to a single mapping per line.
     *
     * https://webpack.js.org/configuration/devtool/
     */
    devtool: 'cheap-module-source-map',

    /**
     * Resource directories
     */
    resolve: config.resolve,

    /**
     * Options in .module determine how the different types of modules within a
     * project will be treated during creation.
     *
     * https://webpack.js.org/concepts/#loaders
     */
    module: {
        /**
         * An array of Rules which are matched to requests when modules are created.
         *
         * https://webpack.js.org/configuration/module/#module-rules
         */
        rules: [
            /**
             * JavaScript
             *
             * This rule transpiles ES6 and ES7 code to JavaScript
             * see also /.babelrc for options.
             *
             * https://github.com/babel/babel-loader
             */
            {
                test: /\.js$/,
                include: path.resolve( config.entry ),
                exclude: /node_modules/,
                loader: 'babel-loader',
            },

            /**
             * Fonts
             *
             * Creates a Data Url of font files when module is created.
             */
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            },

            /**
             * GraphQL
             * https://www.npmjs.com/package/graphql-tag
             */
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: 'graphql-tag/loader'
            },

            /**
             * ExtractTextPlugin
             * Transpile Sass files
             */
            {
                test: /\.scss$/i,
                use: ExtractTextPlugin.extract( {
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'resolve-url-loader'
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                } )
            },

            /**
             * Image files
             *
             * Creates a Data Url of image files when module is created.
             */
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: config.output.images
            },

            /**
             * JSON files
             */
            {
                test: /\.json$/, loader: 'json-loader'
            }
        ]
    },

    /**
     * Since rules only execute transforms on a per-file basis, plugins are most
     * commonly used (but not limited to) performing actions and custom
     * functionality on "compilations" or "chunks" of your bundled modules
     * (and so much more).
     *
     * https://webpack.js.org/concepts/#plugins
     */
    plugins: [

        new webpack.optimize.ModuleConcatenationPlugin(),

        new webpack.optimize.AggressiveMergingPlugin(),

        /**
         * Injects variables into the application.
         */
        new webpack.DefinePlugin( Object.assign( config.defines, {
            'process.env': {
                NODE_ENV: JSON.stringify( 'production' )
            }
        } ) ),

        /**
         * Prints more meaningful module names in the browser console on updates.
         */
        new webpack.NamedModulesPlugin(),

        /**
         * Assign the module and chunk ids by occurrence count.
         * Ids that are used often get lower (shorter) ids. This make ids predictable, reduces total file size and is recommended.
         */
        new webpack.optimize.OccurrenceOrderPlugin( true ),

        /**
         * Add options to loaders.
         * @todo: remove?
         */
        new webpack.LoaderOptionsPlugin( {
            test: /\.scss$/,
            debug: true,
            options: {
                postcss: function () {
                    return [ precss, autoprefixer( { flexbox: false } ) ];
                },
                context: path.resolve(  config.entry ),
                output: {
                    path: path.resolve( config.output.js )
                }
            }
        } ),

        /**
         * Identify common modules and put them in a vendor chunk.
         */
        new webpack.optimize.CommonsChunkPlugin( {
            name: 'vendor',
            minChunks: Infinity
        } ),

        /**
         * Minimize JavaScript code.
         */
        new webpack.optimize.UglifyJsPlugin( {
            compress: {
                warnings: false
            }
        } ),

        /**
         * Move every require("style.css") in entry chunks into a separate css output file.
         */
        new ExtractTextPlugin( {
            filename: '../../css/styles.css',
            allChunks: true
        } ),

        /**
         * Create HTML template
         */
        new HtmlWebpackPlugin( {
            title: config.output.htmlTemplate.title,
            metadata: config.output.htmlTemplate.metadata,
            hash: true,
            inject: false,
            template: path.resolve( config.entry + '/index.template.html' ),
            filename: path.resolve( 'resources/views/index.blade.php' )
        } ),

        /**
         * Create Favicons
         */
        new FaviconPlugin( {
            source: path.resolve( config.entry + '/favicon.png' ),
            output: path.resolve( 'public/images/icons' ),
            path: 'images/icons',
            appName: config.output.htmlTemplate.title,
            appDescription: config.output.htmlTemplate.metadata.description,
            background: config.output.htmlTemplate.metadata.themeColor,
            theme_color: config.output.htmlTemplate.metadata.themeColor,
            start_url: "/?homescreen=1",
            icons: {
                android: { background: config.output.favicons.background },
                appleIcon: { background: config.output.favicons.background },
                appleStartup: { background: config.output.favicons.background },
                coast: { offset: 25 },
                favicons: true,
                firefox: false,
                windows: { background: config.output.favicons.background },
                yandex: { background: config.output.favicons.background }
            }
        } )

    ]
};

// When npm script `analyze` is run, enable the BundleAnalyzer plugin.
if ( processArgs.includes( 'analyze' ) ) {
    webpackConfig.plugins.push(
        new BundleAnalyzerPlugin( {
            analyzerMode: 'server',
            // Host that will be used in `server` mode to start HTTP server.
            analyzerHost: '127.0.0.1',
            // Port that will be used in `server` mode to start HTTP server.
            analyzerPort: 8888,
            // Path to bundle report file that will be generated in `static` mode.
            // Relative to bundles output directory.
            reportFilename: 'report.html',
        } )
    )
}

module.exports = webpackConfig;