require( 'dotenv' ).config();
const path = require( 'path' );

const entry = __dirname;

module.exports = {
    defines: {
        '__APP_URL__': JSON.stringify( process.env.PLANET_URL ),
        '__APP_NAME__': JSON.stringify( process.env.APP_NAME ),
        '__PLANET_EMAIL__': JSON.stringify( process.env.PLANET_EMAIL.replace( /@/, ' [ at ] ' ) ),
        '__PLANET_HEART_BEAT_INTERVAL__': process.env.PLANET_HEART_BEAT_INTERVAL,
        '__PLANET_AUTH0_DOMAIN__': JSON.stringify( process.env.PLANET_AUTH0_DOMAIN ),
        '__PLANET_AUTH0_CLIENT_ID__': JSON.stringify( process.env.PLANET_AUTH0_CLIENT_ID ),
        '__PLANET_AUTH0_CLIENT_REDIRECT_URL__': process.env.NODE_ENV == 'production' ? JSON.stringify( process.env.PLANET_AUTH0_REDIRECT_URL_PROD ) : JSON.stringify( process.env.PLANET_AUTH0_REDIRECT_URL ),
        '__PLANET_GRAPH_QL_ROUTE__': JSON.stringify( process.env.PLANET_GRAPH_QL_ROUTE ),
        '__PLANET_AGGREGATOR_USER_AGENT__': JSON.stringify( process.env.PLANET_AGGREGATOR_USER_AGENT ),
        '__PLANET_AGGREGATOR_CATEGORY_FILTER__': JSON.stringify( process.env.PLANET_AGGREGATOR_CATEGORY_FILTER ),
        '__PLANET_REPO__': JSON.stringify( process.env.PLANET_REPO ),
        '__PLANET_PUBLIC_FEED_IMAGE_DIR__': JSON.stringify( process.env.PLANET_PUBLIC_FEED_IMAGE_DIR ),
        '__PLANET_GOOGLE_SEARCH_CONSOLE_URL__': JSON.stringify( process.env.PLANET_GOOGLE_SEARCH_CONSOLE_URL ),
        '__PLANET_HTML_DEFAULT_META_DESCRIPTION__': JSON.stringify( process.env.PLANET_HTML_DEFAULT_META_DESCRIPTION ),
    },
    resolve: {
        modules: [
            path.resolve( entry + '/app/shared' ),
            path.resolve( './node_modules' )
        ]
    },

    entry: entry,

    output: {
        js: 'public/js/app',
        css: 'public/css',
        publicPath: '/js/app/',
        images: [
            {
                loader: 'file-loader',
                options: {
                    query: {
                        name: '[hash].[ext]',
                        outputPath: 'images/',
                        hash: 'sha512',
                        digest: 'hex'
                    }
                }
            },
            {
                loader: 'image-webpack-loader',
                options: {
                    query: {
                        mozjpeg: {
                            progressive: true
                        },
                        gifsicle: {
                            interlaced: true
                        },
                        optipng: {
                            optimizationLevel: 7
                        }
                    }
                }
            }
        ],

        favicons: {
            background: '#2d2d2d'
        },

        htmlTemplate: {
            title: process.env.APP_NAME,
            metadata: {
                description: process.env.PLANET_HTML_DEFAULT_META_DESCRIPTION,
                googleSiteVerification: '2FD0RbhfwwSlG5_30XFoCNME3qR032FzNxNOAlkN6Vs',
                themeColor: '#2d2d2d',
                splashBackgroundColor: '#fcfcfc'
            }
        }
    },

    devServer: {
        url: 'http://0.0.0.0:3000',
        host: '0.0.0.0',
        port: 3000,

        proxy: {
            url: 'http://localhost:8000',
            host: 'localhost',
            port: 8000
        },

        // Logging
        // https://webpack.js.org/configuration/dev-server/#devserver-stats-
        stats: {
            // Add asset Information
            assets: false,
            // Sort assets by a field
            assetsSort: "field",
            // Add information about cached (not built) modules
            cached: false,
            // Show cached assets (setting this to `false` only shows emitted files)
            cachedAssets: false,
            // Add children information
            children: false,
            // Add chunk information (setting this to `false` allows for a less verbose output)
            chunks: false,
            // Add built modules information to chunk information
            chunkModules: false,
            // Add the origins of chunks and chunk merging info
            chunkOrigins: false,
            // Sort the chunks by a field
            chunksSort: "field",
            // Context directory for request shortening
            context: "../src/",
            // `webpack --colors` equivalent
            colors: true,
            // Display the distance from the entry point for each module
            depth: false,
            // Display the entry points with the corresponding bundles
            entrypoints: true,
            // Add errors
            errors: true,
            // Add details to errors (like resolving log)
            errorDetails: true,
            // Exclude modules which match one of the given strings or regular expressions
            exclude: [],
            // Add the hash of the compilation
            hash: false,
            // Set the maximum number of modules to be shown
            maxModules: 15,
            // Add built modules information
            modules: false,
            // Sort the modules by a field
            modulesSort: "field",
            // Show performance hint when file size exceeds `performance.maxAssetSize`
            performance: true,
            // Show the exports of the modules
            providedExports: false,
            // Add public path information
            publicPath: true,
            // Add information about the reasons why modules are included
            reasons: true,
            // Add the source code of modules
            source: true,
            // Add timing information
            timings: false,
            // Show which exports of a module are used
            usedExports: false,
            // Add webpack version information
            version: true,
            // Add warnings
            warnings: true
        }
    }
};