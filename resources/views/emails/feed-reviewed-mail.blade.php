Hi {{$feed->name}}!

@if ($feed->approved)
Great news! Your feed {{$feed->name}} ({{$feed->feed_url}}) has now been approved at {{$app_name}}
The url is: {{$feed->app_feed_url}}

Login to manage your data
@else
Sorry, your feed was not approved at {{$app_name}}.
Don't hesitate to contact us at {{$planet_email}} if you think your feed should be approved
@endif

Best regards,
{{$app_name}}
{{$app_url}}