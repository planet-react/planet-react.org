Hi!

A feed has been submitted at {{$app_url}}

Name: {{$feed->name}}
Created: {{$feed->created_at}}

- - - - -

Feed Manager: {{$feed_manager_url}}