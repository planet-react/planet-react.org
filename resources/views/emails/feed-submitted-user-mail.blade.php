Thank you for submitting your feed to {{$app_name}}

We will review your feed as soon as possible and you will be receive a status mail when the process is done.

Stay tuned!

Best regards,
{{$app_name}}
{{$app_url}}