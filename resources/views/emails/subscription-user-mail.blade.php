Hi {{$email}}
Planet React have found new links with topics you subscribe to

@if (count($posts['feeds']) > 0)
Feeds:
--------------------------------------------------------------------------------

@foreach ($posts['feeds'] as $feeds)
{{$feeds['feed_name']}}

@foreach ($feeds['posts'] as $post)
{{$post['title']}}
{{$post['link']}}

@endforeach
@endforeach
@endif
@if (count($posts['tags']) > 0)
Tags:
--------------------------------------------------------------------------------
@endif

@foreach ($posts['tags'] as $tags)
[{{$tags['tag_slug']}}]
@foreach ($tags['posts'] as $post)
{{$post['feed_name']}}: {{$post['title']}}
{{$post['link']}}

@endforeach
@endforeach
- - - - -

You are receiving this email because you subscribed on {{$planet_url}} with this email address.
Manage your subscriptions at {{$manage_url}}