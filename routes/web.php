<?php

Route::get( '/api/feed', '\App\PlanetReact\Http\Controllers\AtomFeedController@index' );
Route::get( '/api/debugger', function () {
    if ( ! config( 'app.debug' ) ) {
        return 'Forbidden';
    }

    $debugger = new App\PlanetReact\Http\Debugger\Debugger();
    return $debugger->fetchJson();
} );

Route::get( '/{any}', function () {
    return view( 'index' );
} )->where( 'any', '.*' );
