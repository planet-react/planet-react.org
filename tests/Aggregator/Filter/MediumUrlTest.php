<?php

namespace Tests\Aggregator\Filter;

use App\PlanetReact\Aggregator\Util\MediumUrl;
use TestCase;

class MediumUrlTest extends TestCase {

    public function test_parsePostUrl() {

        $url = 'https://medium.com/@tkh44/emotion-7-7ef119c45869?source=rss-93de0780c5e6------2';

        $mediumUrl = new MediumUrl( $url );

        $parts = $mediumUrl->parse();

        $this->assertEquals( $url, $parts['url'] );
        $this->assertEquals( 'https://medium.com/@tkh44', $parts['author_url'] );
        $this->assertEquals( '93de0780c5e6', $parts['user_id'] );
        $this->assertEquals( '7ef119c45869', $parts['post_id'] );
    }

}
