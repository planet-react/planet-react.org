<?php

namespace Tests\Config;

use File;
use TestCase;

class ConfigTest extends TestCase {

    //@todo: test properties
    public function test_planetConfigFile() {
        $this->assertTrue( File::exists( base_path() . '/config/planet.php' ) );
    }

    public function test_publicHtaccessFileExists() {
        $this->assertTrue( File::exists( base_path() . '/public/.htaccess' ) );
    }

    public function test_envFileExists() {
        $this->assertTrue( File::exists( base_path() . '/.env' ) );
    }

    public function test_envSampleFileExists() {
        $this->assertTrue( File::exists( base_path() . '/.env.example' ) );
    }

    public function test_codeOfConductFileExists() {
        $this->assertTrue( File::exists( base_path() . '/CODE_OF_CONDUCT.md' ) );
    }

    public function test_licenseFileExists() {
        $this->assertTrue( File::exists( base_path() . '/LICENCE' ) );
    }

    public function test_babelRcFileExists() {
        $this->assertTrue( File::exists( base_path() . '/react-app/.babelrc' ) );
    }

    public function test_postCssConfigFileExists() {
        $this->assertTrue( File::exists( base_path() . '/react-app/postcss.config.js' ) );
    }

    public function test_FaviconFileExists() {
        $this->assertTrue( File::exists( base_path() . '/react-app/favicon.png' ) );
    }

    public function test_indexHtmlTemplateExists() {
        $this->assertTrue( File::exists( base_path() . '/react-app/index.template.html' ) );
    }

}
