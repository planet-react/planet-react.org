<?php

namespace Tests\Domain;

use App\PlanetReact\Domain\BaseService;
use App\PlanetReact\Domain\Report\Report;
use TestCase;

class BaseServiceTest extends TestCase {

    protected $baseService;
    protected $auth0User;

    public function __construct() {
        parent::__construct();
        $this->baseService = new BaseService;
        $this->auth0User   = $this->createAuth0User();

    }

    /**
     * @expectedException \Exception
     */
    public function test_checkLoginShouldThrowIfNoUserGiven() {

        $checkLogin = self::getProtectedMethod( 'App\PlanetReact\Domain\BaseService', 'handleLogin' );

        $checkLogin->invokeArgs( $this->baseService, [] );

    }

    public function test_checkLoginShouldNotThrowWhenUserGiven() {

        $checkLogin = self::getProtectedMethod( 'App\PlanetReact\Domain\BaseService', 'handleLogin' );

        $this->assertTrue( $checkLogin->invokeArgs( $this->baseService, [ 'user_id' => 'abc&we#789' ] ) );
    }

    /**
     * @expectedException \Exception
     */
    public function test_throwIfNotOwnerShouldThrow() {

        $throwIfNotOwner = self::getProtectedMethod( 'App\PlanetReact\Domain\BaseService', 'handleNotModelOwner' );

        $model          = new \stdClass();
        $model->user_id = 'abc78#901&23';

        $throwIfNotOwner->invokeArgs( $this->baseService, [ $model, $this->auth0User ] );
    }

    public function test_throwIfNotOwnerShouldNotThrow() {

        $throwIfNotOwner = self::getProtectedMethod( 'App\PlanetReact\Domain\BaseService', 'handleNotModelOwner' );

        $model          = new \stdClass();
        $model->user_id = 'abc123!#';

        $this->assertTrue( $throwIfNotOwner->invokeArgs( $this->baseService, [ $model, $this->auth0User ] ) );
    }

    public function test_saveModel() {

        $model               = new Report;
        $model->content_id   = 143;
        $model->title        = 'Title';
        $model->url          = 'https://test.com/article.html';
        $model->reason       = 'A Reason';
        $model->other_reason = 'Other reason';

        $saved = $this->baseService->saveModel( $model );

        $this->assertTrue( $saved );
    }

    /**
     * @expectedException \Exception
     */
    public function test_saveModel_should_throw_if_not_saved() {

        // Create a model without required attributes
        $model        = new Report;
        $model->title = 'A Title';

        $this->baseService->saveModel( $model );
    }
}