<?php

namespace Tests\Domain\Bookmark;

use App\PlanetReact\Domain\Bookmark\Bookmark;
use App\PlanetReact\Domain\Bookmark\BookmarkService;
use TestCase;

class BookmarkServiceTest extends TestCase {

    protected $bookmarkService;

    public function __construct() {
        $this->bookmarkService = new BookmarkService();
    }

    public function test_toggleSaveShouldCreateNewBookmarkIfModelIsNotFound() {
        $auth0User = $this->createAuth0User();

        $args = [ 'title' => 'Bookmark name', 'url' => 'https://test.com', 'notes' => 'Text' ];

        $createdBookmark = $this->bookmarkService->toggleSave( $args, $auth0User );

        $this->assertInstanceOf( 'App\PlanetReact\Domain\Bookmark\Bookmark', Bookmark::find( $createdBookmark->id ) );
    }

    public function test_toggleSaveShouldDeleteIfModelIsFound() {
        $auth0User = $this->createAuth0User();

        $bookmark = factory( Bookmark::class )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $this->bookmarkService->toggleSave(
                [ 'id' => $bookmark->id ], $auth0User );

        $this->assertNull( Bookmark::find( $bookmark->id ) );
    }

    public function test_save() {
        $auth0User = $this->createAuth0User();

        $args = [ 'title' => 'Bookmark name', 'url' => 'https://test.com', 'notes' => 'Text' ];

        $createdBookmark = $this->bookmarkService->save( $args, $auth0User );

        $this->assertInstanceOf( 'App\PlanetReact\Domain\Bookmark\Bookmark', Bookmark::find( $createdBookmark->id ) );
    }

    /**
     * @expectedException \Exception
     */
    public function test_saveShouldThrowIfNotLoggedIn() {
        $auth0User = false;

        $args = [ 'title' => 'Bookmark name', 'url' => 'https://test.com', 'notes' => 'Text' ];

        $createdBookmark = $this->bookmarkService->save( $args, $auth0User );
    }

    public function test_saveShouldUpdateIf_id_isGiven() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $args = [ 'id' => 1, 'title' => 'Bookmark name', 'url' => 'https://test.com', 'notes' => 'Text' ];

        $updatedBookmark = $this->bookmarkService->save( $args, $auth0User );

        $this->assertTrue( $updatedBookmark->url == 'https://test.com' );
    }

    /**
     * @expectedException \Exception
     */
    public function test_saveShouldThrowOnUpdateIfUserIsNotOwner() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class )->create( [ 'user_id' => 'DUMMY_ID' ] );

        $args = [ 'id' => 1, 'title' => 'Bookmark name', 'url' => 'https://test.com', 'notes' => 'Text' ];

        $updatedBookmark = $this->bookmarkService->save( $args, $auth0User );
    }

    public function test_getBookmarkForUser() {
        $auth0User = $this->createAuth0User();

        $bookmark = factory( Bookmark::class )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $foundBookmark = $this->bookmarkService->getBookmarkForUser( $auth0User, $bookmark->id );

        $this->assertInstanceOf( 'App\PlanetReact\Domain\Bookmark\Bookmark', $foundBookmark );
        $this->assertTrue( $foundBookmark->user_id == $auth0User->getUserId() );
    }

    /**
     * @expectedException \Exception
     */
    public function test_getBookmarkForUserShouldThrowIfNotOwner() {
        $auth0User = $this->createAuth0User();

        $bookmark = factory( Bookmark::class )->create( [ 'user_id' => 'DUMMY_ID' ] );

        $foundBookmark = $this->bookmarkService->getBookmarkForUser( $auth0User, $bookmark->id );
    }

    public function test_getBookmarksForUser() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 5 )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $foundBookmarks = $this->bookmarkService->getBookmarksForUser( $auth0User->getUserId(), null );

        $this->assertTrue( $foundBookmarks->count() == 5 );

        foreach ( $foundBookmarks as $foundBookmark ) {
            $this->assertTrue( $foundBookmark->user_id == $auth0User->getUserId() );
        }
    }

    public function test_forceDestroy() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 3 )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $this->bookmarkService->forceDestroy( Bookmark::find( 1 )->id, $auth0User );

        $this->assertTrue( Bookmark::all()->count() == 2 );
    }

    /**
     * @expectedException \Exception
     */
    public function test_forceDestroyShouldThrowIfNotOwner() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 2 )->create( [ 'user_id' => 'DUMMY_ID' ] );

        $this->bookmarkService->forceDestroy( Bookmark::find( 1 )->id, $auth0User );
    }

    public function test_destroyShouldSoftDeleteByDefault() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 5 )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $this->bookmarkService->destroy( Bookmark::find( 1 )->id, $auth0User );

        $this->assertTrue( Bookmark::withTrashed()->count() == 5 );
        $this->assertTrue( Bookmark::onlyTrashed()->count() == 1 );
    }

    public function test_destroyShouldSupportForceDelete() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 3 )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $this->bookmarkService->destroy( Bookmark::find( 3 )->id, $auth0User, true );

        $this->assertTrue( Bookmark::withTrashed()->count() == 2 );
    }

    /**
     * @expectedException \Exception
     */
    public function test_destroyShouldThrowIfNotOwner() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class, 3 )->create( [ 'user_id' => 'DUMMY_ID' ] );

        $this->bookmarkService->forceDestroy( Bookmark::find( 2 )->id, $auth0User );
    }

    public function test_tag() {
        $auth0User = $this->createAuth0User();

        factory( Bookmark::class )->create( [ 'user_id' => $auth0User->getUserId() ] );

        $bookmark = Bookmark::find( 1 );

        $tags = [ 'Mysterious stranger', 'intense training', 'Cyborg' ];
        $this->bookmarkService->tag( $bookmark, implode( ',', $tags ) );

        $foundBookmark = Bookmark::with( 'tagged' )->first();

        $this->assertTrue( count( $foundBookmark->tags ) == 3 );

        collect( $foundBookmark->tags )->map( function ( $tag, $key ) use ( $tags ) {
            $this->assertTrue( $tag->name == ucwords( $tags[$key] ), "'{$tags[$key]}' should be tagged with name: '{$tag->name}'" );
            $this->assertTrue( $tag->slug == strtolower( str_replace( ' ', '-', $tags[$key] ) ), "'{$tags[$key]}' should be tagged with slug: '{$tag->slug}'" );
        } );
    }

}