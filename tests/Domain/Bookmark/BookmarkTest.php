<?php

namespace Tests\Domain\Bookmark;

use App\PlanetReact\Domain\Bookmark\Bookmark;
use TestCase;

class BookmarkTest extends TestCase {

    public function testCreate() {

        factory( Bookmark::class )->create();

        $this->assertTrue( Bookmark::find(1)->id == 1 );
    }

}
