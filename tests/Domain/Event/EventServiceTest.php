<?php

namespace Tests\Domain\Event;

use App\PlanetReact\Domain\Bookmark\Bookmark;
use App\PlanetReact\Domain\Event\Event;
use App\PlanetReact\Domain\Event\EventService;
use Carbon\Carbon;
use TestCase;

class EventServiceTest extends TestCase {

    protected $eventService;

    public function __construct() {
        $this->eventService = new EventService();
    }

    public function test_save() {
        $auth0User = $this->createAuth0User();

        $args = [
                'user_id'      => $auth0User->getUserId(),
                'active'       => 1,
                'type'         => 3,
                'name'         => 'Event Name',
                'description'  => ' Event Description',
                'start_date'   => '2017-06-15 10:00:00',
                'end_date'     => '2017-06-16 16:30:00',
                'timezone'     => 'Europe/Berlin',
                'location'     => 'Location information',
                'country'      => 'DE',
                'website_link' => 'https://test-link.com',
                'twitter_link' => 'https://twitter-test-link.com',
        ];

        $createdEvent = $this->eventService->save( $args, $auth0User );

        $this->assertInstanceOf( 'App\PlanetReact\Domain\Event\Event', Event::find( $createdEvent->id ) );
        $this->assertTrue( $createdEvent->description == 'Event Description' );
    }

    /**
     * @expectedException \Exception
     */
    public function test_saveShouldThrowOnUpdateIfUserIsNotOwner() {

        factory( Event::class )->create( [ 'user_id' => 'DUMMY_ID' ] );

        $auth0User = $this->createAuth0User();

        $args = [
                'id'           => 1,
                'user_id'      => $auth0User->getUserId(),
                'active'       => 1,
                'type'         => 3,
                'name'         => 'Event Name',
                'description'  => 'Event Description',
                'start_date'   => '2017-06-15 10:00:00',
                'end_date'     => '2017-06-16 16:30:00',
                'timezone'     => 'Europe/Berlin',
                'location'     => 'Location information',
                'country'      => 'DE',
                'website_link' => 'https://test-link.com',
                'twitter_link' => 'https://twitter-test-link.com',
        ];

        $createdEvent = $this->eventService->save( $args, $auth0User );
    }

    public function test_getEvents_shouldReturnActiveAndNotExpiredEvents() {
        factory( Event::class, 3 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->subDays( 4 ),
                'end_date'   => Carbon::today()->subDays( 3 )
        ] );
        factory( Event::class, 2 )->create( [
                'active'     => 0,
                'start_date' => Carbon::today()->subDays( 3 ),
                'end_date'   => Carbon::today()->subDays( 2 )
        ] );
        factory( Event::class, 7 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->addDays( 3 ),
                'end_date'   => Carbon::today()->addDays( 5 )
        ] );

        $events = $this->eventService->getEvents( [ 'type' => null ] )->get();

        $this->assertTrue( $events->count() == 7 );
    }

    public function test_getEvents_defaultOrderShouldBeStartDateAscending() {
        factory( Event::class, 1 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->addDay( 1 ),
                'end_date'   => Carbon::today()->addDay( 2 )
        ] );
        factory( Event::class, 1 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->addDay( 3 ),
                'end_date'   => Carbon::today()->addDay( 4 )
        ] );
        factory( Event::class, 1 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->addDay( 5 ),
                'end_date'   => Carbon::today()->addDay( 6 )
        ] );

        $events = $this->eventService->getEvents( [ 'type' => null ] )->get();

        $events = $events->toArray();

        $this->assertTrue( $events[0]['start_date'] == Carbon::today()->addDay( 1 ) );
        $this->assertTrue( $events[1]['start_date'] == Carbon::today()->addDay( 3 ) );
        $this->assertTrue( $events[2]['start_date'] == Carbon::today()->addDay( 5 ) );
    }

    public function test_getEvents_returnPastEvents() {
        factory( Event::class, 1 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->subDays( 7 ),
                'end_date'   => Carbon::today()->subDays( 6 )
        ] );
        factory( Event::class, 2 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->subDays( 14 ),
                'end_date'   => Carbon::today()->subDays( 13 )
        ] );
        factory( Event::class, 1 )->create( [
                'active'     => 1,
                'start_date' => Carbon::today()->addDays( 1 ),
                'end_date'   => Carbon::today()->addDays( 3 )
        ] );

        $events = $this->eventService->getEvents( [ 'type' => null, 'past' => true ] )->get();

        $this->assertTrue( $events->count() == 3 );
    }

    public function test_getEventsWithType() {
        $tomorrow = Carbon::today()->addDay();

        factory( Event::class, 4 )->create( [
                'active'     => 1,
                'type'       => 3,
                'start_date' => $tomorrow
        ] );
        factory( Event::class, 2 )->create( [
                'active'     => 1,
                'type'       => 2,
                'start_date' => $tomorrow
        ] );
        factory( Event::class, 4 )->create( [
                'active'     => 1,
                'type'       => 1,
                'start_date' => $tomorrow
        ] );

        $events = $this->eventService->getEvents( [ 'type' => 3 ] )->get();

        $this->assertTrue( $events->count() == 4 );
    }

    public function test_getEvent() {
        $tomorrow = Carbon::today()->addDay();

        factory( Event::class, 5 )->create( [
                'active'     => 1,
                'type'       => 3,
                'start_date' => $tomorrow
        ] );

        $event = $this->eventService->getEvent( 3 );

        $this->assertInstanceOf( 'App\PlanetReact\Domain\Event\Event', $event );
    }

    public function test_getEventForUser() {
        $auth0User = $this->createAuth0User();

        $tomorrow = Carbon::today()->addDay();

        factory( Event::class, 3 )->create( [
                'user_id'    => $auth0User->getUserId(),
                'active'     => 1,
                'type'       => 2,
                'start_date' => $tomorrow
        ] );
        factory( Event::class, 2 )->create( [
                'user_id'    => 'DUMMY_ID',
                'active'     => 1,
                'type'       => 2,
                'start_date' => $tomorrow
        ] );

        $events = $this->eventService->getEventsForUser( $auth0User->getUserId() );

        $this->assertTrue( $events->count() == 3 );
    }

    public function test_activate() {
        $auth0User = $this->createAuth0User();

        factory( Event::class, 1 )->create( [
                'user_id' => $auth0User->getUserId(),
                'active'  => 0
        ] );

        $event = $this->eventService->getEvent( 1, true );

        $this->assertTrue( $event->active == 0 );

        $this->eventService->activate( $event->id, 1, $auth0User );

        $event = $this->eventService->getEvent( 1, true );

        $this->assertTrue( $event->active == 1 );

        $this->eventService->activate( $event->id, 0, $auth0User );

        $event = $this->eventService->getEvent( 1, true );

        $this->assertTrue( $event->active == 0 );

    }

    public function test_remove_shouldSoftDeleteByDefault() {
        $auth0User = $this->createAuth0User();

        factory( Event::class, 3 )->create( [
                'user_id' => $auth0User->getUserId(),
                'active'  => 1
        ] );

        $event = $this->eventService->getEvent( 3, true );
        $this->eventService->remove( $event->id, $auth0User );

        $this->assertTrue( Event::withTrashed()->count() == 3 );
        $this->assertTrue( Event::onlyTrashed()->count() == 1 );
    }

    /**
     * @expectedException \Exception
     */
    public function test_remove_shouldThrowIfNotOwner() {
        $auth0User = $this->createAuth0User();

        factory( Event::class, 2 )->create( [
                'user_id' => 'DUMMY_ID',
                'active'  => 1
        ] );

        $this->eventService->remove( Event::find( 2 )->id, $auth0User );
    }

}
