<?php

namespace Tests\Domain\Event;

use App\PlanetReact\Domain\Event\Event;
use TestCase;

class EventTest extends TestCase {

    public function testCreate() {

        factory( Event::class )->create(['active' => 1]);

        $this->assertTrue( Event::find(1)->id == 1 );
    }

}
