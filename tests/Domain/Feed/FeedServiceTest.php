<?php

namespace Tests\Domain\Feed;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedService;
use App\PlanetReact\Domain\Feed\FeedSourceType;
use TestCase;
use Tests\PicoFixture;

class FeedServiceTest extends TestCase {

    protected $feedService;

    protected $url = 'https://medium.com/feed/@dan_abramov';

    public function __construct() {
        $this->feedService = new FeedService();

        parent::__construct();
    }

    public function skip__test_validateFeedUrlFailOnNotValidFeedUrl() {
        $result = $this->feedService->validateFeedUrl( 'htps://test.com' );

        $this->assertEquals( [
                'is_valid' => false,
                'message'  => 'Not a valid url'
        ], $result );
    }

    public function skip__test_validateFeedFailWhenFeedUrlExists() {

        $url = 'https://test.com';
        factory( Feed::class, 1 )->create( [
                'feed_url' => $url
        ] );

        $result = $this->feedService->validateFeedUrl( $url, true );

        $this->assertEquals( [
                'is_valid'    => false,
                'message'     => 'The feed exists',
                'name'        => null,
                'description' => null
        ], $result );
    }

    public function test_validateFeedUrlSuccess() {

        $pico = new PicoFixture( $this->url );

        $feedService = new FeedService( $pico->createReaderStub() );

        $result = $feedService->validateFeedUrl( $this->url );

        $expected = [
                'is_valid'    => true,
                'message'     => 'Valid',
                'name'        => 'The Title',
                'description' => 'The Description',
                'website_url' => 'https://acme.com',
                'source_type' => FeedSourceType::getSourceTypeById('RSS'),
        ];

        $this->assertEquals( $expected, $result );
    }

    /**
     * @expectedException \PicoFeed\Reader\UnsupportedFeedFormatException
     */
    public function test_validateFeedUrlFailOnUnsupportedFeedFormat() {
        $pico = new PicoFixture( $this->url, true );

        $feedService = new FeedService( $pico->createReaderStub() );

        $feedService->validateFeedUrl( $pico->getClient()->getUrl() );

//        $expected = [
//                'is_valid'    => true,
//                'message'     => 'Valid',
//                'name'        => $feedStub->getTitle(),
//                'description' => $feedStub->getDescription()
//        ];
//
//        $this->assertEquals( $expected, $result );
    }

    public function test_getFeedBySlug() {

        factory( Feed::class, 1 )->create( [
                'slug'     => 'a-nice-slug',
                'approved' => 0
        ] );

        $feed = $this->feedService->getFeedBySlug( [ 'slug' => 'a-nice-slug' ], true );
        $this->assertTrue( $feed->slug == 'a-nice-slug' );

        $feed = $this->feedService->getFeedBySlug( [ 'slug' => 'a-nice-slug' ], false );
        $this->assertNull( $feed );
    }

    public function test_removeFeed() {
        $user = $this->createAuth0User();
        factory( Feed::class, 3 )->create( [
                'user_id' => $user->getUserId()
        ] );

        $this->feedService->removeFeed( 1, $user, false );

        $this->assertCount( 2, Feed::all() );

        $this->assertCount( 3, Feed::withTrashed()->get() );
    }

    /**
     * @expectedException \Exception
     */
    public function test_removeFeed_should_throw_if_not_owner() {
        $user = $this->createAuth0User();
        factory( Feed::class, 2 )->create( [
                'user_id' => '__abc_123_xyz'
        ] );

        $this->feedService->removeFeed( 1, $user, false );
    }

    public function test_restoreFeed() {
        $user = $this->createAuth0User();
        factory( Feed::class, 2 )->create( [
                'user_id' => $user->getUserId()
        ] );

        $this->feedService->removeFeed( 1, $user, false );
        $this->assertCount( 1, Feed::all() );

        $this->feedService->restoreFeed( 1, $user, false );
        $this->assertCount( 2, Feed::all() );
    }
}
