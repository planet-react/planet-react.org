<?php

namespace Tests\Domain\Feed;

use App\PlanetReact\Domain\Feed\FeedSourceType;
use TestCase;

class FeedSourceTypeTest extends TestCase {

    public function test_getSourceTypeById() {
        $this->assertTrue( FeedSourceType::getSourceTypeById( 'RSS' ) == 1 );
        $this->assertTrue( FeedSourceType::getSourceTypeById( 'YOUTUBE_CHANNEL' ) == 2 );

    }

    public function test_getSourceTypeByFeedUrl() {
        $this->assertTrue(
                FeedSourceType::getSourceTypeByFeedUrl( 'https://acme.com/feed.xml' ) == 1
        );
        $this->assertTrue(
                FeedSourceType::getSourceTypeByFeedUrl( 'https://www.youtube.com/channel/UCZkjWyyLvzWeoVWEpRemrDQ' ) == 2
        );
    }

}
