<?php

namespace Tests\Domain\Feed;

use App\PlanetReact\Domain\Feed\Feed;
use TestCase;

class FeedTest extends TestCase {

    protected $auth0User;

    public function __construct() {
        parent::__construct();

        $this->auth0User = $this->createAuth0User();
    }

    public function test_create() {
        $feed              = new Feed();
        $feed->user_id     = $this->auth0User->getUserId();
        $feed->source_type = 1;
        $feed->name        = 'Test Feed';
        $feed->slug        = 'test-feed';
        $feed->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ';
        $feed->lang        = 'en';
        $feed->website_url         = 'https://www.acme.org';
        $feed->feed_url    = 'https://www.acme.org/feed';
        $feed->twitter_url = 'https://www.twitter.com/abc_def_ghi';
        $feed->repo_url    = 'https://www.github.com/lalco';
        $feed->image       = 'test-feed.png';
        $feed->approved    = 0;
        $feed->comment     = 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        $this->assertTrue( $feed->validate() );
    }

    public function test_validate() {
        $feed              = new Feed();
        $feed->user_id     = $this->createAuth0User();
        $feed->source_type = 1;
        $feed->name        = 'Test Feed';
        $feed->lang        = 'en';
        $feed->website_url         = 'https://www.acme.org';
        $feed->feed_url    = 'https://www.acme.org/feed';
        $feed->twitter_url = 'https://www.twitter.com/abc_def_ghi';

        $this->assertTrue( $feed->validate() );
    }

    public function test_validate_should_fail_when_urls_are_not_valid() {
        $feed              = new Feed();
        $feed->source_type = 1;
        $feed->name        = 'Test Feed';
        $feed->lang        = 'en';

        $feed->website_url         = 'ww..acme.org';
        $feed->feed_url    = 'www.acme';
        $feed->twitter_url = 'www.twitter.org';

        $isValid = $feed->validate();
        $errors  = $feed->errors();

        $this->assertFalse( $isValid );
        $this->assertCount( 3, $errors );
    }

    public function test_validate_should_fail_when_name_is_not_set() {
        $feed           = new Feed();
        $feed->lang     = 'en';
        $feed->feed_url = 'https://www.acme.com';

        $isValid = $feed->validate();
        $errors  = $feed->errors();

        $this->assertFalse( $isValid );
        $this->assertCount( 1, $errors );
    }

    public function test_validate_should_fail_when_lang_is_not_set() {
        $feed           = new Feed();
        $feed->name     = 'Test Feed';
        $feed->feed_url = 'https://www.acme.com';

        $isValid = $feed->validate();
        $errors  = $feed->errors();

        $this->assertFalse( $isValid );
        $this->assertCount( 1, $errors );
    }

    public function test_new_feed_should_not_be_approved_by_default() {
        $feed           = new Feed();
        $feed->name     = 'Test Feed';
        $feed->lang     = 'en';
        $feed->website_url      = 'https://www.acme.org';
        $feed->feed_url = 'https://www.acme.org/feed';

        $this->assertTrue( $feed->approved == 0 );
    }

    public function test_alreadyExists() {
        $feed           = new Feed();
        $feed->name     = 'Test Feed';
        $feed->lang     = 'en';
        $feed->feed_url = 'https://www.acme.org/feed';
        $feed->save();

        $feed           = new Feed();
        $feed->name     = 'Test Feed 2';
        $feed->lang     = 'en';
        $feed->feed_url = 'https://www.acme.org/feed';

        $this->assertTrue( $feed->alreadyExists() );
    }

    public function test_app_feed_url_attribute() {
        $feed = factory( Feed::class )->create( [
                'slug' => 'arcade-gannon'
        ] );
        $this->assertEquals( 'http://localhost/feeds/arcade-gannon', $feed->app_feed_url );
    }

    public function test_getImage() {
        $feed = factory( Feed::class )->create( [
                'image' => 'image-383.png'
        ] );
        $this->assertEquals( 'image-383.png', $feed->getImage() );
    }

    public function test_getImage_should_return_default_image_when_not_exists() {
        $feed = factory( Feed::class )->create( [
                'image' => null
        ] );
        $this->assertEquals( '_default.png', $feed->getImage() );
    }

}
