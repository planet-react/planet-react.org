<?php

namespace Tests\Domain\Feed\Fetcher;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedSourceType;
use App\PlanetReact\Domain\Feed\Fetcher\Fetcher;
use App\PlanetReact\Domain\Feed\Fetcher\Rss\RssFetcher;
use TestCase;
use Tests\PicoFixture;

class FetcherTest extends TestCase {

    public function test_register() {

        $feed = factory( Feed::class )->create( [
                'feed_url'    => 'https://medium.com/feed/@dan_abramov',
                'source_type' => FeedSourceType::getSourceTypeById('RSS')
        ] );

        $pico       = new PicoFixture( $feed->feed_url );
        $reader     = $pico->createReaderStub();
        $rssFetcher = new RssFetcher( $reader );

        $fetcher = new Fetcher();
        $fetcher->register( $rssFetcher );

        $result = $fetcher->getFetcherForFeedSourceType( $feed->source_type );

        $this->assertTrue( get_class( $result ) == 'App\PlanetReact\Domain\Feed\Fetcher\Rss\RssFetcher' );
    }

}