<?php

namespace Tests\Domain\Feed\Fetcher\Rss;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Feed\FeedSourceType;
use App\PlanetReact\Domain\Feed\Fetcher\Rss\RssFetcher;
use TestCase;
use Tests\PicoFixture;

class RssFetcherTest extends TestCase {

    public function test_implements_interface() {
        $this->assertArrayHasKey(
                'App\PlanetReact\Domain\Feed\Fetcher\FetcherInterface', class_implements( RssFetcher::class )
        );
    }

    public function test_getFeedSourceType() {
        $feed = factory( Feed::class )->create( [
                'feed_url'    => 'https://medium.com/feed/@dan_abramov',
                'source_type' => FeedSourceType::getSourceTypeById( 'RSS' )
        ] );

        $pico   = new PicoFixture( $feed->feed_url );
        $reader = $pico->createReaderStub();

        $rssFetcher = new RssFetcher( $reader );

        $this->assertTrue( $rssFetcher->getFeedSourceType() == FeedSourceType::getSourceTypeById( 'RSS' ) );
    }

    public function test_fetch() {

        $feed = factory( Feed::class )->create( [
                'feed_url'    => 'https://medium.com/feed/@dan_abramov',
                'source_type' => FeedSourceType::getSourceTypeById( 'RSS' )
        ] );

        $pico   = new PicoFixture( $feed->feed_url );
        $reader = $pico->createReaderStub();

        $rssFetcher = new RssFetcher( $reader );

        $result = $rssFetcher->fetch( $feed );

        $this->assertTrue( is_array( $result->getItems() ) );
    }

}