<?php

namespace Tests\Domain\Feed\Fetcher\YouTube;

use App\PlanetReact\Domain\Feed\FeedSourceType;
use App\PlanetReact\Domain\Feed\Fetcher\YouTube\YouTubeChannelFetcher;
use App\PlanetReact\Services\YouTube\Api\YouTubeApi;
use TestCase;

class YouTubeChannelFetcherTest extends TestCase {

    public function test_implements_interface() {
        $this->assertArrayHasKey(
                'App\PlanetReact\Domain\Feed\Fetcher\FetcherInterface', class_implements( YouTubeChannelFetcher::class )
        );
    }

    public function test_getFeedSourceType() {
        $fetcher = new YouTubeChannelFetcher( new YouTubeApi() );
        $this->assertTrue( $fetcher->getFeedSourceType() == FeedSourceType::getSourceTypeById( 'YOUTUBE_CHANNEL' ) );
    }

    public function todo__test_fetch() {

    }

}