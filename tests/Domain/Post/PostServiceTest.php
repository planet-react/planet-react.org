<?php

namespace Tests\Domain\Post;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Post\PostService;
use TestCase;

class PostServiceTest extends TestCase {

    protected $postService;

    public function __construct() {
        $this->postService = new PostService;
        parent::__construct();
    }

    public function test_findPosts_should_only_find_whole_words() {

        factory( Post::class )->create( [
                'title' => 'Well, to be perfectly clear, you can, just outside of Create React App setup',
        ] );
        factory( Post::class )->create( [
                'title' => 'React can do that quite easily actually',
        ] );
        factory( Post::class )->create( [
                'title' => 'cans: A framework for building React MobX application',
        ] );

        $result = $this->postService->findPosts( null, null, null, null, 'can' );

        $this->assertCount( 2, $result->get() );
    }

    public function test_findPosts_shouldFindPostsWithWord() {

        factory( Post::class )->create( [
                'title' => 'Async found in the beginning of the title',
        ] );
        factory( Post::class )->create( [
                'title' => 'Find async in the middle of the title',
        ] );
        factory( Post::class )->create( [
                'title' => 'At the end of the title, async',
        ] );

        $result = $this->postService->findPosts( null, null, null, null, 'async' );

        $this->assertCount( 3, $result->get() );
    }

    public function test_getPosts() {
        factory( Post::class, 3 )->create();
        $this->assertCount( 3, $this->postService->getPosts( [], null )->get() );
    }

    public function test_getPosts_should_support_tag_search() {
        factory( Post::class, 3 )->create();

        $posts = Post::find( [ 1, 3 ] );

        foreach ( $posts as $post ) {
            $post->tag( 'cheese' );
            $post->save();
        }

        $result = $this->postService->getPosts( [ 'search' => '_tag: cheese' ], null )->get();

        $this->assertCount( 2, $result );
    }

    public function test_getPosts_should_support_search_for_whole_words() {
        factory( Post::class, 2 )->create();
        factory( Post::class, 1 )->create( [
                'title' => 'Lorem ipsum dolor sit amet, text to search for ZZZconsecteturZZZ adipiscing'
        ] );
        $result1 = $this->postService->getPosts( [ 'search' => 'text to search for' ], null )->get();

        $this->assertCount( 1, $result1 );

        $result2 = $this->postService->getPosts( [ 'search' => 'consectetur' ], null )->get();

        $this->assertCount( 0, $result2 );
    }

    public function test_getPosts_should_support_search_for_feed_language() {
        factory( Feed::class, 1 )->create( [ 'lang' => 'fi' ] );
        factory( Feed::class, 1 )->create( [ 'lang' => 'en' ] );

        factory( Post::class, 1 )->create( [ 'feed_id' => 1 ] );
        factory( Post::class, 1 )->create( [ 'feed_id' => 2 ] );

        $result = $this->postService->getPosts( [ 'lang' => 'fi' ], null );

        $this->assertCount( 1, $result->get() );
    }

    public function test_getPosts_should_support_search_for_post_id() {
        factory( Post::class, 2 )->create();

        $result = $this->postService->getPosts( [ 'id' => 1 ], null )->get();

        $this->assertCount( 1, $result );
    }

    public function test_getPosts_should_support_search_for_feed_id() {
        factory( Feed::class, 3 )->create();

        factory( Post::class, 1 )->create( [ 'feed_id' => 1 ] );
        factory( Post::class, 1 )->create( [ 'feed_id' => 2 ] );
        factory( Post::class, 1 )->create( [ 'feed_id' => 3 ] );

        $result = $this->postService->getPosts( [ 'feedId' => 2 ], null )->get();

        $this->assertCount( 1, $result );
    }

    public function test_getPosts_should_support_search_for_feed_slug() {
        factory( Feed::class, 1 )->create( [ 'slug' => ' bob-frazier' ] );
        factory( Feed::class, 1 )->create( [ 'slug' => 'keri-romara' ] );

        factory( Post::class, 1 )->create( [ 'feed_id' => 2 ] );
        factory( Post::class, 1 )->create( [ 'feed_id' => 2 ] );
        factory( Post::class, 1 )->create( [ 'feed_id' => 1 ] );

        $result = $this->postService->getPosts( [ 'slug' => 'keri-romara' ], null )->get();

        $this->assertCount( 2, $result );
    }

    public function test_removePost_admins_can_delete() {
        $user = $this->createAuth0User();

        factory( Post::class, 2 )->create( [
                'feed_id' => function () use ( $user ) {
                    return factory( \App\PlanetReact\Domain\Feed\Feed::class )->create( [
                            'user_id' => $user->getUserId()
                    ] )->id;
                }
        ] );

        $this->postService->removePost( 1, $user, true );

        $this->assertCount( 1, Post::all() );
    }

}
