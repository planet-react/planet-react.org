<?php

namespace Tests\Domain\Post;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Post\Post;
use TestCase;

class PostTest extends TestCase {

    public function test_create() {
        $feed = factory( Feed::class )->create( [
                'name' => 'The Source',
        ] );

        $post          = new Post();
        $post->title   = 'Test Post';
        $post->feed_id = $feed->id;
        $post->link    = 'https://blog.com/feed.xml';
        $post->pubdate = '2017-02-11 10:03:21';

        $post->save();

        $this->assertTrue( $post->id == 1 );
    }

    public function test_alreadyExists_prioritizeGuid() {

        $link = 'https://blog.com/feed.xml';

        factory( Post::class )->create( [
                'guid' => 'https://test.com/guid12494833',
                'link' => $link,
        ] );
        $post = factory( Post::class )->make( [
                'guid' => 'https://test.com/guid12494833',
                'link' => $link,
        ] );

        $this->assertTrue( $post->alreadyExists() );
    }

    public function test_alreadyExists_useLinkWhenGuidEmpty() {

        $link = 'https://blog.com/feed.xml';

        factory( Post::class )->create( [
                'guid' => null,
                'link' => $link,
        ] );
        $post2 = factory( Post::class )->make( [
                'guid' => null,
                'link' => $link,
        ] );

        $this->assertTrue( $post2->alreadyExists() );
    }

}
