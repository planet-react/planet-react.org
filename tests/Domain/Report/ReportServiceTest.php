<?php

namespace Tests\Domain\Report;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Report\Report;
use App\PlanetReact\Domain\Report\ReportService;
use TestCase;

class ReportServiceTest extends TestCase {

    protected $reportService;

    public function __construct() {
        $this->reportService = new ReportService;
    }

    public function test_saveShouldCreateNewReport() {
        $createdReport = factory( Report::class )->create();
        $this->assertInstanceOf( 'App\PlanetReact\Domain\Report\Report', Report::find( $createdReport->id ) );
    }

    /**
     * @expectedException \Illuminate\Database\QueryException
     */
    public function test_shouldThrowIfCharLengthIsGreaterThan100() {
        factory( Report::class )->create( [
                'other_reason' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et nunc diam. Nulla elit tellus, varius'
        ] );
    }

    public function test_getReports() {
        factory( Report::class, 5 )->create();

        $this->assertTrue( $this->reportService->getReports()->count() == 5 );
    }

    public function test_postHasBeenReportedAndDealtWith() {
        $url = 'https://test.com/post.html';

        $post = factory( Post::class )->create( [
                'title' => 'The Title',
                'link'  => $url,
        ] );

        $report = factory( Report::class )->create( [
                'content_id' => $post->id,
                'title'      => $post->title,
                'url'        => $url,
        ] );

        $this->reportService->deleteReport( $report->id );

        $post2 = factory( Post::class )->create( [
                'title' => 'The Title',
                'link'  => $url,
        ] );

        $hasBeenReportedInThePast = $this->reportService->postHasBeenReportedAndDealtWith( $post2->link );
        $this->assertTrue( $hasBeenReportedInThePast );

        $hasNotBeenReportedInThePast = $this->reportService->postHasBeenReportedAndDealtWith( 'https://test.com/post2.html' );
        $this->assertFalse( $hasNotBeenReportedInThePast );

    }

}