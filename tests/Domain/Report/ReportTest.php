<?php

namespace Tests\Domain\Report;

use App\PlanetReact\Domain\Report\Report;
use TestCase;

class ReportTest extends TestCase {

    public function test_Reasons() {

        $expected = [
                'SPAM'          => 'This is spam',
                'PERSONAL_INFO' => 'This is personal and confidential information',
                'THREATS'       => 'This is threatening, harassing, or inciting violence',
                'OTHER'         => 'Other (max 100 characters)',
        ];

        $this->assertEquals( $expected, Report::REASONS );
    }

    public function test_Create() {

        factory( Report::class )->create();

        $this->assertTrue( Report::find( 1 )->id == 1 );
    }

}
