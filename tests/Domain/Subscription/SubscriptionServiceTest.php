<?php

namespace Tests\Subscription;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Subscription\Subscription;
use App\PlanetReact\Domain\Subscription\SubscriptionService;
use TestCase;

class SubscriptionServiceTest extends TestCase {

    protected $subscriptionService;
    protected $auth0User;

    public function __construct() {
        $this->subscriptionService = new SubscriptionService;
        $this->auth0User           = $this->createAuth0User();
        parent::__construct();
    }

    public function test_toggle() {

        factory( Subscription::class, 3 )->create( [
                'user_id' => $this->auth0User->getUserId()
        ] );

        $this->subscriptionService->toggle( [
                'id'    => 1,
                'type'  => 'feed',
                'value' => '82',
        ], $this->auth0User );

        $subscriptions = Subscription::all();

        $this->assertTrue( $subscriptions->count() == 2 );
    }

    public function test_subscribe() {

        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '39',
        ], $this->auth0User );

        $subscription = Subscription::find( 1 );

        $this->assertEquals( 'abc123!#', $subscription->user_id );
        $this->assertEquals( 'test@test.com', $subscription->user_email );
        $this->assertEquals( 'feed', $subscription->type );
        $this->assertEquals( '39', $subscription->value );
    }

    public function test_unsubscribe() {

        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '39',
        ], $this->auth0User );
        $this->subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $this->auth0User );
        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '436',
        ], $this->auth0User );

        $result = $this->subscriptionService->getAllSubscriptionsForUser( 'created_at', 'desc', $this->auth0User );

        $this->assertEquals( 3, $result->count() );


        $userId = $this->auth0User->getUserId();
        $this->subscriptionService->unsubscribe( [
                'id'      => 1,
                'user_id' => $userId,
        ], $this->auth0User );
        $this->subscriptionService->unsubscribe( [
                'id'      => 2,
                'user_id' => $userId,
        ], $this->auth0User );

        $result = $this->subscriptionService->getAllSubscriptionsForUser( 'created_at', 'desc', $this->auth0User );

        $this->assertEquals( 1, $result->count() );
    }

    public function test_getSubscriptionForUser() {
        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '39',
        ], $this->auth0User );
        $this->subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $this->auth0User );

        $result = $this->subscriptionService->getSubscriptionForUser( 2, $this->auth0User );

        $this->assertEquals( 'tag', $result->type );
    }

    public function test_getAllSubscriptionsForUser() {

        factory( Subscription::class, 2 )->create();

        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '1',
        ], $this->auth0User );
        $this->subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $this->auth0User );
        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => '436',
        ], $this->auth0User );

        $result = $this->subscriptionService->getAllSubscriptionsForUser( 'created_at', 'desc', $this->auth0User );

        $this->assertEquals( 3, $result->count() );
    }

    /**
     * @expectedException \Exception
     */
    public function test_deleteSubscription_should_throw_if_not_owner() {

        $this->subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $this->auth0User );

        $this->subscriptionService->unsubscribe( [
                'id'      => 1,
                'user_id' => '%%%abc123__x',
        ], $this->auth0User );
    }

    public function test_getSubscriptionsForPosts() {
        // factory( Feed::class, 2 )->create();

        factory( Post::class, 3 )->create();

        $post = Post::find( 1 );
        $post->tag( 'state' );
        $post->tag( 'mobx' );
        $post->tag( 'redux' );

        $this->subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => 1,
        ], $this->auth0User );

        $this->subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $this->auth0User );

        $subscriptions = $this->subscriptionService->getSubscriptionsForPosts( Post::all() );

        $this->assertEquals( 2, $subscriptions->count() );
        $this->assertEquals( 'feed', $subscriptions[0]->type );
        $this->assertEquals( 'tag', $subscriptions[1]->type );

    }
}