<?php

namespace Tests\Subscription;

use App\PlanetReact\Domain\Subscription\SubscriptionType;
use TestCase;

class SubscriptionTypeTest extends TestCase {

    public function test_types() {
        $this->assertEquals( 'tag', SubscriptionType::TAG );
        $this->assertEquals( 'feed', SubscriptionType::FEED );
        $this->assertEquals( 'daily', SubscriptionType::DAILY );
        $this->assertEquals( 'weekly', SubscriptionType::WEEKLY );
    }

}