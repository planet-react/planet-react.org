<?php

namespace Tests\Events;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Events\AggregateCommandCompleteEvent;
use Event;
use TestCase;

class AggregateCommandCompleteTest extends TestCase {

    public function test_event() {
        Event::fake();

        factory( Post::class, 2 )->create();

        $posts      = Post::all();
        $postCollection = collect();
        foreach ( $posts as $post ) {
            $postCollection->push( $post );
        }

        event( new AggregateCommandCompleteEvent( $postCollection ) );

        Event::assertDispatched( AggregateCommandCompleteEvent::class, function ( $event ) use ( $postCollection ) {
            return $event->posts->count() == $postCollection->count();
        } );

    }
}