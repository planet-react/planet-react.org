<?php

namespace Tests\Events;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Events\FeedSubmittedEvent;
use Event;
use TestCase;

class FeedSubmittedTest extends TestCase {

    public function test_event() {
        Event::fake();

        $feed = factory( Feed::class )->create();

        $user = $this->createAuth0User();
        event( new FeedSubmittedEvent( $feed, $user ) );

        Event::assertDispatched( FeedSubmittedEvent::class, function ( $event ) use ( $feed, $user ) {
            return $event->feed->name == $feed->name;
        } );

    }
}