<?php

namespace Tests\Http\Debugger;

use App\PlanetReact\Http\Debugger\Debugger;
use TestCase;

class DebuggerTest extends TestCase {

    private $cacheFileName = 'test-cache-file.json';

    public function test_cache_file_should_be_created() {
        $debugger = new Debugger( true, $this->cacheFileName );
        $this->assertFileExists( $this->cacheFileName );

        $this->removeTestCacheFile();
    }

    public function test_log() {
        $debugger = new Debugger( true, $this->cacheFileName );
        $debugger->log( 'string 1', 'string 2', 'string 3' );

        $this->assertJsonStringEqualsJsonFile( $this->cacheFileName, '{"entries":[["string 1","string 2","string 3"]]}' );

        $this->removeTestCacheFile();
    }

    private function removeTestCacheFile() {
        unlink( $this->cacheFileName );
    }
}