<?php

namespace Tests\Http\Debugger\Middleware;

use App\PlanetReact\Domain\Event\Event;
use TestCase;

class DebuggerMiddlewareTest extends TestCase {

    private $originalCacheFileName;
    private $cacheFileName = 'test-cache-file.json';

    public function test_should_log_sql_queries() {
        $this->onStart();

        $cacheFileName = config( 'planet.debugger.cache_file' );

        $this->assertFileNotExists( $cacheFileName );

        $this->createGraphqlResponse();

        $json = json_decode( file_get_contents( $cacheFileName ) );

        $this->assertCount( 2, $json->entries );

        $this->assertFileExists( $cacheFileName );

        $this->onEnd();
    }

    public function test_should_not_log_sql_queries_when_debug_is_false() {

        $this->onStart();

        app( 'config' )->set( 'app.debug', false );

        $cacheFileName = config( 'planet.debugger.cache_file' );

        $this->createGraphqlResponse();

        $this->assertFileNotExists( $cacheFileName );

        $this->onEnd();
    }

    private function createGraphqlResponse() {
        factory( Event::class, 3 )->create( [ 'type' => 1, 'active' => 1 ] );
        factory( Event::class, 2 )->create( [ 'type' => 2, 'active' => 1 ] );

        return $this->call( 'POST', '/api/graphql', [
                'query'     => 'query EventsQuery($type:Int, $orderBy:String = "start_date", $orderByDirection:String = "asc", $past:Boolean = false, $page:Int = 1) {
                    browseEvents(type: $type, orderBy: $orderBy, orderByDirection: $orderByDirection, past: $past, page: $page) {
                        data {
                            id
                            user_id
                            active
                            type
                            name
                        }
                    }
                }',
                'variables' => [
                        'type' => 1
                ]
        ] );
    }

    private function onStart() {
        $this->originalCacheFileName = app( 'config' )->get( 'planet.debugger.cache_file' );

        app( 'config' )->set( 'planet.debugger.cache_file', $this->cacheFileName );

        if ( file_exists( $this->cacheFileName ) ) {
            unlink( $this->cacheFileName );
        }
    }

    private function onEnd() {
        if ( file_exists( $this->cacheFileName ) ) {
            unlink( $this->cacheFileName );
        }
        app( 'config' )->set( 'planet.debugger.cache_file', $this->originalCacheFileName );
        app( 'config' )->set( 'app.debug', true );
    }


}