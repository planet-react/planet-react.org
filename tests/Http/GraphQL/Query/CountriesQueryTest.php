<?php
namespace Tests\Http\GraphQL\Query;

use TestCase;

class CountriesQueryTest extends TestCase {

    public function test_CountriesQuery() {

        $response = $this->call( 'POST', '/api/graphql', [
                'query' => 'query CountriesQuery {
                    countries {
                        commonName
                        cca2
                    }
                }'
        ] );

        $data = $response->json()['data'];

        $this->assertTrue( is_array( $data['countries'] ) );
        $this->assertArrayHasKey( 'commonName', $data['countries'][0] );
        $this->assertArrayHasKey( 'cca2', $data['countries'][0] );
    }

}