<?php

namespace Tests\Http\GraphQL\Query;

use App\PlanetReact\Domain\Event\Event;
use TestCase;

class EventBrowseQueryTest extends TestCase {

    public function test_EventBrowseQuery() {
        factory( Event::class, 3 )->create( [ 'type' => 1, 'active' => 1 ] );
        factory( Event::class, 2 )->create( [ 'type' => 2, 'active' => 1 ] );

        $response = $this->call( 'POST', '/api/graphql', [
                'query'     => 'query EventsQuery($type:Int, $orderBy:String = "start_date", $orderByDirection:String = "asc", $past:Boolean = false, $page:Int = 1) {
                    browseEvents(type: $type, orderBy: $orderBy, orderByDirection: $orderByDirection, past: $past, page: $page) {
                        data {
                            id
                            user_id
                            active
                            type
                            name
                        }
                    }
                }',
                'variables' => [
                        'type' => 1
                ]
        ] );

        $eventBrowse = $response->json()['data']['browseEvents'];

        $this->assertEquals( 3, count( $eventBrowse['data'] ) );
        $this->assertArrayHasKey( 'id', $eventBrowse['data'][0] );
        $this->assertArrayHasKey( 'user_id', $eventBrowse['data'][0] );
        $this->assertArrayHasKey( 'name', $eventBrowse['data'][0] );
        $this->assertArrayHasKey( 'type', $eventBrowse['data'][0] );
    }

}