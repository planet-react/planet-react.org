<?php
namespace Tests\Http\GraphQL\Query;

use App\PlanetReact\Domain\Feed\Feed;
use TestCase;

class FeedQueryTest extends TestCase {

    public function test_FeedQuery() {
        factory( Feed::class, 3 )->create();
        factory( Feed::class, 1 )->create( [
                'slug'     => 'a-nice-slug',
                'approved' => 1
        ] );

        $response = $this->call( 'POST', '/api/graphql', [
                'query'     => 'query FeedQuery($slug: String!) {
                    feed(slug: $slug) {
                        id
                        name
                    }
                }',
                'variables' => [
                        'slug' => 'a-nice-slug'
                ]
        ] );

        $feedJson = $response->json()['data'];

        $this->assertEquals( 1, count( $feedJson ) );
        $this->assertArrayHasKey( 'id', $feedJson['feed'] );
        $this->assertArrayHasKey( 'name', $feedJson['feed'] );
    }

}