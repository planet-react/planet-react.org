<?php
namespace Tests\Http\GraphQL\Query;

use App\PlanetReact\Domain\Feed\Feed;
use TestCase;

class FeedsQueryTest extends TestCase {

    public function test_FeedsQuery() {
        factory( Feed::class, 3 )->create();

        $response = $this->call( 'POST', '/api/graphql', [
                'query' => 'query FeedsQuery {
                    feeds {
                        id
                        name
                    }
                }'
        ] );

        $feedsJson = $response->json()['data']['feeds'];

        $this->assertEquals( 3, count( $feedsJson ) );
        $this->assertArrayHasKey( 'id', $feedsJson[0] );
        $this->assertArrayHasKey( 'name', $feedsJson[0] );
    }

}