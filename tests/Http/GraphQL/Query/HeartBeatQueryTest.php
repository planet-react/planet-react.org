<?php

namespace Tests\Http\GraphQL\Query;

use App\PlanetReact\Domain\Post\Post;
use TestCase;

class HeartBeatQueryTest extends TestCase {

    public function test_HeartBeatQuery() {
        factory( Post::class, 1 )->create();

        $response = $this->call( 'POST', '/api/graphql', [
                'query' => 'query HeartBeatQuery {
                    heartBeat {
                        timestamp
                        appLastUpdatedAt
                        isAggregatorRunning
                        latestPostId
                    }
                }'
        ] );

        $data = $response->json()['data'];

        $heartBeat = $data['heartBeat'];

        $this->assertArrayHasKey( 'heartBeat', $data );
        $this->assertArrayHasKey( 'timestamp', $heartBeat );
        $this->assertArrayHasKey( 'appLastUpdatedAt', $heartBeat );
        $this->assertArrayHasKey( 'isAggregatorRunning', $heartBeat );
        $this->assertArrayHasKey( 'latestPostId', $heartBeat );
        $this->assertTrue( is_int( $heartBeat['appLastUpdatedAt'] ) );
        $this->assertEquals( 1, $heartBeat['latestPostId'] );
    }

}