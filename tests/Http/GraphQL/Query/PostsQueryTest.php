<?php

namespace Tests\Http\GraphQL\Query;

use App\PlanetReact\Domain\Post\Post;
use TestCase;

class PostsQueryTest extends TestCase {

    public function test_PostsQuery() {
        factory( Post::class, 5 )->create();

        $response = $this->call( 'POST', '/api/graphql', [
                'query' => '
                    query BrowsePostsQuery($feedId: Int, $slug: String, $search: String, $lang: String, $year: Int, $month: Int, $page:Int = 1) {
                        browsePosts(feedId: $feedId, slug: $slug, search: $search, lang: $lang, pubdateYear: $year, pubdateMonth: $month, orderBy:"pubdate" orderByDirection:"desc", page: $page) {
                            data {
                                id
                                title
                            }
                        }
                    }
                '
        ] );

        $browsePosts = $response->json()['data']['browsePosts'];

        $this->assertEquals( 5, count( $browsePosts['data'] ) );
        $this->assertArrayHasKey( 'id', $browsePosts['data'][0] );
        $this->assertArrayHasKey( 'title', $browsePosts['data'][0] );
    }
}