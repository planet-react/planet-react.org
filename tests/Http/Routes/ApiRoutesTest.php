<?php

namespace Tests\Http\Routes;

use App;
use TestCase;

class ApiRoutesTest extends TestCase {
    protected $atomFeedController;

    public function setUp() {
        parent::setUp();

        $postServiceMock = \Mockery::mock( 'App\PlanetReact\Domain\Post\PostService' );

        $this->atomFeedController = \Mockery::mock( 'App\PlanetReact\Http\Controllers\AtomFeedController[index]', $postServiceMock );

        // Bind instance of controller to the mock
        App::instance( 'App\PlanetReact\Http\Controllers\AtomFeedController', $this->atomFeedController );
    }

    public function testRoutes() {
        $this->atomFeedController->shouldReceive( 'index' )->once();
        $this->call( 'GET', '/api/feed' );
    }
}