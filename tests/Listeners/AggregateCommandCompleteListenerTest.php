<?php

namespace Tests\Listeners;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Events\AggregateCommandCompleteEvent;
use App\PlanetReact\Listeners\AggregateCommandCompleteListener;
use TestCase;

class SendSubscriptionEmailsTest extends TestCase {

    public function test_listener() {

        $listener = \Mockery::spy( AggregateCommandCompleteListener::class );
        app()->instance( AggregateCommandCompleteListener::class, $listener );

        factory( Post::class, 2 )->create();

        $posts          = Post::all();
        $postCollection = collect();
        foreach ( $posts as $post ) {
            $postCollection->push( $post );
        }

        event( new AggregateCommandCompleteEvent( $postCollection ) );

        $listener->shouldHaveReceived( 'handle' )->with( \Mockery::on( function ( $event ) use ( $postCollection ) {
            return $event->posts->count() == $postCollection->count();
        } ) );


    }
}