<?php

namespace Tests\Listeners;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Events\FeedSubmittedEvent;
use App\PlanetReact\Listeners\FeedSubmittedListener;
use TestCase;

class FeedSubmittedListenerTest extends TestCase {

    public function test_listener() {

        $listener = \Mockery::spy( FeedSubmittedListener::class );
        app()->instance( FeedSubmittedListener::class, $listener );

        $feed = factory( Feed::class )->create();

        $user = $this->createAuth0User();
        event( new FeedSubmittedEvent( $feed, $user ) );

        $listener->shouldHaveReceived( 'handle' )->with( \Mockery::on( function ( $event ) use ( $feed ) {
            return $event->feed->id == $feed->id;
        } ) );


    }
}