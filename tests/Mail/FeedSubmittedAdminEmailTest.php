<?php

namespace Tests\Mail;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Mail\FeedSubmittedAdminEmail;
use File;
use Illuminate\Support\Facades\Mail;
use TestCase;

class FeedSubmittedAdminEmailTest extends TestCase {

    public function test_send() {

        Mail::fake();

        $feed = factory( Feed::class )->create();

        $mailable = new FeedSubmittedAdminEmail( $feed );

        Mail::send( $mailable );

        Mail::assertSent( FeedSubmittedAdminEmail::class, function ( $mail ) use ( $feed ) {
            $this->assertEquals( $feed->id, $mail->feed->id );
            return true;
        } );
    }

    public function test_templateExists() {
        $this->assertTrue( File::exists( resource_path() . '/views/emails/feed-submitted-admin-mail.blade.php' ) );
    }
}