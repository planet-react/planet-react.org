<?php

namespace Tests\Mail;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Mail\FeedSubmittedUserEmail;
use File;
use Illuminate\Support\Facades\Mail;
use TestCase;

class FeedSubmittedUserEmailTest extends TestCase {

    public function test_send() {

        Mail::fake();

        $feed = factory( Feed::class )->create();

        $user = $this->createAuth0User();

        $mailable = new FeedSubmittedUserEmail( $feed, $user );

        Mail::send( $mailable );

        Mail::assertSent( FeedSubmittedUserEmail::class, function ( $mail ) use ( $feed, $user ) {
            $this->assertEquals( $feed->id, $mail->feed->id );
            $this->assertEquals( $user->getUserId(), $mail->user->getUserId() );
            return true;
        } );
    }

    public function test_templateExists() {
        $this->assertTrue( File::exists( resource_path() . '/views/emails/feed-submitted-user-mail.blade.php' ) );
    }
}