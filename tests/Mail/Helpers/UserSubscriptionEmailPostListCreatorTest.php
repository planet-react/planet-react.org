<?php

namespace Tests\Helpers;

use App\PlanetReact\Domain\Feed\Feed;
use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Subscription\SubscriptionService;
use App\PlanetReact\Mail\Helpers\UserSubscriptionEmailPostListCreator;
use TestCase;

class UserSubscriptionEmailPostListCreatorTest extends TestCase {

    public function test_build() {

        $subscriptionService = new SubscriptionService;

        $auth0User = $this->createAuth0User();
        $toMail    = $auth0User->getEmail();

        factory( Post::class, 1 )->create( [
                'feed_id' => function () {
                    return factory( Feed::class )->create( [
                            'name' => 'Peter Jones'
                    ] )->id;
                }
        ] );
        factory( Post::class, 2 )->create();

        $post = Post::find( 1 );
        $post->tag( 'state' );
        $post->tag( 'mobx' );
        $post->tag( 'redux' );

        $subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => 1,
        ], $auth0User );

        $subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $auth0User );

        $subscriptions = $subscriptionService->getSubscriptionsForPosts( Post::all() );

        $postList = new UserSubscriptionEmailPostListCreator( Post::all(), $subscriptions );

        $data = $postList->build( $toMail );

        $this->assertEquals( $toMail, $data['email'] );

        $this->assertEquals( 1, count( $data['feeds'] ) );
        $this->assertEquals( 'Peter Jones', $data['feeds'][0]['feed_name'] );
        $this->assertEquals( 1, count( $data['tags'] ) );
        $this->assertEquals( 'redux', $data['tags'][0]['tag_slug'] );

    }
}