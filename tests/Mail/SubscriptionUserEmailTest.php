<?php

namespace Tests\Mail;

use App\PlanetReact\Domain\Post\Post;
use App\PlanetReact\Domain\Subscription\SubscriptionService;
use App\PlanetReact\Mail\Helpers\UserSubscriptionEmailPostListCreator;
use App\PlanetReact\Mail\SubscriptionUserEmail;
use File;
use Illuminate\Support\Facades\Mail;
use TestCase;

class SubscriptionUserEmailTest extends TestCase {

    public function test_send() {

        Mail::fake();

        $toMail = 'test@test.com';

        $subscriptionService = new SubscriptionService;

        $auth0User = $this->createAuth0User();

        factory( Post::class, 3 )->create();

        $post = Post::find( 1 );
        $post->tag( 'state' );
        $post->tag( 'mobx' );
        $post->tag( 'redux' );

        $subscriptionService->subscribe( [
                'type'  => 'feed',
                'value' => 1,
        ], $auth0User );

        $subscriptionService->subscribe( [
                'type'  => 'tag',
                'value' => 'redux',
        ], $auth0User );

        $subscriptions = $subscriptionService->getSubscriptionsForPosts( Post::all() );

        $postList = new UserSubscriptionEmailPostListCreator( Post::all(), $subscriptions );

        $data = $postList->build( $toMail );

        $mailable = new SubscriptionUserEmail( $toMail, $data );

        Mail::send( $mailable );

        Mail::assertSent( SubscriptionUserEmail::class, function ( $mail ) use ( $toMail, $data ) {
            $this->assertEquals( $data, $mail->data );
            return $mail->toMail == $toMail;
        } );
    }

    public function test_templateExists() {
        $this->assertTrue( File::exists( resource_path() . '/views/emails/subscription-user-mail.blade.php' ) );
    }
}