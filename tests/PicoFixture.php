<?php

namespace Tests;

use TestCase;

class PicoFixture extends TestCase {

    protected $feed;
    protected $client;
    protected $parser;
    protected $shouldThrow = false;

    /**
     * @var string
     */
    protected $url;

    /**
     * PicoTestHelper constructor.
     * @param null|string $url
     * @param bool $shouldThrow
     */
    public function __construct( $url, $shouldThrow = false ) {
        $this->shouldThrow = $shouldThrow;
        $this->url         = $url;
        $this->client      = $this->createPicoClientStub();
        $this->feed        = $this->createPicoFeedStub();
        $this->parser      = $this->createPicoParserStub( $this->client, $this->feed );


        parent::__construct();
    }

    public function createReaderStub() {
        return $this->createPicoReaderStub( $this->client, $this->parser );
    }

    public function setShouldThrow( $shouldThrow ) {
        $this->shouldThrow = $shouldThrow;
    }

    public function getClient() {
        return $this->client;
    }

    public function getFeed() {
        return $this->feed;
    }

    public function getParser() {
        return $this->parser;
    }

    protected function createPicoFeedStub() {
        $feedStub = $this->getMockBuilder( 'PicoFeed\Parser\Feed' )->getMock();

        $feedStub->expects( $this->any() )
                ->method( 'getTitle' )
                ->will( $this->returnValue( 'The Title' ) );

        $feedStub->expects( $this->any() )
                ->method( 'getDescription' )
                ->will( $this->returnValue( 'The Description' ) );

        $feedStub->expects( $this->any() )
                ->method( 'getSiteUrl' )
                ->will( $this->returnValue( 'https://acme.com' ) );

        $feedStub->expects( $this->any() )
                ->method( 'getItems' )
                ->will( $this->returnValue( [] ) );

        return $feedStub;
    }

    protected function createPicoClientStub() {
        $client = $this->getMockBuilder( 'PicoFeed\Client\Client' )->getMock();

        $client->expects( $this->any() )
                ->method( 'getUrl' )
                ->will( $this->returnValue( $this->url ) );

        if ( $this->shouldThrow ) {
            $client->expects( $this->any() )
                    ->method( 'getContent' )
                    ->will( $this->throwException( new \PicoFeed\Reader\UnsupportedFeedFormatException ) );

        }
        else {

            $client->expects( $this->any() )
                    ->method( 'getContent' )
                    ->will( $this->returnValue( '<xml/>' ) );
        }

        $client->expects( $this->any() )
                ->method( 'getEncoding' )
                ->will( $this->returnValue( 'UTF-8' ) );

        return $client;
    }

    protected function createPicoParserStub( $client, $feedStub ) {
        $parser = $this->getMockBuilder( 'PicoFeed\Parser\Parser' )
                ->setConstructorArgs( [
                        $client->getUrl(),
                        $client->getContent(),
                        $client->getEncoding(),
                ] )
                ->getMock();

        $parser->expects( $this->once() )
                ->method( 'execute' )
                ->will( $this->returnValue( $feedStub ) );

        return $parser;
    }

    protected function createPicoReaderStub( $client, $parser ) {
        $reader = $this->getMockBuilder( 'PicoFeed\Reader\Reader' )->getMock();

        $reader->expects( $this->once() )
                ->method( 'download' )
                ->will( $this->returnValue( $client ) );

        $reader->expects( $this->once() )
                ->method( 'getParser' )
                ->will( $this->returnValue( $parser ) );

        return $reader;
    }

}