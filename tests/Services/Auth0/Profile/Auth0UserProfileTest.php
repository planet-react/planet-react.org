<?php

namespace Tests\Services\Auth0\Profile;

use App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile;
use TestCase;

class Auth0UserProfileTest extends TestCase {

    public function test_auht0UserProfile() {

        $user = new Auth0UserProfile();
        $user->setUserId( 'abc123456' );
        $user->setName( 'Patty Ryan' );
        $user->setEmail( 'patty@test.com' );
        $user->setEmailVerified( true );
        $user->setRoles( [ 'user_manager', 'content_contributor' ] );

        $this->assertEquals( 'abc123456', $user->getUserId() );
        $this->assertEquals( 'Patty Ryan', $user->getName() );
        $this->assertEquals( 'patty@test.com', $user->getEmail() );
        $this->assertTrue( $user->getEmailVerified() );
        $this->assertEquals( [ 'user_manager', 'content_contributor' ], $user->getRoles() );
    }
}