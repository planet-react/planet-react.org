<?php

ini_set('memory_limit', '256M');

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase {
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication() {

        putenv( 'DB_CONNECTION=mysql_testing' );

        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make( Illuminate\Contracts\Console\Kernel::class )->bootstrap();

        \Illuminate\Support\Facades\Artisan::call( 'config:clear' );

        return $app;
    }

    public function setUp() {
        parent::setUp();
        Artisan::call( 'migrate' );
    }

    public function tearDown() {
        Artisan::call( 'migrate:reset' );
        parent::tearDown();
    }

    public static function getProtectedMethod( $class, $name ) {
        $class  = new ReflectionClass( $class );
        $method = $class->getMethod( $name );
        $method->setAccessible( true );
        return $method;
    }

    public static function createAuth0User() {

        $user = new \App\PlanetReact\Services\Auth0\Profile\Auth0UserProfile();
        $user->setUserId('abc123!#');
        $user->setEmail('test@test.com');
        return $user;
    }
}
